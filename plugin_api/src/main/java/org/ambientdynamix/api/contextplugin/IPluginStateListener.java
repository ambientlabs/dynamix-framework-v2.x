/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Event interface for plug-in state listeners
 * 
 * @author Darren Carlson
 * 
 */
public interface IPluginStateListener {
	/**
	 * Plug-in state has been set to PluginState.NEW.
	 */
	void onNew(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.INITIALIZING.
	 */
	void onInitializing(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.INITIALIZED.
	 */
	void onInitialized(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.STARTING.
	 */
	void onStarting(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.STARTED.
	 */
	void onStarted(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.STOPPING.
	 */
	void onStopping(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.DESTROYED.
	 */
	void onDestroyed(ContextPluginInformation plug);

	/**
	 * Plug-in state has been set to PluginState.ERROR.
	 */
	void onError(ContextPluginInformation plug);
}