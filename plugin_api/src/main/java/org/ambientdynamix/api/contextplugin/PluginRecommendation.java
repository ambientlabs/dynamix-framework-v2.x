/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

/**
 * Represents a single plug-in recommendation, including a recommendation strength and associated confidence.
 * Experimental.
 * 
 * @author Darren Carlson
 * 
 */
public class PluginRecommendation {
	private String contextType;
	private String pluginId;
	private float strength;
	private float confidence;

	/**
	 * Creates a PluginRecommendation.
	 * 
	 * @param contextType
	 *            The associated contex type string.
	 * @param pluginId
	 *            The associated plug-in id.
	 * @param strength
	 *            The strength of the recommendation, from 0 to 1.
	 * @param confidence
	 *            The confidence interval of the recommendation, from 0 to 1.
	 */
	public PluginRecommendation(String contextType, String pluginId, float strength, float confidence) {
		this.contextType = contextType;
		this.pluginId = pluginId;
		this.strength = strength;
		this.confidence = confidence;
	}

	public String getContextType() {
		return contextType;
	}

	public void setContextType(String contextType) {
		this.contextType = contextType;
	}

	public String getPluginId() {
		return pluginId;
	}

	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}

	public float getStrength() {
		return strength;
	}

	public void setStrength(float strength) {
		this.strength = strength;
	}

	public float getConfidence() {
		return confidence;
	}

	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}

	@Override
	public String toString() {
		return "Recommendation: " + pluginId + " with context type " + contextType;
	}
}
