/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.DynamixFacade;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;

import java.util.List;
import java.util.UUID;

/**
 * The IPluginFacade provides a mechanism whereby a ContextPluginRuntime can securely interact with Dynamix and Android.
 * ContextPluginRuntimes never interact with Android directly; rather, Android interaction is mediated through
 * implementations of the IPluginFacade, which check if the calling client is authorized to receive a given service,
 * register/unregister a particular BroadcastReceiver, etc. For example, obtaining a secured version of an Android
 * Context, which can be used to access accessing a particular Android service (e.g. Context.LOCATION_SERVICE).
 *
 * @author Darren Carlson
 */
public interface IPluginFacade {
    /**
     * Returns the ContextPluginInformation associated with this plug-in;
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     */
    public ContextPluginInformation getPluginInfo(UUID sessionID);

    /**
     * Closes any configuration view associated with the Context Plugin.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     */
    public void closeConfigurationView(UUID sessionID);

    /**
     * Closes any context acquisition view associated with the Context Plugin.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     */
    public void closeContextAcquisitionView(UUID sessionID);

    /**
     * Returns the ContextPluginSettings persisted for the given ContextPluginRuntime in the Dynamix Framework.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime Returns a ContextPlugin-specific
     *                  ContextPluginSettings object.
     */
    public ContextPluginSettings getContextPluginSettings(UUID sessionID);

    /**
     * Returns a secured version of the Android Context that is customized for the caller. The SecuredContext is
     * configured by a set of Permissions, which allow only specific actions that have been granted by the user.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime Returns a configured SecuredContext, or null
     *                  if the sessionID is incorrect
     */
    public Context getSecuredContext(UUID sessionID);

    /**
     * Returns the current PluginState for the ContextPluginRuntime
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime
     * @return The state of the calling ContextPluginRuntime
     */
    public PluginState getState(UUID sessionID);

    /**
     * Sets the ContextPluginRuntime's associated ContextPlugin to the configured or unconfigured state.
     *
     * @param sessionID  The unique session id of the calling ContextPluginRuntime.
     * @param configured True if the plugin is properly configured; false otherwise Returns true if the configuration status
     *                   was set; false otherwise.
     */
    public boolean setPluginConfiguredStatus(UUID sessionID, boolean configured);

    /**
     * Stores the ContextPluginSettings in the Dynamix Framework on behalf on the calling ContextPluginRuntime.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime
     * @param settings  The ContextPluginSettings to store Returns true if the settings were successfully stored; false
     *                  otherwise.
     */
    public boolean storeContextPluginSettings(UUID sessionID, ContextPluginSettings settings);

    /**
     * Adds a listener that will receive NFC events from Android
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param listener  The listener that should receive the event (as an Intent)
     * @return True if the listener was added; false otherwise.
     */
    public boolean addNfcListener(UUID sessionID, NfcListener listener);

    /**
     * Removes a previously added NfcListener
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param listener  The NfcListener that should be removed.
     * @return True if the listener was removed; false otherwise.
     */
    public boolean removeNfcListener(UUID sessionID, NfcListener listener);

    /**
     * Sends a PluginAlert to Dynamix, which will show it to the user if the plug-in has permission.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param alert     The Bundle containing the alert. Note that Bundle keys must be set properly. For example, to show a
     *                  toast alert to the user, the Bundle keys must be set as bundle.setString(PluginConstants.ALERT_TYPE,
     *                  PluginConstants.ALERT_TYPE_TOAST) and bundle.setString(PluginConstants.ALERT_TOAST_VALUE,
     *                  "Toast Value to Show") and bundle.setInt(PluginConstants.ALERT_TOAST_DURATION, 5000). See
     *                  PluginConstants for other alert types (e.g., PluginConstants.ALERT_TYPE_OPEN_SETTINGS).
     * @return A Result indicating success or failure.
     */
    public Result sendPluginAlert(UUID sessionID, Bundle alert);

    /**
     * Sends a message Bundle to the specified plug-in. Note that this method send the message to the latest version of
     * the plug-in that is installed. To send a message to a specific version of a plug-in, use the 'sendMessage' that
     * includes the version string as an argument.
     *
     * @param sessionID     The unique session id of the calling ContextPluginRuntime.
     * @param pluginId      The id of the plug-in receiver.
     * @param messageBundle The message Bundle to send.
     * @param resultHandler A handler for message results (can be null).
     * @return An IdResult indicating if the message was sent or not. If it was sent, the IdResult's id is the requestId
     * that can be used to differentiate associated results in an IMessageResultHandler.
     */
    public IdResult sendMessage(UUID sessionID, String pluginId, Bundle messageBundle,
                                IMessageResultHandler resultHandler);

    /**
     * Sends a message Bundle to the specified plug-in.
     *
     * @param sessionID     The unique session id of the calling ContextPluginRuntime.
     * @param pluginId      The id of the plug-in receiver.
     * @param pluginVersion The version of the plug-in receiver.
     * @param messageBundle The message Bundle to send.
     * @param resultHandler A handler for message results (can be null).
     * @return An IdResult indicating if the message was sent or not. If it was sent, the IdResult's id is the requestId
     * that can be used to differentiate associated results in an IMessageResultHandler.
     */
    public IdResult sendMessage(UUID sessionID, String pluginId, String pluginVersion, final Bundle messageBundle,
                                final IMessageResultHandler resultHandler);

    /**
     * Returns the number of milliseconds that a context request is considered valid before a timeout. Before a timeout,
     * a plug-in may respond to a context request and the event will be sent; however, after the request timeout occurs,
     * any subsequent requests will be blocked from reaching the calling app. Plug-ins requiring additional processing
     * time should 'ping' Dynamis using the 'pingRequest', which extends the time alloted to the request by the timeout
     * amount. Plug-ins may continue to ping Dynamix for as long as they require a request channel to remain open.
     */
    public int getRequestTimeoutMills();

    /**
     * Returns the IDynamixFacade, making it possible for plug-ins and other types of "internal" Dynamix classes to
     * register for context support (e.g., a plug-in can request services from another plug-in).
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @return A IDynamixFacade or null if the caller is not allowed to access Dynamix the IDynamixFacade.
     */
    public IDynamixFacade getDynamixFacade(UUID sessionID);

    /**
     * Returns a IDynamixFacade wrapped within a DynamixFacade, which provides convenient method overloading that raw
     * AIDL types lack. The DynamixFacade makes it possible for plug-ins and other types of "internal" Dynamix classes
     * to register for context support (e.g., a plug-in can request services from another plug-in).
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @return A DynamixFacade or null if the caller is not allowed to access Dynamix the IDynamixFacade.
     */
    public DynamixFacade getDynamixFacadeWrapper(UUID sessionID);

    /**
     * Returns the list of context listeners that are currently registered to the runtime.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     */
    public List<ContextListenerInformation> getContextListeners(UUID sessionID);

    /**
     * Returns the list of context listeners that are currently registered to the runtime.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     */
    public List<ContextListenerInformation> getContextListeners(UUID sessionID, String contextType);

    /**
     * Returns a serialized version of the IContextInfo in text format, using default encoding format (JSON).
     */
    public String serializeIContextInfo(UUID sessionID, IContextInfo data);

    /**
     * Returns a serialized version of the IContextInfo in text format, using specified text-based encoding format.
     */
    public String serializeIContextInfo(UUID sessionID, IContextInfo data, String stringFormat);

    /**
     * Request the Dynamix open the specified IPluginView using the HostActivity.
     */
    public Result openView(UUID sessionID, IPluginView view);

    /**
     * Perform a system-level operation on the Dynamix Framework instance. Note that plug-ins require system-level
     * permission to use this method, so it is not for general use.
     *
     * @param sessionID        The unique session id of the calling ContextPluginRuntime.
     * @param operationDetails An Android Bundle containing the configuration of the operation. See
     *                         org.ambientdynamix.core.SystemOperations for details.
     */
    public Result dynamixSystemLevelOperation(UUID sessionID, Bundle operationDetails);

    /**
     * Adds a listener that will receive Dynamix events using a simple event type string.
     * See the IDynamixEventListener class for available strings. For more complex event
     * listener setup, use the 'addDynamixEventListener', which takes a Bundle as a configuration argument.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param listener  The listener that should receive the event (as an Intent).
     * @param eventType The Android event type.
     * @return True if the listener was added; false otherwise.
     * @
     */
    public boolean addDynamixEventListener(UUID sessionID, String eventType, IDynamixEventListener listener);

    /**
     * Adds a listener that will receive Dynamix events. See developer docs
     * for details on the eventConfiguration.
     *
     * @param sessionID          The unique session id of the calling ContextPluginRuntime.
     * @param listener           The listener that should receive the event (as an Intent).
     * @param eventConfiguration The Android event configuration (e.g., event type and arguments).
     * @return True if the listener was added; false otherwise.
     * @
     */
    public boolean addDynamixEventListener(UUID sessionID, Bundle eventConfiguration, IDynamixEventListener listener);

    /**
     * Removes a previously added IDynamixEventListener.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param listener  The listener that should be removed.
     * @return True if the listener was removed; false otherwise.
     */
    public boolean removeDynamixEventListener(UUID sessionID, IDynamixEventListener listener);

    /**
     * Sends the event to Dynamix.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param event     The event data to send.
     * @return A Result indicating success or failure.
     */
    public Result sendDynamixEvent(UUID sessionID, Intent event);

    /**
     * Obtains various information from Dynamix. See developer docs for details.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param config    The configuration for the call (indicating which info to obtain).
     * @return A Bundle with the result, or null of the information cannot be obtained.
     */
    public Bundle getDynamixInformation(UUID sessionID, Intent config);

    /**
     * Perform a system-level operation on the Dynamix Framework instance. Note that plug-ins require system-level
     * permission to use this method, so it is not for general use.
     *
     * @param sessionID        The unique session id of the calling ContextPluginRuntime.
     * @param operationDetails An Android Bundle containing the configuration of the operation. See
     *                         org.ambientdynamix.core.SystemOperations for details.
     * @param callback         A callback containing the result of the operation.
     */
    public void dynamixSystemLevelOperation(UUID sessionID, Bundle operationDetails, Callback callback);
}