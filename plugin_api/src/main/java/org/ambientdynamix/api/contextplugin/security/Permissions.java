/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin.security;

/**
 * List of static permission values for the Dynamix Framework.
 * 
 * @author Darren Carlson
 */
public class Permissions {
	public static final String SEND_BROADCASTS = "org.ambientdynamix.security.permissions.SEND_BROADCASTS";
	public static final String MANAGE_BROADCAST_RECEIVERS = "org.ambientdynamix.security.permissions.MANAGE_BROADCAST_RECEIVERS";
	public static final String SECURED_SENSOR_MANAGER = "org.ambientdynamix.security.permissions.SECURED_SENSOR_MANAGER";
	public static final String SEND_INTER_PLUGIN_MESSAGES = "org.ambientdynamix.security.permissions.SEND_INTER_PLUGIN_MESSAGES";
	public static final String RECEIVE_INTER_PLUGIN_MESSAGES = "org.ambientdynamix.security.permissions.RECEIVE_INTER_PLUGIN_MESSAGES";
	public static final String ACCESS_PACKAGE_MANAGER = "org.ambientdynamix.security.permissions.ACCESS_PACKAGE_MANAGER";
	public static final String ACCESS_PACKAGE_NAME = "org.ambientdynamix.security.permissions.ACCESS_PACKAGE_NAME";
	public static final String ACCESS_FULL_CONTEXT = "org.ambientdynamix.security.permissions.ACCESS_FULL_CONTEXT";
	public static final String SECURED_SPEECH_RECOGNIZER = "org.ambientdynamix.security.permissions.SECURED_SPEECH_RECOGNIZER";

	// Singleton constructor
	private Permissions() {
	}
}
