/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a context type in the Dynamix framework.
 *
 * @author Darren Carlson
 */
public class ContextType implements Parcelable, Serializable {
    private String id;
    private String name;
    private String description;
    private String broadcastData;
    private String configurationDataType;
    private List<SupportedInteraction> supportedInteractions = new ArrayList<>();

    // Empty constructor for db4o
    public ContextType(){}

    /**
     * Creates a ContextType.
     *
     * @param id          The unique id of the context type.
     * @param name        The friendly name for the context type that can be presented to the user.
     * @param description The friendly description for the context type that can be presented to the user.
     */
    public ContextType(String id, String name, String description) {
        if(id == null)
            throw new RuntimeException("Context Type ID cannot be null");
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**
     * Returns the id of the context type (e.g., org.ambientdynamix.contextplugins.pedometer).
     */
    public String getId() {
        return id;
    }

    /**
     * Returns a friendly name for the context type that can be presented to the user.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns a friendly description for the context type that can be presented to the user.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the context requests supported by the context type, if provided by the plug-in. Note
     * that not all plug-ins may provide this information, so checking the plug-in documentation is
     * still recommended.
     */
    public List<SupportedInteraction> getSupportedInteractions() {
        return supportedInteractions;
    }

    /**
     * Sets the interactions supported by the context type.
     */
    public void setSupportedInteractions(List<SupportedInteraction> supportedInteractions) {
        this.supportedInteractions = supportedInteractions;
    }

    /**
     * Adds an interaction supported by the context type.
     */
    public void addSupportedInteraction(SupportedInteraction supportedInteraction) {
        if (supportedInteraction != null)
            supportedInteractions.add(supportedInteraction);
    }

    /**
     * Returns the type of data broadcast by this context type, or null if it doesn't broadcast.
     */
    public String getBroadcastData() {
        return broadcastData;
    }

    /**
     * Returns true if this type broadcasts data; false otherwise.
     */
    public boolean hasBroadcastData(){
        return broadcastData != null;
    }
    /**
     * Sets the type of data broadcast by this context type, or null if it doesn't broadcast.
     */
    public void setBroadcastData(String broadcastData) {
        this.broadcastData = broadcastData;
    }

    /**
     * Returns the configuration data type that may be requiring to add context support for this
     * Context Type.
     */
    public String getConfigurationDataType() {
        return configurationDataType;
    }

    /**
     * Sets the configuration data type that may be requiring to add context support for this
     * Context Type.
     */
    public void setConfigurationDataType(String configurationDataType) {
        this.configurationDataType = configurationDataType;
    }


    /**
     * Creates a ContextType from a Parcel.
     */
    private ContextType(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        in.readList(this.supportedInteractions, getClass().getClassLoader());
        this.broadcastData = in.readString();
        this.configurationDataType = in.readString();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeList(this.supportedInteractions);
        dest.writeString(broadcastData);
        dest.writeString(configurationDataType);
    }

    /**
     * @inheritDoc
     */
    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
     */
    public static final Parcelable.Creator<ContextType> CREATOR = new Parcelable.Creator<ContextType>() {
        @Override
        public ContextType createFromParcel(Parcel in) {
            return new ContextType(in);
        }

        @Override
        public ContextType[] newArray(int size) {
            return new ContextType[size];
        }
    };
}
