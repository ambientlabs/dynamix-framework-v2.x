/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Workaround class for returning a list of IContextListener from an IContextHandler AIDL type, which isn't supported at
 * the moment for some reason.
 * 
 * @author Darren Carlson
 */
public class IContextListenerResult implements Parcelable {
	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<IContextListenerResult> CREATOR = new Parcelable.Creator<IContextListenerResult>() {
		@Override
		public IContextListenerResult createFromParcel(Parcel in) {
			return new IContextListenerResult(in);
		}

		@Override
		public IContextListenerResult[] newArray(int size) {
			return new IContextListenerResult[size];
		}
	};
	// Private variables
	private final String TAG = this.getClass().getSimpleName();
	private List<IContextListener> internalList = new ArrayList<IContextListener>();

	/**
	 * Creates an IContextListenerResult.
	 */
	public IContextListenerResult(Collection<IContextListener> collection) {
		this.internalList.addAll(collection);
	}

	/**
	 * Returns the list of IContextListener.
	 */
	public List<IContextListener> getIContextListenerList() {
		return internalList;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(internalList);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	private IContextListenerResult(Parcel in) {
		in.readList(internalList, getClass().getClassLoader());
	}
}
