/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtaa copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;

/**
 * Wrapper for IDynamixFacade instances that provides the method overloading that raw AIDL types lack.
 * 
 * @see IDynamixFacade
 * @author Darren Carlson
 * 
 */
public class DynamixFacade implements Parcelable {
	private String TAG = this.getClass().getSimpleName();
	public static Parcelable.Creator<DynamixFacade> CREATOR = new Parcelable.Creator<DynamixFacade>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
		 * previously been written by Parcelable.writeToParcel().
		 */
		public DynamixFacade createFromParcel(Parcel in) {
			return new DynamixFacade(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public DynamixFacade[] newArray(int size) {
			return new DynamixFacade[size];
		}
	};
	// The local IDynamixFacade instance
	private IDynamixFacade facade;

	/**
	 * Creates a DynamixFacade that wraps the incoming IDynamixFacade.
	 */
	public DynamixFacade(IDynamixFacade facade) {
		this.facade = facade;
	}

	/**
	 * Returns the underlying IDynamixFacade wrapped by this facade.
	 */
	public IDynamixFacade getIDynamixFacade() {
		return this.facade;
	}

	/**
	 * Returns the Dynamix Framework version that is attached to this IDynamixFacade. This method executes
	 * synchronously.
	 */
	public VersionInfo getDynamixVersion() throws RemoteException {
		return facade.getDynamixVersion();
	}

	/**
	 * Returns true if Dynamix is active; false otherwise. This method executes synchronously.
	 * 
	 * @return True if Dynamix is active; false otherwise.
	 */
	public boolean isDynamixActive() throws RemoteException {
		return facade.isDynamixActive();
	}

	/**
	 * Returns true if the application's session is open; false otherwise.
	 * 
	 * @return True if the application's session is open; false otherwise.
	 */
	public boolean isSessionOpen() throws RemoteException {
		return facade.isSessionOpen();
	}

	/**
	 * Opens the Dynamix session. This method executes asynchronously.
	 */
	public void openSession() throws RemoteException {
		facade.openSession();
	}

	/**
	 * Opens the Dynamix session. This method executes asynchronously.
	 * 
	 * @param callback
	 *            The callback to update when the operation completes.
	 */
	public void openSession(ISessionCallback callback) throws RemoteException {
		facade.openSessionWithCallback(callback);
	}

	/**
	 * Opens the Dynamix session. This method executes asynchronously.
	 * 
	 * @param listener
	 *            The session listener.
	 */
	public void openSession(ISessionListener listener) throws RemoteException {
		facade.openSessionWithListener(listener);
	}

	/**
	 * Opens the Dynamix session. This method executes asynchronously.
	 * 
	 * @param listener
	 *            The session listener.
	 * @param callback
	 *            The callback to update when the operation completes.
	 */
	public void openSession(ISessionListener listener, ISessionCallback callback) throws RemoteException {
		facade.openSessionWithListenerAndCallback(listener, callback);
	}

	/**
	 * Opens the Dynamix session. This method executes asynchronously.
	 * 
	 * @param listener
	 *            The session listener.
	 */
	public void setSessionListener(ISessionListener listener) throws RemoteException {
		facade.setSessionListener(listener);
	}

	/**
	 * Closes the Dynamix session. This method executes asynchronously.
	 */
	public void closeSession() throws RemoteException {
		facade.closeSession();
	}

	/**
	 * Closes the Dynamix session. This method executes asynchronously.
	 * 
	 * @param callback
	 *            The callback to update when the operation completes.
	 */
	public void closeSession(ICallback callback) throws RemoteException {
		facade.closeSessionWithCallback(callback);
	}

	/**
	 * Creates an IContextHandler using the listener. This method executes asynchronously.
	 * 
	 * @param callback
	 *            The callback to update when the operation completes.
	 */
	public void createContextHandler(IContextHandlerCallback callback) throws RemoteException {
		facade.createContextHandler(callback);
	}

	/**
	 * Removes the IContextHandler from the session. This method executes asynchronously.
	 * 
	 * @param handler
	 *            The handler to remove.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	public Result removeContextHandler(IContextHandler handler) throws RemoteException {
		return facade.removeContextHandler(handler);
	}

	/**
	 * Removes the IContextHandler from the session. This method executes asynchronously.
	 * 
	 * @param handler
	 *            The handler to remove.
	 * @param callback
	 *            A callback that can be used to track the success or failure of this method.
	 */
	public void removeContextHandler(IContextHandler handler, ICallback callback) throws RemoteException {
		facade.removeContextHandlerWithCallback(handler, callback);
	}

	/**
	 * Opens the latest plug-in's default configuration view (if it has one).
	 * 
	 * @return A Result indicating success or failure.
	 */
	public Result openDefaultContextPluginConfigurationView(String pluginId) throws RemoteException {
		return facade.openDefaultContextPluginConfigurationView(pluginId, null);
	}

	/**
	 * Opens the specified plug-in's default configuration view (if it has one).
	 * 
	 * @return A Result indicating success or failure.
	 */
	public Result openDefaultContextPluginConfigurationView(String pluginId, String pluginVersion)
			throws RemoteException {
		return facade.openDefaultContextPluginConfigurationView(pluginId, pluginVersion);
	}

	/**
	 * Opens the latest plug-in's configuration view (if it has one).
	 * 
	 * @return A Result indicating success or failure.
	 */
	public Result openContextPluginConfigurationView(String pluginId, Bundle viewConfig) throws RemoteException {
		if (viewConfig == null)
			Log.w(TAG, "viewConfig was null... trying to open the default configuration view.");
		return facade.openContextPluginConfigurationView(pluginId, null, viewConfig);
	}

	/**
	 * Opens the specified plug-in's configuration view (if it has one).
	 * 
	 * @return A Result indicating success or failure.
	 */
	public Result openContextPluginConfigurationView(String pluginId, String pluginVersion, Bundle viewConfig)
			throws RemoteException {
		if (viewConfig == null)
			Log.w(TAG, "viewConfig was null... trying to open the default configuration view.");
		return facade.openContextPluginConfigurationView(pluginId, pluginVersion, viewConfig);
	}

	/**
	 * Request that Dynamix install a specific ContextPlugon behalf of the Application. Such a request might be made if
	 * an application has a dependency on a specific ContextPlugin. If the installation request is accepted by Dynamix,
	 * this method operates asynchronously (consider using requestContextPluginInstallationWithCallback to track
	 * results).
	 * 
	 * @param plugInfo
	 *            The plug-in to install.
	 */
	public Result requestContextPluginInstallation(ContextPluginInformation plugInfo) throws RemoteException {
		return facade.requestContextPluginInstallation(plugInfo);
	}

	/**
	 * Request that Dynamix install a specific ContextPlugon behalf of the Application. Such a request might be made if
	 * an application has a dependency on a specific ContextPlugin. If the installation request is accepted by Dynamix,
	 * this method returns its results asynchronously using the IPluginInstallListener.
	 * 
	 * @param plugInfo
	 *            The plug-in to install.
	 * @param callback
	 *            A callback that can be used to track the installation status.
	 */
	public void requestContextPluginInstallation(ContextPluginInformation plugInfo, IPluginInstallCallback callback)
			throws RemoteException {
		facade.requestContextPluginInstallationWithCallback(plugInfo, callback);
	}

	/**
	 * Request that Dynamix uninstall a specific ContextPlugon behalf of the Application. If the request is accepted by
	 * Dynamix, this method operates asynchronously.
	 * 
	 * @param plugInfo
	 *            The plug-in to uninstall.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	public Result requestContextPluginUninstall(ContextPluginInformation plugInfo) throws RemoteException {
		return facade.requestContextPluginUninstall(plugInfo);
	}

	/**
	 * Request that Dynamix uninstall a specific ContextPlugon behalf of the Application. This method operates
	 * asynchronously.
	 * 
	 * @param plugInfo
	 *            The plug-in to uninstall.
	 * @param callback
	 *            A callback that can be used to track the success or failure of this method.
	 */
	public void requestContextPluginUninstall(ContextPluginInformation plugInfo, ICallback callback)
			throws RemoteException {
		facade.requestContextPluginUninstallWithCallback(plugInfo, callback);
	}

	/**
	 * Returns all plug-ins known by Dynamix, regardless of installation status (you can use the returned context
	 * plug-information to determine install status). This method operates synchronously.
	 */
	public ContextPluginInformationResult getAllContextPluginInformationResult() throws RemoteException {
		return facade.getAllContextPluginInformation();
	}

	/**
	 * Returns all plug-ins known by Dynamix, regardless of installation status (you can use the returned context
	 * plug-information to determine install status). This method operates synchronously.
	 */
	public List<ContextPluginInformation> getAllContextPluginInformation() throws RemoteException {
		ContextPluginInformationResult r = facade.getAllContextPluginInformation();
		if (r.wasSuccessful())
			return r.getContextPluginInformation();
		else
			return new ArrayList<ContextPluginInformation>();
	}

	/**
	 * Returns all installed plug-ins (note that installing plug-ins are not included). This method operates
	 * synchronously.
	 */
	public ContextPluginInformationResult getInstalledContextPluginInformationResult() throws RemoteException {
		return facade.getInstalledContextPluginInformation();
	}

	/**
	 * Returns all plug-ins known by Dynamix, regardless of installation status (you can use the returned context
	 * plug-information to determine install status). This method operates synchronously.
	 */
	public List<ContextPluginInformation> getInstalledContextPluginInformation() throws RemoteException {
		ContextPluginInformationResult r = facade.getInstalledContextPluginInformation();
		if (r.wasSuccessful())
			return r.getContextPluginInformation();
		else {
			Log.w(TAG, "getInstalledContextPluginInformation failed: " + r.getMessage());
			return new ArrayList<ContextPluginInformation>();
		}
	}

	/**
	 * Returns the plug-information associated with the specified plug-id. This method operates synchronously.
	 */
	public ContextPluginInformationResult getContextPluginInformation(String pluginId) throws RemoteException {
		return facade.getContextPluginInformation(pluginId);
	}

	/**
	 * Returns the plug-information associated with the specified context type. This method operates synchronously.
	 */
	public ContextPluginInformationResult getAllContextPluginInformationForType(String contextType)
			throws RemoteException {
		return facade.getAllContextPluginInformationForType(contextType);
	}

	/**
	 * Returns the id associated with the incoming handler. This method operates synchronously.
	 */
	public IdResult getHandlerId(IContextHandler handler) throws RemoteException {
		return facade.getHandlerId(handler);
	}

	/**
	 * Returns the facade's session id. This method operates asynchronously.
	 */
	public IdResult getSessionId() throws RemoteException {
		return facade.getSessionId();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStrongBinder(facade.asBinder());
	}

	private DynamixFacade(Parcel in) {
		IBinder binder = in.readStrongBinder();
		facade = IDynamixFacade.Stub.asInterface(binder);
	}
}
