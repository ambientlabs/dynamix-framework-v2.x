/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.ISessionListener;

import android.os.IBinder;
import android.util.Log;

/**
 * Event sending encapsulation based on the command pattern. By abstracting event sending in this way, we are able to
 * simplify the process of sending events to registered Dynamix applications using the ContextManager. IEventCommands
 * can be sent to a filterable list of Dynamix applications through a single ContextManager method named
 * 'sendEventCommand'. The IEventCommand provides pre-processing, event sending and post processing logic.
 * <p>
 * Note that the abstract version of this class (EventCommand) does nothing for each IEventCommand, allowing specific
 * extensions of EventCommand to override only the methods needed.
 * 
 * @author Darren Carlson
 * @param
 * @see ContextManager
 */
abstract class EventCommand<T> extends IEventCommand<T> {
	public final static String TAG = EventCommand.class.getSimpleName();

	/**
	 * Returns true if the listener's IDynamixListener's Binder is alive; false otherwise.
	 */
	private static boolean isBinderAlive(IBinder iBinder) {
		if (iBinder != null)
			if (iBinder != null)
				return iBinder.isBinderAlive();
		Log.w(TAG, "Binder is dead for: " + iBinder);
		return false;
	}

	@Override
	public void postProcess() {
	}

	@Override
	public void preProcess() {
	}

	@Override
	public void processCommand(T listener) throws Exception {
	}

	/**
	 * EventCommand class used to process 'onContextPluginInstalled' events.
	 */
	public static class ContextPluginInstalled extends EventCommand<ISessionListener> {
		private ContextPluginInformation plug;

		public ContextPluginInstalled(ContextPluginInformation plug) {
			this.plug = plug;
		}

		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder()))
				listener.onContextPluginInstalled(plug);
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onContextPluginUninstalled' events.
	 */
	public static class ContextPluginUninstalledCommand extends EventCommand<ISessionListener> {
		private ContextPluginInformation plug;

		public ContextPluginUninstalledCommand(ContextPluginInformation plug) {
			this.plug = plug;
		}

		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder()))
				listener.onContextPluginUninstalled(plug);
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onListenerRemoved' events.
	 */
	public static class ListenerRemovedCommand extends EventCommand<IContextListener> {
		@Override
		public void processCommand(IContextListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder()))
				if (listener != null)
					listener.onContextListenerRemoved();
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onContextSupportRemoved' events.
	 */
	public static class ContextSupportRemovedCommand extends EventCommand<IContextListener> {
		private ContextSupportInfo support;
		private String message = "SUCCESS";
		private int errorCode = ErrorCodes.SUCCESS;

		public ContextSupportRemovedCommand(ContextSupportInfo support, String message, int errorCode) {
			this.support = support;
			this.message = message;
			this.errorCode = errorCode;
		}

		@Override
		public void processCommand(IContextListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder()))
				if (listener != null)
					listener.onContextSupportRemoved(support, message, errorCode);
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onSessionClosed' events.
	 */
	public static class SessionClosedCommand extends EventCommand<ISessionListener> {
		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			try {
				if (isBinderAlive(listener.asBinder()))
					listener.onSessionClosed();
			} catch (Exception e) {
				Log.w(TAG, "SessionClosedCommand Exception: " + e.toString());
			}
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onSessionOpened' events
	 */
	public static class SessionOpenedCommand extends EventCommand<ISessionListener> {
		// private ISessionListener listener;
		private String sessionId;

		public SessionOpenedCommand(String sessionId) {
			this.sessionId = sessionId;
		}

		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder()))
				listener.onSessionOpened(sessionId);
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onContextPluginDiscoveryStarted' events.
	 */
	public static class ContextPluginDiscoveryStartedCommand extends EventCommand<ISessionListener> {
		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			try {
				if (isBinderAlive(listener.asBinder()))
					listener.onContextPluginDiscoveryStarted();
			} catch (Exception e) {
				Log.w(TAG, "ContextPluginDiscoveryStartedCommand Exception: " + e.toString());
			}
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onContextPluginDiscoveryFinished' events.
	 */
	public static class ContextPluginDiscoveryFinishedCommand extends EventCommand<ISessionListener> {
	

		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			try {
				if (isBinderAlive(listener.asBinder()))
					listener.onContextPluginDiscoveryFinished();
			} catch (Exception e) {
				Log.w(TAG, "ContextPluginDiscoveryFinishedCommand Exception: " + e.toString());
			}
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onDynamixFrameworkActive' events.
	 * 
	 * @param <E>
	 */
	public static class DynamixFrameworkActiveCommand extends EventCommand<ISessionListener> {
		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			try {
				if (isBinderAlive(listener.asBinder()))
					listener.onDynamixFrameworkActive();
			} catch (Exception e) {
				Log.w(TAG, "onDynamixFrameworkActive Exception: " + e.toString());
			}
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onDynamixFrameworkInactive' events.
	 */
	public static class DynamixFrameworkInactiveCommand extends EventCommand<ISessionListener> {
		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			try {
				if (isBinderAlive(listener.asBinder()))
					listener.onDynamixFrameworkInactive();
			} catch (Exception e) {
				Log.w(TAG, "onDynamixFrameworkInactive Exception: " + e.toString());
			}
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onContextPluginError' events.
	 */
	public static class ContextPluginErrorCommand extends EventCommand<ISessionListener> {
		private ContextPluginInformation plugin;
		private String errorMessage;
		private int errorCode;

		public ContextPluginErrorCommand(ContextPluginInformation plugin, String errorMessage, int errorCode) {
			this.plugin = plugin;
			this.errorMessage = errorMessage;
			this.errorCode = errorCode;
		}

		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder()))
				listener.onContextPluginError(plugin, errorMessage, errorCode);
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}

	/**
	 * EventCommand class used to process 'onContextPluginEnabled' and 'onContextPluginDisabled' events.
	 */
	public static class ContextPluginEnabledStateChangeCommand extends EventCommand<ISessionListener> {
		// private ISessionListener listener;
		private ContextPluginInformation plugin;

		public ContextPluginEnabledStateChangeCommand(ContextPluginInformation plugin) {
			this.plugin = plugin;
		}

		@Override
		public void processCommand(ISessionListener listener) throws Exception {
			if (isBinderAlive(listener.asBinder())) {
				if (plugin.isEnabled()) {
					listener.onContextPluginEnabled(plugin);
				} else {
					listener.onContextPluginDisabled(plugin);
				}
			}
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName();
		}
	}
}