/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.IContextListener;

import android.os.RemoteException;
import android.util.Log;

/**
 * Enables internal Dynamix classes (e.g., plug-ins) to listen for other internal component (e.g.,g another plug-ins')
 * events.
 * 
 * @author Darren Carlson
 * 
 */
public class InternalContextListener extends IContextListener.Stub {
	// Private data
	private String TAG = this.getClass().getSimpleName();
	// private IBinder binder;
	private int identityHash;
	private IContextListener listener;
	private String appId;
	private String className;

	/**
	 * Creates an InternalDynamixListener for the specified appId and listener.
	 */
	public InternalContextListener(String appId, IContextListener listener) {
		this.appId = appId;
		this.className = listener.getClass().getName();
		this.identityHash = System.identityHashCode(listener);
		this.listener = listener;
		// this.binder = new SimpleBinder(getInternalListenerId());
		Log.d(TAG, "Created InternalDynamixListener for " + getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... check if their ids are the same
		InternalContextListener other = (InternalContextListener) candidate;
		return this.getId().equalsIgnoreCase(other.getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.getId().hashCode();
		return result;
	}

	/**
	 * Returns the id of this handler.
	 */
	final public String getId() {
		/*
		 * Note that the identity of the calling class may not be unique, but will be unique among 'live' objects. See
		 * http://stackoverflow.com/questions/909843/java-how-to-get-the-unique-id-of-an-object-which-overrides-hashcode
		 */
		return appId + "-" + className + "-" + identityHash;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextListenerRemoved() throws RemoteException {
		listener.onContextListenerRemoved();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextSupportRemoved(ContextSupportInfo supportInfo, String message, int errorCode)
			throws RemoteException {
		listener.onContextSupportRemoved(supportInfo, message, errorCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextResult(ContextResult result) throws RemoteException {
		listener.onContextResult(result);
	}
}
