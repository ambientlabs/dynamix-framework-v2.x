/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

/**
 * Bundle keys for various system-level operations that plug-ins can use to interact with the Dynamix runtime at a low
 * level. Note that plug-ins require system-level permission to use these operations, so it is not for general use.
 * 
 * @author Darren Carlson
 *
 */
public class SystemOperations {
	public static final String OPERATION = "OPERATION";

	public static class AddDynamixApplication {
		public static final String ADD_DYNAMIX_APPLICATION_OPERATION = "ADD_DYNAMIX_APPLICATION_OPERATION";
		public static final String APP_NAME = "APP_NAME";
		public static final String APP_TYPE = "APP_TYPE";

		// 2016.06.08: New
		public static final String LOOKUP_KEY = "LOOKUP_KEY";
		public static final String PAIRING_CODE = "PAIRING_CODE";
		public static final String TEMPORARY = "TEMPORARY";
	}

	public static class AddPairingOperation {
		public static final String ADD_PAIRING_OPERATION = "ADD_PAIRING_OPERATION";
		public static final String PAIRING_CODE = "PAIRING_CODE";
	}

	public static class GetFrameworkInfo {
		public static final String INSTANCE_ID = "INSTANCE_ID";
	}
}
