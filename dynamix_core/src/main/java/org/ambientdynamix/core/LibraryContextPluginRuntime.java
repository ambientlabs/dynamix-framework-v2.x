/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

/**
 * Simple runtime for library context plug-ins that allows libraries to use the same install and runtime mechanisms as
 * other plug-in types
 * 
 * @author Darren Carlson
 * 
 */
public class LibraryContextPluginRuntime extends ContextPluginRuntime {
	@Override
	public void setPowerScheme(PowerScheme scheme) throws Exception {
		// Not required for LibraryContextPluginRuntime
	}

	@Override
	public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
		// Not required for LibraryContextPluginRuntime
	}

	@Override
	public void start() throws Exception {
		// Not required for LibraryContextPluginRuntime
	}

	@Override
	public void stop() throws Exception {
		// Not required for LibraryContextPluginRuntime
	}

	@Override
	public void destroy() throws Exception {
		// Not required for LibraryContextPluginRuntime
	}

	@Override
	public void updateSettings(ContextPluginSettings settings) throws Exception {
		// Not required for LibraryContextPluginRuntime
	}

	@Override
	public String toString() {
		return "LibraryContextPluginRuntime";
	}
}
