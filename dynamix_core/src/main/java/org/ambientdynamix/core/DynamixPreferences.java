/*
 * Copyright (context) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.ambientdynamix.update.DynamixUpdates;

/**
 * Class for managing Dynamix preferences. Provides utility methods that allow simplified access to preference settings.
 *
 * @author Darren Carlson
 */
public class DynamixPreferences {
    // Preference Keys
    public static final String DYNAMIX_ENABLED = "DYNAMIX_ENABLED";
    public static final String AUTO_START_DYNAMIX = "AUTO_START_DYNAMIX";
    public static final String WEB_CONNECTOR = "WEB_CONNECTOR";
    public static final String VIBRATION_ALERTS = "VIBRATION_ALERTS";
    public static final String AUDIBLE_ALERTS = "AUDIBLE_ALERTS";
    public static final String BACKGROUND_MODE = "BACKGROUND_MODE";
    public static final String LOCAL_CONTEXT_PLUGIN_DISCOVERY = "LOCAL_CONTEXT_PLUGIN_DISCOVERY";
    public static final String DYNAMIX_PLUGIN_DISCOVERY_ENABLED = "DYNAMIX_PLUGIN_DISCOVERY_ENABLED";
    public static final String EXTERNAL_PLUGIN_DISCOVERY_ENABLED = "EXTERNAL_PLUGIN_DISCOVERY_ENABLED";
    public static final String AUTO_CONTEXT_PLUGIN_UPDATES = "AUTO_CONTEXT_PLUGIN_UPDATES";
    public static final String CONTEXT_PLUGIN_UPDATE_INTERVAL = "CONTEXT_PLUGIN_UPDATE_INTERVAL";
    public static final String AUTO_CONTEXT_PLUGIN_INSTALL = "AUTO_CONTEXT_PLUGIN_INSTALL";
    public static final String AUTO_APP_UNINSTALL = "AUTO_APP_UNINSTALL";
    public static final String ACCEPT_SELF_SIGNED_CERTS = "ACCEPT_SELF_SIGNED_CERTS";
    public static final String PRIMARY_CONTEXT_PLUGIN_REPO_PATH = "PRIMARY_CONTEXT_PLUGIN_REPO_PATH";
    public static final String EXTERNAL_CONTEXT_PLUGIN_REPO_PATH = "EXTERNAL_CONTEXT_PLUGIN_REPO_PATH";
    public static final String LOCAL_CONTEXT_PLUGIN_REPO_PATH = "LOCAL_CONTEXT_PLUGIN_REPO_PATH";
    public static final String USE_WIFI_NETWORK_ONLY = "USE_WIFI_NETWORK_ONLY";
    public static final String CERT_COLLECT = "CERT_COLLECT";
    public static final String ADMIN_DEBUG_MODE = "ADMIN_DEBUG_MODE";
    public static final String DETAILED_LOGGING = "DETAILED_LOGGING";
    public static final String AUTO_DEPENDENCY_UNINSTALL = "AUTO_DEPENDENCY_UNINSTALL";
    public static final String AUTO_ADMIN_PLUGIN_APPS = "AUTO_ADMIN_PLUGIN_APPS";
    public static final String ALLOW_UNVERIFIED_WEB_AGENT_ACCESS = "ALLOW_UNVERIFIED_WEB_ACCESS";
    public static final String DYNAMIX_UPDATE_AVAILABLE = "DYNAMIX_UPDATE_AVAILABLE";
    public static final String DYNAMIX_UPDATE_REQUIRED = "DYNAMIX_UPDATE_REQUIRED";
    public static final String DYNAMIX_UPDATE_MESSAGE = "DYNAMIX_UPDATE_MESSAGE";
    public static final String DYNAMIX_UPDATE_URL = "DYNAMIX_UPDATE_URL";
    public static final String DYNAMIX_PAIRING_SERVER_URL = "DYNAMIX_PAIRING_SERVER_URL";
    public static final String DYNAMIX_FIRST_RUN = "DYNAMIX_FIRST_RUN";
    public static final String DYNAMIX_INSTANCE_ID = "DYNAMIX_INSTANCE_ID";
    public static final String DYNAMIX_KEY = "DYNAMIX_KEY";
    private static final String DYNAMIX_INSTANCE_IP_SERVER_URL = "DYNAMIX_INSTANCE_IP_SERVER_URL";
    private static Context context;
    static boolean detailedLoggingCached = false;
    static boolean defailedLoggingCachedValue = false;

    // Singleton constructor
    private DynamixPreferences() {
    }

    static void setContext(Context context) {
        DynamixPreferences.context = context;
    }

    public static String getDynamixInstanceId() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(DYNAMIX_INSTANCE_ID, "id_not_set");
    }

    public static void setDynamixInstanceId(String instanceId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(DYNAMIX_INSTANCE_ID, instanceId);
        prefsEditor.commit();
    }

    public static boolean isFirstRun() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(DYNAMIX_FIRST_RUN, true);
    }

    public static void setFirstRun(boolean firstRunStatus) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(DYNAMIX_FIRST_RUN, firstRunStatus);
        prefsEditor.commit();
    }

    public static boolean collectCerts() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(CERT_COLLECT, false);
    }

    public static boolean useWifiNetworkOnly() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(USE_WIFI_NETWORK_ONLY, true);
    }

    public static boolean audibleAlertsEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(AUDIBLE_ALERTS, false);
    }

    public static boolean autoAppUninstall() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(AUTO_APP_UNINSTALL, true);
    }

    public static boolean autoContextPluginInstallEnabled() {
        if (DynamixService.isEmbedded())
            return DynamixService.getConfig().allowAutoContextPluginInstall();
        else {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPrefs.getBoolean(AUTO_CONTEXT_PLUGIN_INSTALL, DynamixService.getConfig()
                    .allowAutoContextPluginInstall());
        }
    }

    public static boolean autoContextPluginUpdateCheck() {
        if (DynamixService.isEmbedded()) {
            return DynamixService.getConfig().isAutoContextPluginUpdateCheckEnabled();
        } else {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPrefs.getBoolean(AUTO_CONTEXT_PLUGIN_UPDATES, false);
        }
    }

    public static boolean autoStartDynamix() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(AUTO_START_DYNAMIX, true);
    }

    public static boolean backgroundModeEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(BACKGROUND_MODE, false);
    }

    public static int getContextPluginUpdateInterval() {
        if (DynamixService.isEmbedded()) {
            return DynamixService.getConfig().getContextPluginUpdateCheckInterval();
        } else {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return Integer.parseInt(sharedPrefs.getString(CONTEXT_PLUGIN_UPDATE_INTERVAL, "60000"));
        }
    }

    public static boolean isDynamixEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(DYNAMIX_ENABLED, true);
    }

    /**
     * A debug mode that gives all apps admin-level rights, which enables them to perform tasks such as plug-in installs
     * and uninstalls, which is a pretty darn dangerous this to do. Only enable when necessary.
     *
     * @return True if admin debug mode is enabled; false otherwise.
     */
    public static boolean isAdminDebugModeEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(ADMIN_DEBUG_MODE, false);
    }

    /**
     * A debug mode that automatically authorizes plug-in apps as admin, which is a pretty darn dangerous thing to do.
     * Only enable when necessary.
     *
     * @return True if plug-in-apps should be authorized as admin automatically; false otherwise.
     */
    public static boolean isAutoAdminPluginAppsEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(AUTO_ADMIN_PLUGIN_APPS, false);
    }

    /**
     * Returns true if dependent plug-ins should be uninstalled; false otherwise.
     */
    public static boolean isAutoDependencyUninstallEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(AUTO_DEPENDENCY_UNINSTALL, true);
    }

    /**
     * Returns true if detailed logging is enabled; false otherwise.
     */
    public static boolean isDetailedLoggingEnabled() {
        // Cache result for performance
        if (!detailedLoggingCached) {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            defailedLoggingCachedValue = sharedPrefs.getBoolean(DETAILED_LOGGING, false);
        }
        return defailedLoggingCachedValue;
    }

    /**
     * Returns true if unverified web agent access is allowed; false otherwise. This is for debugging only and is HIGHLY
     * unsafe to enable;
     */
    public static boolean allowUnverifiedWebAgentAccess() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(ALLOW_UNVERIFIED_WEB_AGENT_ACCESS, false);
    }

    /**
     * Resets all preferences to their defaults.
     */
    public static void setDefaults() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Clear the existing SharedPreferences
        if (sharedPrefs != null) {
            SharedPreferences.Editor e = sharedPrefs.edit();
            e.clear();
            e.commit();
        }
        // Restore the default framework power scheme
        DynamixService.setNewPowerScheme(FrameworkConstants.DEFAULT_POWER_SCHEME);
    }

    public static void setDynamixEnabledState(boolean enabledState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(DYNAMIX_ENABLED, enabledState);
        prefsEditor.commit();
    }

    public static boolean vibrationAlertsEnabled() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(VIBRATION_ALERTS, false);
    }

    public static void setDynamixUpdateAvailable(DynamixUpdates updates) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        // Set the update state
        prefsEditor.putBoolean(DYNAMIX_UPDATE_AVAILABLE, updates.hasUpdate());
        // If there is an update, store the details
        if (updates.hasUpdate()) {
            prefsEditor.putBoolean(DYNAMIX_UPDATE_REQUIRED, updates.isUpdateRequired());
            prefsEditor.putString(DYNAMIX_UPDATE_MESSAGE, updates.getUpdateMessage());
            prefsEditor.putString(DYNAMIX_UPDATE_URL, updates.getUpdateUrl());
        }
        prefsEditor.commit();
    }

    public static boolean isDynamixUpdateAvailable() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(DYNAMIX_UPDATE_AVAILABLE, false);
    }

    public static boolean isDynamixUpdateRequired() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getBoolean(DYNAMIX_UPDATE_REQUIRED, false);
    }

    public static String getDynamixUpdateMessage() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(DYNAMIX_UPDATE_MESSAGE, "No Message Provided");
    }

    public static String getDynamixUpdateUrl() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(DYNAMIX_UPDATE_URL, "No URL Provided");
    }

    public static String getPairingServerURL() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(DYNAMIX_PAIRING_SERVER_URL,
                "http://pairing.ambientdynamix.org/securePairing/newPairRequest.php");
    }

    public static String getInstanceIpServerURL() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(DYNAMIX_INSTANCE_IP_SERVER_URL,
                "http://pairing.ambientdynamix.org/securePairing/registerDynamixInstance.php");
    }
}