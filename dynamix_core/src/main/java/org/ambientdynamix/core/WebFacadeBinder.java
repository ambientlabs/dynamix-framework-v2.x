/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.content.Context;
import android.os.RemoteException;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;

/**
 * The WebFacadeBinder provides an implementation of the IDynamixFacade API for web clients. This class is used in
 * combination with the DynamixService to handle API calls from Dynamix web applications. Note that not all
 * IDynamixFacade methods are supported for web applications.
 *
 * @author Darren Carlson
 * @see IDynamixFacade
 */
public class WebFacadeBinder extends AppFacadeBinder {
    // Private data
    private final String TAG = this.getClass().getSimpleName();
    private String webAppId;

    /**
     * Creates a WebFacadeBinder.
     */
    public WebFacadeBinder(Context context, ContextManager conMgr, boolean embeddedMode, String webAppId, String facadeId) {
        super(context, conMgr, embeddedMode, true);
        this.webAppId = webAppId;
        super.facadeId = facadeId;
    }

    /**
     * Returns the Dynamix application associated with this binder.
     */
    public DynamixApplication getDynamixApp() {
        return getAuthorizedApplication(getCallerId());
    }

    /**
     * Not supported for web clients (throws UnsupportedOperationException).
     */
    @Override
    public Result requestContextPluginInstallation(ContextPluginInformation plugInfo) throws RemoteException {
        // Throw Unsupported Exception
        throw new UnsupportedOperationException();
    }

    /**
     * Not supported for web clients (throws UnsupportedOperationException).
     */
    @Override
    public Result requestContextPluginUninstall(ContextPluginInformation plugInfo) throws RemoteException {
        // Throw Unsupported Exception
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DynamixApplication createNewApplicationFromCaller(String id, boolean admin) {
        // Construct a new application for the caller
        DynamixApplication app = new DynamixApplication(APP_TYPE.WEB, id, id);
        app.setAppDescription("Web Agent");
        app.addAppDetail(DynamixApplication.VERSION_CODE, "1.0");
        return app;
    }

    /**
     * Returns the DynamixApplication associated with the incoming id, or null if the id is not authorized.
     *
     * @param id The id of the application.
     * @return The DynamixApplication associated with the incoming id, or null if the id is not authorized.
     */
    @Override
    public DynamixApplication getAuthorizedApplication(String id) {
        // We override here to force the use of the webAppId
        return DynamixService.getDynamixApplication(webAppId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getCallerId() {
        // Log.d(TAG, "Returning caller ID: " + webAppId);
        return this.webAppId;
    }
}
