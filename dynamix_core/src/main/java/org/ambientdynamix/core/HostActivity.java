/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.lang.Thread.UncaughtExceptionHandler;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.api.contextplugin.security.Permissions;
import org.ambientdynamix.ui.activities.HomeActivity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ClipDescription;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

/**
 * Host activity for dynamically injected interfaces provided by plug-ins.
 * 
 * @author Darren Carlson
 *
 */
public class HostActivity extends Activity {
	public final static String TAG = HostActivity.class.getSimpleName();
	private static ContextPlugin plugin;;
	private static IPluginView pluginView;
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private IntentFilter[] mFilters;
	private Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		handler.getLooper().getThread().setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				Log.w(TAG, "uncaughtException: " + ex);
				Utils.showGlobalAlert(HomeActivity.activity, "Dynamix encoutered an error and must restart", true);
			}
		});
		if (isBound()) {
			// Setup NFC dispatching
			mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
			if (mNfcAdapter != null) {
				mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
				// Create a generic PendingIntent that will be deliver to this activity. The NFC stack
				// will fill in the intent with the details of the discovered tag before delivering to
				// this activity.
				mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), 0);
				// Setup intent filters for all types of NFC dispatches to intercept all discovered tags
				IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
				try {
					ndef.addDataType("*/*");
				} catch (IntentFilter.MalformedMimeTypeException e) {
					throw new RuntimeException("fail", e);
				}
				IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
				tech.addDataScheme("vnd.android.nfc");
				tech.addCategory(Intent.CATEGORY_DEFAULT);
				try {
					tech.addDataType(ClipDescription.MIMETYPE_TEXT_PLAIN);
				} catch (IntentFilter.MalformedMimeTypeException e) {
					throw new RuntimeException("Check your mime type.");
				}
				mFilters = new IntentFilter[] { ndef, tech, new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED), };
			}
			/*
			 * Give the ActivityController to the view. If the plug-in has the permission ACCESS_FULL_CONTEXT, send true
			 * to the second argument (
			 */
			pluginView.setActivityController(new ActivityController(this, plugin.getPermissions() != null
					&& plugin.getPermissions().contains(Permission.createPermission("Permissions.ACCESS_FULL_CONTEXT"))));
		} else {
			Log.w(TAG, "onCreate when activity was not bound");
			this.finish();
		}
	}

	@Override
	protected void onResume() {
		Log.v(TAG, "onResume");
		super.onResume();
		if (isBound()) {
			if (mNfcAdapter != null) {
				// Enable foreground NFC processing
				String[][] techList = new String[][] {};
				mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters, techList);
			}
			/*
			 * Setup a Handler to run the View, making sure to handle uncaughtExceptions that may be thrown.
			 */
			handler.getLooper().getThread().setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread thread, Throwable ex) {
					Log.w(TAG, "Exeption in view " + ex.toString());
					ex.printStackTrace();
					DynamixService.disableContextPlugin(plugin.getContextPluginInformation());
				}
			});
			handler.post(new Runnable() {
				@Override
				public void run() {
					// Set the screen orientation
					setRequestedOrientation(pluginView.getPreferredOrientation());
					// Inject the plugin's View into the host Activity to show it
					try {
						setContentView(pluginView.getView(), new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.MATCH_PARENT));
					} catch (Exception e) {
						Log.w(TAG, "Exception when loading a plug-in's view: " + plugin + " | exeption was " + e);
						finish();
					}
				}
			});
		} else {
			Log.w(TAG, "onResume when activity was not bound");
			this.finish();
		}
	}

	@Override
	protected void onNewIntent(final Intent intent) {
		Log.v(TAG, "onNewIntent: " + intent);
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (isBound())
					pluginView.handleIntent(intent);
			}
		});
	}

	@Override
	protected void onPause() {
		Log.v(TAG, "onPause");
		if (mNfcAdapter != null)
			mNfcAdapter.disableForegroundDispatch(this);
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.v(TAG, "onStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.v(TAG, "onDestroy. Finishing = " + isFinishing());
		if (pluginView != null) {
			pluginView.destroyView();
		}
		clearBinding();
		super.onDestroy();
	}

	public static synchronized boolean bindPlugin(ContextPlugin plugin, IPluginView pluginView) {
		if (!isBound()) {
			if (plugin != null && pluginView != null) {
				HostActivity.plugin = plugin;
				HostActivity.pluginView = pluginView;
				Log.d(TAG, "bindPlugin successfully bound " + plugin + " to view " + pluginView);
				return true;
			} else {
				Log.w(TAG, "bindPlugin received null plugin or view: plugin " + plugin + ", view " + pluginView);
				return false;
			}
		} else {
			Log.w(TAG, "Cannot bind host activity to plug-in since the host is already bound");
			return false;
		}
	}

	public static synchronized void clearBinding() {
		plugin = null;
		pluginView = null;
	}

	public static synchronized boolean isBound() {
		return plugin != null;
	}

	@Override
	public void onBackPressed() {
		Log.d(TAG, "onBackPressed");
		try {
			pluginView.destroyView();
		} catch (Exception e) {
			Log.w(TAG, "onBackPressed.exception: " + e);
		}
        super.onBackPressed();
	}
}
