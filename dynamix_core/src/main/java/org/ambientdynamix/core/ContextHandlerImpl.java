/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportCallback;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.ContextType;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextListenerResult;
import org.ambientdynamix.api.application.IContextRequestCallback;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.VersionedPlugin;
import org.ambientdynamix.core.FirewallRule.FirewallAccess;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Internal implementation of the IContextHandler interface.
 *
 * @author Darren Carlson
 * @see IContextHandler
 */
public class ContextHandlerImpl extends IContextHandler.Stub {
    // Private data
    private final String TAG = this.getClass().getSimpleName();
    private Map<ContextRequestData, IContextRequestCallback> contextRequestCallbackMap = new ConcurrentHashMap<ContextRequestData, IContextRequestCallback>();
    private ContextManager conMgr;
    private String id = UUID.randomUUID().toString();
    private DynamixApplication handlerApp;
    private Context context;
    private IDynamixFacade facade;

    class ContextSubscription {
        UUID subscriptionId = UUID.randomUUID();
        ContextSupport contextSupport;
        IContextListener listener;
    }

    /**
     * Creates a ContextHandlerImpl.
     */
    public ContextHandlerImpl(ContextManager conMgr, DynamixApplication handlerApp, Context context, IDynamixFacade facade) {
        this.conMgr = conMgr;
        this.handlerApp = handlerApp;
        this.context = context;
        this.facade = facade;
    }

    @Override
    public String toString() {
        return "Context Handler for: " + handlerApp.getAppID();
    }

    public IDynamixFacade getDynamixFacade() {
        return this.facade;
    }

    /**
     * Returns the listener for the specified id, or null if there is no mapping.
     */
    public IContextListener getContextListener(String id) {
        DynamixSession s = SessionManager.getSession(this);
        if (s != null) {
            for (ContextSupport sup : s.getAllContextSupport()) {
                if (sup.getSupportId().toString().equalsIgnoreCase(id) && sup.hasContextListener())
                    return sup.getContextListener();
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addPluginContextSupportWithListener(String pluginId, String contextType, IContextListener listener) {
        return doAddContextSupport(listener, pluginId, contextType, false, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPluginContextSupportWithCallback(String pluginId, String contextType,
                                                    IContextSupportCallback callback) {
        doAddContextSupport(null, pluginId, contextType, false, null, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addPluginContextSupport(String pluginId, String contextType) throws RemoteException {
        return doAddContextSupport(null, pluginId, contextType, false, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPluginContextSupportWithListenerAndCallback(String pluginId, String contextType,
                                                               IContextListener listener, IContextSupportCallback callback) throws RemoteException {
        doAddContextSupport(listener, pluginId, contextType, false, null, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addPluginConfiguredContextSupport(String pluginId, String contextType, Bundle contextSupportConfig)
            throws RemoteException {
        return doAddContextSupport(null, pluginId, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPluginConfiguredContextSupportWithCallback(String pluginId, String contextType,
                                                              Bundle contextSupportConfig, IContextSupportCallback callback) throws RemoteException {
        doAddContextSupport(null, pluginId, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addPluginConfiguredContextSupportWithListener(String pluginId, String contextType, Bundle config,
                                                                IContextListener listener) {
        return doAddContextSupport(listener, pluginId, contextType, false, config, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPluginConfiguredContextSupportWithListenerAndCallback(String pluginId, String contextType,
                                                                         Bundle contextSupportConfig, IContextListener listener, IContextSupportCallback callback) {
        doAddContextSupport(listener, pluginId, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addPluginsConfiguredContextSupport(List<String> pluginIds, String contextType,
                                                     Bundle contextSupportConfig) throws RemoteException {
        List<VersionedPlugin> plugList = new ArrayList<VersionedPlugin>();
        for (String id : pluginIds)
            plugList.add(new VersionedPlugin(id));
        return doAddContextSupport(null, plugList, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPluginsConfiguredContextSupportWithCallback(List<String> pluginIds, String contextType,
                                                               Bundle contextSupportConfig, IContextSupportCallback callback) throws RemoteException {
        List<VersionedPlugin> plugList = new ArrayList<VersionedPlugin>();
        for (String id : pluginIds)
            plugList.add(new VersionedPlugin(id));
        doAddContextSupport(null, plugList, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addPluginsConfiguredContextSupportWithListener(List<String> pluginIds, String contextType,
                                                                 Bundle contextSupportConfig, IContextListener listener) throws RemoteException {
        List<VersionedPlugin> plugList = new ArrayList<VersionedPlugin>();
        for (String id : pluginIds)
            plugList.add(new VersionedPlugin(id));
        return doAddContextSupport(listener, plugList, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPluginsConfiguredContextSupportWithListenerAndCallback(List<String> pluginIds, String contextType,
                                                                          Bundle contextSupportConfig, IContextListener listener, IContextSupportCallback callback)
            throws RemoteException {
        List<VersionedPlugin> plugList = new ArrayList<VersionedPlugin>();
        for (String id : pluginIds)
            plugList.add(new VersionedPlugin(id));
        doAddContextSupport(listener, plugList, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addVersionedPluginContextSupport(String pluginId, String pluginVersion, String contextType)
            throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        return doAddContextSupport(null, plugins, contextType, false, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVersionedPluginContextSupportWithCallback(String pluginId, String pluginVersion, String contextType,
                                                             IContextSupportCallback callback) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        doAddContextSupport(null, plugins, contextType, false, null, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addVersionedPluginContextSupportWithListener(String pluginId, String pluginVersion,
                                                               String contextType, IContextListener listener) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        return doAddContextSupport(listener, plugins, contextType, false, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVersionedPluginContextSupportWithListenerAndCallback(String pluginId, String pluginVersion,
                                                                        String contextType, IContextListener listener, IContextSupportCallback callback) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        doAddContextSupport(listener, plugins, contextType, false, null, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addVersionedPluginConfiguredContextSupport(String pluginId, String pluginVersion, String contextType,
                                                             Bundle contextSupportConfig) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        return doAddContextSupport(null, plugins, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVersionedPluginConfiguredContextSupportWithCallback(String pluginId, String pluginVersion,
                                                                       String contextType, Bundle contextSupportConfig, IContextSupportCallback callback) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        doAddContextSupport(null, plugins, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addVersionedPluginConfiguredContextSupportWithListener(String pluginId, String pluginVersion,
                                                                         String contextType, Bundle contextSupportConfig, IContextListener listener) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        return doAddContextSupport(listener, plugins, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVersionedPluginConfiguredContextSupportWithListenerAndCallback(String pluginId,
                                                                                  String pluginVersion, String contextType, Bundle contextSupportConfig, IContextListener listener,
                                                                                  IContextSupportCallback callback) throws RemoteException {
        List<VersionedPlugin> plugins = new ArrayList<VersionedPlugin>();
        plugins.add(new VersionedPlugin(pluginId, pluginVersion));
        doAddContextSupport(listener, plugins, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addVersionedPluginsConfiguredContextSupport(List<VersionedPlugin> plugins, String contextType,
                                                              Bundle contextSupportConfig) throws RemoteException {
        return doAddContextSupport(null, plugins, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVersionedPluginsConfiguredContextSupportWithCallback(List<VersionedPlugin> plugins,
                                                                        String contextType, Bundle contextSupportConfig, IContextSupportCallback callback) throws RemoteException {
        doAddContextSupport(null, plugins, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result addVersionedPluginsConfiguredContextSupportWithListener(List<VersionedPlugin> plugins,
                                                                          String contextType, Bundle contextSupportConfig, IContextListener listener) throws RemoteException {
        return doAddContextSupport(listener, plugins, contextType, false, contextSupportConfig, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVersionedPluginsConfiguredContextSupportWithListenerAndCallback(List<VersionedPlugin> plugins,
                                                                                   String contextType, Bundle contextSupportConfig, IContextListener listener, IContextSupportCallback callback)
            throws RemoteException {
        doAddContextSupport(listener, plugins, contextType, false, contextSupportConfig, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextSupportResult getContextSupport() throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Try to grab the cached app from the DynamixService
        DynamixSession session = SessionManager.getSession(this);
        if (session != null) {
            return conMgr.getContextSupport(session, this);
        } else {
            Log.w(TAG, "Session Not found for: " + getApplication());
            return new ContextSupportResult("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeContextSupport(ContextSupportInfo info) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Remove the context support
        removeContextSupportWithCallback(info, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeContextSupportWithCallback(ContextSupportInfo info, final ICallback callback)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (info != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getApplication();
            // Continue if the app is authorized
            if (app != null) {
                // App is authorized
                conMgr.removeContextSupport(this, info, callback);
            } else {
                Log.w(TAG, app + " is not authorized!");
                SessionManager.sendCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Missing parameters in removeContextSupport");
            SessionManager.sendCallbackFailure(callback, "MISSING_PARAMETERS: ContextSupportInfo",
                    ErrorCodes.MISSING_PARAMETERS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IContextListenerResult getContextListenerResult() throws RemoteException {
        Collection<IContextListener> listeners = new ArrayList<IContextListener>();
        for (ContextSupport sup : SessionManager.getContextSupport(this)) {
            if (sup.hasContextListener())
                listeners.add(sup.getContextListener());
        }
        return new IContextListenerResult(listeners);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result resendCachedContextEvents(IContextListener listener, int previousMills) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Check for the listener
        if (listener != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getApplication();
            // Continue if the app is authorized
            if (app != null) {
                // App is authorized
                DynamixSession session = SessionManager.getSession(this);
                if (session != null && session.isSessionOpen()) {
                    conMgr.resendCachedEvents(app, this, listener, previousMills);
                    return new Result();
                } else {
                    Log.w(TAG, "could not find open session for: " + app);
                    return new Result("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
                }
            } else {
                Log.w(TAG, app + " is not authorized!");
                return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Listener not found");
            return new Result("Listener not found", ErrorCodes.NOT_FOUND);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result resendAllTypedCachedContextEvents(IContextListener listener, String contextType)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Check for the listener
        if (listener != null) {
            if (contextType != null) {
                // Access the application securely... returns null if the app is not authorized
                DynamixApplication app = getApplication();
                // Continue if the app is authorized
                if (app != null) {
                    // App is authorized
                    DynamixSession session = SessionManager.getSession(this);
                    if (session != null && session.isSessionOpen()) {
                        conMgr.resendCachedEvents(app, this, listener, Utils.trim(contextType));
                        return new Result();
                    } else {
                        Log.w(TAG, "could not find open session for: " + app);
                        return new Result("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
                    }
                } else {
                    Log.w(TAG, app + " is not authorized!");
                    return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
                }
            } else {
                Log.w(TAG, "Missing parameters in resendAllCachedContextEvents");
                return new Result("Missing parameters in resendAllCachedContextEvents", ErrorCodes.MISSING_PARAMETERS);
            }
        } else {
            Log.w(TAG, "Listener not found");
            return new Result("Listener not found", ErrorCodes.NOT_FOUND);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result resendTypedCachedContextEvents(IContextListener listener, String contextType, int previousMills)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Check the listener
        if (listener != null) {
            if (contextType != null) {
                // Access the application securely... returns null if the app is not authorized
                DynamixApplication app = getApplication();
                // Continue if the app is authorized
                if (app != null) {
                    // App is authorized
                    DynamixSession session = SessionManager.getSession(this);
                    if (session != null && session.isSessionOpen()) {
                        conMgr.resendCachedEvents(app, this, listener, Utils.trim(contextType), previousMills);
                        return new Result();
                    } else {
                        Log.w(TAG, "could not find open session for: " + app);
                        return new Result("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
                    }
                } else {
                    Log.w(TAG, app + " is not authorized!");
                    return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
                }
            } else {
                Log.w(TAG, "Missing parameters in resendTypedCachedContextEvents");
                return new Result("Missing parameters in resendTypedCachedContextEvents", ErrorCodes.MISSING_PARAMETERS);
            }
        } else {
            Log.w(TAG, "Listener not found");
            return new Result("Listener not found", ErrorCodes.NOT_FOUND);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result pluginContextRequest(String pluginId, String contextType) throws RemoteException {
        return doContextRequest(new VersionedPlugin(pluginId), contextType, null, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pluginContextRequestWithCallback(String pluginId, String contextType, IContextRequestCallback callback)
            throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId), contextType, null, callback, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result configuredPluginContextRequest(String pluginId, String contextType, Bundle contextConfig)
            throws RemoteException {
        return doContextRequest(new VersionedPlugin(pluginId), contextType, contextConfig, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configuredPluginContextRequestWithCallback(String pluginId, String contextType, Bundle contextConfig,
                                                           IContextRequestCallback callback) {
        doContextRequest(new VersionedPlugin(pluginId), contextType, contextConfig, callback, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pluginContextRequestWithCallbackAndStatus(String pluginId, String contextType,
                                                          IContextRequestCallback callback, IContextSupportCallback status) throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId), contextType, null, callback, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configuredPluginContextRequestWithCallbackAndStatus(String pluginId, String contextType,
                                                                    Bundle contextConfig, IContextRequestCallback callback, IContextSupportCallback status)
            throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId), contextType, contextConfig, callback, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result versionedPluginContextRequest(String pluginId, String pluginVersion, String contextType)
            throws RemoteException {
        return doContextRequest(new VersionedPlugin(pluginId, pluginVersion), contextType, null, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void versionedPluginContextRequestWithCallback(String pluginId, String pluginVersion, String contextType,
                                                          IContextRequestCallback callback) throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId, pluginVersion), contextType, null, callback, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void versionedPluginContextRequestWithCallbackAndStatus(String pluginId, String pluginVersion,
                                                                   String contextType, IContextRequestCallback callback, IContextSupportCallback status)
            throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId, pluginVersion), contextType, null, callback, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result configuredVersionedPluginContextRequest(String pluginId, String pluginVersion, String contextType,
                                                          Bundle contextConfig) throws RemoteException {
        return doContextRequest(new VersionedPlugin(pluginId, pluginVersion), contextType, contextConfig, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configuredVersionedPluginContextRequestWithCallback(String pluginId, String pluginVersion,
                                                                    String contextType, Bundle contextConfig, IContextRequestCallback callback) throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId, pluginVersion), contextType, contextConfig, callback, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configuredVersionedPluginContextRequestWithCallbackAndStatus(String pluginId, String pluginVersion,
                                                                             String contextType, Bundle contextConfig, IContextRequestCallback callback, IContextSupportCallback status)
            throws RemoteException {
        doContextRequest(new VersionedPlugin(pluginId, pluginVersion), contextType, contextConfig, callback, status);
    }

    /**
     * Utility method for sending context requests.
     *
     * @param plugin      The target plug-in.
     * @param contextType The context type associated with this request.
     * @param config      Optional context request config data.
     * @param callback    A context request callback.
     * @param status      A context support callback.
     * @return A Result indicating success or failure.
     */
    private Result doContextRequest(final VersionedPlugin plugin, final String contextType, final Bundle config,
                                    final IContextRequestCallback callback, IContextSupportCallback status) {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        Log.d(TAG, "doContextRequest with config " + config + " and calling listener appId " + getApplication());
        // Make sure Dynamix is active
        if (DynamixService.isFrameworkStarted()) {
            // Access the application securely... returns null if the app is not authorized
            final DynamixApplication app = getApplication();
            // Continue if the app is authorized
            if (app != null) {
                // App is authorized, check for session and context support
                final DynamixSession session = SessionManager.getSession(this);
                if (session != null && session.isSessionOpen()) {
                    if (session.hasContextSupportForType(this, contextType)) {
                        // First, send install success to the status callback
                        SessionManager.sendContextSupportSuccess(status, session.getAllContextSupportInfo(contextType));
                        // Request the context request
                        IdResult result = DynamixService.handleContextRequest(app, session, this, plugin,
                                Utils.trim(contextType), config);
                        if (result.wasSuccessful()) {
                            if (callback != null)
                                contextRequestCallbackMap.put(new ContextRequestData(UUID.fromString(result.getId()),
                                        plugin), callback);
                        } else {
                            Log.w(TAG, "Context Request Failed 1: " + result.getMessage());
                            SessionManager.sendContextRequestCallbackFailure(callback, result.getMessage(),
                                    result.getErrorCode());
                        }
                        return result;
                    } else {
                        final IContextSupportCallback fStatus = status;
                        doAddContextSupport(null, plugin, contextType, false, config, new ContextSupportCallback() {
                            @Override
                            public void onWarning(String message, int errorCode) throws RemoteException {
                                SessionManager.sendContextSupportWarning(fStatus, message, errorCode);
                            }

                            @Override
                            public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                                SessionManager.sendContextSupportSuccess(fStatus, supportInfo);
                                // Request the context request
                                IdResult result = DynamixService.handleContextRequest(app, session,
                                        ContextHandlerImpl.this, plugin, Utils.trim(contextType), config);
                                if (result.wasSuccessful()) {
                                    if (callback != null)
                                        contextRequestCallbackMap.put(
                                                new ContextRequestData(UUID.fromString(result.getId()), plugin),
                                                callback);
                                } else {
                                    Log.w(TAG, "Context Request Failed 2: " + result.getMessage());
                                    SessionManager.sendContextRequestCallbackFailure(callback, result.getMessage(),
                                            result.getErrorCode());
                                }
                            }

                            @Override
                            public void onProgress(int percentComplete) throws RemoteException {
                                SessionManager.sendContextSupportProgress(fStatus, percentComplete);
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                SessionManager.sendContextSupportFailure(fStatus, message, errorCode);
                                SessionManager.sendContextRequestCallbackFailure(callback, message, errorCode);
                            }
                        });
                        return new Result();
                    }
                } else {
                    Log.w(TAG, "could not find open session for: " + app);
                    SessionManager.sendContextRequestCallbackFailure(callback, "SESSION_NOT_FOUND",
                            ErrorCodes.SESSION_NOT_FOUND);
                    return new Result("SESSION_NOT_FOUND", ErrorCodes.SESSION_NOT_FOUND);
                }
            } else {
                Log.w(TAG, app + " is not authorized!");
                SessionManager.sendContextRequestCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
                return new Result("NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            SessionManager.sendContextRequestCallbackFailure(callback, "Dynamix NOT_READY!", ErrorCodes.NOT_READY);
            return new Result("Dynamix NOT_READY!", ErrorCodes.NOT_READY);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeContextSupportForContextType(String contextType) throws RemoteException {
        removeContextSupportForContextTypeWithCallback(contextType, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeContextSupportForContextTypeWithCallback(String contextType, ICallback callback)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (contextType != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getApplication();
            // Continue if the app is authorized
            if (app != null) {
                // App is authorized
                conMgr.removeContextSupportForContextType(this, Utils.trim(contextType), callback);
            } else {
                Log.w(TAG, app + " is not authorized!");
                SessionManager.sendCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Missing parameters in removeContextSupportForContextType");
            SessionManager.sendCallbackFailure(callback, "MISSING_PARAMETERS: contextType",
                    ErrorCodes.MISSING_PARAMETERS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllContextSupport() throws RemoteException {
        removeAllContextSupportWithCallback(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllContextSupportWithCallback(ICallback callback) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app is not authorized
        DynamixApplication app = getApplication();
        // Continue if the app is authorized
        if (app != null) {
            // App is authorized
            DynamixSession session = SessionManager.getSession(this);
            if (session != null)
                conMgr.removeAllContextSupport(session, this, callback);
            else{
                Log.w(TAG, app + " has no session!");
                SessionManager.sendCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, app + " is not authorized!");
            SessionManager.sendCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result resendAllCachedContextEvents(IContextListener listener) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Check the listener
        if (listener != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getApplication();
            // Continue if the app is authorized
            if (app != null) {
                // App is authorized
                DynamixSession session = SessionManager.getSession(this);
                if (session != null && session.isSessionOpen()) {
                    conMgr.resendCachedEvents(app, this, listener);
                    return new Result();
                } else {
                    Log.w(TAG, "could not find open session for: " + app);
                    return new Result("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
                }
            } else {
                Log.w(TAG, app + " is not authorized!");
                return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Listener not found");
            return new Result("Listener not found", ErrorCodes.NOT_FOUND);
        }
    }

    /**
     * Performs adding context support.
     */
    protected Result doAddContextSupport(final IContextListener listener, final String pluginId,
                                         final String contextType, final boolean forcePluginInstall, final Bundle config,
                                         final IContextSupportCallback callback) {
        List<VersionedPlugin> pluginIds = new ArrayList<VersionedPlugin>();
        pluginIds.add(new VersionedPlugin(pluginId));
        return doAddContextSupport(listener, pluginIds, contextType, forcePluginInstall, config, callback);
    }

    /**
     * Performs adding context support.
     */
    protected Result doAddContextSupport(final IContextListener listener, final VersionedPlugin plugin,
                                         final String contextType, final boolean forcePluginInstall, final Bundle config,
                                         final IContextSupportCallback callback) {
        List<VersionedPlugin> pluginIds = new ArrayList<VersionedPlugin>();
        pluginIds.add(plugin);
        return doAddContextSupport(listener, pluginIds, contextType, forcePluginInstall, config, callback);
    }

    /**
     * Performs adding context support.
     */
    protected synchronized Result doAddContextSupport(final IContextListener listener,
                                                      final List<VersionedPlugin> plugins, final String contextType, final boolean forcePluginInstall,
                                                      final Bundle config, final IContextSupportCallback callback) {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // We only allow adding context support when Dynamix is started
        if (DynamixService.isFrameworkStarted()) {
            if (contextType != null && plugins != null && !plugins.isEmpty()) {
                // Access the application securely... returns null if the app is not authorized
                final DynamixApplication app = getApplication();
                // Continue if the app is authorized
                if (app != null) {
                    // Try to grab the cached app from the DynamixService
                    DynamixSession session = SessionManager.getSession(this);
                    if (session != null && session.isSessionOpen()) {
                        /*
						 * Verify that each plugin id is known by Dynamix.
						 */
                        List<ContextPluginInformation> allRequestedPlugins = new ArrayList<ContextPluginInformation>();
                        for (VersionedPlugin plug : plugins) {
                            ContextPluginInformation info = DynamixService.getContextPluginInfo(plug.getPluginId(),
                                    plug.getPluginVersion());
                            if (info != null) {
                                allRequestedPlugins.add(info);
                            } else {
                                if (Utils.isWaitingForWifi()) {
                                    String warning = plug + " was not found. Warning: Waiting for WiFi.";
                                    Log.w(TAG, warning);
                                    SessionManager.sendContextSupportWarning(callback, warning,
                                            ErrorCodes.CANNOT_ACCESS_NETWORK);
                                    SessionManager.sendContextSupportFailure(callback, warning,
                                            ErrorCodes.CONFIGURATION_ERROR);
                                    return new Result(warning, ErrorCodes.CONFIGURATION_ERROR);
                                } else {
                                    String warning = plug + " was not found.";
                                    SessionManager.sendContextSupportFailure(callback, warning,
                                            ErrorCodes.CONFIGURATION_ERROR);
                                    return new Result(warning, ErrorCodes.CONFIGURATION_ERROR);
                                }
                            }
                        }
						/*
						 * Check if access to the context type and plug-in is granted.
						 */
                        List<FirewallRule> pendingRules = new ArrayList<FirewallRule>();
                        if (!app.isAdmin()) {
                            // This grabs rules from both app and its session (if open)
                            FirewallRule rule = app.getFirewallRule(contextType, allRequestedPlugins);
                            // Check if a firewall rule exists
                            if (rule != null) {
                                // Handled BLOCKED
                                if (rule.getFirewallAccess() == FirewallAccess.BLOCKED) {
                                    SessionManager.sendContextSupportFailure(callback,
                                            "Access to context support BLOCKED: " + contextType,
                                            ErrorCodes.NOT_AUTHORIZED);
                                    return new Result("Access to context support BLOCKED: " + contextType,
                                            ErrorCodes.NOT_AUTHORIZED);
                                } else {
                                    // Check if access is granted
                                    if (!rule.isAccessGranted()) {
                                        // Nope, handle DENIED, PENDING
                                        pendingRules.add(rule);
                                    } else {
										/*
										 * Yep, rule is allowed, so do nothing and we'll fall through and process below.
										 */
                                    }
                                }
                            } else {
                                // No rule, so set one up
                                ContextType ctype = allRequestedPlugins.get(0).getContextType(contextType);
                                if (ctype != null)
                                    pendingRules.add(new FirewallRule(contextType, ctype.getName(), ctype
                                            .getDescription(), allRequestedPlugins, FirewallAccess.PENDING));
                                else {
                                    Log.w(TAG, "Context type UNKNOWN by requested plug-ins for " + contextType);
                                    SessionManager.sendContextSupportFailure(callback,
                                            "Context type UNKNOWN by requested plug-ins for " + contextType,
                                            ErrorCodes.CONFIGURATION_ERROR);
                                    return new Result("Context type UNKNOWN by requested plug-ins for " + contextType,
                                            ErrorCodes.CONFIGURATION_ERROR);
                                }
                            }
                        } else {
							/*
							 * Admin apps are always allowed, so do nothing and we'll fall through and process below.
							 */
                        }
                        // If we have pending rules, delegate them to the firewall
                        if (!pendingRules.isEmpty()) {
                            PendingContextSupportRequest pendingRequest = new PendingContextSupportRequest(app,
                                    session, listener, allRequestedPlugins, contextType, forcePluginInstall, config,
                                    callback, this);
                            DynamixService.addContextFirewallRequest(pendingRequest, pendingRules);
                            return new Result();
                        } else {
							/*
							 * No pending rules, so the request is allowed.
							 */
							/*
							 * For the moment, we're not refreshing plug-ins here, since we need a better way to handle
							 * this without blocking
							 */
                            // Log.d(TAG, "Refreshing context plug-ins...");
                            // Refresh plug-in list in blocking-mode
                            // DynamixService.checkForContextPluginUpdates(false);
                            // Log.d(TAG, "Refreshing context plug-ins... DONE!");
                            conMgr.addContextSupport(app, ContextHandlerImpl.this, Utils.trim(contextType), plugins,
                                    listener, config, callback);
                            // Return success
                            return new Result();
                        }
                    } else {
                        Log.w(TAG, "addContextSupport could not find open session for: " + app);
                        SessionManager.sendContextSupportFailure(callback, "SESSION_NOT_FOUND",
                                ErrorCodes.SESSION_NOT_FOUND);
                        return new Result("SESSION_NOT_FOUND", ErrorCodes.SESSION_NOT_FOUND);
                    }
                } else {
                    Log.w(TAG, app + " is not authorized!");
                    SessionManager.sendContextSupportFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
                    return new Result("NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
                }
            } else {
                Log.w(TAG, "MISSING_PARAMETERS: contextType and plug-ins are required");
                SessionManager.sendContextSupportFailure(callback,
                        "MISSING_PARAMETERS: contextType and plug-ins are required", ErrorCodes.MISSING_PARAMETERS);
                return new Result("MISSING_PARAMETERS: contextType", ErrorCodes.MISSING_PARAMETERS);
            }
        } else {
            Log.w(TAG, "Dynamix not started!");
            SessionManager.sendContextSupportFailure(callback, "Dynamix NOT_READY", ErrorCodes.NOT_READY);
            return new Result("Dynamix NOT_READY", ErrorCodes.NOT_READY);
        }
    }

    /**
     * Returns the DynamixApplication associated with the handler, or null if the handler's facade is not authorized.
     *
     * @return The DynamixApplication associated with the handler, or null if the id is not authorized.
     */
    DynamixApplication getApplication() {
        return handlerApp;
    }

    /**
     * Returns the context request callback for the specified responseId (or null if no mapping is found)
     */
    IContextRequestCallback getContextRequestCallback(UUID responseId) {
        ContextRequestData returnValue = null;
        synchronized (contextRequestCallbackMap) {
            for (ContextRequestData rd : contextRequestCallbackMap.keySet())
                if (rd.requestId.equals(responseId)) {
                    returnValue = rd;
                    break;
                }
        }
        if (returnValue != null)
            return contextRequestCallbackMap.remove(returnValue);
        else
            return null;
    }

    /**
     * Calls looper prepare on the calling thread, if the thread has not had looper prepare called yet.
     */
    synchronized void setupThreadLooper() {
        if (Looper.myLooper() == null)
            Looper.prepare();
    }
}