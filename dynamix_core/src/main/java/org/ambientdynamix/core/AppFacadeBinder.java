/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextPluginInformationResult;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextHandlerCallback;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IPluginInstallCallback;
import org.ambientdynamix.api.application.ISessionCallback;
import org.ambientdynamix.api.application.ISessionListener;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The AppFacadeBinder provides an implementation of the IDynamixFacade API, as defined through AIDL. This class is used
 * in combination with the DynamixService to handle API calls from Dynamix applications.
 *
 * @author Darren Carlson
 * @see IDynamixFacade
 */
public class AppFacadeBinder extends IDynamixFacade.Stub {
    // Private data
    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private boolean embeddedMode;
    protected IDynamixFrameworkListener frameworkListener = new DynamixFrameworkListener() {
        @Override
        public void onDynamixInitialized(DynamixService dynamix) {
            processCachedUserIds();
        }
    };
    protected ContextManager conMgr;
    protected static Map<String, OpenSessionListeners> cachedUserIds = new ConcurrentHashMap<String, OpenSessionListeners>();
    protected String facadeId = UUID.randomUUID().toString();

    /**
     * Creates an AppFacadeBinder.
     */
    protected AppFacadeBinder(Context context, ContextManager conMgr, boolean embeddedMode,
                              boolean addDynamixFrameworkListener) {
        this.context = context;
        this.conMgr = conMgr;
        this.embeddedMode = embeddedMode;
        if (addDynamixFrameworkListener)
            DynamixService.addDynamixFrameworkListener(frameworkListener);
    }

    /**
     * Returns the facadeId for this binder.
     */
    public String getFacadeId() {
        return this.facadeId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeSession() {
        closeSessionWithCallback(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeSessionWithCallback(final ICallback callback) {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app
        final DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null) {
            doCloseSession(SessionManager.makeAppKey(getCallerId(), facadeId), callback);
        } else {
            Log.w(TAG, "closeSessionWithCallback could not find app for " + SessionManager.makeAppKey(getCallerId(), getFacadeId()));
            SessionManager.sendCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createContextHandler(final IContextHandlerCallback callback) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        DynamixApplication app = getAuthorizedApplication(getCallerId());
        if (app != null) {
            if (callback == null)
                throw new RemoteException();
            // Create the handler
            ContextHandlerImpl handler = new ContextHandlerImpl(conMgr, app, context, this);
            /*
             * Add a remote process monitor to clean up state if the calling app dies.
			 */
            handler.asBinder().linkToDeath(new RemoteProcessMonitor(handler.asBinder(), app), 0);
            SessionManager.addContextHandler(SessionManager.makeAppKey(getCallerId(), getFacadeId()), handler, callback);
        } else {
            Log.w(TAG, "createContextHandler failed since the app is not authorized: " + getCallerId());
            SessionManager.sendContextHandlerCallbackFailure(callback, "App is NOT_AUTHORIZED: " + getCallerId(),
                    ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextPluginInformationResult getAllContextPluginInformation() throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app is not authorized
        DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null)
            return new ContextPluginInformationResult(DynamixService.getAllContextPluginInfo());
        else {
            Log.w(TAG, app + " is not authorized!");
            return new ContextPluginInformationResult("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextPluginInformationResult getAllContextPluginInformationForType(String contextType)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app is not authorized
        DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null) {
            if (contextType != null && contextType.length() > 0) {
                List<ContextPluginInformation> plugInfo = new ArrayList<ContextPluginInformation>();
                for (ContextPluginInformation info : DynamixService.getAllContextPluginInfo()) {
                    // Check if the plug-in supports the type
                    if (info.supportsContextType(contextType)) {
                        plugInfo.add(info);
                    }
                }
                return new ContextPluginInformationResult(plugInfo);
            } else
                return new ContextPluginInformationResult("contextType not provided!", ErrorCodes.MISSING_PARAMETERS);
        } else {
            Log.w(TAG, app + " is not authorized!");
            return new ContextPluginInformationResult("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * Returns the DynamixApplication associated with the incoming id, or null if the id is not authorized.
     *
     * @param id The id of the application.
     * @return The DynamixApplication associated with the incoming id, or null if the id is not authorized.
     */
    public DynamixApplication getAuthorizedApplication(String id) {
        if (id != null) {
            // Handle embedded mode (or PLUG_IN apps), if necessary
            if (embeddedMode) {
                Log.w(TAG, "Setting up Admin app for " + id);
                if (!DynamixService.hasDynamixApplication(id)) {
                    DynamixApplication app = createNewApplicationFromCaller(id, true);
                    DynamixService.addDynamixApplicationToSettings(app);
                }
            }
            // Handle auto admin mode for plug-in apps
            else if (id.startsWith(APP_TYPE.PLUG_IN.toString()) && DynamixPreferences.isAutoAdminPluginAppsEnabled()) {
                if (!DynamixService.hasDynamixApplication(id)) {
                    Log.w(TAG, "Giving admin permissions to plug-in app: " + id);
                    DynamixApplication app = createNewApplicationFromCaller(id, true);
                    DynamixService.addDynamixApplicationToSettings(app);
                }
            }
            return DynamixService.getDynamixApplication(id);
        } else {
            Log.w(TAG, "getAuthorizedApplication received null application id");
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextPluginInformationResult getContextPluginInformation(String pluginId) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app
        DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null) {
            for (ContextPluginInformation info : DynamixService.getAllContextPluginInfo()) {
                if (info.getPluginId().equalsIgnoreCase(Utils.trim(pluginId)))
                    return new ContextPluginInformationResult(info);
            }
            return new ContextPluginInformationResult("Plug-in Not Found", ErrorCodes.PLUG_IN_NOT_FOUND);
        } else {
            Log.w(TAG, app + " is not authorized!");
            return new ContextPluginInformationResult("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VersionInfo getDynamixVersion() throws RemoteException {
        return DynamixService.getDynamixFrameworkVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IdResult getHandlerId(IContextHandler handler) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (handler != null) {
            DynamixSession session = SessionManager.getSession(SessionManager.makeAppKey(getCallerId(), facadeId));
            if (session != null) {
                return new IdResult(session.getContextHandlerId(handler));
            } else {
                Log.w(TAG, "could not find open session for: " + handler);
                return new IdResult("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
            }
        } else {
            Log.w(TAG, "Listener was null in getListenerId");
            return new IdResult("Listener was null in getListenerId", ErrorCodes.MISSING_PARAMETERS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextPluginInformationResult getInstalledContextPluginInformation() throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app is unauthorized
        DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null)
            return new ContextPluginInformationResult(DynamixService.getInstalledContextPluginInformationFromDatabase());
        else {
            Log.w(TAG, app + " is not authorized!");
            return new ContextPluginInformationResult("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IdResult getSessionId() throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app
        DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null) {
            // App is authorized
            DynamixSession session = SessionManager.getSession(SessionManager.makeAppKey(getCallerId(), facadeId));
            if (session != null && session.isSessionOpen()) {
                return new IdResult(session.getSessionId().toString());
            } else {
                Log.w(TAG, "could not find open session for: " + app);
                return new IdResult("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
            }
        } else {
            Log.w(TAG, app + " is not authorized!");
            return new IdResult("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDynamixActive() throws RemoteException {
        return DynamixService.isFrameworkStarted();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSessionOpen() throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        return SessionManager.isSessionOpen(getCallerId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result openDefaultContextPluginConfigurationView(String pluginId, String pluginVersion)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (pluginId != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getAuthorizedApplication(getCallerId());
            // Continue if the app is authorized
            if (app != null) {
                return DynamixService.openContextPluginConfigurationForApp(app, pluginId, pluginVersion, null, false);
            } else {
                Log.w(TAG, app + " is not authorized!");
                return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Missing parameters in openDefaultContextPluginConfigurationView");
            return new Result("Missing parameters in openDefaultContextPluginConfigurationView",
                    ErrorCodes.MISSING_PARAMETERS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result openContextPluginConfigurationView(String pluginId, String pluginVersion, Bundle viewConfig)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (pluginId != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getAuthorizedApplication(getCallerId());
            // Continue if the app is authorized
            if (app != null) {
                return DynamixService.openContextPluginConfigurationForApp(app, pluginId, pluginVersion, viewConfig,
                        false);
            } else {
                Log.w(TAG, app + " is not authorized!");
                return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Missing parameters in openContextPluginConfigurationView");
            return new Result("Missing parameters in openContextPluginConfigurationView", ErrorCodes.MISSING_PARAMETERS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void openSession() {
        openSessionWithListenerAndCallback(null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void openSessionWithCallback(ISessionCallback callback) {
        openSessionWithListenerAndCallback(null, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void openSessionWithListener(ISessionListener listener) {
        openSessionWithListenerAndCallback(listener, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void openSessionWithListenerAndCallback(ISessionListener listener, ISessionCallback callback) {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Open the session
        doOpenSession(getCallerId(), listener, callback, false);
    }

    /**
     * Synchronous version of openSessionWithListenerAndCallback.
     */
    public void openSessionWithListenerAndCallback(ISessionListener listener, ISessionCallback callback,
                                                   boolean synchronous) {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Open the session
        doOpenSession(getCallerId(), listener, callback, synchronous);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result removeContextHandler(IContextHandler handler) throws RemoteException {
        return doRemoveContextHandler(handler, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeContextHandlerWithCallback(IContextHandler handler, ICallback callback) throws RemoteException {
        doRemoveContextHandler(handler, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result requestContextPluginInstallation(final ContextPluginInformation plugInfo) throws RemoteException {
        return doContextPluginInstallation(plugInfo, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestContextPluginInstallationWithCallback(final ContextPluginInformation plugInfo,
                                                             final IPluginInstallCallback callback) throws RemoteException {
        doContextPluginInstallation(plugInfo, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result requestContextPluginUninstall(ContextPluginInformation plugInfo) throws RemoteException {
        return doContextPluginUninstall(plugInfo, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestContextPluginUninstallWithCallback(ContextPluginInformation plugInfo, ICallback callback)
            throws RemoteException {
        doContextPluginUninstall(plugInfo, callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSessionListener(ISessionListener newListener) throws RemoteException {
        DynamixSession session = SessionManager.getSession(SessionManager.makeAppKey(getCallerId(), getFacadeId()));
        if (session != null) {
            // Cache the old listener for a bit
            ISessionListener oldListener = session.getSessionListener();
            // Update the session with the newListener
            session.setSessionListener(newListener);
            // Finally, update the SessionManager
            SessionManager.updateSessionListener(oldListener, newListener);
        }
    }

    /**
     * Handles closing a Dynamix session.
     */
    protected void doCloseSession(String appKey, final ICallback callback) {
        DynamixSession session = SessionManager.getSession(appKey);
        Log.d(TAG, "doCloseSession for session " + session + " with appKey " + appKey);
        // Remove context support
        conMgr.removeAllContextSupport(session, callback);
        // Close session (use the SessionManager to make sure the appKey is cleared)
        SessionManager.closeSession(appKey, true, callback);
    }

    /**
     * Utility method for performing context plug-in installations.
     */
    private synchronized Result doContextPluginInstallation(final ContextPluginInformation plugInfo,
                                                            final IPluginInstallCallback listener) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (plugInfo != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getAuthorizedApplication(getCallerId());
            // Continue if the app is authorized
            if (app != null && (app.isAdmin() || DynamixPreferences.isAdminDebugModeEnabled())) {
                List<PendingContextPlugin> pendingPlugs = DynamixService.getPendingContextPlugins();
                for (PendingContextPlugin pending : pendingPlugs) {
                    if (pending.getPendingContextPlugin().getContextPluginInformation().equals(plugInfo)) {
                        DynamixService.installPlugin(pending.getPendingContextPlugin(), listener);
                        return new Result();
                    }
                }
                SessionManager.sendPluginInstallFailed(listener, plugInfo, "Plug-in not found",
                        ErrorCodes.PLUG_IN_NOT_FOUND);
                return new Result("Plug-in not found", ErrorCodes.PLUG_IN_NOT_FOUND);
            } else {
                Log.w(TAG, app + " is not authorized!");
                SessionManager.sendPluginInstallFailed(listener, plugInfo, "Not Authorized", ErrorCodes.NOT_AUTHORIZED);
                return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Null plugInfo in requestContextPluginInstallation");
            SessionManager.sendPluginInstallFailed(listener, plugInfo,
                    "Null plugInfo in requestContextPluginInstallation", ErrorCodes.MISSING_PARAMETERS);
            return new Result("Null plugInfo in requestContextPluginInstallation", ErrorCodes.MISSING_PARAMETERS);
        }
    }

    /*
     * Utility method for performing context plug-in uninstalls.
     */
    private synchronized Result doContextPluginUninstall(ContextPluginInformation plugInfo, ICallback uninstallCallback)
            throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        if (plugInfo != null) {
            // Access the application securely... returns null if the app is not authorized
            DynamixApplication app = getAuthorizedApplication(getCallerId());
            // Continue if the app is authorized
            if (app != null && (app.isAdmin() || DynamixPreferences.isAdminDebugModeEnabled())) {
                List<ContextPlugin> installed = DynamixService.getInstalledContextPluginsFromDatabase();
                for (ContextPlugin plug : installed) {
                    if (plug.getContextPluginInformation().equals(plugInfo)) {
                        DynamixService.uninstallPlugin(plug, null, uninstallCallback);
                        return new Result();
                    }
                }
                SessionManager
                        .sendCallbackFailure(uninstallCallback, "Plug-in not found", ErrorCodes.PLUG_IN_NOT_FOUND);
                return new Result("Plug-in not found", ErrorCodes.PLUG_IN_NOT_FOUND);
            } else {
                Log.w(TAG, app + " is not authorized!");
                SessionManager.sendCallbackFailure(uninstallCallback, "Not Authorized", ErrorCodes.NOT_AUTHORIZED);
                return new Result("Not Authorized", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "Null plugInfo in requestContextPluginUninstall");
            SessionManager.sendCallbackFailure(uninstallCallback, "Null plugInfo in requestContextPluginUninstall",
                    ErrorCodes.MISSING_PARAMETERS);
            return new Result("Null plugInfo in requestContextPluginUninstall", ErrorCodes.MISSING_PARAMETERS);
        }
    }

    private Result doRemoveContextHandler(final IContextHandler handler, ICallback callback) throws RemoteException {
        // Make sure Looper.prepare has been called for the incoming Thread
        setupThreadLooper();
        // Access the application securely... returns null if the app
        final DynamixApplication app = getAuthorizedApplication(getCallerId());
        // Continue if the app is authorized
        if (app != null) {
            if (handler != null) {
                // Remove context support for the handler
                handler.removeAllContextSupportWithCallback(new Callback() {
                    @Override
                    public void onSuccess() throws RemoteException {
                        completeRemove();
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        completeRemove();
                    }

                    private void completeRemove() {
                        SessionManager.removeContextHandler(handler, new Callback() {
                            @Override
                            public void onSuccess() throws RemoteException {
                                Log.i(TAG, "Context Handler Removed: " + handler);
                                app.removeProcessMonitor(handler.asBinder());
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                Log.w(TAG, "Context Handler Removal Fail for: " + handler + ", with message " + message);
                                app.removeProcessMonitor(handler.asBinder());
                            }
                        });
                    }
                });
                return new Result();
            } else {
                SessionManager.sendCallbackFailure(callback, "MISSING_PARAMETERS: handler",
                        ErrorCodes.MISSING_PARAMETERS);
                return new Result("MISSING_PARAMETERS: handler", ErrorCodes.MISSING_PARAMETERS);
            }
        } else {
            SessionManager.sendCallbackFailure(callback, "NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
            return new Result("NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * Adds the user id to the list of cached ids, whose sessions are opened once Dynamix is initialized.
     *
     * @param id The id to cache.
     */
    protected void addCachedUserId(String id, ISessionListener listener, ISessionCallback callback) {
        synchronized (cachedUserIds) {
            if (!cachedUserIds.containsKey(id)) {
                cachedUserIds.put(id, new OpenSessionListeners(listener, callback));
            } else {
                Log.w(TAG, "addCachedUserId is replacing original listener and callback!");
                cachedUserIds.put(id, new OpenSessionListeners(listener, callback));
            }
        }
    }

    /**
     * Creates a new application using the caller's unique UID from Android.
     */
    protected DynamixApplication createNewApplicationFromCaller(String id, boolean admin) {
        // Construct a new application for the caller
        ApplicationInfo info = null;
        PackageManager pm = context.getPackageManager();
        String[] packages = pm.getPackagesForUid(Integer.parseInt(id));
        PackageInfo pkgInfo = null;
        try {
            info = pm.getApplicationInfo(packages[0], PackageManager.GET_UNINSTALLED_PACKAGES);
            // We need to include the GET_PERMISSIONS flag to introspect app permissions
            pkgInfo = pm.getPackageInfo(packages[0], PackageManager.GET_PERMISSIONS);
            DynamixApplication app = new DynamixApplication(pm, pkgInfo, info);
            app.setAdmin(admin);
            return app;
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Count not get information for calling UID: " + e.getMessage());
        }
        return null;
    }

    /**
     * Utility method that attempts to open a Dynamix session for the incoming userId.
     */
    protected synchronized void doOpenSession(String callerId, ISessionListener listener, ISessionCallback callback,
                                              boolean synchronous) {
        Log.d(TAG, "doOpenSession for caller " + callerId + " with facade " + getFacadeId());
        if (DynamixService.isFrameworkInitialized()) {
            DynamixApplication app = null;
            if (callerId == "-1") {
                Log.w(TAG, "Invalid caller id " + callerId);
                return;
            }
            // Access the application
            app = DynamixService.getDynamixApplication(callerId);
            // If the app is not null, it's authorized
            if (app != null) {
                try {
                    if (callback != null)
                        callback.asBinder().linkToDeath(new RemoteProcessMonitor(callback.asBinder(), app), 0);
                } catch (RemoteException e) {
                    Log.w(TAG, "Attaching Remote Processing Monitoring Failed: " + e);
                }
                // Open the session for the app
                SessionManager.openSession(app, SessionManager.makeAppKey(callerId, facadeId), listener);
                // Ping the app and notify it that Dynamix is active
                app.pingConnected();
                // Notify framework listeners
                DynamixService.onDynamixApplicationConnected(app);
                // Send notification
                SessionManager.sendSessionCallbackSuccess(callback, this, synchronous);
            } else {
                // The application is new... so set it up!
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "Application ID " + callerId + " is new!");
                DynamixApplication newApp = createNewApplicationFromCaller(callerId, false);
                if (newApp != null) {
                    // Add a new app to the SettingsManager
                    if (DynamixService.addDynamixApplicationToSettings(newApp)) {
                        // Open the session for the app
                        SessionManager.openSession(newApp, SessionManager.makeAppKey(callerId, facadeId), listener);
                        // Ping the app and update notifications
                        newApp.pingConnected();
                        // Notify framework listeners
                        DynamixService.onDynamixApplicationConnected(newApp);
                        // Send notification
                        SessionManager.sendSessionCallbackSuccess(callback, this, synchronous);
                    } else {
                        Log.e(TAG, "Could not add app");
                        SessionManager.sendSessionCallbackFailure(callback, "Could not add app for " + callerId,
                                ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                    }
                } else {
                    Log.e(TAG, "App was null after doOpenSession");
                    SessionManager.sendSessionCallbackFailure(callback, "Could not create app for " + callerId,
                            ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                }
            }
        } else {
            Log.w(TAG, "DynamixService not initialized during openSession... caching request for: " + callerId);
            addCachedUserId(callerId, listener, callback);
        }
    }

    /**
     * Returns a properly formatted application id for the AIDL-based app.
     */
    protected String getCallerId() {
        String appId = "-1";
        if (embeddedMode)
            appId = Integer.toString(android.os.Process.myUid());
        else {
            if (Binder.getCallingUid() == android.os.Process.myUid()) {
                Log.w(TAG, "Caller was Dynamix when not running in embedded mode... invalid");
                appId = "-1";
            } else
                appId = Integer.toString(Binder.getCallingUid());
        }
        Log.d(TAG, "Returning caller ID: " + appId);
        return appId;
    }

    /**
     * Sets the ContextManager
     */
    protected ContextManager getConMgr() {
        return conMgr;
    }

    /**
     * Calls doOpenSession for any apps that called openSession when Dynamix was not yet booted
     */
    protected void processCachedUserIds() {
        synchronized (cachedUserIds) {
            for (String userId : cachedUserIds.keySet()) {
                Log.d(TAG, "Processing openSession for cached ID: " + userId);
                doOpenSession(userId, cachedUserIds.get(userId).listener, cachedUserIds.get(userId).callback, false);
            }
            cachedUserIds.clear();
        }
    }

    /**
     * Calls looper prepare on the calling thread, if the thread has not had looper prepare called yet.
     */
    protected synchronized void setupThreadLooper() {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
    }

    /*
     * Support struct-like class for caching apps wishing to open sessions while Dynamix is not ready.
     */
    private class OpenSessionListeners {
        protected ISessionListener listener;
        protected ISessionCallback callback;

        public OpenSessionListeners(ISessionListener listener, ISessionCallback callback) {
            this.listener = listener;
            this.callback = callback;
        }
    }

    /*
     * Utility class that monitors a remote process (via a callback (Binder), keeps the binder from garbage collection
     * by checking periodically if it's alive, and closes the associated session if the Binder dies.
     */
    public class RemoteProcessMonitor implements DeathRecipient {
        private DynamixApplication app;
        private IBinder binder;

        // private IBinder binder;
        public RemoteProcessMonitor(final IBinder binder, final DynamixApplication app) {
            this.binder = binder;
            this.app = app;
            Log.d(TAG, "Attaching RemoteProcessMonitor for: " + app);
            app.addProcessMonitor(this);
            Utils.dispatch(true, new Runnable() {
                @Override
                public void run() {
                    try {
                        while (binder.isBinderAlive() && SessionManager.isSessionOpen(app.getAppID())) {
                            Thread.sleep(1000);
                        }
                    } catch (Exception e) {
                    }
                }
            });
        }

        public IBinder getBinder() {
            return this.binder;
        }

        @Override
        public void binderDied() {
            Log.e(TAG,
                    "Remote client died: " + app + " with session open " + SessionManager.isSessionOpen(app.getAppID()));
            if (SessionManager.isSessionOpen(app.getAppID()))
                doCloseSession(SessionManager.makeAppKey(getCallerId(), facadeId), null);
        }
    }
}