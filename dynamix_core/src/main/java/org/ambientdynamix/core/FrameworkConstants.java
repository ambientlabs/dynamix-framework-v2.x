/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.PowerScheme;

/**
 * Various framework constants.
 * 
 * @author Darren Carlson
 */
public class FrameworkConstants {
	public final static VersionInfo DYNAMIX_VERSION = new VersionInfo(2, 1, 27);
	// public final static boolean DEBUG = true;
	public final static PowerScheme DEFAULT_POWER_SCHEME = PowerScheme.BALANCED;
	public final static boolean PRODUCTION_RELEASE = false;
	public final static boolean COLLECT_PLUGIN_STATS = false;
	public final static boolean DISABLE_CONTEXT_RESULT_CACHING = true;
	public final static int CONTEXT_REQUEST_TIMEOUT = 30000;
	public final static String ACTION_FIREWALL = "org.ambientdynamix.ACTION_FIREWALL";
	public final static String FIREWALL_INTERACTION = "org.ambientdynamix.FIREWALL_INTERACTION";
	public final static String APPLICATION_KEY = "application_key";

	/*
	 * Start state constants
	 */
	public static enum StartState {
		STOPPED, STARTING, STARTED, STOPPING, PAUSING, PAUSED
	}

	/*
	 * System privilege constants
	 */
	public static enum SystemPrivilege {
		NORMAL, ADMIN, FRAMEWORK
	}

	// Singleton constructor
	private FrameworkConstants() {
	}
}
