/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.db4o.foundation.NotImplementedException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.ambientdynamix.api.application.AppConstants;
import org.ambientdynamix.api.application.AppConstants.ContextPluginType;
import org.ambientdynamix.api.application.AppConstants.PluginInstallStatus;
import org.ambientdynamix.api.application.BundleContextInfo;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.DynamixFacade;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextRequestCallback;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.application.VersionedPlugin;
import org.ambientdynamix.api.contextplugin.ContextInfoSet;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.DynamixHelper;
import org.ambientdynamix.api.contextplugin.IContextPluginRuntimeFactory;
import org.ambientdynamix.api.contextplugin.IDynamixEventListener;
import org.ambientdynamix.api.contextplugin.IMessageResultHandler;
import org.ambientdynamix.api.contextplugin.IPluginContextListener;
import org.ambientdynamix.api.contextplugin.IPluginFacade;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.Message;
import org.ambientdynamix.api.contextplugin.NfcListener;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.PluginConstants.EventType;
import org.ambientdynamix.api.contextplugin.PluginState;
import org.ambientdynamix.api.contextplugin.PluginStateListener;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.api.contextplugin.security.Permissions;
import org.ambientdynamix.api.contextplugin.security.PermissionsHandler;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;
import org.ambientdynamix.core.FrameworkConstants.StartState;
import org.ambientdynamix.core.FrameworkConstants.SystemPrivilege;
import org.ambientdynamix.data.ContextEventCache;
import org.ambientdynamix.data.ContextResultCacheEntry;
import org.ambientdynamix.event.SimpleEventHandler;
import org.ambientdynamix.event.SourcedContextInfoSet;
import org.ambientdynamix.event.StreamController;
import org.ambientdynamix.remote.RemotePairing;
import org.ambientdynamix.security.SecuredContext;
import org.ambientdynamix.ui.activities.HomeActivity;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.update.contextplugin.PluginInstallCallback;
import org.ambientdynamix.util.Channel;
import org.ambientdynamix.util.PluginLooperThread;
import org.ambientdynamix.web.WebConnector;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The ContextManager orchestrates the runtime behavior of a set of dynamically installed context plug-ins and their
 * associated runtimes. This class implements IPluginContextListener in order to receive events from dependent plug-ins.
 * It also implements IPluginFacade, so that it can securely provision Android services to plug-ins.
 *
 * @author Darren Carlson
 * @see IPluginContextListener
 * @see IPluginFacade
 */
public class ContextManager implements IPluginContextListener, IPluginFacade {
    // Private data
    private final static String TAG = ContextManager.class.getSimpleName();
    private Map<ContextPlugin, ContextPluginRuntimeWrapper> pluginMap = new ConcurrentHashMap<ContextPlugin, ContextPluginRuntimeWrapper>();
    private Map<ContextPlugin, SecuredContextSettings> securedContextMap = new ConcurrentHashMap<ContextPlugin, SecuredContextSettings>();
    private Map<ContextPlugin, PluginStats> statMap = new ConcurrentHashMap<ContextPlugin, PluginStats>();
    private Map<ContextPlugin, List<NfcListener>> nfcListeners = new ConcurrentHashMap<ContextPlugin, List<NfcListener>>();
    private Map<ContextPlugin, Map<String, IDynamixEventListener>> dynamixEventListeners = new ConcurrentHashMap<>();
    private Map<String, IAndroidEventProvider> androidEventProviders = new HashMap<>();
    private static Map<ContextPlugin, PluginLooperThread> threadMap = new ConcurrentHashMap<ContextPlugin, PluginLooperThread>();
    private Map<UUID, Channel> channelMap = new ConcurrentHashMap<UUID, Channel>();
    private Map<ContextPlugin, Activity> configActivityMap = new ConcurrentHashMap<ContextPlugin, Activity>();
    private Map<ContextPlugin, Activity> acquisitionActivityMap = new ConcurrentHashMap<ContextPlugin, Activity>();
    private Map<ContextPlugin, PlugStopper> stopperMap = new ConcurrentHashMap<ContextPlugin, ContextManager.PlugStopper>();
    private Map<String, ContextPlugin> sessionIdToPluginMap = new ConcurrentHashMap<String, ContextPlugin>();
    private Map<ContextPlugin, PendingStopActions> pendingPluginStopMap = new ConcurrentHashMap<ContextPlugin, PendingStopActions>();
    private Map<ContextPlugin, PluginFacadeBinder> facadeBinderMap = new ConcurrentHashMap<ContextPlugin, PluginFacadeBinder>();
    private static PowerScheme scheme = PowerScheme.BALANCED;
    private Context context;
    private ContextEventCache contextCache;
    private volatile StartState managerStartState = StartState.STOPPED;
    private Handler uiHandler = new Handler();
    private int progressCount = 0;
    private Timer progressMonitorTimer = null;
    private Object cachedStartRequestLock = new Object();
    private boolean cachedStartRequest;
    private ObjectMapper mapper = new ObjectMapper();

    static {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
    }

    // Simple struct class for pending stop plug-in actions
    private class PendingStopActions {
        boolean destroy;
        boolean uninstall;
        List<ICallback> uninstallCallbacks = new ArrayList<ICallback>();
        List<ICallback> stopCallbacks = new ArrayList<ICallback>();
    }

    // Simple struct class for holding a SecuredContext and its associated
    // PermissionsHandler
    private class SecuredContextSettings {
        Context securedContext;
        PermissionsHandler permHandler;

        public SecuredContextSettings(Context securedContext, PermissionsHandler permHandler) {
            this.securedContext = securedContext;
            this.permHandler = permHandler;
        }
    }

    /**
     * Creates a new ContextManager.
     *
     * @param context          The Android context.
     * @param scheme           The initial PowerScheme.
     * @param maxCacheCapacity The maximum capacity of the ContextDataCache.
     * @param maxCacheMills    How long to cache context events (in milliseconds)
     * @param cullInterval     How often to cull the context cache (in milliseconds)
     */
    protected ContextManager(Context context, PowerScheme scheme, int maxCacheCapacity, int maxCacheMills,
                             int cullInterval) {
        this.context = context;
        ContextManager.scheme = scheme;
        contextCache = new ContextEventCache(maxCacheMills, cullInterval, maxCacheCapacity);
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager cm =
                        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if (isConnected) {
                    boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                    if (isWiFi) {
                        Utils.updateInstanceIp();
                    }
                }
            }
        }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * @return An appropriate Thread priority for the current PowerScheme
     */
    protected static int getThreadPriorityForPowerScheme() {
        if (scheme == PowerScheme.HIGH_PERFORMANCE)
            return Thread.MAX_PRIORITY;
        if (scheme == PowerScheme.POWER_SAVER)
            return Thread.MIN_PRIORITY;
        return Thread.NORM_PRIORITY;
    }

    /**
     * Clears the specified ContextPlugin's statistics
     *
     * @param plug The Plugin to clear.
     * @return True if the stats were cleared; false otherwise.
     */
    public boolean clearPluginStats(ContextPlugin plug) {
        if (plug != null)
            synchronized (statMap) {
                PluginStats stats = statMap.get(plug);
                if (stats != null) {
                    Log.d(TAG, "Clearing statistics for: " + plug);
                    stats.clear();
                    return true;
                }
                return false;
            }
        else
            return false;
    }

    /**
     * Runs the specified DynamixFeature using reflection to extract and invoke the specified runtime method. Features
     * are dispatched on a separate thread.
     */
    public void runDynamixFeature(final DynamixFeature feature) {
        Utils.dispatch(true, new Runnable() {
            @Override
            public void run() {
                // Grab the runtime using the feature's plug'in id
                ContextPluginRuntimeWrapper runtime = getContextPluginRuntime(feature.getPlugId(),
                        feature.getPluginVersion());
                if (runtime != null) {
                    try {
                        Log.d(TAG, "runDynamixFeature is running feature: " + feature + " on thread " + Thread.currentThread());
                        Method method = runtime.getContextPluginRuntime().getClass()
                                .getMethod(feature.getRuntimeMethod());
                        method.invoke(runtime.getContextPluginRuntime());
                    } catch (Exception e) {
                        Log.w(TAG, "runDynamixFeature exception: " + e);
                    }
                }
            }
        });
    }

    /**
     * Programmatically closes a previously launched configuration activity.
     *
     * @param sessionId The sessionId of the ContextPluginRuntime wishing to close its configuration Activity.
     */
    @Override
    public void closeConfigurationView(UUID sessionId) {
        Log.v(TAG, "closeConfigurationView for " + sessionId);
        ContextPluginInformation plug = getContextPluginInfo(sessionId.toString());
        if (plug != null) {
            if (configActivityMap.containsKey(plug)) {
                Activity act = configActivityMap.remove(plug);
                Log.d(TAG, "Closing Activity: " + act);
                Intent i = new Intent();
                i.putExtra("plug", (Parcelable) plug);
                if (act.getParent() != null) {
                    act.getParent().setResult(Activity.RESULT_OK, i);
                } else
                    act.setResult(Activity.RESULT_OK, i);
                act.finish();
            } else {
                Log.w(TAG, "closeConfigurationView could not find an activity for plugin: " + plug);
            }
        } else
            Log.w(TAG, "closeConfigurationView could not find a plugin for UUID: " + sessionId);
    }

    /**
     * Programmatically closes a previously launched context acquisition activity.
     *
     * @param sessionId The sessionId of the ContextPluginRuntime wishing to close its context acquisition Activity.
     */
    @Override
    public void closeContextAcquisitionView(UUID sessionId) {
        Log.v(TAG, "closeContextAcquisitionView for " + sessionId);
        ContextPluginInformation plug = getContextPluginInfoForSessionId(sessionId.toString());
        if (plug != null) {
            synchronized (acquisitionActivityMap) {
                if (acquisitionActivityMap.containsKey(plug)) {
                    Activity act = acquisitionActivityMap.remove(plug);
                    Log.d(TAG, "Closing Activity: " + act);
                    act.finish();
                } else {
                    Log.w(TAG, "closeContextAcquisitionView could not find an activity for plugin: " + plug);
                }
            }
        } else
            Log.w(TAG, "closeContextAcquisitionView could not find a plugin for UUID: " + sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextPluginSettings getContextPluginSettings(UUID sessionId) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            return DynamixService.getContextPluginSettings(plug);
        } else
            Log.w(TAG, "getContextPluginSettings blocked for invalid sessionId: " + sessionId);
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDynamixFacade getDynamixFacade(UUID sessionID) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // TODO Check if plug-in is allowed to add context support?
        if (plug != null) {
            Log.d(TAG, "getDynamixFacade for " + plug);
            synchronized (facadeBinderMap) {
                if (facadeBinderMap.containsKey(plug)) {
                    PluginFacadeBinder b = facadeBinderMap.get(plug);
                    Log.d(TAG, "getDynamixFacade found " + b + " was already created for for " + plug);
                    return b;
                } else {
                    PluginFacadeBinder b = new PluginFacadeBinder(plug, context, this, DynamixService.isEmbedded());
                    facadeBinderMap.put(plug, b);
                    Log.d(TAG, "getDynamixFacade created InternalFacadeBinder for " + plug);
                    return b;
                }
            }
        } else
            Log.w(TAG, "getDynamixFacade blocked for invalid sessionId: " + sessionID);
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DynamixFacade getDynamixFacadeWrapper(UUID sessionID) {
        IDynamixFacade facade = getDynamixFacade(sessionID);
        if (facade != null)
            return new DynamixFacade(facade);
        else
            return null;
    }

    /**
     * Returns the PluginStats for the specified ContextPlugin, or null if the plug-in was not found.
     */
    public PluginStats getPluginStats(ContextPlugin plug) {
        return statMap.get(plug);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Context getSecuredContext(UUID sessionId) {
        /*
         * Important: we have to maintain control over the secured context and NOT give a new one each time a plug-in
		 * asks for it because they could possibly cache it and achieve permissions that changed over time.
		 */
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            PluginLooperThread plt = threadMap.get(plug);
            if (plt != null) {
                synchronized (securedContextMap) {
                    // Check if we've already created a SecuredContext for this
                    // plug
                    SecuredContextSettings sc = securedContextMap.get(plug);
                    if (sc != null) {
                        return sc.securedContext;
                    } else {
                        ClassLoader cl = DynamixService.getContextPluginClassLoader(plug);
                        PermissionsHandler ph = new PermissionsHandler(DynamixService.getConfig()
                                .isPluginPermissionCheckingEnabled());
                        ph.updatePermissions(plug.getPermissions());
                        if (cl == null) {
                            Log.w(TAG, "Could not find ClassLoader for: " + plug);
                            return null;
                        }
                        if (ph.checkPermission(Permissions.ACCESS_FULL_CONTEXT, false)) {
                            Log.w(TAG, "!!! WARNING: Providing full Android Context !!! to " + plug);
                            sc = new SecuredContextSettings(DynamixService.getAndroidContext(), ph);
                        } else {
                            Log.d(TAG, "Providing SecuredContext to " + plug);
                            File privateFiles = null;
                            try {
                                // Create private storage path for plug-in
                                ContextWrapper cWrap = new ContextWrapper(DynamixService.getAndroidContext());
                                String privateFilesPath = Utils.getPluginPrivateDataDir(plug).getAbsolutePath();
                                Log.d(TAG, plug.getId() + " private files path: " + privateFilesPath);
                            } catch (Exception e) {
                                Log.w(TAG, "Error creating private file path for: " + plug);
                            }
                            sc = new SecuredContextSettings(new SecuredContext(context, uiHandler, plt.getLooper(),
                                    DynamixService.getContextPluginClassLoader(plug), ph, privateFiles), ph);
                        }
                        securedContextMap.put(plug, sc);
                        return sc.securedContext;
                    }
                }
            } else
                Log.w(TAG, "Could not find PluginLooperThread for: " + plug);
        } else
            Log.w(TAG, "getSecuredContext blocked for invalid sessionId: " + sessionId);
        Log.w(TAG, "No SecuredContext could be created for: " + plug);
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PluginState getState(UUID sessionId) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            ContextPluginRuntimeWrapper wrapper = pluginMap.get(plug);
            if (wrapper != null)
                return wrapper.getState();
            else
                Log.w(TAG, "getState encountered null ContextPluginRuntimeWrapper for: " + plug);
        } else
            Log.w(TAG, "getState blocked for invalid sessionId: " + sessionId);
        return null;
    }

    /**
     * Returns true if the ContextManager is started; false otherwise.
     */
    public boolean isStarted() {
        updateManagerState();
        return managerStartState == StartState.STARTED;
    }

    /**
     * Returns true if the ContextManager is stopped; false otherwise.
     */
    public boolean isStopped() {
        updateManagerState();
        return managerStartState == StartState.STOPPED;
    }

    /**
     * Returns true if the ContextManager is paused; false otherwise.
     */
    public boolean isPaused() {
        updateManagerState();
        return managerStartState == StartState.PAUSED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContextListenerInformation> getContextListeners(UUID sessionID) {
        List<ContextListenerInformation> info = new ArrayList<ContextListenerInformation>();
        // Get the ContextPlugin using the secure sessionId
        ContextPluginInformation plug = getContextPluginInfoForSessionId(sessionID.toString());
        // If we get a plug, we have a valid sessionId, so continue...
        if (plug != null) {
            // Loop through the context support for the plug-in
            for (ContextSupport sup : SessionManager.getContextSupport(plug)) {
                if (sup.hasContextListener()) {
                    info.add(new ContextListenerInformation(sup.getSupportId(), sup.getContextType(), sup
                            .getSupportConfig()));
                }
            }
        } else
            Log.w(TAG, "Could not find ContextPlugin for sessionId: " + sessionID);
        return info;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContextListenerInformation> getContextListeners(UUID sessionID, String contextType) {
        List<ContextListenerInformation> info = new ArrayList<ContextListenerInformation>();
        // Get the ContextPlugin using the secure sessionId
        ContextPluginInformation plug = getContextPluginInfoForSessionId(sessionID.toString());
        // If we get a plug, we have a valid sessionId, so continue...
        if (plug != null) {
            // Loop through the context support for the plug-in
            for (ContextSupport sup : SessionManager.getContextSupport(plug)) {
                // If a support has listeners, it's a context subscription
                if (sup.getContextType().equalsIgnoreCase(contextType) && sup.hasContextListener()) {
                    info.add(new ContextListenerInformation(sup.getSupportId(), sup.getContextType(), sup
                            .getSupportConfig()));
                }
            }
        } else
            Log.w(TAG, "Could not find ContextPlugin for sessionId: " + sessionID);
        return info;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onContextRequestSuccess(UUID sessionId, UUID responseId) {
        onContextRequestSuccess(sessionId, responseId, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onContextRequestSuccess(UUID sessionId, UUID responseId, Intent intent) {
        // Get the ContextPlugin using the secure sessionId
        ContextPluginInformation plug = getContextPluginInfoForSessionId(sessionId.toString());
        // If we get a plug, we have a valid sessionId, so continue...
        if (plug != null) {
            // Handle intent, if present
            if (intent != null)
                sendDynamixEvent(sessionId, intent);
            // Find the associated channel, removing it from the map
            Channel channel = channelMap.remove(responseId);
            if (channel != null) {
                // Handle callback
                if (channel.getContextHandler() instanceof ContextHandlerImpl) {
                    ContextHandlerImpl handler = (ContextHandlerImpl) channel.getContextHandler();
                    IContextRequestCallback callback = handler.getContextRequestCallback(responseId);
                    if (callback != null) {
                        Bundle b = new Bundle();
                        b.putBoolean("success", true);
                        BundleContextInfo resultData = new BundleContextInfo(b, channel.getContextType());
                        ContextResult result = new ContextResult(resultData);
                        result.setResultSource(plug);
                        result.setResponseId(responseId.toString());
                        SessionManager.sendContextRequestCallbackSuccess(callback, result);
                    }
                }
            } else
                Log.w(TAG, "Could not find channel for responseId " + responseId);
        } else
            Log.w(TAG, "Could not find ContextPlugin for sessionId: " + sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onContextRequestFailed(UUID sessionId, UUID responseId, String errorMessage, int errorCode) {
        // Get the ContextPlugin using the secure sessionId
        ContextPluginInformation plug = getContextPluginInfoForSessionId(sessionId.toString());
        // If we get a plug, we have a valid sessionId, so continue...
        if (plug != null) {
            // Update stats
            updateStats(plug, errorMessage);
            // Find the associated channel, removing it from the map
            Channel channel = channelMap.remove(responseId);
            if (channel != null) {
                // Handle callback
                if (channel.getContextHandler() instanceof ContextHandlerImpl) {
                    ContextHandlerImpl handler = (ContextHandlerImpl) channel.getContextHandler();
                    IContextRequestCallback callback = handler.getContextRequestCallback(responseId);
                    if (callback != null) {
                        // TODO: Provide responseId like in
                        // 'onContextRequestSuccess'?
                        SessionManager.sendContextRequestCallbackFailure(callback, errorMessage, errorCode);
                    }
                }
            } else
                Log.w(TAG, "Could not find channel for responseId " + responseId);
        } else
            Log.w(TAG, "Could not find ContextPlugin for sessionId: " + sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPluginContextEvent(final UUID sessionId, final ContextInfoSet infoSet) {
        // Grab the plugin
        final ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, it had a valid sessionId, so continue...
        if (plug != null) {
            if (infoSet != null) {
                // Run event handling on a thread so it doesn't block Dynamix
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // First, verify that the context type is actually
                        // supported by the plugin
                        if (!plug.supportsContextType(infoSet.getContextType()))
                            Log.e(TAG, "Context type: " + infoSet.getContextType()
                                    + " is not listed as supported by plugin: " + plug);
                        /**
                         * Handle any intent associated with this event
                         */
                        if (infoSet.hasIntent()) {
                            sendDynamixEvent(sessionId, infoSet.getIntent());
                        }

                        /*
                         * Create a SourcedContextInfoSet, which associates the infoSet with the plug-in that generated
						 * it.
						 */
                        SourcedContextInfoSet sourcedSet = null;
                        try {
                            sourcedSet = new SourcedContextInfoSet(infoSet, plug.getContextPluginInformation(), false);
                        } catch (Exception e1) {
                            Log.e(TAG, "Error creating SourcedContextInfoSet (out of memory?) for: " + plug + ": " + e1);
                            return;
                        }
                        /*
                         * Create a map of listeners and associated results to send. Note that this map is only used if
						 * filled with events, which may not occur
						 */
                        Map<IContextListener, List<ContextResult>> eventMap = new HashMap<IContextListener, List<ContextResult>>();
                        // Make sure that we have event data to process
                        if (sourcedSet != null && sourcedSet.getContextInfoSet() != null) {
                            // Update the plug-ins stats
                            updateStats(plug, sourcedSet);
                            // Cache the SourcedContextInfoSet
                            contextCache.cacheResult(plug, sourcedSet);
                            List<ContextSupport> contextSupport = new ArrayList<ContextSupport>();
                            boolean useEventMap = false;
                            /*
                             * Handle event data of type BROADCAST. In this case, we send the event to ALL context
							 * support subscriptions of the infoSet's context type, regardless of session.
							 */
                            if (infoSet.getEventType() == EventType.BROADCAST) {
                                useEventMap = true;
                                // Get the context support associated with the
                                // infoSet's context type for all sessions.
                                for (DynamixSession session : SessionManager.getAllSessions()) {
                                    contextSupport.addAll(session.getActiveContextSupport(infoSet.getContextType()));
                                }
                            } else if (infoSet.getEventType() == EventType.UNICAST) {
                                /*
                                 * Unicast events are returned to a specific receiver (in the case of a CONTEXT_REQUEST
								 * channel) or potentially several receivers (in the case of a CONTEXT_SUBSCRIPTION
								 * channel).
								 */
                                Channel channel = channelMap.get(infoSet.getResponseId());
                                if (channel != null) {
                                    // Grab the associated Dynamix session
                                    DynamixSession session = SessionManager.getSession(channel.getContextHandler());
                                    if (session != null) {
                                        // Handle CONTEXT_SUBSCRIPTION channel
                                        if (channel.getType() == Channel.Type.CONTEXT_SUBSCRIPTION) {
                                            useEventMap = true;
                                            /*
                                             * Outdated (2014.09.29): We are handling a CONTEXT_SUBSCRIPTION channel, so
											 * we need to grab all the listeners attached to the plug-in (and context
											 * type) for the channel's handler. We pack up all the all the resulting
											 * context support, which the 'GENERAL EVENT PROCESSING' section below will
											 * filter and send events based on the correct context type. Note that there
											 * may be several support registrations if multiple plug-ins were setup to
											 * handle the context type (e.g., for meta context types).
											 */
                                            // contextSupport =
                                            // session.getAllContextSupport(channel.getContextHandler(),
                                            // plug, infoSet.getContextType());
                                            /*
                                             * New approach (2014.09.29): We are handling a CONTEXT_SUBSCRIPTION
											 * channel, so we grab the context support attached to the responseID. We
											 * pack up the resulting context support, which the 'GENERAL EVENT
											 * PROCESSING' section below will filter and send events based on the
											 * correct context type. Note that there may be several support
											 * registrations if multiple plug-ins were setup to handle the context type
											 * (e.g., for meta context types).
											 */
                                            ContextSupport sup = session.getContextSupportFromId(
                                                    channel.getContextHandler(), infoSet.getResponseId());
                                            if (sup != null)
                                                contextSupport.add(sup);
                                        }
                                        // Handle CONTEXT_REQUEST channel
                                        else if (channel.getType() == Channel.Type.CONTEXT_REQUEST) {
                                            // Remove channel
                                            channelMap.remove(infoSet.getResponseId());
                                            // Cast to ContextHandlerImpl
                                            if (channel.getContextHandler() instanceof ContextHandlerImpl) {
                                                ContextHandlerImpl handler = (ContextHandlerImpl) channel
                                                        .getContextHandler();
                                                // Try to find a callback for
                                                // the result
                                                IContextRequestCallback callback = handler
                                                        .getContextRequestCallback(infoSet.getResponseId());
                                                if (callback != null) {
                                                    // Create result and send it
                                                    // via the callback
                                                    ContextResult result = createContextEventForApplication(
                                                            channel.getApp(), sourcedSet, infoSet.getResponseId());
                                                    if (result != null)
                                                        SessionManager.sendContextRequestCallbackSuccess(callback,
                                                                result);
                                                    else if (DynamixPreferences.isDetailedLoggingEnabled())
                                                        Log.w(TAG,
                                                                "Could not create context event for receiver (check app permission): "
                                                                        + channel.getApp());
                                                } else
                                                    Log.w(TAG, "No callback registered for context event!");
                                            } else {
                                                Log.w(TAG, "IContext Handler was not of type ContextHandlerImpl!");
                                            }
                                        } else {
                                            Log.w(TAG, "Handling unsupported channel type: " + channel.getType());
                                        }
                                    } else
                                        Log.w(TAG, "Could not find session for: " + channel.getApp());
                                } else
                                    Log.w(TAG, "Could not find recipients for responseId: " + infoSet.getResponseId());
                            } else
                                throw new RuntimeException("Unsupported event type: " + infoSet.getEventType());
                            /*
                             * GENERAL EVENT PROCESSING: Used for BOTH BROADCAST events and CONTEXT_SUBSCRIPTION events.
							 * Note that CONTEXT_REQUEST data is returned using a callback.
							 */
                            if (useEventMap) {
                                if (contextSupport != null && contextSupport.size() > 0) {
                                    /*
                                     * For each ContextSupport, get the highest fidelity event suitable for the context
									 * support's DynamixApplication owner, adding it to the eventMap.
									 */
                                    for (ContextSupport sup : contextSupport) {
                                        /*
                                         * Extract the highest fidelity context data available for the support's owner
										 */
                                        ContextResult event = null;
                                        /*
                                         * Make sure the originating plug-in doesn't send the event to itself. This can
										 * occur if the plug-in is acting as an app and does a broadcast.
										 */
                                        if (sup.getDynamixApplication().getAppType() == APP_TYPE.PLUG_IN) {
                                            if (sup.getDynamixApplication().getAppID()
                                                    .equalsIgnoreCase(Utils.makeAppIdForPlugin(plug))) {
                                                break;
                                            }
                                        }
                                        try {
                                            /*
                                             * Create the event for the app. Note: We use the context support's id as
											 * the responsId for BROADCAST events and CONTEXT_SUBSCRIPTION channels.
											 */
                                            event = createContextEventForApplication(sup.getDynamixApplication(),
                                                    sourcedSet, sup.getSupportId());
                                        } catch (Exception e) {
                                            Log.e(TAG, "Exception when creating ContextEvent: " + e);
                                        }
                                        /*
                                         * A null event here means that the app didn't have the security rights to
										 * receive the event (as determined by the call to
										 * 'createContextEventForApplication' above).
										 */
                                        if (event != null) {
                                            /*
                                             * App is allowed to receive the event, so update its list of events, if the
											 * support registration has listeners.
											 */
                                            if (sup.hasContextListener()) {
                                                boolean found = false;
                                                for (IContextListener l : eventMap.keySet()) {
                                                    if (l.asBinder().equals(sup.getContextListener().asBinder())) {
                                                        found = true;
                                                        break;
                                                    }
                                                }
                                                if (found) {
                                                    /*
                                                     * Listener already exists in the eventMap... so just extract its
													 * event list.
													 */
                                                    List<ContextResult> eventList = eventMap.get(sup
                                                            .getContextListener());
                                                    // Create the List of
                                                    // ContextEvents, if
                                                    // necessary
                                                    if (eventList == null)
                                                        eventList = new ArrayList<ContextResult>();
                                                    // Add the ContextEvent to
                                                    // the application's list of
                                                    // events
                                                    eventList.add(event);
                                                } else {
                                                    /*
                                                     * Listener does not yet exist in the eventMap... so add it
													 */
                                                    List<ContextResult> eventList = new ArrayList<ContextResult>();
                                                    eventList.add(event);
                                                    eventMap.put(sup.getContextListener(), eventList);
                                                }
                                            } else if (DynamixPreferences.isDetailedLoggingEnabled())
                                                Log.d(TAG, "Context Support has no listeners: " + sup);
                                        } else if (DynamixPreferences.isDetailedLoggingEnabled())
                                            Log.w(TAG,
                                                    "Could not create context event for reciever (check app permission): "
                                                            + sup.getDynamixApplication());
                                    }
                                } else {
                                    /*
                                     * In this case, an app maybe registered for a particular context type with a
									 * plug-in, but the plug-in may generate other types too (e.g., background battery
									 * level scanning). This is not a warning in most cases (except for debugging, in
									 * which case enable detailed logging).
									 */
                                    if (DynamixPreferences.isDetailedLoggingEnabled())
                                        Log.w(TAG,
                                                "Context result cannot be sent, since there are is no context support registration for "
                                                        + infoSet.getContextType());
                                }
                                /*
                                 * Finally, notify the context listeners of the new events
								 */
                                if (DynamixPreferences.isDetailedLoggingEnabled())
                                    Log.v(TAG, "onPluginContextEvent generated an eventMap of size: " + eventMap.size());
                                if (eventMap.size() > 0) {
                                    SessionManager.notifyContextListeners(eventMap);
                                }
                            }
                        } else
                            Log.w(TAG, "eventData was NULL... this should not happen!");
                    }
                });
                t.setDaemon(true);
                t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread thread, Throwable ex) {
                        Log.e(TAG, "onPluginContextEvent uncaughtException: " + ex.getMessage());
                        ex.printStackTrace();
                        disablePluginOnError(plug, plug + " caused an error and was disabled",
                                ErrorCodes.INTERNAL_PLUG_IN_ERROR, false);
                    }
                });
                t.start();
            } else
                Log.w(TAG, "Info set was null in onPluginContextEvent");
        } else
            Log.w(TAG, "Call blocked for invalid sessionId: " + sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setPluginConfiguredStatus(UUID sessionId, boolean configured) {
        // Get the ContextPlugin based on the sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, then the caller has an valid UUID, so continue...
        if (plug != null) {
            // Found the plug, so set its configured state
            plug.setConfigured(configured);
            // Update the SettingsManager
            DynamixService.updateContextPlugin(plug);
            // Start the plugin, if needed
            startPlugin(plug, false);
        } else
            Log.w(TAG, "setPluginConfiguredStatus blocked for invalid sessionId: " + sessionId);
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean storeContextPluginSettings(UUID sessionId, ContextPluginSettings settings) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            return DynamixService.storeContextPluginSettings(plug, settings);
        } else
            Log.w(TAG, "storeContextPluginSettings blocked for invalid sessionId: " + sessionId);
        return false;
    }

    @Override
    public Result openView(UUID sessionId, IPluginView view) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionId.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            // TODO: Make sure the interface is launchable
            if (true) {
                if (!HostActivity.isBound() && HostActivity.bindPlugin(plug, view)) {
                    Intent intent = new Intent(DynamixService.getAndroidContext(), HostActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    DynamixService.getAndroidContext().startActivity(intent);
                    return new Result();
                } else {
                    return new Result("RESOURCE_BUSY: Cannot launch UI", ErrorCodes.RESOURCE_BUSY);
                }
            } else {
                return new Result("NOT_AUTHORIZED to launch UI", ErrorCodes.NOT_AUTHORIZED);
            }
        } else {
            Log.w(TAG, "storeContextPluginSettings blocked for invalid sessionId: " + sessionId);
            return new Result("SessionID NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * Initializes the incoming plug-in by creating its runtime thread and runtime. Note that plug-ins must be
     * installed, enabled and in state NEW.
     *
     * @param plug     The target ContextPlugin
     * @param factory  The target ContextPlugin's factory
     * @param settings The target ContextPlugin's settings
     * @param callback An optional callback to fire on init completion (may be null)
     */
    protected synchronized void initializeContextPlugin(final ContextPlugin plug,
                                                        final IContextPluginRuntimeFactory factory, final ContextPluginSettings settings, final ICallback callback) {
        Log.d(TAG, "Initialize plug-in: " + plug + " with enabled " + plug.isEnabled());
        Utils.dispatch(true, new Runnable() {
            @Override
            public void run() {
                // Only init if installed
                if (plug.isInstalled() || plug.getInstallStatus() == PluginInstallStatus.INSTALLING) {
                    // Only init if enabled
                    if (plug.isEnabled()) {
                        // Only init if we have a factory (or are handling a library)
                        if (plug.getContextPluginType() == ContextPluginType.LIBRARY) {
                            SessionManager.sendCallbackSuccess(callback);
                        } else if (factory != null) {
                            // Handle pluginMap updating
                            synchronized (pluginMap) {
                                if (!pluginMap.containsKey(plug)) {
                                    Log.d(TAG, "Adding a new runtime wrapper for " + plug);
                                    pluginMap.put(plug, new ContextPluginRuntimeWrapper(plug));
                                } else {
                                    /*
                                     * Plug-in is already in the pluginMap, make sure it's new or initialized
                                     */
                                    ContextPluginRuntimeWrapper tmpWrapper = pluginMap.get(plug);
                                    if (tmpWrapper.getState() == PluginState.DESTROYED
                                            || tmpWrapper.getState() == PluginState.ERROR
                                            || tmpWrapper.getState() == PluginState.INITIALIZING
                                            || tmpWrapper.getState() == PluginState.STARTED
                                            || tmpWrapper.getState() == PluginState.STARTING) {
                                        Log.w(TAG,
                                                "Ignoring init for existing plug-in " + plug + " with state: "
                                                        + tmpWrapper.getState());
                                        SessionManager.sendCallbackFailure(callback, "Ignoring init for plug-in " + plug
                                                + " with improper state: " + tmpWrapper.getState(), ErrorCodes.STATE_ERROR);
                                        return;
                                    } else {
                                        Log.d(TAG,
                                                "Found existing runtime wrapper for " + plug + " with state: "
                                                        + tmpWrapper.getState());
                                    }
                                }
                            }
                            // Setup the plug-in's runtime thread
                            final ContextPluginRuntimeWrapper pluginWrapper = pluginMap.get(plug);
                            PluginLooperThread t = null;
                            synchronized (threadMap) {
                                Log.d(TAG, "Creating runtime thread for " + plug);
                                if (threadMap.containsKey(plug)) {
                                    Log.d(TAG, "Plug-in already had thread: " + plug);
                                    t = threadMap.get(plug);
                                } else {
                                    // Register the plugin's looper thread
                                    t = registerLooperThreadForPlug(plug);
                                }
                            }
                            /*
                             * Create the plug-in's runtime using its Looper thread
                             */
                            t.handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    // Set initializing state
                                    pluginWrapper.setState(PluginState.INITIALIZING, true);
                                    ContextPluginRuntime runtime = null;
                                    if (plug.getContextPluginType() == ContextPluginType.LIBRARY) {
                                        runtime = new LibraryContextPluginRuntime();
                                    } else {
                                        /*
                                         * For security, ensure that the incoming IContextPluginRuntimeFactory is implemented by
                                         * org.ambientdynamix.api.contextplugin. ContextPluginRuntimeFactory
                                         */
                                        if (!(factory instanceof org.ambientdynamix.api.contextplugin.ContextPluginRuntimeFactory)) {
                                            Log.w(TAG, "factory not an instanceof ContextPluginRuntimeFactory");
                                            // Disable plug-in...
                                            disablePluginOnError(plug, "Exception during init for plug-in " + plug,
                                                    ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                                            SessionManager.sendCallbackFailure(callback,
                                                    "Plugin factory not an instanceof ContextPluginRuntimeFactory for " + plug,
                                                    ErrorCodes.INTERNAL_PLUG_IN_ERROR);
                                            return;
                                        } else {
                                            // Create the new ContextPluginRuntime using the incoming factory
                                            try {
                                                runtime = factory.makeContextPluginRuntime(UUID.randomUUID(),
                                                        ContextManager.this, new SimpleEventHandler(ContextManager.this));
                                            } catch (Exception e1) {
                                                Log.w(TAG, "Exception during makeContextPluginRuntime: " + e1);
                                                // Make sure the runtime is null so the method exits
                                                runtime = null;
                                                // Disable plug-in...
                                                disablePluginOnError(plug, "Exception during init for plug-in " + plug,
                                                        ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                                                SessionManager.sendCallbackFailure(callback, plug
                                                                + " encoutered an exception during 'makeContextPluginRuntime':" + e1,
                                                        ErrorCodes.INTERNAL_PLUG_IN_ERROR);
                                            }
                                        }
                                    }
                                    // Make sure we got a runtime
                                    if (runtime != null) {
                                        // Setup the pluginWrapper using the runtime
                                        Log.d(TAG, "Setting runtime " + runtime + " on wrapper " + pluginWrapper);
                                        pluginWrapper.setContextPluginRuntime(runtime);
                                        // Map the runtime's sessionId to the plug-in
                                        if (plug.getContextPluginType() != ContextPluginType.LIBRARY) {
                                            synchronized (sessionIdToPluginMap) {
                                                if (runtime.getSessionId() != null && plug != null)
                                                    sessionIdToPluginMap.put(runtime.getSessionId().toString(), plug);
                                                else
                                                    Log.w(TAG, "Null issue: sessionId=" + runtime.getSessionId() + ", plug="
                                                            + plug);
                                            }
                                        }
                                        /*
                                         * Initialize the ContextPluginRuntime using the event handler, power scheme and
                                         * settings.
                                         */
                                        try {
                                            Log.v(TAG, "Initializing runtime for: " + plug);
                                            Utils.dispatch(true, new Runnable() {
                                                @Override
                                                public void run() {
                                                    // Handle hanging inits
                                                    Date startInit = new Date();
                                                    // Ensure plug-in initializes (i.e., arrives at state INITIALIZED or STARTED)
                                                    while (pluginWrapper != null &&
                                                            pluginWrapper.getState() != PluginState.INITIALIZED && pluginWrapper.getState() != PluginState.STARTED
                                                            ) {
                                                        try {
                                                            // Log.i(TAG, "Checking " + pluginWrapper.getParentPlugin() + " with state " + pluginWrapper.getState());
                                                            Thread.sleep(500);
                                                        } catch (InterruptedException e) {
                                                        }
                                                        // Timeout if init hangs
                                                        if (new Date().getTime() - startInit.getTime() > 60000) {
                                                            // Disable plug-in...
                                                            String message = "Init timeout for " + plug;
                                                            Log.e(TAG, message);
                                                            disablePluginOnError(plug, message,
                                                                    ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                                                            SessionManager.sendCallbackFailure(callback, message, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                                            break;
                                                        }
                                                    }
                                                }
                                            });
                                            // Try to initialize the runtime
                                            runtime.init(scheme, settings);
                                            // Init succeeded, so setup INITIALIZED state
                                            pluginWrapper.setState(PluginState.INITIALIZED, true);
                                            // Clear any previous status messages
                                            Log.v(TAG, "Runtime is initialized for: " + plug);
                                            // Setup plug-in statistics
                                            synchronized (statMap) {
                                                PluginStats stats = statMap.get(plug);
                                                if (stats == null) {
                                                    // Need new PluginStats
                                                    stats = new PluginStats(plug, 20);
                                                    statMap.put(plug, stats);
                                                }
                                            }
                                            // Check for a pending stop request
                                            synchronized (pendingPluginStopMap) {
                                                if (pendingPluginStopMap.keySet().contains(plug)) {
                                                    Log.w(TAG, "Found pending stop for " + plug);
                                                    PendingStopActions actions = pendingPluginStopMap.remove(plug);
                                                    SessionManager.sendCallbackFailure(callback, "Found pending stop for "
                                                            + plug, ErrorCodes.STATE_ERROR);
                                                    stopPlugin(plug, true, actions.destroy, actions.uninstall);
                                                } else {
                                                    /*
                                                     * We previously would try to start the plug-in here; however, after
                                                     * introducing the callback, the caller should issue start, if needed.
                                                     */
                                                    // startPlugin(plug, false);
                                                    SessionManager.sendCallbackSuccess(callback);
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            // Error initializing the plugin. Clean up.
                                            Log.w(TAG, "Exception during init for plug-in " + plug + ": " + e);
                                            // Disable plug-in...
                                            disablePluginOnError(plug, "Exception during init for plug-in " + plug + ": " + e,
                                                    ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                                            SessionManager.sendCallbackFailure(callback, "Exception during init for plug-in "
                                                    + plug + ": " + e, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                        }
                                    } else {
                                        Log.w(TAG, "Runtime was null after factory creation for " + plug);
                                        disablePluginOnError(plug, "Runtime was null after factory creation for plug-in "
                                                + plug, ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                                        SessionManager.sendCallbackFailure(callback,
                                                "Runtime was null after factory creation for " + plug,
                                                ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                    }
                                }
                            });
                        } else {
                            Log.w(TAG, "Null factory for plug-in " + plug);
                            SessionManager.sendCallbackFailure(callback, "Null factory for plug-in " + plug,
                                    ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                        }
                    } else {
                        Log.w(TAG, "Cannot initialize disabled plug-in " + plug);
                        SessionManager.sendCallbackFailure(callback, "Cannot initialize disabled plug-in " + plug,
                                ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                    }
                } else {
                    String error = "Cannot initialize plug-in in state " + plug.getInstallStatus() + ": " + plug;
                    Log.w(TAG, error);
                    SessionManager.sendCallbackFailure(callback, error, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                }
            }
        });
    }

    /**
     * Returns true if the context type is supported by the currently installed plug-ins; false otherwise.
     */
    protected boolean isContextTypeSupported(String contextType) {
        synchronized (pluginMap) {
            for (ContextPlugin plug : pluginMap.keySet()) {
                if (plug.supportsContextType(contextType))
                    return true;
            }
        }
        return false;
    }

    /**
     * Returns true if the plug-in is installed; false otherwise.
     */
    protected boolean isPluginInstalled(ContextPlugin plug) {
        synchronized (pluginMap) {
            if (pluginMap.containsKey(plug))
                return pluginMap.get(plug).getParentPlugin().isInstalled();
            else
                return false;
        }
    }

    /**
     * Adds context support to an app using the specific plug-in or any ContextPlugins supporting the requested
     * contextDataType. Note that this method will only return valid ContextPlugins, meaning that ContextPlugins with
     * installation problems will not be returned.
     *
     * @param app           the application requesting context support.
     * @param handler       The associated context handler.
     * @param contextType   A String representing the desired context support type. Returns a List of ContextPlugins enlisted to
     *                      provide the requested support, or an empty List if no context support is available.
     * @param pluginId      The specific plug-in setup context support with. If null, we attempt map the incoming contextType to
     *                      to a supporting plug-in
     * @param pluginVersion The optional version of the plug-in to use (if null, the latest known version is used).
     * @param listener      An optional context listener to be associated with the context support registration.
     * @param callback      An optional callback to notify when on success of failure.
     * @throws Exception
     */
    protected synchronized void addContextSupport(DynamixApplication app, IContextHandler handler, String contextType,
                                                  String pluginId, String pluginVersion, IContextListener listener, Bundle config,
                                                  IContextSupportCallback callback) {
        List<VersionedPlugin> pluginIds = new ArrayList<VersionedPlugin>();
        pluginIds.add(new VersionedPlugin(pluginId, pluginVersion));
        addContextSupport(app, handler, contextType, pluginIds, listener, config, callback);
    }

    /**
     * Adds context support to an app using the specific plug-in or any ContextPlugins supporting the requested
     * contextDataType. Note that this method will only return valid ContextPlugins in the callback, meaning that ContextPlugins with
     * installation problems will not be returned.
     *
     * @param app         the application requesting context support.
     * @param handler     The associated context handler.
     * @param contextType A String representing the desired context support type. Returns a List of ContextPlugins enlisted to
     *                    provide the requested support, or an empty List if no context support is available.
     * @param listener    An optional context listener to be associated with the context support registration.
     * @param plugins     The list of versioned plug-ins requested to provide context support.
     * @param callback    An optional callback to notify when on success of failure.
     */
    protected synchronized void addContextSupport(final DynamixApplication app, final IContextHandler handler,
                                                  final String contextType, List<VersionedPlugin> plugins, IContextListener listener, Bundle config,
                                                  final IContextSupportCallback callback) {
        Log.d(TAG, "addContextSupport for " + handler + " context type " + contextType + " and plug-ins " + plugins);
        // Grab the DynamixSession for the handler
        final DynamixSession session = SessionManager.getSession(handler);
        if (session != null) {
            /*
             * Create a combined list of plug-ins from the plugMap and available plug-ins (no duplicates!)
			 */
            List<ContextPlugin> currentlyManagedPlugins = new ArrayList<ContextPlugin>();
            /*
             * First, add all installed plug-ins to the list
			 */
            currentlyManagedPlugins.addAll(DynamixService.getInstalledContextPlugins());
            /*
             * Next, double-check the pluginMap for any stragglers (new, installing,etc).
			 */
            synchronized (pluginMap) {
                for (ContextPlugin plug : pluginMap.keySet())
                    if (!currentlyManagedPlugins.contains(plug))
                        currentlyManagedPlugins.add(plug);
            }
            /*
             * Scan through the currentlyManagedPlugins adding plug-ins that match the incoming pluginIds and
			 * contextType to the supportingPlugins list
			 */
            List<ContextPlugin> supportingPlugins = new ArrayList<ContextPlugin>();
            for (VersionedPlugin plugBase : plugins) {
                // Check all currently managed plug-ins for the requested plugId
                for (ContextPlugin plug : currentlyManagedPlugins) {
                    // Check if the plug-in matches the context support request
                    if (isPluginMatch(plugBase, plug, contextType, false)) {
                        // Make sure the plug-in is not in an error state
                        if (plug.getInstallStatus() == PluginInstallStatus.ERROR) {
                            Log.w(TAG,
                                    "Can't install context support because of an INTERNAL_PLUG_IN_ERROR in "
                                            + plug.getId());
                            SessionManager.sendContextSupportFailure(callback,
                                    "INTERNAL_PLUG_IN_ERROR in " + plug.getId(), ErrorCodes.INTERNAL_PLUG_IN_ERROR);
                            return;
                        } else {
                            // Found a plug-in to support the request
                            if (!supportingPlugins.contains(plug))
                                supportingPlugins.add(plug);
                        }
                    }
                }
            }
            /*
             * Now try to discover and install any pending plug-ins that support the context support request
			 */
            for (ContextPlugin pending : getSupportingPendingPlugins(plugins, contextType, false)) {
                if (!supportingPlugins.contains(pending))
                    supportingPlugins.add(pending);
            }
            // Make sure we have at least one supporting plug-in
            if (supportingPlugins.size() > 0) {
                /*
                 * Warn and remove plug-ins that have the same id and version (e.g., from 2 repositories)
				 */
                List<ContextPlugin> keep = new ArrayList<ContextPlugin>();
                List<ContextPlugin> dupes = new ArrayList<ContextPlugin>();
                for (ContextPlugin scanner : supportingPlugins) {
                    for (ContextPlugin candidate : supportingPlugins) {
                        // Check all plug-ins except for the current dupeCheck
                        if (!scanner.equals(candidate)) {
                            // Check if the id and version are the same
                            if (scanner.getId().equalsIgnoreCase(candidate.getId())
                                    && scanner.getVersion().equals(candidate.getVersion())) {
                                // Yep, keep the scanner and add candidate to the dupes
                                if (keep.contains(scanner)) {
                                    dupes.add(candidate);
                                } else {
                                    keep.add(scanner);
                                }
                            }
                        }
                        if (!(keep.contains(scanner) || dupes.contains(scanner))) {
                            keep.add(scanner);
                        }
                    }
                }
                // Remove dupes
                for (ContextPlugin dupe : dupes) {
                    Log.w(TAG, "Found duplicate plug-in, ignoring it (check for duplicates in all repos): " + dupe);
                    SessionManager.sendContextSupportWarning(callback,
                            "Found duplicate plug-in - ignoring it (check duplicates in repos): " + dupe,
                            ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                }
                supportingPlugins.removeAll(dupes);
                Log.d(TAG, "addContextSupport found " + supportingPlugins.size()
                        + " plug-in(s) to handle context support " + contextType + " for handler " + handler);
                for (ContextPlugin plug : supportingPlugins) {
                    Log.d(TAG, "addContextSupport found: " + plug);
                }
                // Create a context support entity
                final ContextSupport sup = new ContextSupport(UUID.randomUUID(), session, handler, contextType,
                        supportingPlugins, listener, config);
                // Try to add the ContextSupport to the session
                try {
                    session.addContextSupport(sup);
                } catch (Exception e) {
                    // Session add failure
                    SessionManager.sendContextSupportFailure(callback,
                            "Could not add context support to session: " + e.toString(),
                            ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                    return;
                }
                // Persist the callback
                sup.addPendingContextSupportCallback(callback);
                /*
                 * Context support was added to the session. Now handle the states of each plug-in enlisted to serve the
				 * context support.
				 */
                final List<ContextPlugin> plugsToStart = new ArrayList<ContextPlugin>(supportingPlugins);
                for (final ContextPlugin plug : supportingPlugins) {
                    switch (plug.getInstallStatus()) {
                        case ERROR:
                        /*
                         * Handle plug-in error state by removing the entire context support entity. This may be
						 * redundant, since we're checking for error state above.
						 */
                            sup.fireContextSupportFailureOnAllCallbacks(
                                    "Cannot install context support due to an INTERNAL_PLUG_IN_ERROR in " + plug,
                                    ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                            removeContextSupport(handler, sup.getContextSupportInfo(), null);
                            break;
                        case INSTALLED:
                            Log.d(TAG, "addContextSupport is trying to call start on PREVIOUSLY INSTALLED plug-in: " + plug);
                            startPlugin(plug, false, new Callback() {
                                @Override
                                public void onSuccess() throws RemoteException {
                                    Log.d(TAG, "Started PREVIOUSLY INSTALLED plug-in " + plug + " to handle " + sup);
                                /*
                                 * If we've started all plug-ins for this context support, complete the install.
								 */
                                    if (allPluginsStarted(plugsToStart)) {
                                        completeContextSupportInstall(sup);
                                    } else
                                        Log.d(TAG, "Still waiting for plug-ins to start for context support: " + sup);
                                }

                                @Override
                                public void onFailure(String message, int errorCode) throws RemoteException {
                                    Log.w(TAG, "Previously installed plug-in " + plug + " could not be started to handle "
                                            + sup + " due to error: " + message);
                                    sup.fireContextSupportFailureOnAllCallbacks("Previously installed plug-in " + plug
                                                    + " could not be started to handle " + sup + " due to error: " + message,
                                            errorCode, true);
                                    removeContextSupport(handler, sup.getContextSupportInfo(), null);
                                }
                            });
                            break;
                        case NOT_INSTALLED:
                            Log.d(TAG, "addContextSupport is calling install plug-in for " + plug);
                        /*
                         * If the plug-in is not installed, install it! Note that the app may disconnect before the
						 * plug-in is installed. Currently, that case is handled by making sure no calls to the app are
						 * made if the session is closed during an install.
						 */
                            DynamixService.installPlugin(plug, new PluginInstallCallback() {
                                @Override
                                public void onInstallComplete(ContextPluginInformation plugin) {
                                    Log.d(TAG, "Install complete for " + plugin);
                                    startPlugin(plug, false, new Callback() {
                                        @Override
                                        public void onSuccess() throws RemoteException {
                                            Log.d(TAG, "Started NEWLY INSTALLED plug-in " + plug + " to handle " + sup);
                                            /*
                                             * If we've started all plug-ins for this context support, complete the install.
                                             */
                                            if (allPluginsStarted(plugsToStart)) {
                                                completeContextSupportInstall(sup);
                                            } else
                                                Log.d(TAG, "Still waiting for plug-ins to start for context support: "
                                                        + sup);
                                        }

                                        @Override
                                        public void onFailure(String message, int errorCode) throws RemoteException {
                                            Log.w(TAG, "Newly installed plug-in " + plug
                                                    + " could not be started to handle " + sup + " due to error: "
                                                    + message);
                                            sup.fireContextSupportFailureOnAllCallbacks("Newly installed plug-in " + plug
                                                    + " could not be started to handle " + sup + " due to error: "
                                                    + message, errorCode, true);
                                            removeContextSupport(handler, sup.getContextSupportInfo(), null);
                                        }
                                    });
                                }

                                @Override
                                public void onInstallFailed(ContextPluginInformation plugin, String message, int errorCode) {
                                    String m = "onInstallFailed for plug-in " + plug + " during install for " + sup
                                            + ". Error message was: " + message;
                                    Log.w(TAG, m);
                                    sup.fireContextSupportFailureOnAllCallbacks(m, errorCode, true);
                                    removeContextSupport(handler, sup.getContextSupportInfo(), null);
                                }
                            });
                            break;
                        case INSTALLING:
                        case PENDING_INSTALL:
                        case WAITING_FOR_DEPENDENCY:
                            // Plug-in is already installing
                            Log.d(TAG, "addContextSupport was already installing " + plug
                                    + ", which will be used to handle " + sup);
                            /*
                             * We need to hook into the plug-in started logic above and then fire the callback when the list
                             * of plug-ins to install reaches zero.
                             */
                            final ContextPluginRuntimeWrapper wrapper = getContextPluginRuntimeWrapper(plug);
                            if (wrapper != null) {
                                wrapper.addStateListener(new PluginStateListener() {
                                    @Override
                                    public void onStarted(ContextPluginInformation plug) {
                                        Log.d(TAG, "Plug-in " + plug + " was just started to handle " + sup);
                                        // Remove the state listener
                                        wrapper.removeStateListener(this);
                                        /*
                                         * If we've started all plug-ins for this context support, complete the install.
                                         */
                                        if (allPluginsStarted(plugsToStart)) {
                                            completeContextSupportInstall(sup);
                                        } else
                                            // Wait for the rest of the plug-ins
                                            Log.d(TAG, "Still waiting for plug-ins to start to handle context support: "
                                                    + sup);
                                    }

                                    @Override
                                    public void onError(ContextPluginInformation plug) {
                                        String m = "Cannot install context support, since the plug-in " + plug.getPluginId()
                                                + " encountered an error state.";
                                        Log.w(TAG, m);
                                        wrapper.removeStateListener(this);
                                        sup.fireContextSupportFailureOnAllCallbacks(m, ErrorCodes.STATE_ERROR, true);
                                        removeContextSupport(handler, sup.getContextSupportInfo(), null);
                                    }

                                    @Override
                                    public void onDestroyed(ContextPluginInformation plug) {
                                        // TODO Handle this state?
                                        String m = "Plug-in " + plug.getPluginId()
                                                + " was destroyed during context support install. Should we handle this?";
                                        Log.w(TAG, m);
                                        wrapper.removeStateListener(this);
                                    }
                                });
                            } else
                                Log.w(TAG, "No runtime for " + plug);
                            break;
                        default:
                            /*
                             * Default handles states: UNINSTALLING, NOT_INSTALLED, ERROR
                             */
                            removeContextSupport(handler, sup.getContextSupportInfo(), null);
                            String m = "Plug-in " + plug + " had an INVALID STATE for adding context support "
                                    + plug.getInstallStatus();
                            Log.w(TAG, m);
                            sup.fireContextSupportFailureOnAllCallbacks(m, ErrorCodes.STATE_ERROR, true);
                            break;
                    }
                }
            } else {
                Log.w(TAG, "CONTEXT_SUPPORT_NOT_FOUND for type " + contextType);
                SessionManager.sendContextSupportFailure(callback, "CONTEXT_SUPPORT_NOT_FOUND for type " + contextType,
                        ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
            }
        } else {
            Log.w(TAG, "SESSION_NOT_FOUND for: " + app);
            SessionManager.sendContextSupportFailure(callback, "SESSION_NOT_FOUND for: " + app,
                    ErrorCodes.SESSION_NOT_FOUND);
        }
    }

    /**
     * Utility method that completes a context support install by adding listeners to runtimes, notifying callbacks, and
     * setting up communication channels if needed.
     *
     * @param support The context support to complete
     */
    private void completeContextSupportInstall(final ContextSupport support) {
        Log.d(TAG, "All plug-ins have started for context support: " + support);
        // Check if we have a listener to handle
        if (support.hasContextListener()) {
            // Collect all the associated runtimes.
            final List<ContextPluginRuntime> runtimes = new ArrayList<ContextPluginRuntime>();
            for (ContextPlugin plug : support.getContextPlugins()) {
                ContextPluginRuntime runtime = getContextPluginRuntime(plug.getContextPluginInformation());
                if (runtime != null) {
                    runtimes.add(runtime);
                } else
                    Log.e(TAG, "Runtime wrapper was null after context support install!");
            }
            /*
             * Next, call 'addContextlistener' on all plug-in runtimes associated with the context support. Note that
			 * the dispatch below mankes sure to only fire callbacks only after 'addContextlistener' has been called on
			 * each runtime.
			 */
            Utils.dispatch(true, new Runnable() {
                @Override
                public void run() {
                    // Call addContextlistener on each runtime
                    for (final ContextPluginRuntime target : runtimes) {
                        try {
                            target.addContextlistener(new ContextListenerInformation(support.getSupportId(), support
                                    .getContextType(), support.getSupportConfig()));
                        } catch (Exception e) {
                            Log.w(TAG, "Exception during AddContextSubscription: " + e.toString() + " target was "
                                    + target);
                        }
                    }
                    // Register the subscription communication channel for ongoing events
                    registerSubscriptionChannel(support.getSupportId(), support.getDynamixApplication(),
                            support.getContextHandler(), support.getContextPlugins(), support.getContextType());
                    // Set the context support active
                    support.setActive(true);
                    Log.d(TAG, "Context support just activated: " + support + " (with listeners)");
                    // Notify callbacks about context support
                    support.fireContextSupportSuccessOnAllCallbacks(true);
                }
            });
        } else {
            Log.d(TAG, "Context support just activated: " + support + " (no listeners)");
            // No listeners, so simply notify context support callbacks
            support.fireContextSupportSuccessOnAllCallbacks(true);
        }
    }

    /**
     * Removes the context support for the specified handler, returning status information using the callback.
     */
    public synchronized Result removeContextSupport(IContextHandler handler,
                                                    ContextSupportInfo supportInfo, ICallback callback) {
        DynamixSession session = SessionManager.getSession(handler);
        if (session != null) {
            Log.d(TAG, "removeContextSupport " + supportInfo + " for session " + session + " and handler " + handler);
            final ContextSupport sup = session.getContextSupport(supportInfo);
            if (sup != null) {
                return doRemoveContextSupport(sup, callback);
            } else {
                SessionManager.sendCallbackFailure(callback, "Context Support Not Found",
                        ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
                return new Result("Context Support Not Found", ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
            }
        } else {
            Log.w(TAG, "Session not found for handler " + handler);
            SessionManager.sendCallbackFailure(callback, "Session Not found", ErrorCodes.SESSION_NOT_FOUND);
            return new Result("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
        }
    }

    /**
     * Removes all context support for the specified context handler.
     */
    public synchronized Result removeAllContextSupport(DynamixSession session, IContextHandler handler,
                                                       final ICallback callback) {
        if (session != null) {
            final List<ContextSupport> subList = session.getContextSupport(handler);
            if (subList.isEmpty()) {
                SessionManager.sendCallbackSuccess(callback);
                return new Result();
            } else {
                final List<ContextSupport> removeList = new ArrayList<ContextSupport>(subList);
                for (final ContextSupport sub : subList) {
                    removeContextSupport(handler, sub.getContextSupportInfo(), new Callback() {
                        @Override
                        public void onSuccess() throws RemoteException {
                            synchronized (removeList) {
                                removeList.remove(sub);
                                if (removeList.isEmpty())
                                    SessionManager.sendCallbackSuccess(callback);
                            }
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            Log.w(TAG, "removeContextSupport.onFailure: " + message);
                            synchronized (removeList) {
                                if (removeList.isEmpty())
                                    SessionManager.sendCallbackSuccess(callback);
                            }
                        }
                    });
                }
                return new Result();
            }
        } else {
            Log.w(TAG, "Session was null");
            SessionManager.sendCallbackFailure(callback, "Session Not found", ErrorCodes.SESSION_NOT_FOUND);
            return new Result("Session Not found", ErrorCodes.SESSION_NOT_FOUND);
        }
    }

    /**
     * Removes all context support for the specified session (all listeners).
     */
    public synchronized void removeAllContextSupport(DynamixSession session, final ICallback callback) {
        final List<ContextSupport> subList = session.getAllContextSupport();
        Log.d(TAG, "removeAllContextSupport for " + session + " with context support count " + subList.size());
        if (subList.isEmpty()) {
            SessionManager.sendCallbackSuccess(callback);
        } else {
            final List<ContextSupport> removeList = new ArrayList<ContextSupport>(subList);
            for (final ContextSupport sub : subList) {
                removeContextSupport(sub.getContextHandler(), sub.getContextSupportInfo(), new Callback() {
                    @Override
                    public void onSuccess() throws RemoteException {
                        synchronized (removeList) {
                            removeList.remove(sub);
                            if (removeList.isEmpty())
                                SessionManager.sendCallbackSuccess(callback);
                        }
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        Log.w(TAG, "removeAllContextSupport.onFailure: " + message);
                        synchronized (removeList) {
                            removeList.remove(sub);
                            if (removeList.isEmpty())
                                SessionManager.sendCallbackSuccess(callback);
                        }
                    }
                });
            }
        }
    }

    /**
     * Removes all context support of a particular context type for the specified context handler.
     */
    public synchronized void removeContextSupportForContextType(IContextHandler handler,
                                                                String contextType, ICallback callback) {
        DynamixSession session = SessionManager.getSession(handler);
        if (session != null) {
            List<ContextSupport> removeList = session.getContextSupport(handler);
            for (ContextSupport sub : removeList) {
                if (sub.getContextType().equalsIgnoreCase(contextType)) {
                    removeContextSupport(handler, sub.getContextSupportInfo(), callback);
                    return;
                }
            }
            SessionManager.sendCallbackFailure(callback, "Context Support Not Found",
                    ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
        } else {
            Log.w(TAG, "could not find open session for: " + handler);
            SessionManager.sendCallbackFailure(callback, "Session Not found", ErrorCodes.SESSION_NOT_FOUND);
        }
    }

    /**
     * Returns true if all of the plug-ins have started; false otherwise.
     */
    private synchronized boolean allPluginsStarted(List<ContextPlugin> plugs) {
        for (ContextPlugin plug : plugs) {
            ContextPluginRuntimeWrapper wrapper = getContextPluginRuntimeWrapper(plug.getContextPluginInformation());
            if (wrapper != null) {
                if (wrapper.getState() != PluginState.STARTED)
                    return false;
            } else
                return false;
        }
        return true;
    }

    /**
     * Removes all context support of a particular plug-in.
     */
    public synchronized void removeContextSupportForContextPlugin(ContextPlugin plug, ICallback callback) {
        List<ContextSupport> subList = new ArrayList<ContextSupport>();
        List<ContextSupport> removeList = new ArrayList<ContextSupport>();
        // Grab the context support list for all sessions
        for (DynamixSession session : SessionManager.getAllSessions()) {
            subList.addAll(session.getAllContextSupport());
        }
        // Scan the complete list looking for support that include the plug-in
        for (ContextSupport sub : subList) {
            if (sub.containsContextPlugin(plug.getContextPluginInformation())) {
                removeList.add(sub);
            }
        }
        // Remove all subscriptions related to the plug-in
        for (ContextSupport sup : removeList) {
            doRemoveContextSupport(sup, callback);
        }
    }

    /**
     * Adds the specified ContextPlugin without its associated IContextPluginRuntimeFactory, which is used to create and
     * initialize the ContextPlugin. This method adds an EmptyContextPluginRuntime as a placeholder until a
     * IContextPluginRuntimeFactory can be provided (usually after a dynamic install). When the
     * IContextPluginRuntimeFactory is finally available, use the initializeContextPlugin method to update the plugin
     * with its associated IContextPluginRuntimeFactory.
     *
     * @param plug The new ContextPlugin to add.
     */
    protected boolean addNewContextPlugin(ContextPlugin plug) {
        Log.d(TAG, "addNewContextPlugin for: " + plug);
        synchronized (pluginMap) {
            /*
             * Check if the plugin's runtime is already being managed (i.e., it's listed in the pluginMap)
			 */
            if (!pluginMap.containsKey(plug)) {
                pluginMap.put(plug, new ContextPluginRuntimeWrapper(plug));
                return true;
            } else
                Log.d(TAG, "addNewContextPlugin found existing plugin: " + plug);
        }
        return true;
    }

    /**
     * Returns an immutable list of all ContextPlugins currently managed by the ContextManager.
     */
    protected List<ContextPlugin> getAllContextPlugins() {
        return new ArrayList<ContextPlugin>(this.pluginMap.keySet());
    }

    /**
     * Returns the context plug-in associated with the incoming pluginId. Returns the latest if there are multiple
     * versions of the plug-in.
     */
    protected ContextPluginInformation getContextPluginInfo(String pluginId) {
        ContextPluginInformation latest = null;
        synchronized (pluginMap) {
            for (ContextPlugin plug : pluginMap.keySet()) {
                if (plug.getId().equalsIgnoreCase(pluginId)) {
                    if (latest != null) {
                        if (plug.getVersion().compareTo(latest.getVersion()) > 0)
                            latest = plug.getContextPluginInformation();
                    } else
                        latest = plug.getContextPluginInformation();
                }
            }
        }
        return latest;
    }

    /**
     * Returns the context plug-in runtime wrapper associated with the incoming pluginId. Returns the latest if there
     * are multiple versions of the plug-in.
     */
    protected ContextPluginRuntimeWrapper getContextPluginRuntime(String pluginId) {
        ContextPluginRuntimeWrapper latest = null;
        for (ContextPlugin plug : pluginMap.keySet()) {
            if (plug.getId().equalsIgnoreCase(pluginId))
                if (latest != null) {
                    if (plug.getVersion().compareTo(latest.getParentPlugin().getVersion()) > 0)
                        latest = pluginMap.get(plug);
                } else
                    latest = pluginMap.get(plug);
        }
        return latest;
    }

    /**
     * Returns the context plug-in information associated with the incoming versioned plug-in, or null if the
     * plug-in is not found.
     */
    protected ContextPluginInformation getContextPluginInfo(VersionedPlugin vPlug) {
        if (vPlug.getPluginVersion() != null) {
            synchronized (pluginMap) {
                for (ContextPlugin plug : pluginMap.keySet()) {
                    if (plug.getId().equalsIgnoreCase(vPlug.getPluginId()) && plug.getVersion().equals(vPlug.getPluginVersion()))
                        return plug.getContextPluginInformation();
                }
                // Didn't find the specified plug-in, so return null
                return null;
            }
        } else
            return getContextPluginInfo(vPlug.getPluginId());
    }

    /**
     * Returns the context plug-in information associated with the incoming pluginId and versionString, or null if the
     * plug-in is not found.
     */
    protected ContextPluginInformation getContextPluginInfo(String pluginId, String versionString) {
        if (versionString != null) {
            VersionInfo version = VersionInfo.createVersionInfo(versionString);
            if (version != null) {
                synchronized (pluginMap) {
                    for (ContextPlugin plug : pluginMap.keySet()) {
                        if (plug.getId().equalsIgnoreCase(pluginId) && plug.getVersion().equals(version))
                            return plug.getContextPluginInformation();
                    }
                }
            } else
                Log.w(TAG, "Could not create version from string: " + versionString);
            return null;
        } else
            return getContextPluginInfo(pluginId);
    }

    /**
     * Returns the ContextPlugin associated with the incoming pluginId and pluginVersion, or null if the plug-in is not
     * found.
     */
    protected ContextPlugin getContextPlugin(String pluginId, String pluginVersion) {
        return getContextPlugin(getContextPluginInfo(pluginId, pluginVersion));
    }

    /**
     * Returns the ContextPlugin associated with the incoming plugInfo object, or null if the plug-in is not found.
     */
    protected ContextPlugin getContextPlugin(ContextPluginInformation plugInfo) {
        synchronized (pluginMap) {
            for (ContextPlugin plug : pluginMap.keySet()) {
                if (plug.getContextPluginInformation().equals(plugInfo))
                    return plug;
            }
        }
        return null;
    }

    /**
     * Returns the ContextPluginRuntime associated with the incoming plugInfo object, or null if the plug-in is not
     * found.
     */
    protected ContextPluginRuntime getContextPluginRuntime(ContextPluginInformation plugInfo) {
        synchronized (pluginMap) {
            for (ContextPlugin plug : pluginMap.keySet()) {
                if (plug.getContextPluginInformation().equals(plugInfo))
                    return pluginMap.get(plug).getContextPluginRuntime();
            }
        }
        return null;
    }

    /**
     * Returns the ContextPluginRuntimeWrapper associated with the incoming ContextPluginInformation, or null if the
     * wrapper is not found.
     */
    protected ContextPluginRuntimeWrapper getContextPluginRuntimeWrapper(ContextPluginInformation plugInfo) {
        synchronized (pluginMap) {
            for (ContextPlugin plug : pluginMap.keySet()) {
                if (plug.getContextPluginInformation().equals(plugInfo))
                    return pluginMap.get(plug);
            }
        }
        return null;
    }

    /**
     * Returns the ContextPluginRuntime associated with the incoming pluginId and version, or null if the runtime is not
     * found.
     */
    protected ContextPluginRuntimeWrapper getContextPluginRuntime(String pluginId, VersionInfo version) {
        synchronized (pluginMap) {
            for (ContextPlugin plug : pluginMap.keySet()) {
                if (plug.getId().equalsIgnoreCase(pluginId) && plug.getVersion().equals(version))
                    return pluginMap.get(plug);
            }
        }
        return null;
    }

    /**
     * Returns the ContextPluginRuntime associated with the incoming pluginId and versionString, or null if the runtime
     * is not found.
     */
    protected ContextPluginRuntimeWrapper getContextPluginRuntime(String pluginId, String versionString) {
        return getContextPluginRuntime(pluginId, VersionInfo.createVersionInfo(versionString));
    }

    /**
     * Returns the ContextPluginRuntimeWrapper associated with the ContextPlugin, or null if the plugin is not found.
     */
    protected ContextPluginRuntimeWrapper getContextPluginRuntimeWrapper(ContextPlugin plug) {
        return pluginMap.get(plug);
    }

    /**
     * Returns the ContextPluginRuntime associated with the ContextPlugin, or null if the plugin is not found.
     */
    protected ContextPluginRuntime getContextPluginRuntime(ContextPlugin plug) {
        ContextPluginRuntimeWrapper wrapper = getContextPluginRuntimeWrapper(plug);
        if (wrapper != null)
            return wrapper.getContextPluginRuntime();
        else
            return null;
    }

    /**
     * Returns a ContextSupportResult for the incoming session and listener.
     *
     * @param session The requesting session.
     * @param handler The handler to return context support for.
     */
    protected ContextSupportResult getContextSupport(DynamixSession session, IContextHandler handler) {
        List<ContextSupportInfo> subList = new ArrayList<ContextSupportInfo>();
        if (session != null) {
            List<ContextSupport> subs = session.getContextSupport(handler);
            if (subs != null)
                for (ContextSupport sub : subs) {
                    subList.add(sub.getContextSupportInfo());
                }
        } else {
            Log.w(TAG, "getContextSupport received null session");
        }
        return new ContextSupportResult(subList);
    }

    /**
     * Registers the specified Activity as belonging to the ContextPluginRuntime. Used to programmatically close the
     * Activity later.
     *
     * @param runtime  The ContextPluginRuntime.
     * @param activity The associated Activity.
     */
    protected void registerConfigurationActivity(ContextPluginRuntime runtime, Activity activity) {
        Log.v(TAG, "registerConfigurationActivity for " + runtime);
        ContextPlugin plug = getContextPluginForSessionId(runtime.getSessionId().toString());
        if (plug != null) {
            if (configActivityMap.containsKey(plug)) {
                /*
                 * Close the current activity, since there's already another activity running
				 */
                Log.w(TAG, "Exiting existing configuration activity... closing it");
                Activity act = configActivityMap.remove(plug);
                act.finish();
            } else {
                // Store the activity
                configActivityMap.put(plug, activity);
            }
        } else
            Log.w(TAG, "registerConfigurationActivity could not find a plugin for: " + runtime);
    }

    /**
     * Registers the specified Activity as belonging to the ContextPluginRuntime. Used to programmatically close the
     * Activity later.
     *
     * @param runtime  The ContextPluginRuntime.
     * @param activity The associated Activity.
     */
    protected void registerContextAcquisitionActivity(ContextPluginRuntime runtime, Activity activity) {
        Log.v(TAG, "registerContextAcquisitionActivity for " + runtime);
        ContextPlugin plug = getContextPluginForSessionId(runtime.getSessionId().toString());
        if (plug != null) {
            synchronized (acquisitionActivityMap) {
                // Check if the runtime is already bound to an Activity
                if (!acquisitionActivityMap.containsKey(plug)) {
                    acquisitionActivityMap.put(plug, activity);
                } else {
                    /*
                     * The runtime is already bound to an Activity. Log warnings.
					 */
                    Activity existing = acquisitionActivityMap.get(plug);
                    if (existing != null) {
                        if (existing.equals(activity))
                            Log.d(TAG, runtime + " is already bound to context acquisition activity: " + activity);
                        else
                            Log.w(TAG, runtime + " is bound, but not to context acquisition activity: " + activity);
                    } else
                        Log.w(TAG, "registerContextAcquisitionActivity could not find existing Activity for: "
                                + runtime);
                }
            }
        } else
            Log.w(TAG, "registerContextAcquisitionActivity could not find a plugin for: " + runtime);
    }

    /**
     * Unregisters the specified context acquisition Activity from the runtime.
     *
     * @param runtime The runtime unregister.
     */
    protected void unregisterContextAcquisitionActivity(ContextPluginRuntime runtime) {
        Log.d(TAG, "unregisterContextAcquisitionActivity for " + runtime);
        if (runtime != null) {
            ContextPlugin plug = getContextPluginForSessionId(runtime.getSessionId().toString());
            if (plug != null) {
                Activity a = acquisitionActivityMap.remove(plug);
                if (a != null)
                    Log.d(TAG, "Unregistered context acquisition activity for " + runtime);
                else
                    Log.d(TAG, "Could not find context acquisition activity... probably closed by " + runtime);
            } else
                Log.w(TAG, "unregisterContextAcquisitionActivity could not find a plugin for: " + runtime);
        } else
            Log.w(TAG, "unregisterContextAcquisitionActivity received null runtime");
    }

    /**
     * Unregisters the specified configuration Activity from the runtime.
     *
     * @param runtime The runtime to unregister.
     */
    protected void unRegisterConfigurationActivity(ContextPluginRuntime runtime) {
        Log.d(TAG, "unRegisterConfigurationActivity for " + runtime);
        if (runtime != null) {
            ContextPlugin plug = getContextPluginForSessionId(runtime.getSessionId().toString());
            if (plug != null) {
                Activity a = configActivityMap.remove(plug);
                if (a != null)
                    Log.d(TAG, "Unregistered plugin configuration activity for " + runtime);
                else
                    Log.d(TAG, "Could not find plugin configuration activity... probably closed by " + runtime);
            } else
                Log.w(TAG, "unRegisterConfigurationActivity could not find a plugin for: " + runtime);
        } else
            Log.w(TAG, "unRegisterConfigurationActivity received null runtime");
    }

    /**
     * Registers a new request channel.
     *
     * @param app         The target app for the response.
     * @param handler     The target handler for the response.
     * @param plugin      The plug-in.
     * @param contextType The context type.
     * @return The request id.
     */
    protected UUID registerContextRequestChannel(DynamixApplication app, IContextHandler handler, ContextPlugin plugin,
                                                 String contextType) {
        List<ContextPlugin> plugins = new ArrayList<ContextPlugin>();
        plugins.add(plugin);
        return registerContextRequestChannel(app, handler, plugins, contextType);
    }

    /**
     * Registers a new request channel.
     *
     * @param app         The target app for the response.
     * @param handler     The target handler for the response.
     * @param plugins     The plug-ins.
     * @param contextType The context type.
     * @return The request id.
     */
    protected UUID registerContextRequestChannel(DynamixApplication app, IContextHandler handler,
                                                 List<ContextPlugin> plugins, String contextType) {
        synchronized (channelMap) {
            UUID id = UUID.randomUUID();
            channelMap.put(id, new Channel(app, handler, plugins, contextType, Channel.Type.CONTEXT_REQUEST));
            return id;
        }
    }

    /**
     * Registers a subscription channel using the incoming id.
     *
     * @param id          The id to register.
     * @param app         The target app for the channel.
     * @param handler     The target handler for the channel.
     * @param plugin      The plug-in.
     * @param contextType The context type.
     * @return True if the id was registered; false otherwise.
     */
    protected boolean registerSubscriptionChannel(UUID id, DynamixApplication app, IContextHandler handler,
                                                  ContextPlugin plugin, String contextType) {
        List<ContextPlugin> plugins = new ArrayList<ContextPlugin>();
        plugins.add(plugin);
        return registerSubscriptionChannel(id, app, handler, plugins, contextType);
    }

    /**
     * Registers an event subscription channel using the incoming id.
     *
     * @param id          The id to register.
     * @param app         The target app for the channel.
     * @param handler     The target handler for the channel.
     * @param plugins     The plug-ins.
     * @param contextType The context type.
     * @return True if the id was registered; false otherwise.
     */
    protected boolean registerSubscriptionChannel(UUID id, DynamixApplication app, IContextHandler handler,
                                                  List<ContextPlugin> plugins, String contextType) {
        synchronized (channelMap) {
            if (!channelMap.containsKey(id)) {
                channelMap.put(id, new Channel(app, handler, plugins, contextType, Channel.Type.CONTEXT_SUBSCRIPTION));
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Removes all events for a given context listener and context type, regardless of expiration time.
     */
    protected void removeCachedContextEvents(IContextListener listener, String contextType) {
        try {
            contextCache.removeContextResults(listener, contextType);
        } catch (Exception e) {
            Log.w(TAG, "removeCachedContextEvents exception: " + e);
        }
    }

    /**
     * Removes all events for a given context handler and context type, regardless of expiration time.
     */
    protected void removeCachedContextEvents(IContextHandler handler, String contextType) {
        try {
            contextCache.removeContextResults(handler, contextType);
        } catch (Exception e) {
            Log.w(TAG, "removeCachedContextEvents exception: " + e);
        }
    }

    /**
     * Removes a previously added ContextPlugin from ContextManager handling.
     *
     * @param plug The ContextPlugin to remove.
     */
    protected synchronized void removeContextPlugin(ContextPlugin plug, ICallback stopCallback,
                                                    ICallback uninstallCallback) {
        if (plug != null) {
            // First cancel any associated installs
            DynamixService.cancelInstallation(plug);
            /*
             * All dependent plug-ins must also be uninstalled, since the classes from the library will be removed.
			 */
            if (DynamixPreferences.isAutoDependencyUninstallEnabled())
                for (ContextPlugin depPlug : Utils.getDependentPlugins(plug, true)) {
                    /*
                     * TODO: Currently, we're uninstalling all dependent plug-ins in parallel; however, there might be
					 * cases where the order of uninstalls is critical, such as uninstalling 'leaf' nodes first (those
					 * without dependents) and then working our way up the graph.
					 */
                    stopPlugin(depPlug, true, true, true);
                }
            stopPlugin(plug, true, true, true, stopCallback, uninstallCallback);
        } else
            Log.w(TAG, "removeContextPlugin called with null plug-in");
    }

    /**
     * Resends all cached SecuredEvents in the contextCache to the specified listener.
     */
    protected void resendCachedEvents(DynamixApplication app, IContextHandler handler, IContextListener listener) {
        doResendCachedEvents(app, handler, listener, null, -1);
    }

    /**
     * Resends the cached SecuredEvents in the contextCache to the specified listener that have occurred in the
     * specified number of previousMills.
     */
    protected void resendCachedEvents(DynamixApplication app, IContextHandler handler, IContextListener listener,
                                      int previousMills) {
        doResendCachedEvents(app, handler, listener, null, previousMills);
    }

    /**
     * Resends all cached SecuredEvents (of type contextType) in the contextCache to the specified listener.
     */
    protected void resendCachedEvents(DynamixApplication app, IContextHandler handler, IContextListener listener,
                                      String contextType) {
        doResendCachedEvents(app, handler, listener, contextType, -1);
    }

    /**
     * Resends the cached SecuredEvents (of type contextType) in the contextCache to the specified listener that have
     * occurred in the specified number of previousMills.
     */
    protected void resendCachedEvents(DynamixApplication app, IContextHandler handler, IContextListener listener,
                                      String contextType, int previousMills) {
        doResendCachedEvents(app, handler, listener, contextType, previousMills);
    }

    /**
     * Changes the ContextManager's PowerScheme to the specified newScheme. This method also tells each dependent
     * ContextPlugin to use the new PowerScheme.
     *
     * @param newScheme The new PowerScheme to use.
     */
    protected synchronized void setPowerScheme(PowerScheme newScheme) {
        // Remember the original PowerScheme for later state handling.
        PowerScheme originalScheme = scheme;
        // Save the new PowerScheme.
        scheme = newScheme;
        // Tell each ContextPluginRuntime to use the new PowerScheme
        for (ContextPluginRuntimeWrapper runtime : pluginMap.values()) {
            try {
                runtime.getContextPluginRuntime().setPowerScheme(newScheme);
            } catch (Exception e) {
                Log.w(TAG, runtime + " threw an exception during setPowerScheme: " + e.toString());
            }
        }
        // Restart if we were PowerScheme.MANUAL
        if (scheme != PowerScheme.MANUAL && originalScheme == PowerScheme.MANUAL)
            startContextManager();
    }

    /**
     * Starts context handling for all ContextPlugins.
     */
    protected synchronized void startContextManager() {
        synchronized (managerStartState) {
            if (managerStartState == StartState.STARTED)
                return;
            else if (managerStartState == StartState.STOPPING) {
                Log.w(TAG, "startContextManager called when in state: STOPPING");
                synchronized (cachedStartRequestLock) {
                    cachedStartRequest = true;
                }
            } else if (managerStartState == StartState.STOPPED || managerStartState == StartState.PAUSED) {
                Log.d(TAG, "ContextManager starting! PluginMap count: " + pluginMap.size());
                managerStartState = StartState.STARTING;
                // Start our contextCache
                contextCache.start();
                // Start our plug-ins
                synchronized (pluginMap) {
                    if (pluginMap.isEmpty()) {
                        // No plug-ins to start
                        synchronized (managerStartState) {
                            managerStartState = StartState.STARTED;
                        }
                    } else {
                        // Start each ContextPlugin we're managing.
                        for (ContextPlugin plug : pluginMap.keySet()) {
                            startPlugin(plug, false);
                        }
                    }
                }
                // Start Android Event providers
                synchronized (androidEventProviders) {
                    for (IAndroidEventProvider provider : androidEventProviders.values()) {
                        provider.start();
                    }
                }
            } else {
                Log.w(TAG, "Cannot start context manager from state: " + managerStartState + ", caching request");
                synchronized (cachedStartRequestLock) {
                    cachedStartRequest = true;
                }
            }
        }
    }

    /**
     * Start the specified ContextPlugin.
     */
    protected synchronized boolean startPlugin(ContextPlugin plug, boolean forceStart) {
        return startPlugin(plug, forceStart, null);
    }

    /**
     * Start the specified ContextPlugin.
     */
    protected synchronized boolean startPlugin(final ContextPlugin plug, final boolean forceStart,
                                               final ICallback callback) {
        Log.d(TAG, "startPlugin for: " + plug);
        String errorMessage = "";
        /*
         * Only start the ContextPlugin if the ContextManager is started or starting
		 */
        if (managerStartState == StartState.STARTED || managerStartState == StartState.STARTING) {
            /*
             * Only start background service plug-ins or plug-ins with 1 or more context support registrations.
			 */
            if (plug.isBackgroundService()
                    || SessionManager.getContextSupportCount(plug.getContextPluginInformation()) > 0) {
                // Access the plug-in's wrapper
                final ContextPluginRuntimeWrapper wrapper = pluginMap.get(plug);
                if (wrapper != null) {
                    // Access the plug-in's runtime
                    final ContextPluginRuntime runtime = wrapper.getContextPluginRuntime();
                    if (runtime != null) {
                        // Make sure that the plug-in is enabled
                        if (plug.isEnabled()) {
                            // Make sure that the plug-in is configured
                            if (plug.isConfigured()) {
                                // Handle start based on the wrapper's state
                                if (wrapper.getState() == PluginState.STARTING
                                        || wrapper.getState() == PluginState.STARTED) {
                                    Log.d(TAG,
                                            "Ignoring start since the ContextPlugin is starting or was already started: "
                                                    + wrapper.getState());
                                    SessionManager.sendCallbackSuccess(callback);
                                    return true;
                                } else if (wrapper.getState() == PluginState.INITIALIZED
                                        || wrapper.getState() == PluginState.ERROR) {
                                    Log.d(TAG, "Starting: " + runtime);
                                    // Set STARTING state
                                    wrapper.setState(PluginState.STARTING, true);
                                    final PluginLooperThread t = threadMap.get(plug);
                                    if (t != null) {
                                        t.handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    /*
                                                     * Set a default exception handler to catch any weird problems from
													 * the runtime.
													 */
                                                    t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                                                        @Override
                                                        public void uncaughtException(Thread thread, Throwable ex) {
                                                            Log.e(TAG,
                                                                    "ContextPluginRuntime uncaughtException: "
                                                                            + ex.getMessage());
                                                            ex.printStackTrace();
                                                            for (PluginLooperThread looperThread : threadMap.values()) {
                                                                if (thread.equals(looperThread)) {
                                                                    ContextPluginRuntimeWrapper problemWrapper = pluginMap
                                                                            .get(looperThread.getContextPlugin());
                                                                    problemWrapper.setState(PluginState.ERROR, true);
                                                                    disablePluginOnError(
                                                                            looperThread.getContextPlugin(),
                                                                            looperThread.getContextPlugin()
                                                                                    + " caused an error and was disabled",
                                                                            ErrorCodes.INTERNAL_PLUG_IN_ERROR, false);
                                                                    return;
                                                                }
                                                            }
                                                            Log.w(TAG, "Could not find problem plug-in for exception: "
                                                                    + ex);
                                                        }
                                                    });
                                                    // Check for a pending stop request
                                                    synchronized (pendingPluginStopMap) {
                                                        if (pendingPluginStopMap.keySet().contains(plug)) {
                                                            PendingStopActions actions = pendingPluginStopMap
                                                                    .remove(plug);
                                                            Log.w(TAG,
                                                                    "startPlugin found and is launching a pending stop for "
                                                                            + plug);
                                                            stopPlugin(plug, true, actions.destroy, actions.uninstall);
                                                            return;
                                                        }
                                                    }
													/*
													 * Make sure this context manager is still started or starting
													 * before starting the plug-in.
													 */
                                                    if (managerStartState == StartState.STARTED
                                                            || managerStartState == StartState.STARTING) {
														/*
														 * We need to set STARTED on the wrapper since the
														 * 'runtime.start()' method may block; however, the first time
														 * we don't send the state event, since context support entities
														 * waiting for an install need to have start() called on their
														 * runtime first. We call 'setState' again after 100ms (this
														 * time with the event) to make sure start is called before the
														 * event.
														 */
                                                        wrapper.setState(PluginState.STARTED, false);
                                                        updateManagerState();
                                                        wrapper.setExecuting(true);
														/*
														 * Fire the callback after 100ms to allow the start call below
														 * to execute first.
														 */
                                                        Utils.dispatch(true, new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                try {
                                                                    Thread.sleep(100);
                                                                } catch (InterruptedException e) {
                                                                }
                                                                SessionManager.sendCallbackSuccess(callback);
                                                                wrapper.setState(PluginState.STARTED, true);
                                                            }
                                                        });
														/*
														 * Note that start may block
														 */
                                                        Log.d(TAG, "Calling start on " + runtime);
                                                        runtime.start();
                                                        wrapper.setExecuting(false);
                                                    } else
                                                        throw new Exception(
                                                                "ContextManager was stopped before plug-in could be started");
                                                } catch (Exception e) {
                                                    Log.e(TAG, "ContextPluginRuntime Exception: " + e);
                                                    wrapper.setExecuting(false);
                                                    wrapper.setState(PluginState.ERROR, true);
                                                    // Fire callback
                                                    SessionManager.sendCallbackFailure(callback, e.toString(),
                                                            ErrorCodes.INTERNAL_PLUG_IN_ERROR);
                                                    disablePluginOnError(plug, plug
                                                                    + " caused an error and was disabled",
                                                            ErrorCodes.INTERNAL_PLUG_IN_ERROR, true);
                                                    updateManagerState();
                                                }
                                            }
                                        });
                                    } else {
                                        Log.w(TAG, "Could not find thread for: " + plug);
                                        SessionManager.sendCallbackFailure(callback, "Could not find thread for: "
                                                + plug, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                        // Update state
                                        updateManagerState();
                                        return false;
                                    }
                                    // Update state
                                    updateManagerState();
                                    // Return true
                                    return true;
                                } else
                                    errorMessage = "Cannot start " + plug + " from state: " + wrapper.getState();
                            } else
                                errorMessage = "Cannot start unconfigured plugin: " + plug;
                        } else
                            errorMessage = "Cannot start disabled plugin: " + plug;
                    } else
                        errorMessage = "Could not find runtime for " + plug;
                } else
                    errorMessage = "Could not find wrapper for: " + plug;
            } else {
				/*
				 * TODO: Is no context support registrations really a start failure? In this case, we don't set an
				 * errorMessage but do call fail on the callback.
				 */
            }
        } else
            errorMessage = "Cannot start " + plug + " when the context manager is in state " + managerStartState;
        // Update date
        updateManagerState();
        // Log error
        if (errorMessage != null && errorMessage.length() > 0)
            Log.w(TAG, errorMessage);
        // Fire callback
        SessionManager.sendCallbackFailure(callback, errorMessage, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
        // Return false
        return false;
    }

    /*
	 * Utility method that removes the specified context support, stopping the associated plug-ins if necessary.
	 */
    private synchronized Result doRemoveContextSupport(final ContextSupport support, final ICallback callback) {
        Log.d(TAG, "doRemoveContextSupport is processing " + support);
        // Set the context support inactive
        support.setActive(false);
        // Remove the context support from the session
        Result r = support.getSession().removeContextSupport(support);
        if (r.wasSuccessful()) {
            Log.d(TAG, "doRemoveContextSupport successfully removed context support from its session: " + support);
            if (support.getContextPlugins().size() == 0) {
                Log.w(TAG, "No plug-ins assigned to: " + support);
                // Nothing to remove, so send callback
                SessionManager.sendCallbackSuccess(callback);
            } else {
                // Dispatch the removal task in its own thread
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
						/*
						 * Remove the channel, if needed
						 */
                        if (support.hasContextListener()) {
                            // Remove the listener's channel
                            channelMap.remove(support.getSupportId());
                        }
						/*
						 * Create a list of plug-ins to stop, using the plug-ins assigned to the context support.
						 */
                        final List<ContextPlugin> plugsToStop = new ArrayList<ContextPlugin>();
                        plugsToStop.addAll(support.getContextPlugins());
                        for (final ContextPlugin plug : support.getContextPlugins()) {
                            // Remove each context listener, if needed
                            if (support.hasContextListener()) {
                                // Grab the wrapper
                                ContextPluginRuntimeWrapper wrapper = getContextPluginRuntimeWrapper(plug);
                                if (wrapper != null) {
                                    // Grab the runtime
                                    final ContextPluginRuntime runtime = wrapper.getContextPluginRuntime();
                                    if (runtime != null) {
                                        // Remove the listener using a thread to
                                        // prevent blocking
                                        Utils.dispatch(true, new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    runtime.removeContextListener(support.getSupportId());
                                                } catch (Exception e) {
                                                    Log.w(TAG, "Exception during doRemoveContextSupport: " + e);
                                                }
                                            }
                                        });
                                    } else {
                                        Log.w(TAG, "doRemoveContextSupport could not find runtime for " + plug);
                                    }
                                }
                            }
							/*
							 * Call stopPlugin if there are no more context support registrations and if the plug-in
							 * isn't a background service.
							 */
                            if (plug.getInstallStatus() == PluginInstallStatus.INSTALLED && !plug.isBackgroundService()
                                    && SessionManager.getContextSupportCount(plug.getContextPluginInformation()) == 0) {
                                Log.d(TAG, "Stopping plug-in because it has no more context support registrations: "
                                        + plug);
                                stopPlugin(plug, false, false, false, new Callback() {
                                    @Override
                                    public void onSuccess() throws RemoteException {
                                        synchronized (plugsToStop) {
                                            plugsToStop.remove(plug);
                                            if (plugsToStop.isEmpty()) {
                                                SessionManager.sendCallbackSuccess(callback);
												/*
												 * notifyContextSupportRemoved handled by
												 * DynamixSession.removeContextSupport
												 */
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(String message, int errorCode) throws RemoteException {
                                        Log.w(TAG, "stopPlugin.onFailure: " + message);
                                        synchronized (plugsToStop) {
                                            plugsToStop.remove(plug);
                                            if (plugsToStop.isEmpty()) {
                                                SessionManager.sendCallbackFailure(callback,
                                                        "Could not stop context support due to a plug-in error while stopping. Plugin "
                                                                + plug + " encoutered error " + message, errorCode);
                                            }
                                        }
                                    }
                                }, null);
                            } else {
                                synchronized (plugsToStop) {
                                    plugsToStop.remove(plug);
                                    if (plugsToStop.isEmpty()) {
                                        SessionManager.sendCallbackSuccess(callback);
										/*
										 * Note: notifyContextSupportRemoved handled by
										 * DynamixSession.removeContextSupport
										 */
                                    }
                                }
                            }
                        }
                    }
                });
            }
        } else {
            Log.w(TAG, "doRemoveContextSupport could not remove context support from its session: " + support);
            SessionManager.sendCallbackFailure(callback, r.getMessage(), r.getErrorCode());
        }
        return r;
    }

    /*
	 * Utility that returns the List of pending (i.e., known but not installed) ContextPlugins that support the
	 * specified pluginId (optional) and/or contextType.
	 */
    private synchronized List<ContextPlugin> getSupportingPendingPlugins(List<VersionedPlugin> plugins,
                                                                         String contextType, boolean addAllContextTypes) {
        List<ContextPlugin> compatiblePlugs = new ArrayList<ContextPlugin>();
        // Check if auto-install is allowed
        if (DynamixPreferences.autoContextPluginInstallEnabled()) {
            Log.d(TAG, "getSupportingPendingPlugins for context type " + contextType);
            // Check through the previously discovered plug-ins that have not
            // yet been installed
            for (PendingContextPlugin discoveryResult : UpdateManager.getPendingContextPlugins()) {
                for (VersionedPlugin pluginId : plugins) {
                    // Check if the plug-in matches the context support request
                    if (isPluginMatch(pluginId, discoveryResult.getPendingContextPlugin(), contextType,
                            addAllContextTypes)) {
                        ContextPlugin targetPlug = discoveryResult.getPendingContextPlugin();
                        if (targetPlug.getRepoSource().isNetworkSource() && Utils.isWaitingForWifi()) {
							/*
							 * Target is compatible but remote and Dynamix isn't allowed to access the cell network
							 */
                            Log.w(TAG,
                                    "Plug-in is able to handle context support, but cannot be installed because Dynamix cannot access the network: "
                                            + targetPlug);
                            targetPlug.setInstallStatus(PluginInstallStatus.CANNOT_ACCESS_NETWORK);
                        } else {
							/*
							 * If the plug-in previously had state CANNOT_ACCESS_NETWORK, set it to NOT_INSTALLED now
							 * that we're allowed to access the network.
							 */
                            if (targetPlug.getInstallStatus() == PluginInstallStatus.CANNOT_ACCESS_NETWORK)
                                targetPlug.setInstallStatus(PluginInstallStatus.NOT_INSTALLED);
                        }
                        // Add the plug-in to the install list
                        compatiblePlugs.add(targetPlug);
                    }
                }
            }
        } else
            Log.w(TAG,
                    "Not checking repositories for plug-ins since 'Auto Plug-in Install' is disabled in the settings");
        return compatiblePlugs;
    }

    /*
	 * Utility that returns true if the pluginId equals the testPlug AND the testPlug supports the specified
	 * contextType.
	 */
    private boolean isPluginMatch(VersionedPlugin originalPlug, ContextPlugin comparisonPlug, String contextType,
                                  boolean addAllTypesForPlugin) {
        // Check for specified plugin id
        if (originalPlug != null) {
            if (!originalPlug.hasVersion()) {
                if (comparisonPlug.getId().equalsIgnoreCase(originalPlug.getPluginId()))
                    // Check for context support
                    return addAllTypesForPlugin || comparisonPlug.supportsContextType(contextType);
            } else {
                if (comparisonPlug.getId().equalsIgnoreCase(originalPlug.getPluginId())
                        && comparisonPlug.getVersion().equals(originalPlug.getPluginVersion()))
                    // Check for context support
                    return addAllTypesForPlugin || comparisonPlug.supportsContextType(contextType);
            }
        } else {
            // Handle no plugin id case
            if (comparisonPlug.supportsContextType(contextType))
                return true;
        }
        return false;
    }

    /*
	 * Updates the ContextManager's StartState based on the plug-ins in the pluginMap.
	 */
    private synchronized void updateManagerState() {
        synchronized (managerStartState) {
			/*
			 * We have to sync on pluginMap since there may be multiple PluginStoppers running simultaneously.
			 */
            synchronized (pluginMap) {
				/*
				 * Handle stopping
				 */
                if (managerStartState == StartState.STOPPING) {
					/*
					 * When stopping, we only set started to false when ALL plugins have been removed.
					 */
                    if (pluginMap.isEmpty()) {
                        managerStartState = StartState.STOPPED;
                        synchronized (cachedStartRequestLock) {
                            if (cachedStartRequest) {
                                Log.d(TAG, "Processing cached start request from STOPPED");
                                cachedStartRequest = false;
                                startContextManager();
                            }
                        }
                    } else {
                        for (ContextPlugin plug : pluginMap.keySet())
                            Log.d(TAG, "Waiting for plug-in to stop: " + plug);
                    }
                }
				/*
				 * Handle stopped
				 */
                else if (managerStartState == StartState.STOPPED) {
                    synchronized (cachedStartRequestLock) {
                        if (cachedStartRequest) {
                            cachedStartRequest = false;
                            startContextManager();
                        }
                    }
                }
				/*
				 * Handle pausing
				 */
                else if (managerStartState == StartState.PAUSING) {
                    if (pluginMap.isEmpty()) {
                        managerStartState = StartState.PAUSED;
                        synchronized (cachedStartRequestLock) {
                            if (cachedStartRequest) {
                                Log.d(TAG, "Processing cached start request from PAUSED");
                                cachedStartRequest = false;
                                startContextManager();
                            }
                        }
                    } else {
                        boolean allPluginsPaused = true;
                        // Check for pause state on the plug-in
                        for (ContextPluginRuntimeWrapper wrapper : pluginMap.values()) {
                            if (!wrapper.getParentPlugin().isBackgroundService()) {
                                if (wrapper.getState() != PluginState.INITIALIZED
                                        && wrapper.getState() != PluginState.ERROR
                                        && wrapper.getState() != PluginState.NEW) {
                                    Log.d(TAG, "Waiting for plug-in to stop (for pause): " + wrapper.getParentPlugin()
                                            + ", which is in state " + wrapper.getState());
                                    allPluginsPaused = false;
                                }
                            } else
                                Log.d(TAG, "Not stopping: " + wrapper.getParentPlugin()
                                        + ", since it is a background service and is in state " + wrapper.getState());
                        }
                        if (allPluginsPaused) {
                            managerStartState = StartState.PAUSED;
                            synchronized (cachedStartRequestLock) {
                                if (cachedStartRequest) {
                                    cachedStartRequest = false;
                                    startContextManager();
                                }
                            }
                        }
                    }
                }
				/*
				 * Handle paused
				 */
                else if (managerStartState == StartState.PAUSED) {
                    synchronized (cachedStartRequestLock) {
                        if (cachedStartRequest) {
                            cachedStartRequest = false;
                            startContextManager();
                        }
                    }
                }
				/*
				 * Handle starting
				 */
                else if (managerStartState == StartState.STARTING) {
                    // Assume we're started at first
                    boolean started = true;
                    for (ContextPlugin plug : pluginMap.keySet()) {
                        // We only consider enabled plug-ins when starting
                        if (plug.isEnabled()) {
                            // Check for configured status
                            if (plug.isConfigured()) {
                                ContextPluginRuntimeWrapper runtime = pluginMap.get(plug);
                                // Make sure the plug-in does not have an error
                                if (runtime.getState() != PluginState.ERROR) {
									/*
									 * We only consider plug-ins that have at least one context support registration.
									 * Otherwise, they are not started to conserve power, unless the plug-in is a
									 * background service.
									 */
                                    if (plug.isBackgroundService()
                                            || SessionManager.hasContextSupportRegistrations(plug
                                            .getContextPluginInformation()))
                                        // Check if the plug-in is already
                                        // started
                                        if (runtime.getState() != PluginState.STARTED) {
                                            Log.d(TAG, "Waiting for plug-in to start: " + plug);
                                            // We have a plug-in that has not
                                            // started yet
                                            started = false;
                                            break;
                                        }
                                }
                            }
                        } else
                            Log.d(TAG, "Plug-in is disabled: " + plug);
                    }
                    if (started) {
                        managerStartState = StartState.STARTED;
                    }
                }
				/*
				 * Handle started
				 */
                else if (managerStartState == StartState.STARTED) {
                    // Do nothing
                } else {
                    Log.e(TAG, "Unhandled state: " + managerStartState);
                }
            }
        }
    }

    /**
     * Pauses context handling for all ContextPlugins without removing them from management or destroying associated
     * ContextPluginRuntimes. Call startContextHandling to re-start the ContextManager. Call stopContextHandling to
     * completely reset ContextManager state.
     */
    protected synchronized void pauseContextHandling() {
        // Remove potentially cached start request
        synchronized (cachedStartRequestLock) {
            cachedStartRequest = false;
        }
        // Update state
        updateManagerState();
        synchronized (managerStartState) {
            // Only pause if started
            if (managerStartState == StartState.STARTED) {
                Log.d(TAG, "ContextManager is pausing...");
                managerStartState = StartState.PAUSING;
                // Stop Android Event providers
                synchronized (androidEventProviders) {
                    for (IAndroidEventProvider provider : androidEventProviders.values()) {
                        provider.stop();
                    }
                }
                // Stop each ContextPlugin that we're managing.
                synchronized (pluginMap) {
                    for (ContextPlugin plug : pluginMap.keySet()) {
                        if (!plug.isBackgroundService()) {
                            stopPlugin(plug, false, false, false);
                        }
                    }
                }
                // Maintain channelMap during pause
                // channelMap.clear();
                if (progressMonitorTimer == null) {
                    progressCount = 0;
					/*
					 * Launch the progressMonitorTimer to check for stopped state
					 */
                    progressMonitorTimer = new Timer();
                    progressMonitorTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            checkStopped();
                        }
                    }, 0, 500);
                } else {
                    Log.w(TAG, "progressMonitorTimer was not null in pauseContextHandling");
                }
            } else
                Log.w(TAG, "Cannot pause context detection from state " + managerStartState);
        }
    }

    /**
     * Stops the ContextManager. Removes all ContextPlugins from the pluginMap and destroys all associated
     * ContextPluginRuntimes.
     */
    protected synchronized void stopContextHandling() {
		/*
		 * TODO We may be left in an inconsistent state if all plug-ins don't properly stop.
		 */
        // Remove potentially cached start request
        synchronized (cachedStartRequestLock) {
            cachedStartRequest = false;
        }
        // Update state
        updateManagerState();
        synchronized (managerStartState) {
            // Only stop if starting, started, or paused
            if (managerStartState == StartState.STARTED || managerStartState == StartState.STARTING
                    || managerStartState == StartState.PAUSED) {
                managerStartState = StartState.STOPPING;
                // Stop the context cache (removes cached events)
                contextCache.stop();
                // Destroy all available plug-ins
                synchronized (pluginMap) {
                    if (pluginMap.values() != null && !pluginMap.isEmpty()) {
                        for (ContextPlugin plug : pluginMap.keySet())
                            stopPlugin(plug, true, true, false);
                    } else {
                        Log.d(TAG, "No context plug-ins to stop");
                        synchronized (managerStartState) {
                            managerStartState = StartState.STOPPED;
                        }
                    }
                }
                // Clear the request cache
                channelMap.clear();
                if (progressMonitorTimer == null) {
                    progressCount = 0;
					/*
					 * Launch the progressMonitorTimer to check for stopped state
					 */
                    progressMonitorTimer = new Timer();
                    progressMonitorTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            checkStopped();
                        }
                    }, 0, 500);
                } else {
                    Log.w(TAG, "progressMonitorTimer was not null in stopContextHandling");
                }
            } else
                Log.w(TAG, "stopContextHandling called when we were not active. State was: " + managerStartState);
        }
    }

    /**
     * Clears the list of mapped InternalFacadeBinders.
     */
    protected void clearInternalFacadeBinders() {
        facadeBinderMap.clear();
    }

    /**
     * Stops the specified ContextPlugin, removing any cached context events and statistics.
     */
    protected synchronized void stopPlugin(ContextPlugin plug, boolean clearCachedEvents, boolean destroy,
                                           boolean uninstall) {
        stopPlugin(plug, clearCachedEvents, destroy, uninstall, new ArrayList<ICallback>(), new ArrayList<ICallback>());
    }

    /**
     * Stops the specified ContextPlugin.
     */
    protected synchronized void stopPlugin(ContextPlugin plug, boolean clearCachedEvents, boolean destroy,
                                           boolean uninstall, ICallback stopCallback, ICallback uninstallCallback) {
        List<ICallback> uninstallCallbacks = new ArrayList<ICallback>();
        uninstallCallbacks.add(uninstallCallback);
        List<ICallback> stopCallbacks = new ArrayList<ICallback>();
        stopCallbacks.add(stopCallback);
        stopPlugin(plug, clearCachedEvents, destroy, uninstall, stopCallbacks, uninstallCallbacks);
    }

    /**
     * Stops the specified ContextPlugin.
     */
    protected synchronized void stopPlugin(ContextPlugin plug, boolean clearCachedEvents, boolean destroy,
                                           boolean uninstall, List<ICallback> stopCallbacks, List<ICallback> uninstallCallbacks) {
        Log.d(TAG, "stopPlugin for: " + plug + " with destroy: " + destroy + " and uninstall " + uninstall);
        if (plug != null) {
            // Make sure destroy is set for uninstalls
            if (uninstall)
                destroy = true;
            // Access the ContextPlugin's runtime wrapper
            ContextPluginRuntimeWrapper wrapper = pluginMap.get(plug);
            // Make sure we get a wrapper
            if (wrapper != null) {
                Log.d(TAG, "stopPlugin for: " + plug + " found  plug-in at state " + wrapper.getState());
                // Set UNINSTALLING state, if needed
                if (uninstall)
                    plug.setInstallStatus(PluginInstallStatus.UNINSTALLING);
				/*
				 * Remove any cached events from the contextCache for the plug-in, if requested
				 */
                if (clearCachedEvents)
                    contextCache.removeContextEvents(plug);
                // Handle wrapper states
                if (wrapper.getState() == PluginState.NEW) {
					/*
					 * For new plug-ins, just remove from the pluginMap when destroying, since they don't have resources
					 * attached to them.
					 */
                    if (destroy || uninstall) {
                        pluginMap.remove(plug);
                    }
                    // Fire stopped callbacks
                    if (stopCallbacks != null)
                        for (ICallback cb : stopCallbacks)
                            SessionManager.sendCallbackSuccess(cb);
                    // Handle uninstall, if needed
                    if (uninstall) {
                        plug.setInstallStatus(PluginInstallStatus.NOT_INSTALLED);
                        DynamixService.removeUninstalledContextPlugin(plug, uninstallCallbacks);
                    }
                }
                // Handle STARTED, INITIALIZED and ERROR states
                else if (wrapper.getState() == PluginState.STARTED || wrapper.getState() == PluginState.INITIALIZED
                        || wrapper.getState() == PluginState.ERROR) {
                    PluginLooperThread t = threadMap.get(plug);
                    PlugStopper stopper = new PlugStopper(plug, wrapper, t, context, destroy, uninstall,
                            wrapper.getState(), stopCallbacks, uninstallCallbacks);
                    wrapper.setState(PluginState.STOPPING, true);
                    stopperMap.put(plug, stopper);
                    stopper.launch();
                }
                // Handle INITIALIZING and STARTING states
                else if (wrapper.getState() == PluginState.INITIALIZING || wrapper.getState() == PluginState.STARTING) {
					/*
					 * Since there is an asynchronous init or start process running, we create a pending stop request
					 * that will run once the init or start has completed.
					 */
                    synchronized (pendingPluginStopMap) {
                        if (pendingPluginStopMap.containsKey(plug)) {
                            // Update the existing pending stop request
                            PendingStopActions pendingStopRequests = pendingPluginStopMap.get(plug);
                            pendingStopRequests.destroy = destroy;
                            pendingStopRequests.uninstall = uninstall;
                            if (stopCallbacks != null)
                                stopCallbacks.addAll(stopCallbacks);
                            if (uninstallCallbacks != null)
                                pendingStopRequests.uninstallCallbacks.addAll(uninstallCallbacks);
                        } else {
                            // Add the new pending stop request
                            PendingStopActions newPendingStop = new PendingStopActions();
                            newPendingStop.destroy = destroy;
                            newPendingStop.uninstall = uninstall;
                            if (stopCallbacks != null)
                                newPendingStop.stopCallbacks.addAll(stopCallbacks);
                            if (uninstallCallbacks != null)
                                newPendingStop.uninstallCallbacks.addAll(uninstallCallbacks);
                            pendingPluginStopMap.put(plug, newPendingStop);
                        }
                    }
                }
                // Handle the STOPPING state
                else if (wrapper.getState() == PluginState.STOPPING) {
                    Log.d(TAG, "Already stopping: " + plug);
                    // Try to access the existing plug stopper
                    PlugStopper existingStopper = stopperMap.get(plug);
                    if (existingStopper != null) {
						/*
						 * Update the existing stopper with the incoming stop callbacks.
						 */
                        if (stopCallbacks != null) {
                            for (ICallback cb : stopCallbacks)
                                existingStopper.addStopCallback(cb);
                        }
						/*
						 * Update the existing stopper with the incoming uninstall callbacks.
						 */
                        if (uninstall && uninstallCallbacks != null) {
                            for (ICallback cb : uninstallCallbacks)
                                existingStopper.addUninstallCallback(cb);
                        }
                        // Check for conflicting state
                        if ((uninstall && !existingStopper.uninstall) || (destroy && !existingStopper.destroy)) {
                            Log.w(TAG, "PlugStopper conflict: Existing stopper had destroy " + existingStopper.destroy
                                    + " and uninstall " + existingStopper.uninstall
                                    + " while incoming stopper had destroy " + destroy + " and uninstall " + uninstall
                                    + " for plug-in: " + plug);
							/*
							 * We have the situation where an existing PlugStopper is not compatible with the new
							 * incoming request. For example, the existing PlugStopper running in "stop" mode, but a new
							 * PlugStopper was requested in "destroy" mode. This is tricky to handle because
							 * PlugStoppers set for "stop" mode automatically reinitialize the plug-in once complete,
							 * which is done asynchronously. In such cases, we set a pending destroy, which is handled
							 * after the plug-in re-initializes when its original PlugStopper completes.
							 */
                            synchronized (pendingPluginStopMap) {
                                if (pendingPluginStopMap.containsKey(plug)) {
                                    // Update the existing request
                                    Log.w(TAG, "Updating an existing pending stop request for " + plug);
                                    PendingStopActions existing = pendingPluginStopMap.get(plug);
                                    existing.destroy = destroy;
                                    existing.uninstall = uninstall;
                                    if (stopCallbacks != null)
                                        existing.stopCallbacks.addAll(stopCallbacks);
                                    if (uninstallCallbacks != null)
                                        existing.uninstallCallbacks.addAll(uninstallCallbacks);
                                } else {
                                    // Add the new stop request
                                    Log.w(TAG, "Adding a new pending stop request for " + plug);
                                    PendingStopActions actions = new PendingStopActions();
                                    actions.destroy = destroy;
                                    actions.uninstall = uninstall;
                                    if (stopCallbacks != null)
                                        actions.stopCallbacks.addAll(stopCallbacks);
                                    if (uninstallCallbacks != null)
                                        actions.uninstallCallbacks.addAll(uninstallCallbacks);
                                    pendingPluginStopMap.put(plug, actions);
                                }
                            }
                        } else
                            Log.d(TAG, "Already destroying: " + plug);
                    } else {
                        Log.w(TAG, "State STOPPING but no PlugStopper was found for: " + plug);
                        // Fire stop callbacks with the failure
                        if (stopCallbacks != null) {
                            for (ICallback cb : stopCallbacks)
                                SessionManager.sendCallbackFailure(cb,
                                        "State STOPPING but no PlugStopper was found for: " + plug,
                                        ErrorCodes.NOT_FOUND);
                        }
                        // Fire uninstall callbacks with the failure (if needed)
                        if (uninstall && uninstallCallbacks != null) {
                            for (ICallback cb : uninstallCallbacks)
                                SessionManager.sendCallbackFailure(cb,
                                        "State STOPPING but no PlugStopper was found for: " + plug,
                                        ErrorCodes.NOT_FOUND);
                        }
                    }
                } else {
                    Log.w(TAG, "Cannot stop plugin " + plug + " from state " + wrapper.getState());
                    // Fire stop callbacks with the failure
                    if (stopCallbacks != null) {
                        for (ICallback cb : stopCallbacks)
                            SessionManager.sendCallbackFailure(cb, "STATE_ERROR when stopping: " + plug,
                                    ErrorCodes.STATE_ERROR);
                    }
                    // Fire uninstall callbacks with the failure (if needed)
                    if (uninstall && uninstallCallbacks != null) {
                        for (ICallback cb : uninstallCallbacks)
                            SessionManager.sendCallbackFailure(cb, "STATE_ERROR when uninstalling: " + plug,
                                    ErrorCodes.STATE_ERROR);
                    }
                }
            } else {
                Log.d(TAG, "stopPlugin found no runtime for: " + plug + " | " + plug.getInstallStatus());
                // Fire stop callbacks
                if (stopCallbacks != null)
                    for (ICallback cb : stopCallbacks)
                        SessionManager.sendCallbackSuccess(cb);
				/*
				 * Handle uninstall, if needed. Note that the DynamixService.removeUninstalledContextPlugin will fire
				 * the uninstall callbacks when completed.
				 */
                if (uninstall) {
                    DynamixService.removeUninstalledContextPlugin(plug, uninstallCallbacks);
                }
            }
        } else {
            Log.w(TAG, "stopPlugin received null plug-in: perhaps the plug-in was already removed.");
            if (stopCallbacks != null)
                for (ICallback cb : stopCallbacks)
                    SessionManager.sendCallbackSuccess(cb);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public ContextPluginInformation getPluginInfo(UUID sessionID) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            return plug.getContextPluginInformation();
        } else {
            Log.w(TAG, "No plug-in found for sessionId " + sessionID);
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean addDynamixEventListener(UUID sessionID, String eventType, IDynamixEventListener listener) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            if (listener != null) {
                synchronized (dynamixEventListeners) {
                    if (dynamixEventListeners.containsKey(plug)) {
                        Map<String, IDynamixEventListener> listeners = dynamixEventListeners.get(plug);
                        if (!listeners.containsValue(listener)) {
                            listeners.put(eventType, listener);
                        } else
                            Log.d(TAG, "addAndroidEventListener IDynamixEventListener already registered for: " + plug);
                    } else {
                        Map<String, IDynamixEventListener> listeners = new HashMap<>();
                        listeners.put(eventType, listener);
                        dynamixEventListeners.put(plug, listeners);
                    }
                    // Setup Android event provider, if needed
                    synchronized (androidEventProviders) {
                        Log.d(TAG, "addAndroidEventListener checking for IAndroidEventProvider for " + eventType);
                        if (!androidEventProviders.containsKey(eventType)) {
                            IAndroidEventProvider provider = AndroidEventProviderFactory.getProvider(context, eventType);
                            Log.d(TAG, "addAndroidEventListener found no IAndroidEventProvider for " + eventType + ", created one for this type: " + provider);
                            if (provider != null) {
                                Log.d(TAG, "addAndroidEventListener starting Android Event Provider: " + provider);
                                provider.start();
                                androidEventProviders.put(eventType, provider);
                            } else
                                Log.w(TAG, "addAndroidEventListener cannot find IAndroidEventProvider for type " + eventType);
                        } else
                            Log.d(TAG, "addAndroidEventListener found androidEventProviders has existing provider for: " + eventType + ", " + androidEventProviders.get(eventType));
                    }
                }
            } else
                Log.w(TAG, "addAndroidEventListener received null listener from " + plug);
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean addDynamixEventListener(UUID sessionID, Bundle eventConfiguration, IDynamixEventListener listener) {
        /*
         * Not implemented yet. Use the string-based version for now.
         */
        throw new NotImplementedException();
    }

    /**
     * Removes all Android Event listeners registered for the specified plug-in.
     *
     * @param plug      The plug-in to remove Android listeners for.
     * @param sessionID The plug-in's current runtime session id.
     * @return True on success; false otherwise.
     */
    private boolean removeAllAndroidEventListeners(ContextPlugin plug, UUID sessionID) {
        Log.d(TAG, "removeAllAndroidEventListeners for  " + plug);
        synchronized (dynamixEventListeners) {
            if (dynamixEventListeners.containsKey(plug)) {
                // Access the android event listener map for the plug-in
                Map<String, IDynamixEventListener> listeners = dynamixEventListeners.get(plug);
                for (IDynamixEventListener listener : listeners.values())
                    removeDynamixEventListener(sessionID, listener);
                return true;
            } else
                Log.d(TAG, "removeAllAndroidEventListeners could not find android event listeners for  " + plug);
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean removeDynamixEventListener(UUID sessionID, IDynamixEventListener listener) {
        Log.d(TAG, "removeDynamixEventListener for " + listener);
        // Variable for the target android event type of the listener
        String targetAndroidEventType = null;
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            // Check if the dynamixEventListeners contains the plug-in
            synchronized (dynamixEventListeners) {
                if (dynamixEventListeners.containsKey(plug)) {
                    // Access the android event listener map for the plug-in
                    Map<String, IDynamixEventListener> listeners = dynamixEventListeners.get(plug);
                    // Check if the listener map contains the listener to remove
                    for (String listenerKey : listeners.keySet()) {
                        if (listeners.get(listenerKey).equals(listener)) {
                            // The 'listenerKey' is the 'targetAndroidEventType'
                            targetAndroidEventType = listenerKey;
                            break;
                        }
                    }
                    Log.d(TAG, "removeDynamixEventListener found listener to potentially remove " + listener);
                    // Remove the listener, if found
                    if (targetAndroidEventType != null) {
                        IDynamixEventListener removed = listeners.remove(targetAndroidEventType);
                        Log.d(TAG, "removeDynamixEventListener removed Android event listener " + removed);
                        /**
                         * Finally, loop through the dynamixEventListeners map again, checking if the
                         * targetAndroidEventType still requires an Android Event Provider.
                         */
                        for (Map<String, IDynamixEventListener> map : dynamixEventListeners.values()) {
                            if (map.containsKey(targetAndroidEventType)) {
                                // Return, since we still need the mapped event provider
                                Log.d(TAG, "removeDynamixEventListener retained event provider " + map.get(targetAndroidEventType) + " for type " + targetAndroidEventType);
                                return true;
                            }
                        }
                        /**
                         * If we reach this point, the android event provider mapped to targetAndroidEventType
                         * is no longer needed, so remove it.
                         */
                        synchronized (androidEventProviders) {
                            Log.d(TAG, "removeDynamixEventListener trying to remove provider for " + targetAndroidEventType);
                            if (androidEventProviders.containsKey(targetAndroidEventType)) {
                                IAndroidEventProvider provider = androidEventProviders.get(targetAndroidEventType);
                                if (provider != null) {
                                    Log.d(TAG, "removeDynamixEventListener is stopping and removing " + provider + " for type " + targetAndroidEventType);
                                    provider.stop();
                                    androidEventProviders.remove(targetAndroidEventType);
                                } else
                                    Log.w(TAG, "removeDynamixEventListener cannot find IAndroidEventProvider to remove for type " + targetAndroidEventType);
                            } else
                                Log.w(TAG, "removeDynamixEventListener could not find IAndroidEventProvider to remove for type " + targetAndroidEventType);
                        }
                    }
                } else
                    Log.w(TAG, "removeDynamixEventListener could not find a plug-in mapped to listener  " + listener);
            }
        } else
            Log.w(TAG, "removeDynamixEventListener did not find any listeners registered: " + plug);
        return false;
    }

    /**
     * @inheritDoc
     * @deprecated
     */
    @Override
    public boolean addNfcListener(UUID sessionID, NfcListener listener) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            if (listener != null) {
                synchronized (nfcListeners) {
                    if (nfcListeners.containsKey(plug)) {
                        List<NfcListener> listeners = nfcListeners.get(plug);
                        if (!listeners.contains(listener)) {
                            listeners.add(listener);
                        } else
                            Log.w(TAG, "NfcListener already registered for: " + plug);
                    } else {
                        List<NfcListener> listeners = new ArrayList<NfcListener>();
                        listeners.add(listener);
                        nfcListeners.put(plug, listeners);
                    }
                }
            } else
                Log.w(TAG, "addNfcListener received null listener from " + plug);
        }
        return false;
    }

    /**
     * @inheritDoc
     * @deprecated
     */
    @Override
    public boolean removeNfcListener(UUID sessionID, NfcListener listener) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            synchronized (nfcListeners) {
                if (nfcListeners.containsKey(plug)) {
                    List<NfcListener> listeners = nfcListeners.get(plug);
                    if (listeners.contains(listener)) {
                        return listeners.remove(listener);
                    } else
                        Log.w(TAG, "Could not find specified listener to remove for: " + plug);
                } else
                    Log.w(TAG, "Plugin did not have any listeners registered: " + plug);
            }
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void dynamixSystemLevelOperation(final UUID sessionID, final Bundle operationDetails, final Callback callback) {
        doDynamixSystemLevelOperation(sessionID, operationDetails, callback, true);
    }

    private interface ResultRunnable extends Runnable {
        Result getResult();

        void setLock(Object lock);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Result dynamixSystemLevelOperation(final UUID sessionID, final Bundle operationDetails) {
        // Create a lock for this operation
        final Object l = new Object();
        // Create a ResultRunnable to handle the request while we block the calling Thread.
        final ResultRunnable runner = new ResultRunnable() {
            Result result = null;
            Object lock = null;

            @Override
            public void run() {
                doDynamixSystemLevelOperation(sessionID, operationDetails, new Callback() {
                    @Override
                    public void onSuccess() throws RemoteException {
                        if (getResult() != null)
                            result = new Result(getResult().getString("result"), ErrorCodes.SUCCESS);
                        else
                            result = new Result();
                        synchronized (lock) {
                            lock.notify();
                        }
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        result = new Result(message, errorCode);
                        synchronized (lock) {
                            lock.notify();
                        }
                    }
                }, true);
            }

            @Override
            public Result getResult() {
                return result;
            }

            @Override
            public void setLock(Object lock) {
                this.lock = lock;
            }
        };
        // Give the lock to the runner
        runner.setLock(l);
        // Fire off a thread to handle the runner
        Utils.dispatch(true, runner);
        // Wait on the lock (will be notified by the runner when complete)
        synchronized (l) {
            try {
                l.wait();
            } catch (InterruptedException ie) {
            }
        }
        // Return the result from the runner
        return runner.getResult();
    }

    /**
     * Handles the logic of doDynamixSystemLevelOperation.
     */
    private void doDynamixSystemLevelOperation(final UUID sessionID, final Bundle operationDetails, final Callback callback, boolean async) {
        Utils.dispatch(async, new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "dispatch doDynamixSystemLevelOperation running on thread " + Thread.currentThread());
                // Get the ContextPlugin using the secure sessionId
                ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
                // If we get a plug, we have a valid UUID, so continue
                if (plug != null) {
                    /*
                     * Make sure to check that the plug-in has SystemPrivilege.FRAMEWORK or this is not a production release
                     */
                    if (plug.getSystemPrivilege() == SystemPrivilege.FRAMEWORK || !FrameworkConstants.PRODUCTION_RELEASE) {
                        Log.w(TAG, "doDynamixSystemLevelOperation addDynamixApplication: Plug-in verification disabled for testing. Enable for production!");
                        // Check operation
                        String operation = operationDetails.getString(SystemOperations.OPERATION);
                        // Handle the different SystemOperations
                        switch (operation) {
                            case SystemOperations.AddDynamixApplication.ADD_DYNAMIX_APPLICATION_OPERATION:
                                /**
                                 * Access the ADD_DYNAMIX_APPLICATION_OPERATION arguments
                                 */
                                String lookupKey = operationDetails.getString(SystemOperations.AddDynamixApplication.LOOKUP_KEY);
                                String appName = operationDetails.getString(SystemOperations.AddDynamixApplication.APP_NAME);
                                String type = operationDetails.getString(SystemOperations.AddDynamixApplication.APP_TYPE);
                                String pairingCode = operationDetails.getString(SystemOperations.AddDynamixApplication.PAIRING_CODE, null);
                                boolean temporary = operationDetails.getBoolean(SystemOperations.AddDynamixApplication.TEMPORARY, false);
                                APP_TYPE app_type = APP_TYPE.valueOf(type);
                                // Make sure we have at least an id
                                if (pairingCode != null) {
                                    // Create a name if the caller doesn't provide one
                                    if (appName == null)
                                        appName = "Remotely Paired App";
                                    // Create app (using a randome UUID for the appId in this case)
                                    final DynamixApplication app = new DynamixApplication(app_type, UUID.randomUUID().toString(), appName);
                                    // Add pairing code, if needed
                                    if (pairingCode != null) {
                                        try {
                                            RemotePairing p = new RemotePairing(pairingCode, lookupKey, temporary);
                                            // Set the remote pairing, which kicks off a server update
                                            app.setRemotePairing(p, new Callback() {
                                                @Override
                                                public void onSuccess() throws RemoteException {
                                                    // Add the new Dynamix Application to the settings
                                                    if (WebConnector.addTempPairedApp(app))
                                                        //if (DynamixService.addDynamixApplicationToSettings(app))
                                                        SessionManager.sendCallbackSuccess(callback);
                                                    else
                                                        SessionManager.sendCallbackFailure(callback, "Could not add Dynamix app to settings.", ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                                }

                                                @Override
                                                public void onFailure(String message, int errorCode) throws RemoteException {
                                                    SessionManager.sendCallbackFailure(callback, message, errorCode);
                                                }
                                            });
                                        } catch (Exception e) {
                                            String message = e.toString();
                                            Log.w(TAG, message);
                                            SessionManager.sendCallbackFailure(callback, message, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                        }
                                    }
                                } else {
                                    String message = "ADD_DYNAMIX_APPLICATION_OPERATION: missing app id and/or seed!";
                                    Log.w(TAG, message);
                                    SessionManager.sendCallbackFailure(callback, message, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                }
                            case SystemOperations.GetFrameworkInfo.INSTANCE_ID:
                                Bundle result = new Bundle();
                                result.putString("result", DynamixPreferences.getDynamixInstanceId());
                                SessionManager.sendCallbackSuccess(callback);
                            default:
                                SessionManager.sendCallbackFailure(callback, "Unsupported System Operation: " + operation, ErrorCodes.CONFIGURATION_ERROR);
                        }
                    } else {
                        String message = "NOT_AUTHORIZED: Plug-in does not have SystemPrivilege.FRAMEWORK";
                        Log.w(TAG, message);
                        SessionManager.sendCallbackFailure(callback, message, ErrorCodes.NOT_AUTHORIZED);
                    }
                } else {
                    String message = "addDynamixApplication: Session ID NOT_AUTHORIZED!";
                    Log.w(TAG, message);
                    SessionManager.sendCallbackFailure(callback, message, ErrorCodes.NOT_AUTHORIZED);
                }
            }
        });
    }

    /**
     * Dispatches a NFC Intent to registered NFC listeners.
     *
     * @param i The NFC Intent.
     * @deprecated Use 'dispatchDynamixEvent' instead.
     */
    protected void dispatchNfcEvent(final Intent i) {
        Log.v(TAG, "dispatchNfcEvent " + i);
        if (isStarted()) {
            synchronized (nfcListeners) {
                for (ContextPlugin plug : nfcListeners.keySet()) {
                    List<NfcListener> listeners = nfcListeners.get(plug);
                    if (listeners != null) {
                        for (NfcListener listener : listeners) {
                            final NfcListener finalListener = listener;
							/*
							 * Use a daemon thread to send the event to listeners just in case the listener hangs.
							 */
                            Utils.dispatch(true, new Runnable() {
                                @Override
                                public void run() {
                                    finalListener.onNfcEvent(i);
                                }
                            });
                        }
                    }
                }
            }
        } else
            Log.w(TAG, "Not dispatching NFC event because we're not started");
    }

    /**
     * Dispatches a Dynamix event to registered listeners.
     *
     * @param event The event to send.
     */
    protected void dispatchDynamixEvent(final Intent event) {
        Log.v(TAG, "dispatchDynamixEvent " + event);
        if (isStarted()) {
            synchronized (dynamixEventListeners) {
                for (ContextPlugin plug : dynamixEventListeners.keySet()) {
                    // Grab the listeners registered to the plug-in
                    Map<String, IDynamixEventListener> listeners = dynamixEventListeners.get(plug);
                    if (listeners != null) {
                        // Grab the listener that matches the incoming event type
                        final IDynamixEventListener listener = listeners.get(event.getAction());
                        if (listener != null) {
							/*
							 * Use a daemon thread to send the event to listeners just in case the listener hangs.
							 */
                            Utils.dispatch(true, new Runnable() {
                                @Override
                                public void run() {
                                    listener.onDynamixEvent(event);
                                }
                            });
                        } else {
                            Log.w(TAG, "No listener found for event type: " + event.getAction());
                        }
                    }
                }
            }
        } else
            Log.w(TAG, "Not dispatching Dynamix event because we're not started");
    }

    @Override
    public Bundle getDynamixInformation(UUID sessionID, final Intent config) {
        Log.v(TAG, "getDynamixInformation");
        if (isStarted()) {
            // Get the sending ContextPlugin using the secure sessionId
            ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
            if (plug != null) {
                if (config != null) {
                    if (config.getAction() == DynamixHelper.DynamixInformation.GET_REQUEST_CONTEXT_TYPE) {
                        UUID requestId = (UUID) config.getSerializableExtra(DynamixHelper.DynamixInformation.REQUEST_ID);
                        synchronized (channelMap) {
                            Channel c = channelMap.get(requestId);
                            if (c != null) {
                                Bundle b = new Bundle();
                                b.putString(DynamixHelper.DynamixInformation.REQUEST_CONTEXT_TYPE, c.getContextType());
                                return b;
                            } else {
                                Log.w(TAG, "getDynamixInformation: could not find channel for request!");
                                return null;
                            }
                        }
                    } else {
                        Log.w(TAG, "getDynamixInformation: config type not supported: " + config.getAction());
                        return null;
                    }
                } else {
                    Log.w(TAG, "getDynamixInformation: received null config!");
                    return null;
                }
            } else {
                Log.w(TAG, "getDynamixInformation: sessionID NOT_AUTHORIZED!");
                return null;
            }
        } else {
            Log.w(TAG, "getDynamixInformation not available because we're not started");
            return null;
        }
    }

    @Override
    public Result sendDynamixEvent(UUID sessionID, final Intent event) {
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.v(TAG, "sendDynamixEvent " + event);
        if (isStarted()) {
            if (event != null) {
                // Get the sending ContextPlugin using the secure sessionId
                ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
                if (plug != null) {
                    // TODO: Check for event types (e.g., interaction reports, etc.)
                    synchronized (dynamixEventListeners) {
                        // Create an array list for targets of the event
                        ArrayList<IDynamixEventListener> targets = new ArrayList<>();
                        // Grab the listeners registered to the event's action string
                        for (ContextPlugin target : dynamixEventListeners.keySet()) {
                        /*
                         * Make sure the target plug-in has SystemPrivilege.FRAMEWORK or this is not a production release
                         */
                            if (target.getSystemPrivilege() == SystemPrivilege.FRAMEWORK || !FrameworkConstants.PRODUCTION_RELEASE) {
                                Map<String, IDynamixEventListener> targetListenerMap = dynamixEventListeners.get(target);
                                if (targetListenerMap.containsKey(event.getAction())) {
                                    targets.add(targetListenerMap.get(event.getAction()));
                                }
                            }
                        }
                        for (final IDynamixEventListener listener : targets) {
                        /*
                         * Use a daemon thread to send the event to listeners just in case the listener hangs.
                         */
                            Utils.dispatch(true, new Runnable() {
                                @Override
                                public void run() {
                                    listener.onDynamixEvent(event);
                                }
                            });
                        }
                        return new Result();
                    }
                } else {
                    Log.w(TAG, "sendDynamixEvent: sessionID NOT_AUTHORIZED!");
                    return new Result("Session ID NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
                }
            } else {
                Log.w(TAG, "sendDynamixEvent received null event!");
                return new Result("sendDynamixEvent received null event", ErrorCodes.MISSING_PARAMETERS);
            }
        } else {
            Log.w(TAG, "Not dispatching Dynamix event because we're not started");
            return new Result("Dynamix NOT_READY", ErrorCodes.NOT_READY);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public String serializeIContextInfo(UUID sessionID, IContextInfo data) {
        return serializeIContextInfo(sessionID, data, AppConstants.JSON_WEB_ENCODING);
    }

    /**
     * @inheritDoc
     */
    @Override
    public String serializeIContextInfo(UUID sessionID, IContextInfo data, String stringFormat) {
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null && data != null && stringFormat != null) {
            if (data != null && stringFormat != null) {
                if (stringFormat.equalsIgnoreCase(AppConstants.JSON_WEB_ENCODING)) {
                    JsonNode encodedNode = mapper.createObjectNode();
                    encodedNode = mapper.valueToTree(data);
                    return encodedNode.toString();
                } else
                    Log.w(TAG, "serializeIContextInfo received unsupported string format: " + stringFormat);
            } else
                Log.w(TAG, "serializeIContextInfo received null data or format");
        } else
            Log.w(TAG, "serializeIContextInfo received invalid session id");
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Result sendPluginAlert(UUID sessionID, Bundle alert) {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        // Get the ContextPlugin using the secure sessionId
        ContextPlugin plug = getContextPluginForSessionId(sessionID.toString());
        // If we get a plug, we have a valid UUID, so continue
        if (plug != null) {
            String type = alert.getString(PluginConstants.ALERT_TYPE);
            if (type.equalsIgnoreCase(PluginConstants.ALERT_TYPE_TOAST)) {
                String toastValue = alert.getString(PluginConstants.ALERT_TOAST_VALUE);
                int toastDuration = alert.getInt(PluginConstants.ALERT_TOAST_DURATION);
                if (toastValue != null && (toastDuration == Toast.LENGTH_LONG || toastDuration == Toast.LENGTH_SHORT)) {
                    HomeActivity.toast(toastValue, toastDuration);
                    //Toast.makeText(context, toastValue, toastDuration).show();
                    return new Result();
                } else {
                    Log.w(TAG, "Missing ALERT_TOAST_VALUE or ALERT_TOAST_DURATION or illegal ALERT_TOAST_DURATION");
                    return new Result("Missing ALERT_TOAST_VALUE", ErrorCodes.MISSING_PARAMETERS);
                }
            } else if (type.equalsIgnoreCase(PluginConstants.ALERT_TYPE_OPEN_SETTINGS)) {
                String settingsValue = alert.getString(PluginConstants.ALERT_OPEN_SETTINGS_VALUE);
                if (settingsValue != null) {
                    try {
                        Field f = Settings.class.getDeclaredField(settingsValue);
                        Log.d(TAG, "Opening Settings Activity Intent: " + settingsValue);
                        context.startActivity(new Intent(settingsValue));
                        return new Result();
                    } catch (Exception e) {
                        Log.w(TAG, "Plug-in requested an illegal Settings Activity: " + settingsValue);
                        return new Result("Plug-in requested an illegal Settings Activity: " + settingsValue,
                                ErrorCodes.CONFIGURATION_ERROR);
                    }
                } else {
                    Log.w(TAG, "Missing ALERT_OPEN_SETTINGS_VALUE");
                    return new Result("Missing ALERT_OPEN_SETTINGS_VALUE", ErrorCodes.MISSING_PARAMETERS);
                }
            } else {
                Log.w(TAG, "Unsupported ALERT_TYPE: " + type);
                return new Result("Unsupported ALERT_TYPE: " + type, ErrorCodes.CONFIGURATION_ERROR);
            }
        } else
            return new Result("NOT_AUTHORIZED", ErrorCodes.NOT_AUTHORIZED);
    }

    @Override
    public IdResult sendMessage(UUID sessionID, String pluginId, String versionString, final Bundle messageBundle,
                                final IMessageResultHandler resultHandler) {
        return doSendMessage(getContextPluginForSessionId(sessionID.toString()), pluginId, versionString,
                messageBundle, resultHandler);
    }

    /**
     * @inheritDoc
     */
    @Override
    public IdResult sendMessage(UUID sessionID, String pluginId, final Bundle messageBundle,
                                final IMessageResultHandler resultHandler) {
        return doSendMessage(getContextPluginForSessionId(sessionID.toString()), pluginId, null, messageBundle,
                resultHandler);
    }

    /**
     * Utility method that handles message sending.
     *
     * @param senderPlug          The sender plug-in
     * @param targetPluginId      The target plug-in id.
     * @param targetPluginVersion The target plug-in version (if null, we target the highest known version).
     * @param messageBundle       The message Bundle.
     * @param resultHandler       A result handler callback.
     * @return An IdResult indicating success or failure.
     */
    private IdResult doSendMessage(final ContextPlugin senderPlug, String targetPluginId, String targetPluginVersion,
                                   final Bundle messageBundle, final IMessageResultHandler resultHandler) {
// Create a requestId
        final UUID requestId = UUID.randomUUID();
        // Create a String version of the target for logging
        String targetString = targetPluginId + ": " + targetPluginVersion;
        if (senderPlug != null) {
// Check for a target
            final ContextPluginRuntimeWrapper targetWrapper;
            if (targetPluginVersion != null)
                targetWrapper = getContextPluginRuntime(targetPluginId, targetPluginVersion);
            else
                targetWrapper = getContextPluginRuntime(targetPluginId);
			/*
			 * TODO: Check if installing plug-in communication targets should be allowed. The code below provides this
			 * functionality, but is currently disabled due to security concerns.
			 */
            if (targetWrapper == null && false) {
				/*
				 * If the targetWrapper is null, the target plug-in is NOT_INSTALLED. Check if Dynamix knows about the
				 * requested target in its repos.
				 */
                ContextPlugin pending = DynamixService.getContextPlugin(targetPluginId, targetPluginVersion);
                if (pending != null) {
                    Result commAllowed = isPluginCommAllowed(senderPlug, pending);
					/*
					 * TODO: We should also probably have a permission for installing plug-ins. Now, if comm is allowed
					 * between both parties, we install any necessary target plug-in.
					 */
                    if (commAllowed.wasSuccessful()) {
                        if (pending.getInstallStatus() == PluginInstallStatus.NOT_INSTALLED) {
                            // We want to attach a state listener, to check when
                            // the plug-in becomes started
                            DynamixService.installPlugin(pending, new PluginInstallCallback() {
                                @Override
                                public void onInstallFailed(ContextPluginInformation plug, String message, int errorCode) {
                                    String errorMessage = "Message target plug-in " + plug.getPluginId()
                                            + " failed to install. Error message: " + message;
                                    Log.w(TAG, errorMessage);
                                    if (resultHandler != null) {
                                        resultHandler.onFailure(errorMessage, errorCode);
                                    }
                                }

                                @Override
                                public void onInstallComplete(ContextPluginInformation plug) {
                                    ContextPluginRuntimeWrapper newWrapper = getContextPluginRuntimeWrapper(plug);
                                    if (newWrapper != null) {
										/*
										 * Create and enqueue the message
										 */
                                        Message message = new Message(plug, newWrapper.getParentPlugin()
                                                .getContextPluginInformation(), messageBundle, resultHandler);
                                        newWrapper.enqueueMessage(message);
                                    } else {
                                        String errorMessage = "Could not find wrapper to enqueue message with after install of "
                                                + plug;
                                        Log.w(TAG, errorMessage);
                                        if (resultHandler != null) {
                                            resultHandler.onFailure(errorMessage, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                        }
                                    }
                                }
                            });
                        } else {
							/*
							 * This should not happen, since if a plug-in has any other state than NOT_INSTALLED, it
							 * should have a wrapper.
							 */
                            String errorMessage = "Could not find wrapper to enqueue message for " + senderPlug
                                    + " with install state " + senderPlug.getInstallStatus();
                            Log.w(TAG, errorMessage);
                            return new IdResult(errorMessage, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                        }
                    } else
                        return new IdResult(commAllowed.getMessage(), commAllowed.getErrorCode());
                } else {
                    // Plugin was not found in repos
                    String errorMessage = "Unknown target plug-in: " + targetString;
                    Log.w(TAG, errorMessage);
                    return new IdResult(errorMessage, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                }
            }
            if (targetWrapper != null) {
                if (targetWrapper.getState() == PluginState.INITIALIZED
                        || targetWrapper.getState() == PluginState.STARTING
                        || targetWrapper.getState() == PluginState.STARTED) {
                    final ContextPlugin targetPlug = targetWrapper.getParentPlugin();
                    if (targetPlug != null) {
                        Result commAllowed = isPluginCommAllowed(senderPlug, targetPlug);
                        if (commAllowed.wasSuccessful()) {
                            // Dispatch the event
                            Utils.dispatch(true, new Runnable() {
                                @Override
                                public void run() {
                                    // Handle success
                                    Message message = new Message(senderPlug.getContextPluginInformation(), targetPlug
                                            .getContextPluginInformation(), messageBundle, resultHandler);
                                    targetWrapper.enqueueMessage(message);
                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    // Handle exception
                                    if (resultHandler != null)
                                        resultHandler.onFailure("Exception during message dispatch",
                                                ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                }
                            });
                            if (DynamixPreferences.isDetailedLoggingEnabled())
                                Log.d(TAG, senderPlug.getId() + " successfully sent an intra-plug-in message to "
                                        + targetPlug.getId() + ": " + System.identityHashCode(targetPlug));
                            // Return success with the requestId
                            return new IdResult(requestId.toString());
                        } else
                            return new IdResult(commAllowed.getMessage(), commAllowed.getErrorCode());
                    } else {
                        // Error about no targetPlug
                        String errorMessage = "Message receiver not found: " + targetPluginId + ", wrapper in state "
                                + targetWrapper.getState();
                        Log.w(TAG, errorMessage);
                        return new IdResult(errorMessage, ErrorCodes.NOT_FOUND);
                    }
                } else {
                    String errorMessage = "Cannot send message to " + targetString + " when its in state  "
                            + targetWrapper.getState();
                    Log.w(TAG, errorMessage);
                    return new IdResult(errorMessage, ErrorCodes.STATE_ERROR);
                }
            } else {
                String errorMessage = "Message receiver not found: " + targetString;
                Log.w(TAG, errorMessage);
                return new IdResult(errorMessage, ErrorCodes.NOT_FOUND);
            }
        } else {
            String errorMessage = "Session ID is NOT_AUTHORIZED";
            Log.w(TAG, errorMessage);
            return new IdResult(errorMessage, ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public int getRequestTimeoutMills() {
        return FrameworkConstants.CONTEXT_REQUEST_TIMEOUT;
    }

    /*
	 * Returns true if communication is allowed between the sender and target; false otherwise. Currently we only check
	 * if the proper permissions are in place; however, we may want to consider allowing receivers to specify who can
	 * send them messages (can also be all '*').
	 */
    private Result isPluginCommAllowed(ContextPlugin senderPlug, ContextPlugin targetPlug) {
        boolean sendAllowed = false;
        boolean receiveAllowed = false;
        StringBuilder error = new StringBuilder();
        if (senderPlug != null)
            for (Permission perm : senderPlug.getPermissions()) {
                if (perm != null && perm.getPermissionString().equalsIgnoreCase(Permissions.SEND_INTER_PLUGIN_MESSAGES)) {
                    sendAllowed = perm.isPermissionGranted();
                    break;
                }
            }
        else
            Log.w(TAG, "isPluginCommAllowed Error: Sender Plugin is null!");
        if (targetPlug != null)
            for (Permission perm : targetPlug.getPermissions()) {
                if (perm != null
                        && perm.getPermissionString().equalsIgnoreCase(Permissions.RECEIVE_INTER_PLUGIN_MESSAGES)) {
                    receiveAllowed = perm.isPermissionGranted();
                    break;
                }
            }
        else
            Log.w(TAG, "isPluginCommAllowed Error: Target Plugin is null!");
        if (!sendAllowed)
            error.append(senderPlug.getId()
                    + " can't send messages without permission: org.ambientdynamix.security.permissions.SEND_INTRA_PLUGIN_MESSAGES");
        if (!receiveAllowed)
            error.append(targetPlug.getId()
                    + " can't send receive messages without permission: org.ambientdynamix.security.permissions.RECEIVE_INTRA_PLUGIN_MESSAGES");
        if (!sendAllowed || !receiveAllowed) {
            Log.w(TAG, error.toString());
            return new Result(error.toString(), ErrorCodes.NOT_AUTHORIZED);
        } else
            return new Result();
    }

    /*
	 * Registers a PluginLooperThread with the threadMap for the incoming plug-in.
	 */
    private PluginLooperThread registerLooperThreadForPlug(final ContextPlugin plug) {
        synchronized (threadMap) {
            // if(!threadMap.containsKey(plug)){
            PluginLooperThread t = new PluginLooperThread(plug);
            // Set the thread name for performance profiling
            t.setName(plug.getId().toString());
            // Set the plug-in's thread priority
            t.setPriority(getThreadPriorityForPowerScheme());
            // Start the plug-in's thread
            t.start();
            // Wait for the handler to become active
            while (t.handler == null) {
                try {
                    Thread.sleep(25);
                } catch (InterruptedException e2) {
                }
            }
            Log.d(TAG, "Registered LooperThread for: " + plug);
            // Update the threadMap with the PluginLooperThread
            PluginLooperThread oldThread = threadMap.put(plug, t);
            if (oldThread != null) {
                Log.w(TAG, "Found existing PluginLooperThread for " + plug);
                oldThread.quit();
            }
			/*
			 * We need the UncaughtExceptionHandler because, if a plug-in is corrupt (e.g., calls methods from old
			 * Dynamix version), Android will throw exceptions that can't be caught below.
			 */
            t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    Log.e(TAG, "PluginLooperThread uncaughtException: " + ex.getMessage());
                    ex.printStackTrace();
                    disablePluginOnError(plug, "PluginLooperThread uncaughtException for: " + plug,
                            ErrorCodes.INTERNAL_PLUG_IN_ERROR, false);
                }
            });
            return t;
        }
    }

    /*
	 * Called periodically by progressMonitorTimer Timer to check for stopped state (active == false). Active is set to
	 * false when state is set by the PlugStoppers Checks if the ContextManager has stopped. Once the ContextManager
	 * stops, the method closes any dialog boxes and needed timers.
	 */
    private synchronized void checkStopped() {
        // Update our startState
        updateManagerState();
        // Handle state
        if (managerStartState == StartState.STOPPED || managerStartState == StartState.PAUSED) {
            stopProgressMonitorTimer();
            Log.i(TAG, "ContextManager has reached state " + managerStartState);
        } else if (managerStartState == StartState.STARTED) {
            stopProgressMonitorTimer();
            Log.i(TAG, "ContextManager has reached state " + managerStartState);
        } else {
            progressCount++;
            if (progressCount > 50) {
                stopProgressMonitorTimer();
                // TODO: throw events to host app when running in embedded mode?
                DynamixService.notifyOnPluginStopTimeout();
            }
            Log.d(TAG, "Waiting for plug-ins to stop... current state is: " + managerStartState);
        }
    }

    /*
	 * Updates the plug-in's stats with the event.
	 */
    private void updateStats(ContextPlugin plug, SourcedContextInfoSet event) {
        if (FrameworkConstants.COLLECT_PLUGIN_STATS) {
            synchronized (statMap) {
                PluginStats stats = statMap.get(plug);
                if (stats != null) {
                    stats.handlePluginContextEvent(event);
                }
            }
        }
    }

    /*
	 * Updates the plug-in's stats with the error message.
	 */
    private void updateStats(ContextPluginInformation plug, String errorMessage) {
        if (FrameworkConstants.COLLECT_PLUGIN_STATS) {
            synchronized (statMap) {
                PluginStats stats = statMap.get(plug);
                if (stats != null) {
                    stats.handleContextScanFailed(errorMessage);
                }
            }
        }
    }

    /*
	 * Returns a ContextResult representing the highest fidelity level appropriate for the incoming
	 * SourcedContextInfoSet, or null if no ContextResult can be created (e.g. the app does not have permission).
	 */
    private ContextResult createContextEventForApplication(DynamixApplication app, SourcedContextInfoSet sourcedSet,
                                                           UUID responseId) {
        ContextResult result = null;
        // Check if this appID is authorized to receive events
        if (DynamixService.hasDynamixApplication(app.getAppID())) {
            // Check if the application is enabled
            if (app.isEnabled()) {
                if (app.isAccessGranted(sourcedSet.getContextType(), sourcedSet.getEventSource())) {
                    // Create the event
                    result = new ContextResult(sourcedSet.getContextInfoSet().getContextInfo(),
                            sourcedSet.getTimestamp(), sourcedSet.getExireMills());
                    // Set auto web encoding state, defaulting to None
                    result.setNoWebEncoding();
                    if (sourcedSet.getContextInfoSet().autoWebEncode())
                        result.setAutoWebEncode();
                    else if (sourcedSet.getContextInfoSet().getWebEncodingFormat() != AppConstants.NO_AUTO_WEB_ENCODING) {
                        // Find the matching format in the IContextInfo object
                        for (String format : sourcedSet.getContextInfoSet().getContextInfo()
                                .getStringRepresentationFormats()) {
                            if (format.equalsIgnoreCase(result.getWebEncodingFormat())) {
                                result.setManualWebEncode(format);
                                break;
                            }
                        }
                    }
					/*
					 * TODO: Streaming setup. This is disabled for now, but we may want to use it for very large events.
					 */
                    if (false) {
                        float threshold = DynamixService.getConfig().getHeapMemoryProtectionThreshold() / 100f;
                        result.prepStreaming(new StreamController(context, 500, threshold));
                    }
                    // Set the event source
                    // Log.i(TAG, "createContextEventForApplication is setting result source to " +
                    // sourcedSet.getEventSource());
                    result.setResultSource(sourcedSet.getEventSource());
					/*
					 * Set the event's target app id and response id. The SessionManager needs this info for error
					 * handling during ContextEvent sending.
					 */
                    result.setTargetAppId(app.getAppID());
                    if (responseId != null)
                        result.setResponseId(responseId.toString());
                }
            }
        }
        // Note: this will return null if no appropriate ContextEvent can be
        // made for the app
        return result;
    }

    /*
	 * Utility method for re-sending cached events.
	 * @param app The app requesting the events.
	 * @param handler The context handler requesting the events.
	 * @param listener The context listener requesting the events.
	 * @param contextType The type of events to send (or null for all).
	 * @param previousMills The time (in milliseconds) to filter events (or -1 for no filter).
	 */
    private void doResendCachedEvents(DynamixApplication app, IContextHandler handler, IContextListener listener,
                                      String contextType, int previousMills) {
        Log.d(TAG, "doResendCachedEvents for app: " + app + " and listener: " + listener);
		/*
		 * TODO Warning, this method may not work with the new handler/listener model.
		 */
        for (DynamixSession session : SessionManager.getAllSessions()) {
            // Check if the DynamixSession matches our requesting
            // DynamixApplication
            if (session.isSessionOpen() && session.getApp().equals(app)) {
                // Found it... now update the app with cached events
                List<ContextResult> events = new ArrayList<ContextResult>();
                List<ContextResultCacheEntry> cacheSnapshot = null;
                if (contextType != null)
                    cacheSnapshot = contextCache.getCachedEvents(contextType);
                else
                    cacheSnapshot = contextCache.getCachedEvents();
                // Process each cached event in the cacheSnapshot, checking if
                // the requesting app should receive it
                for (ContextResultCacheEntry cachedEvent : cacheSnapshot) {
                    if (cachedEvent.hasTargetListener()) {
                        if (cachedEvent.getTargetListener().asBinder().equals(listener.asBinder())) {
                            // The event targets us, so make sure we still hold
                            // a context support registration
                            for (ContextSupport sub : session.getContextSupport(cachedEvent.getTargetHandler())) {
                                if (sub.getContextType().equalsIgnoreCase(
                                        cachedEvent.getSourcedContextEventSet().getContextType())) {
                                    // Make sure the current time is not past
                                    // the event's expiration time
                                    if (previousMills != -1) {
                                        // Get current the system time
                                        Date now = new Date();
                                        if (now.getTime() - previousMills > cachedEvent.getCachedTime().getTime()) {
											/*
											 * Try to create an event for the app
											 */
                                            ContextResult event = null;
                                            try {
                                                event = createContextEventForApplication(app,
                                                        cachedEvent.getSourcedContextEventSet(), cachedEvent
                                                                .getSourcedContextEventSet().getContextInfoSet()
                                                                .getResponseId());
                                            } catch (Exception e) {
                                                Log.w(TAG, "Exception when creating ContextEvent: " + e);
                                                // TODO: Send failed event to app
                                            }
                                            if (event != null)
                                                events.add(event);
                                        }
                                    } else {
										/*
										 * We hold a context support registration, so try to create the event for the
										 * receiver
										 */
                                        ContextResult event = null;
                                        try {
                                            event = createContextEventForApplication(app,
                                                    cachedEvent.getSourcedContextEventSet(), cachedEvent
                                                            .getSourcedContextEventSet().getContextInfoSet()
                                                            .getResponseId());
                                        } catch (Exception e) {
                                            Log.w(TAG, "Exception when creating ContextEvent: " + e);
                                            // TODO: Send failed event to app?
                                        }
                                        if (event != null)
                                            events.add(event);
                                    }
                                }
                            }
                        }
                    }
                }
                // Create an empty eventMap for event handling
                Map<IContextListener, List<ContextResult>> eventMap = new HashMap<IContextListener, List<ContextResult>>();
                // If the SecuredEvent list is not empty, add the events to the
                // eventMap
                if (events.size() > 0) {
                    Log.v(TAG, "Found SecuredEvents for app: Total = " + events.size());
                    eventMap.put(listener, events);
                } else
                    Log.v(TAG, "No cached events found for app");
				/*
				 * Notify the requesting DynamixApplication of the results.
				 */
                SessionManager.notifyContextListeners(eventMap);
                // We're done updating the app, so break out of loop
                break;
            }
        }
    }

    /*
	 * Returns the ContextPlugin from the pluginMap for the incoming sessionId.
	 * @param sessionId The secure sessionId of the caller Returns the ContextPlugin associated with the sessionId, or
	 * null if no ContextPlugin is found
	 */
    private ContextPluginInformation getContextPluginInfoForSessionId(String sessionId) {
        synchronized (sessionIdToPluginMap) {
            if (sessionIdToPluginMap.containsKey(sessionId)) {
                return sessionIdToPluginMap.get(sessionId).getContextPluginInformation();
            }
        }
        return null;
    }

    private ContextPlugin getContextPluginForSessionId(String sessionId) {
        synchronized (sessionIdToPluginMap) {
            if (sessionIdToPluginMap.containsKey(sessionId)) {
                return sessionIdToPluginMap.get(sessionId);
            }
        }
        return null;
    }

    /*
	 * Stops the progress monitor timer.
	 */
    private synchronized void stopProgressMonitorTimer() {
        if (progressMonitorTimer != null) {
            progressMonitorTimer.cancel();
            progressMonitorTimer = null;
        }
    }

    /*
	 * Utility method for disabling a plug-in after an error.
	 */
    private void disablePluginOnError(ContextPlugin plug, String message, int errorCode, boolean destroyPlugin) {
        Log.w(TAG, "disablePluginOnError for " + plug + " with message " + message);
        // Remove pending stops
        if (pendingPluginStopMap.keySet().contains(plug)) {
            pendingPluginStopMap.remove(plug);
        }
        // Set plug-in disabled
        plug.setEnabled(false);
        // Set the wrapper state to error
        ContextPluginRuntimeWrapper wrapper = pluginMap.get(plug);
        if (wrapper != null)
            wrapper.setState(PluginState.ERROR, true);
        // Update the plug-ins disabled status using the DynamixService
        DynamixService.updateContextPluginValues(plug, false);
		/*
		 * We currently remove plug-ins when they produce errors. Note: use 'removeContextPlugin' not 'stopPlugin' since
		 * it removes dependent plug-ins too.
		 */
        removeContextPlugin(plug, null, null);
        // Notify listeners about the error
        SessionManager.notifyAllContextPluginError(plug, message, errorCode);
    }

    /**
     * Private class that handles plug-in stopping and plug-in destroying.
     *
     * @author Darren Carlson
     */
    private class PlugStopper {
        private final String TAG = this.getClass().getSimpleName();
        private ContextPlugin plug;
        private ContextPluginRuntimeWrapper runtimeWrapper;
        private PluginLooperThread thread;
        private ContextPluginRuntime runtime;
        private boolean destroy;
        private boolean uninstall;
        private PluginState previousState;
        private List<ICallback> uninstallCallbacks = new ArrayList<ICallback>();
        private List<ICallback> stopCallbacks = new ArrayList<ICallback>();
        private Timer timer = new Timer();

        /**
         * Creates a PlugStopper.
         */
        public PlugStopper(ContextPlugin plug, ContextPluginRuntimeWrapper runtimeWrapper, PluginLooperThread thread,
                           Context context, boolean destroy, boolean uninstall, PluginState previousState,
                           List<ICallback> stopCallbacks, List<ICallback> uninstallCallbacks) {
            this.plug = plug;
            this.runtimeWrapper = runtimeWrapper;
            this.thread = thread;
            this.destroy = destroy;
            this.uninstall = uninstall;
            this.previousState = previousState;
            this.uninstallCallbacks = uninstallCallbacks;
            this.stopCallbacks = stopCallbacks;
            runtime = runtimeWrapper.getContextPluginRuntime();
            Log.d(TAG, "PlugStopper created for " + plug + " with destroy: " + destroy + " and plug-in state "
                    + runtimeWrapper.getState());
        }

        /**
         * Adds a callback to fire once the plug-in stops.
         */
        public void addStopCallback(ICallback callback) {
            synchronized (stopCallbacks) {
                if (callback != null)
                    stopCallbacks.add(callback);
            }
        }

        /**
         * Adds a callback to fire once the plug-in is uninstalled.
         */
        public void addUninstallCallback(ICallback callback) {
            synchronized (uninstallCallbacks) {
                if (uninstall && callback != null)
                    uninstallCallbacks.add(callback);
            }
        }

        /**
         * Launch the ThreadStopper, which initiates a timer that checks for stop completion.
         */
        public void launch() {
            Log.d(TAG, "PlugStopper has launched for " + plug + " running on thread " + thread);
            if (thread != null) {
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (runtime != null) {
                            synchronized (managerStartState) {
                                /**
                                 * If the Context Manager is not PAUSING or PAUSED, remove the
                                 * Android Event Listeners; otherwise, retain them, since we don't
                                 * want to lose these if we're restarted.
                                 */
                                if (managerStartState != StartState.PAUSING && managerStartState != StartState.PAUSED) {
                                    // Remove any ncfListeners
                                    synchronized (nfcListeners) {
                                        Log.d(TAG, "PlugStopper removing NCF listeners for " + plug);
                                        nfcListeners.remove(plug);
                                    }
                                    // Remove any Android Event listeners
                                    synchronized (dynamixEventListeners) {
                                        Log.d(TAG, "PlugStopper removing Android Event listeners for " + plug);
                                        removeAllAndroidEventListeners(plug, runtime.getSessionId());
                                    }
                                }
                            }
                            // Ask the plug-in to stop (or destroy) itself
                            try {
                                if (destroy) {
                                    Log.d(TAG, "Requesting runtime destroy for: " + plug);
                                    // Remove any context listeners, if needed
                                    for (ContextSupport sup : SessionManager.getContextSupport(plug
                                            .getContextPluginInformation())) {
                                        if (sup.hasContextListener()) {
                                            runtime.removeContextListener(sup.getSupportId());
                                        }
                                    }
                                    // Destroy the plug-in
                                    runtime.destroy();
                                } else {
                                    if (previousState == PluginState.STARTED) {
                                        Log.d(TAG, "Requesting runtime stop for: " + plug);
                                        runtime.stop();
                                    } else
                                        Log.d(TAG, "Ignoring stop, since plug-in was in state " + previousState);
                                }
                            } catch (Exception e) {
                                String message = plug.getName() + " encountered and exception during stop: " + e;
                                Log.w(TAG, message);
                                disablePluginOnError(plug, message, ErrorCodes.INTERNAL_PLUG_IN_ERROR, false);
								/*
								 * Set destroy state and cleanup. TODO: This error handling is incomplete... we might
								 * still have state hanging around
								 */
                                cleanUp(true);
                            } finally {
								/*
								 * 'cleanUp' is called below from the monitor thread, which waits for 'isExecuting' to
								 * return false, meaning the thread that called start on the plug-in has exited.
								 */
                                // cleanUp(false);
                            }
                        } else
                            Log.e(TAG, "Runtime was null in PlugStopper for: " + plug);
                    }
                });
				/*
				 * Set an setUncaughtExceptionHandler for handling problems with stop and destroy
				 */
                t1.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread thread, Throwable ex) {
                        Log.e(TAG, "PluginStopper uncaughtException: " + ex.getMessage());
                        ex.printStackTrace();
                        if (!destroy)
                            disablePluginOnError(plug, plug + " caused an error and was disabled",
                                    ErrorCodes.INTERNAL_PLUG_IN_ERROR, false);
                        cleanUp(true);
                    }
                });
                t1.setDaemon(true);
                t1.start();
                // Create a monitor thread to check the stop progress
                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
						/*
						 * Sleep a bit so that plug-ins have a chance to unregister broadcast receivers, etc. Plug-ins
						 * that rely on Android events may not have their start method blocked, hence if we detect
						 * stopped too fast, they may not have a chance to clean up state.
						 */
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                        }
                        int count = 0;
                        while (runtimeWrapper.isExecuting()) {
                            count++;
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                            if (count > 200)
                                break;
                        }
                        // Check for exit
                        if (runtimeWrapper.isExecuting()) {
                            String message = plug.getName()
                                    + " did not stop in a timely manner... deactivating and interrupting the plug-in";
                            Log.w(TAG, message);
                            // Deactivate plug-in, since it's behaving badly
                            disablePluginOnError(plug, message, ErrorCodes.INTERNAL_PLUG_IN_ERROR, destroy);
                        } else
                            cleanUp(false);
                    }
                });
                t2.setDaemon(true);
                t2.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread thread, Throwable ex) {
                        Log.e(TAG, "PluginStopper uncaughtException: " + ex.getMessage());
                        ex.printStackTrace();
                        disablePluginOnError(plug, plug + " caused an error and was disabled",
                                ErrorCodes.INTERNAL_PLUG_IN_ERROR, false);
                    }
                });
                t2.start();
            } else {
                Log.d(TAG, "Plug-in had no runtime thread... it was probably not started: " + plug);
                // Since we don't have a thread to stop, just clean up
                cleanUp(false);
            }
        }

        /*
         * Handles state cleanup for the PlugStopper.
         */
        private void cleanUp(boolean pluginException) {
            Log.d(TAG, "PlugStopper Cleanup for " + plug);
            timer.cancel();
            // Remove the PlugStopper from the stopperMap
            stopperMap.remove(plug);
            // Setup wrapper state
            if (pluginException)
                // On exceptions, set ERROR state
                runtimeWrapper.setState(PluginState.ERROR, true);
            else {
                // No exception, so set INITIALIZED state
                runtimeWrapper.setState(PluginState.INITIALIZED, true);
            }
			/*
			 * Check for any pending stop actions that may have arrived since we launched.
			 */
            PendingStopActions actions = pendingPluginStopMap.remove(plug);
            if (actions != null) {
                Log.w(TAG, "Found a pending stop request for " + plug);
                // If we're not destroying, but a destroy was requested... run
                // the destroy
                if ((!destroy && actions.destroy) || (!uninstall && actions.uninstall)) {
                    // Do not send stopCallbacks here, since we already called
                    // them above.
                    stopPlugin(plug, true, actions.destroy, actions.uninstall, stopCallbacks, uninstallCallbacks);
                    // Return since we will be re-stopping the plug-in
                    return;
                }
            }
            // Destroy the plugin, if needed
            if (destroy) {
                // Quit the thread
                // TODO: Make sure the thread quits properly
                if (thread != null)
                    thread.quit();
                // Remove the plug-in's runtime thread
                threadMap.remove(plug);
                // Remove the framework listener
                if (runtime != null)
                    runtime.unbindDynamix(ContextManager.this);
                // Remove the plug-in's context support
                removeContextSupportForContextPlugin(plug, null);
                // Remove the plugin from the pluginMap
                pluginMap.remove(plug);
				/*
				 * Remove session mapping. Remember, LIBRARY plug-ins won't have a sessionId, so check for this.
				 */
                synchronized (sessionIdToPluginMap) {
                    if (runtime != null && runtime.getSessionId() != null) {
                        if (sessionIdToPluginMap.containsKey(runtime.getSessionId()))
                            sessionIdToPluginMap.remove(runtime.getSessionId());
                    }
                }
                // Remove any PluginStats
                PluginStats stats = statMap.remove(plug);
                if (stats != null) {
                    stats.clear();
                    stats = null;
                }
                // Remove the plug-ins SecuredContext (if created)
                Log.d(TAG, "Removing SecuredContext for: " + plug);
                synchronized (securedContextMap) {
                    SecuredContextSettings c = securedContextMap.remove(plug);
                    if (c != null && c.securedContext instanceof SecuredContext) {
                        // Remove all listeners and receivers from the
                        // SecureContext
                        ((SecuredContext) c.securedContext).removeAllListeners();
                    }
                }
                // Set DESTROYED state
                runtimeWrapper.setState(PluginState.DESTROYED, true);
                // Notify any stop callbacks
                if (stopCallbacks != null) {
                    for (ICallback cb : stopCallbacks) {
                        SessionManager.sendCallbackSuccess(cb);
                    }
                }
                Log.d(TAG, "PlugStopper destroyed: " + plug);
                // Kick off uninstall, if requested
                if (uninstall) {
                    DynamixService.removeUninstalledContextPlugin(plug, uninstallCallbacks);
                }
            } else {
				/*
				 * Restart the plug-in's OSGi Bundle. This may provide better cleanup of in-memory data structures in
				 * OSGi.
				 */
                DynamixService.restartPluginBundle(plug);
                // Update the manager's state
                updateManagerState();
                // Notify any stop callbacks
                if (stopCallbacks != null) {
                    for (ICallback cb : stopCallbacks) {
                        SessionManager.sendCallbackSuccess(cb);
                    }
                }
                Log.d(TAG, "PlugStopper stopped: " + plug);
            }
        }
    }
}