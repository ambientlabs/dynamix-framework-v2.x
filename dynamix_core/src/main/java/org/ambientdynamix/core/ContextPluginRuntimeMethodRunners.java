/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.os.Bundle;
import android.util.Log;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Set of utilities for launching threaded method runners for ContextPluginRuntimes.
 *
 * @author Darren Carlson
 */
abstract class ContextPluginRuntimeMethodRunners {
    // Private data
    private final static String TAG = ContextPluginRuntimeMethodRunners.class.getSimpleName();
    /**
     * Static ThreadPoolExecutor that submit Callable tasks, which can throw checked exceptions.
     */
    private static ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 10, 60, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>()) {
        /*
         * See: http://stackoverflow.com/questions/4492273/catching-thread-exceptions-from-java-executorservice
         */
        @Override
        public <T> Future<T> submit(final Callable<T> task) {
            Callable<T> wrappedTask = new Callable<T>() {
                @Override
                public T call() throws Exception {
                    try {
                        return task.call();
                    } catch (Exception e) {
                        Log.w(TAG, "Exception: " + e);
                        return null;
                    }
                }

                @Override
                protected void finalize() throws Throwable {
                    super.finalize();
                }
            };
            return super.submit(wrappedTask);
        }
    };

    protected void afterExecute(Runnable r, Throwable t) {
    }

    /**
     * Static utility method for launching Daemon threads.
     *
     * @param runner         The Callable to run.
     * @param threadPriority The thread priority.
     * @return Returns the launched Future.
     */
    public static Future<?> launchThread(Callable<?> runner, int threadPriority) {
        return executor.submit(runner);
    }

    /**
     * Callable that provides functionality for performing a context scan request on a dedicated thread
     *
     * @author Darren Carlson
     */
    public static class HandleContextRequest implements Callable<Object> {
        private DynamixApplication app;
        private ContextPluginInformation plugin;
        private ContextPluginRuntime target;
        private UUID requestId;
        private String contextInfoType;
        private Bundle requestConfig;

        public HandleContextRequest(DynamixApplication app,
                                    ContextPluginInformation plugin, ContextPluginRuntime target, UUID requestId, String contextInfoType,
                                    Bundle requestConfig) {
            this.app = app;
            this.plugin = plugin;
            this.target = target;
            this.requestId = requestId;
            this.contextInfoType = contextInfoType;
            this.requestConfig = requestConfig;
        }

        @Override
        public Object call() throws Exception {
            try {
                if (requestConfig == null) {
                    target.handleContextRequest(requestId, contextInfoType);
                } else {
                    target.handleConfiguredContextRequest(requestId, contextInfoType, requestConfig);
                }
            } catch (Exception e) {
                Log.w(TAG, "Exception during HandleContextRequest: " + e.toString());
            }
            return null;
        }
    }

    /**
     * Callable that provides functionality for removing context subscriptions.
     *
     * @author Darren Carlson
     */
    public static class RemoveContextSubscriptions implements Callable<Object> {
        private List<ContextPluginRuntime> runtimes;
        private UUID subscriptionId;

        public RemoveContextSubscriptions(List<ContextPluginRuntime> runtimes, UUID subscriptionId) {
            if (runtimes == null)
                throw new RuntimeException("RemoveContextSubscription: runtimes cannot be null");
            this.runtimes = runtimes;
            if (subscriptionId == null)
                throw new RuntimeException("RemoveContextSubscription: subscriptionId cannot be null");
            this.subscriptionId = subscriptionId;
        }

        @Override
        public Object call() throws Exception {
            for (final ContextPluginRuntime runtime : runtimes) {
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            runtime.removeContextListener(subscriptionId);
                        } catch (Exception e) {
                            Log.w(TAG, "Exception during RemoveContextSubscriptions: " + e.toString());
                        }
                    }
                });
            }
            return null;
        }
    }
}