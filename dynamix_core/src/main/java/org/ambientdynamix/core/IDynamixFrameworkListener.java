/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Interface for Dynamix framework events.
 * 
 * @see DynamixFrameworkListener
 * @author Darren Carlson
 *
 */
public interface IDynamixFrameworkListener {
	/**
	 * Raised when Dynamix begins initializing.
	 */
	void onDynamixInitializing();

	/**
	 * Raised after an initializing error.
	 */
	void onDynamixInitializingError(String message);

	/**
	 * Raised when Dynamix reaches initialized.
	 */
	void onDynamixInitialized(DynamixService dynamix);

	/**
	 * Raised when Dynamix begins starting.
	 */
	void onDynamixStarting();

	/**
	 * Raised when Dynamix reaches started.
	 */
	void onDynamixStarted();

	/**
	 * Raised when Dynamix begins stopping.
	 */
	void onDynamixStopping();

	/**
	 * Raised when Dynamix reaches stopped.
	 */
	void onDynamixStopped();

	/**
	 * Raised when a Dynamix application has been added.
	 */
	void onDynamixApplicationAdded(DynamixApplication app);

	/**
	 * Raised when a Dynamix application has been removed.
	 */
	void onDynamixApplicationRemoved(DynamixApplication app);

	/**
	 * Raised when a Dynamix application has connected.
	 */
	void onDynamixApplicationConnected(DynamixApplication app);

	/**
	 * Raised when a Dynamix application has disconnected.
	 */
	void onDynamixApplicationDisconnected(DynamixApplication app);

	/**
	 * Raised when Dynamix encounters an error.
	 */
	void onDynamixError(String message);

	/**
	 * Raised when Dynamix plug-in discovery starts.
	 */
	void onPluginDiscoveryStarted();

	/**
	 * Raised when Dynamix plug-in discovery completes.
	 */
	void onPluginDiscoveryCompleted();

	/**
	 * Raised when Dynamix plug-in discovery encouteres an error.
	 */
	void onPluginDiscoveryError(String message);

	/**
	 * Raised when a Dynamix plug-in fails to stop in a timely manner.
	 */
	void onPluginStopTimeout();

	/**
	 * Raised when Dynamix plug-in is installed.
	 */
	void onPluginInstalled(ContextPluginInformation plug);

	/**
	 * Raised when Dynamix plug-in is uninstalled.
	 */
	void onPluginUninstalled(ContextPluginInformation plug);

	/**
	 * Raised when Dynamix plug-in is updated.
	 */
	void onPluginUpdated(ContextPluginInformation plug);
}