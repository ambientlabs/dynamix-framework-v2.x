/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.AppConstants.PluginInstallStatus;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.VersionedPlugin;

import android.os.Bundle;

/**
 * Represents context support registration that is associated with a Dynamix session.
 * 
 * @author Darren Carlson
 */
public class ContextSupport {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private UUID supportId;
	private DynamixSession session;
	private String contextType;
	private IContextHandler handler;
	private List<ContextPlugin> plugs = new ArrayList<ContextPlugin>();
	private Bundle supportConfig;
	private IContextListener listener;
	private List<IContextSupportCallback> pendingCallbacks = new ArrayList<IContextSupportCallback>();
	private boolean active = false;

	/**
	 * Creates a context support.
	 */
	public ContextSupport(UUID supportId, DynamixSession session, IContextHandler handler, String contextType,
			List<ContextPlugin> plugs, IContextListener listener, Bundle supportConfig) {
		this.session = session;
		this.handler = handler;
		this.plugs = plugs;
		this.contextType = contextType;
		this.supportId = supportId;
		this.supportConfig = supportConfig;
		this.listener = listener;
	}

	/**
	 * Fires 'sendContextSupportSuccess' on all pending context support callbacks.
	 */
	public void fireContextSupportSuccessOnAllCallbacks(boolean clearPendingList) {
		synchronized (pendingCallbacks) {
			for (IContextSupportCallback callback : pendingCallbacks)
				SessionManager.sendContextSupportSuccess(callback, this.getContextSupportInfo());
		}
		if (clearPendingList)
			clearPendingContextSupportCallbacks();
	}

	/**
	 * Fires 'sendContextSupportFailure' on all pending context support callbacks.
	 */
	public void fireContextSupportFailureOnAllCallbacks(String message, int errorCode, boolean clearPendingList) {
		synchronized (pendingCallbacks) {
			for (IContextSupportCallback callback : pendingCallbacks)
				SessionManager.sendContextSupportFailure(callback, message, errorCode);
		}
		if (clearPendingList)
			clearPendingContextSupportCallbacks();
	}

	/**
	 * Adds the pending context support callback.
	 */
	public void addPendingContextSupportCallback(IContextSupportCallback callback) {
		synchronized (pendingCallbacks) {
			pendingCallbacks.add(callback);
		}
	}

	/**
	 * Clears all pending context support callbacks.
	 */
	public void clearPendingContextSupportCallbacks() {
		synchronized (pendingCallbacks) {
			pendingCallbacks.clear();
		}
	}

	/**
	 * Returns true if all the context support is valid (has associated plug-ins in a non-error state) and all
	 * associated plug-ins are installed; false otherwise.
	 */
	public boolean isInstalled() {
		if (isValid()) {
			for (ContextPlugin plug : plugs)
				if (plug.getInstallStatus() != PluginInstallStatus.INSTALLED)
					return false;
			return true;
		}
		return false;
	}

	/**
	 * Returns true if this context support is valid, meaning that it has 1 or more plug-ins that are not in an error
	 * state; false otherwise.
	 */
	public boolean isValid() {
		if (plugs != null && plugs.size() > 0) {
			for (ContextPlugin plug : plugs)
				if (plug.getInstallStatus() == PluginInstallStatus.ERROR
						|| plug.getInstallStatus() == PluginInstallStatus.CANNOT_ACCESS_NETWORK)
					return false;
			return true;
		}
		return false;
	}

	/**
	 * Returns true if this context support contains the plug-in; false otherwise.
	 */
	public boolean containsContextPlugin(ContextPluginInformation plug) {
		synchronized (plugs) {
			for (ContextPlugin p : plugs)
				if (p.getContextPluginInformation().equals(plug))
					return true;
		}
		return false;
	}

//	public boolean replaceContextPlugin(ContextPlugin original, ContextPlugin replacement) {
//		synchronized (plugs) {
//			if (plugs.contains(original)) {
//				plugs.remove(original);
//				plugs.add(replacement);
//				return true;
//			} else
//				return false;
//		}
//	}

	public ContextPluginInformation getContextPluginInformation(VersionedPlugin plug) {
		synchronized (plugs) {
			for (ContextPlugin testPlug : plugs) {
				if (plug.hasVersion()) {
					if (testPlug.getId().equalsIgnoreCase(plug.getPluginId())
							&& testPlug.getVersion().equals(plug.getPluginVersion()))
						return testPlug.getContextPluginInformation();
				} else if (testPlug.getId().equalsIgnoreCase(plug.getPluginId()))
					return testPlug.getContextPluginInformation();
			}
		}
		return null;
	}

	/**
	 * Adds the listener.
	 * 
	 * @return True if the listener was added; false otherwise.
	 */
	public void setContextListener(IContextListener listener) {
		this.listener = listener;
	}

	/**
	 * Returns the context listener associated with the subscription, or null if there is no listener.
	 */
	public IContextListener getContextListener() {
		return this.listener;
	}

	/**
	 * Returns true if the listener is registered; false otherwise.
	 */
	public boolean hasContextListener() {
		return this.listener != null;
	}

	/**
	 * Removes the listener.
	 */
	public void removeContextListener(IContextListener listener) {
		this.listener = null;
	}

	/**
	 * Returns the support config bundle, or null if non is registered.
	 */
	public Bundle getSupportConfig() {
		return this.supportConfig;
	}

	/**
	 * Returns the ContextPlugins associated with this context support.
	 */
	public List<ContextPlugin> getContextPlugins() {
		return plugs;
	}

	/**
	 * Returns the ContextPluginInformation associated with this context support.
	 */
	public List<ContextPluginInformation> getContextPluginInformation() {
		List<ContextPluginInformation> returnList = new ArrayList<ContextPluginInformation>();
		for (ContextPlugin plug : plugs)
			returnList.add(plug.getContextPluginInformation());
		return returnList;
	}

	/**
	 * Sets the ContextPlugins associated with this context support.
	 */
	public void setContextPlugins(List<ContextPlugin> plugs) {
		this.plugs = plugs;
	}

	/**
	 * Returns the context type associated with this context support.
	 */
	public String getContextType() {
		return contextType;
	}

	/**
	 * Returns the DynamixApplication associated with this context support.
	 */
	public DynamixApplication getDynamixApplication() {
		return session.getApp();
	}

	/**
	 * Returns the handler associated with this context support.
	 */
	public IContextHandler getContextHandler() {
		return this.handler;
	}

	/**
	 * Returns this context support's id.
	 */
	public UUID getSupportId() {
		return this.supportId;
	}

	/**
	 * Returns this context support's session.
	 */
	public DynamixSession getSession() {
		return this.session;
	}

	/**
	 * Returns true if this context support is active (i.e., able to receive events); false otherwise.
	 */
	public boolean isActive() {
		return this.active;
	}

	/**
	 * Set true if this context support is active (i.e., able to receive events); false otherwise.
	 */
	public void setActive(boolean isActive) {
		this.active = isActive;
	}

	/**
	 * Returns the ContextSupportInfo associated with this ContextSupport.
	 */
	public ContextSupportInfo getContextSupportInfo() {
		List<ContextPluginInformation> plugInfo = new ArrayList<ContextPluginInformation>();
		for (ContextPlugin plug : plugs)
			plugInfo.add(plug.getContextPluginInformation());
		return new ContextSupportInfo(supportId.toString(), plugInfo, contextType);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + handler.hashCode() + contextType.hashCode() + supportId.hashCode();
		return result;
	}

	@Override
	public String toString() {
		if (session.isSessionOpen())
			return "ContextSupport: App = " + getDynamixApplication().getAppName() + " | Handler = " + handler
					+ " | Listener = " + listener + " | plug-in size = " + plugs.size() + " | context type = "
					+ getContextType() + " | support id = " + getSupportId();
		else
			return "ContextSupport: App = Disconnected app (session was closed) | Handler = " + handler
					+ " | Listener = " + listener + " | plug-in size = " + plugs.size() + " | context type = "
					+ getContextType() + " | support id = " + getSupportId();
	}

	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class. (MAX)comparing the cannonical strings here because they may be from different class loaders which makes comparing
//		the class objects through != impossible
		if (candidate == null || !candidate.getClass().getCanonicalName().equals(getClass().getCanonicalName()))
			return false;
		ContextSupport other = (ContextSupport) candidate;
		if (this.handler.asBinder().equals(other.getContextHandler().asBinder())
				&& this.contextType.equalsIgnoreCase(other.getContextType()) && this.plugs.containsAll(other.plugs))
//				&& this.supportId.equals(other.getSupportId()))
			return true;
		else
			return false;
	}
}
