/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.os.IBinder;
import android.util.Log;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextHandlerCallback;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.ISessionListener;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.core.ContextPluginRuntimeMethodRunners.RemoveContextSubscriptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents a session for a specific DynamixApplicaiton.
 *
 * @author Darren Carlson
 */
public class DynamixSession implements Serializable {
    // Private data
    private static final long serialVersionUID = -2174448383460352412L;
    private final String TAG = this.getClass().getSimpleName();
    private String appKey;
    private String sessionId;
    private DynamixApplication app;
    private ISessionListener sessionListener;
    private Map<IBinder, ContextHandlerMapping> handlerMap = new ConcurrentHashMap<IBinder, ContextHandlerMapping>();
    private Map<IBinder, Set<ContextSupport>> contextSupportMap = new ConcurrentHashMap<IBinder, Set<ContextSupport>>();

    /*
     * Struct-like class that holds a context handler and its associated id.
     */
    private class ContextHandlerMapping {
        UUID id;
        ContextHandlerImpl handler;

        public ContextHandlerMapping(ContextHandlerImpl handler, UUID id) {
            this.handler = handler;
            this.id = id;
        }
    }

    /**
     * Creates a DynamixSession using the appKey. The session is not open initially.
     */
    public DynamixSession(DynamixApplication app, String appKey, ISessionListener sessionListener) {
        this.app = app;
        this.appKey = appKey;
        this.sessionListener = sessionListener;
        Log.d(TAG, "DynamixSession created for " + appKey);
    }

    /**
     * Refreshes the app using the Dynamix database, which updates the app's in memory details.
     */
    public synchronized void refreshApp() {
        if (isSessionOpen()) {
            DynamixApplication refreshed = DynamixService.getDynamixApplication(app.getAppID());
            if (refreshed != null) {
                this.app = refreshed;
                //this.appId = app.getAppID();
            } else
                Log.w(TAG, "Could not refresh app: " + app);
        }
    }

    /**
     * Adds the specified handler to the session.
     *
     * @param handler The handler to add.
     * @return The handler's id
     */
    public synchronized String addContextHandler(final IContextHandler handler, final IContextHandlerCallback callback) {
        Log.d(TAG, "addContextHandler for " + handler);
        String id;
        synchronized (handlerMap) {
            if (!handlerMap.containsKey(handler.asBinder())) {
                UUID handlerId = UUID.randomUUID();
                handlerMap.put(handler.asBinder(), new ContextHandlerMapping((ContextHandlerImpl) handler, handlerId));
                id = handlerId.toString();
            } else {
                // The handler is already there
                Log.d(TAG, "addDynamixHandler found existing handler for: " + handler);
                id = handlerMap.get(handler.asBinder()).toString();
            }
        }
        SessionManager.sendContextHandlerCallbackSuccess(callback, handler);
        return id;
    }

    public boolean hasContextSupport(ContextSupport contextSupport) {
        synchronized (contextSupportMap) {
            for (Set<ContextSupport> supList : contextSupportMap.values()) {
                if (supList.contains(contextSupport))
                    return true;
            }
        }
        return false;
    }

    /**
     * Adds ContextSupport.
     *
     * @return True if the context support was added; false otherwise.
     */
    public synchronized void addContextSupport(ContextSupport contextSupport) throws Exception {
        Log.d(TAG, "Adding " + contextSupport);
        if (contextSupport != null) {
            synchronized (handlerMap) {
                // Ensure that the handler has already been registered
                if (handlerMap.containsKey(contextSupport.getContextHandler().asBinder())) {
                    ContextHandlerMapping mapping = handlerMap.get(contextSupport.getContextHandler().asBinder());
                    if (!contextSupportMap.keySet().contains(mapping.handler.asBinder())) {
                        // No support yet, so set it up
                        Log.d(TAG, "Session: " + this + " had context support " + contextSupport
                                + " successfully registered to " + mapping.handler);
                        Set<ContextSupport> supportList = Collections.newSetFromMap(new ConcurrentHashMap<ContextSupport, Boolean>());
                        supportList.add(contextSupport);
                        contextSupportMap.put(mapping.handler.asBinder(), supportList);
                    } else {
                        // Grab the mapped list of context support
                        Set<ContextSupport> supportList = contextSupportMap.get(mapping.handler.asBinder());
                        boolean exists = false;
                        for (ContextSupport support : supportList) {
                            if (support.equals(contextSupport)) {
                                exists = true;
                                break;
                            }
                        }
//						if (!supportList.contains(contextSupport)) {
                        if (!exists) {
                            supportList.add(contextSupport);
                            Log.d(TAG, "Session: " + this + " had context support " + contextSupport
                                    + " successfully registered to " + mapping.handler);
                        } else {
                            // Support already registered!
                            Log.d(TAG, "Session: " + this + " ALREADY had context support " + contextSupport
                                    + " registered to " + mapping.handler);
                        }
                    }
                } else {
                    Log.w(TAG,
                            "addContextSupport called, but handler is not registered: "
                                    + contextSupport.getContextHandler());
                    throw new Exception("addContextSupport called, but handler is not registered: "
                            + contextSupport.getContextHandler());
                }
            }
        } else {
            Log.w(TAG, "addContextSupport received null contextSupport");
            throw new Exception("addContextSupport received null contextSupport");
        }
    }

    @Override
    public boolean equals(Object candidate) {
        // first determine if they are the same object reference
        if (this == candidate)
            return true;
        // make sure they are the same class
        if (candidate == null || candidate.getClass() != getClass())
            return false;
        DynamixSession other = (DynamixSession) candidate;
        return this.app.equals(other.app) && this.getSessionId().equals(other.getSessionId());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + this.app.hashCode() + getSessionId().hashCode();
        return result;
    }

    /**
     * Returns the application
     */
    public DynamixApplication getApp() {
        return app;
    }

    /**
     * Returns all context support registrations for the session.
     */
    public List<ContextSupport> getAllContextSupport() {
        List<ContextSupport> subs = new ArrayList<ContextSupport>();
        synchronized (contextSupportMap) {
            for (Set<ContextSupport> l : contextSupportMap.values())
                subs.addAll(l);
        }
        return subs;
    }

    /**
     * Returns a list of context support for the IContextHandler.
     */
    public List<ContextSupport> getContextSupport(IContextHandler handler) {
        List<ContextSupport> returnList = new ArrayList<ContextSupport>();
        synchronized (contextSupportMap) {
            Set<ContextSupport> subs = contextSupportMap.get(handler.asBinder());
            // Need a null check otherwise 'addAll' throws NPEs
            if (subs != null)
                returnList.addAll(subs);
        }
        return returnList;
    }

    /**
     * Returns a list of context support attached to the specified plugin within the IContextHandler.
     */
    public ContextSupport getContextSupportFromId(IContextHandler handler, UUID contextSupportId) {
        synchronized (contextSupportMap) {
            Set<ContextSupport> subs = contextSupportMap.get(handler.asBinder());
            // Need a null check otherwise 'addAll' throws NPEs
            if (subs != null) {
                for (ContextSupport sub : subs) {
                    if (sub.getSupportId().equals(contextSupportId))
                        return sub;
                }
            }
        }
        return null;
    }

    /**
     * Returns a list of context support attached to the specified plugin within the IContextHandler.
     */
    public List<ContextSupport> getAllContextSupport(IContextHandler handler, ContextPlugin plug, String contextType) {
        List<ContextSupport> returnList = new ArrayList<>();
        synchronized (contextSupportMap) {
            Set<ContextSupport> subs = contextSupportMap.get(handler.asBinder());
            // Need a null check otherwise 'addAll' throws NPEs
            if (subs != null) {
                for (ContextSupport sub : subs) {
                    if (sub.getContextPlugins().contains(plug)
                            && sub.getContextType().equalsIgnoreCase(contextType))
                        returnList.addAll(subs);
                }
            }
        }
        return returnList;
    }

    /**
     * Returns a list of context support for the specified handler and listener.
     */
    public List<ContextSupport> getContextSupport(IContextHandler handler, IContextListener listener) {
        List<ContextSupport> returnList = new ArrayList<ContextSupport>();
        Set<ContextSupport> subs = contextSupportMap.get(handler.asBinder());
        // Need a null check otherwise 'addAll' throws NPEs
        if (subs != null && listener != null) {
            for (ContextSupport sup : subs) {
                if (sup.hasContextListener())
                    if (sup.getContextListener().asBinder().equals(listener.asBinder()))
                        returnList.add(sup);
            }
        }
        return returnList;
    }
    /**
     * Returns the ContextSupport instance for the specified handler and context type, or null if not found.
     * Note: This method should probably not be used since it lacks a plug-in filter and simply
     * returns the first support found.
     */
//	public ContextSupport getContextSupport(IContextHandler handler, String contextType) {
//		for (ContextSupport sup : contextSupportMap.get(handler.asBinder())) {
//			if (sup.getContextType().equalsIgnoreCase(contextType))
//				return sup;
//		}
//		return null;
//	}

    /**
     * Returns a list of all context support matching the specified contextType string.
     */
    public List<ContextSupport> getAllContextSupport(String contextType) {
        List<ContextSupport> returnList = new ArrayList<ContextSupport>();
        for (Set<ContextSupport> subs : contextSupportMap.values())
            for (ContextSupport sub : subs)
                if (sub.getContextType().equalsIgnoreCase(contextType))
                    returnList.add(sub);
        return returnList;
    }

    /**
     * Returns a list of active context support matching the specified contextType string.
     */
    public List<ContextSupport> getActiveContextSupport(String contextType) {
        List<ContextSupport> returnList = new ArrayList<ContextSupport>();
        for (Set<ContextSupport> subs : contextSupportMap.values())
            for (ContextSupport sub : subs)
                if (sub.isActive() && sub.getContextType().equalsIgnoreCase(contextType))
                    returnList.add(sub);
        return returnList;
    }

    /**
     * Returns a list of all context support info matching the specified contextType string.
     */
    public ContextSupportInfo getAllContextSupportInfo(String contextType) {
        for (ContextSupport sup : getAllContextSupport(contextType)) {
            return sup.getContextSupportInfo();
        }
        return null;
    }

    /**
     * Returns a list of active context support info matching the specified contextType string.
     */
    public ContextSupportInfo getActiveContextSupportInfo(String contextType) {
        for (ContextSupport sup : getActiveContextSupport(contextType)) {
            return sup.getContextSupportInfo();
        }
        return null;
    }

    /**
     * Returns the ContextSupport associated with the incoming ContextSupportInfo.
     */
    public synchronized ContextSupport getContextSupport(ContextSupportInfo supportInfo) {
        for (Set<ContextSupport> subList : contextSupportMap.values()) {
            for (ContextSupport sub : subList) {
                if (sub.getSupportId().equals(UUID.fromString(supportInfo.getSupportId())))
                    return sub;
            }
        }
        return null;
    }

    /**
     * Returns the handler registered with the specified id, or null if the handler is not found.
     */
    public synchronized IContextHandler getContextHandler(UUID id) {
        if (id != null) {
            for (IBinder l : handlerMap.keySet()) {
                ContextHandlerMapping test = handlerMap.get(l);
                if (test != null) {
                    if (id.equals(test.id)) {
                        return test.handler;
                    }
                }
            }
        } else
            Log.w(TAG, "getDynamixHandler received null id");
        return null;
    }

    /**
     * Returns the handler registered with the specified binder, or null if the handler is not found.
     */
    public synchronized IContextHandler getContextHandler(IBinder binder) {
        if (binder != null) {
            for (IBinder l : handlerMap.keySet()) {
                ContextHandlerMapping test = handlerMap.get(l);
                if (test != null) {
                    if (test.equals(binder)) {
                        return test.handler;
                    }
                }
            }
        } else
            Log.w(TAG, "getDynamixHandler received null binder");
        return null;
    }

    /**
     * Returns true if the session contains the context handler; false otherwise.
     */
    public boolean hasContextHandler(IContextHandler handler) {
        if (handler != null) {
            return handlerMap.containsKey(handler.asBinder());
        } else
            Log.w(TAG, "hasContextHandler received null handler");
        return false;
    }

    /**
     * Returns the id for the specified handler, or null if the handler is not found.
     */
    public String getContextHandlerId(IContextHandler handler) {
        ContextHandlerMapping mapping = handlerMap.get(handler.asBinder());
        if (mapping != null)
            return mapping.id.toString();
        else
            return null;
    }

    /**
     * Returns the registered context handlers for this session.
     */
    public List<IContextHandler> getContextHandlers() {
        List<IContextHandler> handlers = new ArrayList<IContextHandler>();
        for (ContextHandlerMapping mapping : handlerMap.values()) {
            handlers.add(mapping.handler);
        }
        return handlers;
    }

    /**
     * Returns the appKey for this session
     */
    public String getAppKey() {
        return this.appKey;
    }

    /**
     * Returns the session identifier
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Returns true if the handler has a context support of the specified type.
     *
     * @param handler     The handler to check.
     * @param contextType The context type to check.
     */
    public synchronized boolean hasContextSupportForType(IContextHandler handler, String contextType) {
        List<ContextSupport> installedSupport = getContextSupport(handler);
        for (ContextSupport support : installedSupport) {
            if (support.getContextType().equalsIgnoreCase(contextType))
                return true;
        }
        return false;
    }

    /**
     * Returns true if the specified handler is registered with the session; false otherwise.
     */
    public boolean isContextHandlerRegistered(IContextHandler handler) {
        return handlerMap.containsKey(handler.asBinder());
    }

    /**
     * Returns true if the session is open; false otherwise
     */
    public boolean isSessionOpen() {
        return app != null;
    }

    /**
     * Opens the session for the app, which establishes a unique sessionId.
     */
    public synchronized void openSession(String sessionId) {
        this.sessionId = sessionId;
        Log.d(TAG, this.app + " had its session opened with sessionId: " + sessionId);
    }

    /**
     * Closes the session, removing the app's context support and cleaning up state.
     *
     * @param notify True if the app should be notified of the session close; false otherwise.
     */
    public synchronized void closeSession(boolean notify, ICallback callback) {
        Log.d(TAG, "Closing session for appKey " + appKey + " with app " + app);
        if (isSessionOpen()) {
            // Clear all session firewall rules
            app.clearSessionFirewallRules();
            /*
			 * Make sure that the contextSupportMap is empty. Note that the contextSupportMap should be empty, since the
			 * ContextManager is typically called first during a session close operation so that it can remove context
			 * support resources. During this process, the ContextManager also calls removeContextSupport
			 */
            if (!contextSupportMap.isEmpty()) {
                for (Set<ContextSupport> supList : contextSupportMap.values()) {
                    for (final ContextSupport sup : supList) {
                        // Notify any listeners
                        if (sup.hasContextListener()) {
							/*
							 * Gather up all the runtimes associated with the subscription.
							 */
                            List<ContextPluginRuntime> runtimes = new ArrayList<ContextPluginRuntime>();
                            for (ContextPlugin plug : sup.getContextPlugins()) {
                                ContextPluginRuntimeWrapper wrapper = DynamixService.getContextPluginRuntime(
                                        plug.getId(), plug.getVersion());
                                if (wrapper != null) {
                                    runtimes.add(wrapper.getContextPluginRuntime());
                                } else
                                    Log.e(TAG, "Runtime wrapper was null in closeSession!");
                            }
                            ContextPluginRuntimeMethodRunners.launchThread(
                                    new RemoveContextSubscriptions(runtimes, sup.getSupportId()),
                                    ContextManager.getThreadPriorityForPowerScheme());
                        } else {
                            // Nothing to do?
                        }
                    }
                }
                removeAllContextSupport();
                contextSupportMap.clear();
                Log.d(TAG, "Removed all context support from the session");
            }
            Log.w(TAG, "TODO: Cancelling outstanding requests...");
            Log.w(TAG, "TODO: Closing any context acquisition interfaces...");
            Log.d(TAG, "Session closed for app: " + app);
            // Handle callback
            SessionManager.sendCallbackSuccess(callback);
            // Handle notification
            if (notify)
                SessionManager.notifySessionClosed(this);
            // Remove the app, if it was remotely paired
            if (app.isRemotelyPaired() && app.getRemotePairing().isTemporary()) {
                Log.d(TAG, "DynamixSession is removing temporarily paired app on session close: " + app);
                DynamixService.removeApplicationFromSettings(app);
            }
            // Setting the app to null closes the session
            this.app = null;
            // Also null out the sessionId
            this.sessionId = null;
        } else {
            Log.d(TAG, "Session was already closed for " + app);
            // Handle callback
            SessionManager.sendCallbackSuccess(callback);
            // Handle notification
            if (notify)
                SessionManager.notifySessionClosed(this);
        }
    }

    /**
     * Removes the context handler from the session.
     *
     * @param handler The handler to remove
     * @return True if the listener was removed; false otherwise.
     */
    public synchronized boolean removeContextHandler(IContextHandler handler) {
        if (handler != null) {
            // Remove the handler
            if (handlerMap.containsKey(handler.asBinder())) {
                handlerMap.remove(handler.asBinder());
                Log.d(TAG, "Removed handler " + handler);
                return true;
            } else
                Log.v(TAG, this + " did not find handler " + handler + " to remove");
        }
        return false;
    }

    /**
     * Removes all context support for all registered listeners.
     */
    public synchronized void removeAllContextSupport() {
        // Create a snapshot of the support registrations
        List<ContextSupport> supportSnapshot = new ArrayList<ContextSupport>();
        for (IBinder listener : contextSupportMap.keySet()) {
            supportSnapshot.addAll(contextSupportMap.get(listener));
        }
        for (ContextSupport sub : supportSnapshot)
            removeContextSupport(sub);
    }

    /**
     * Removes the handler's context support and any associated cached events.
     *
     * @param handler The handler to remove context support from.
     */
    public synchronized void removeContextSupport(IContextHandler handler) {
        Log.d(TAG, "removeContextSupport for: " + handler);
        List<ContextSupport> removeList = new ArrayList<ContextSupport>();
        Set<ContextSupport> supportSnapshot = contextSupportMap.get(handler.asBinder());
        if (supportSnapshot != null) {
            for (ContextSupport support : supportSnapshot)
                removeList.add(support);
        } else
            Log.d(TAG, this + " did not find handler " + handler + " to remove");
        // Remove each support registration
        for (ContextSupport support : removeList)
            removeContextSupport(support);
    }

    /**
     * Removes all context support registrations from the specified plug-in.
     *
     * @param plug The plug-in to remove context support for.
     */
    public synchronized void removeContextSupportFromPlugin(ContextPluginInformation plug) {
        Log.d(TAG, "removeContextSupportForPlugin for: " + plug);
        List<ContextSupport> removeList = new ArrayList<ContextSupport>();
        // Create a snapshot of all support registrations
        Collection<Set<ContextSupport>> allSupportRegistrations = contextSupportMap.values();
        // Go through all support registrations, storing those that match the plug-in
        if (allSupportRegistrations != null) {
            for (Set<ContextSupport> supportList : allSupportRegistrations) {
                for (ContextSupport sub : supportList) {
                    if (sub.containsContextPlugin(plug))
                        removeList.add(sub);
                }
            }
        }
        // Remove the necessary support registrations
        for (ContextSupport support : removeList)
            removeContextSupport(support);
    }

    public synchronized Result removeContextSupport(ContextSupport contextSupport) {
        return removeContextSupport(contextSupport, "NO_ERROR", ErrorCodes.SUCCESS);
    }

    /**
     * Removes a specific context support from the listener.
     */
    public synchronized Result removeContextSupport(ContextSupport contextSupport, String message, int errorCode) {
        IContextHandler handler = contextSupport.getContextHandler();
        Log.d(TAG, "Removing " + contextSupport);
        String returnErrorMessage = null;
        int returnErrorCode = 0;
        boolean contextSupportFound = false;
        if (handler != null) {
            if (contextSupport != null) {
                synchronized (contextSupportMap) {
                    // Check if the handler was registered for context support
                    if (contextSupportMap.keySet().contains(handler.asBinder())) {
                        // Access the registrations for the handler
                        Set<ContextSupport> contextSupportRegistrations = contextSupportMap.get(handler.asBinder());
                        // Check if the incoming context support is registered
                        if (contextSupportRegistrations.contains(contextSupport)) {
                            // Yep, so remove the support from the listener
                            contextSupportFound = true;
                            Log.d(TAG, "removeContextSupport is removing " + contextSupport);
                            if (contextSupportRegistrations.remove(contextSupport)) {
                                // Remove cached events
                                DynamixService.removeCachedContextEvents(handler, contextSupport.getContextType());
                            }
                        }
                    }
                    if (contextSupportFound) {
                        // Update cache and listener
                        if (contextSupport.hasContextListener()) {
                            DynamixService.removeCachedContextEvents(contextSupport.getContextListener(),
                                    contextSupport.getContextType());
                            SessionManager.notifyContextSupportRemoved(contextSupport, message, errorCode);
                        }
                        // Return success
                        return new Result();
                    } else {
                        returnErrorCode = ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND;
                        returnErrorMessage = "Context support not present for: " + handler;
                        Log.w(TAG, "Context support not present for: " + handler);
                    }
                }
            } else {
                returnErrorCode = ErrorCodes.MISSING_PARAMETERS;
                returnErrorMessage = "removeContextSupport received null contextSupport";
                Log.w(TAG, "removeContextSupport received null contextSupport");
            }
        } else {
            returnErrorCode = ErrorCodes.MISSING_PARAMETERS;
            returnErrorMessage = "removeContextSupport received null handler";
            Log.w(TAG, "removeContextSupport received null handler");
        }
        return new Result(returnErrorMessage, returnErrorCode);
    }

    @Override
    public String toString() {
        return "DynamixSession for app = " + app;
    }

    /**
     * Update's the application for this session.
     */
    public void updateApp(DynamixApplication app) {
        this.app = app;
        verifyContextSupportAccess();
    }

    public ISessionListener getSessionListener() {
        return sessionListener;
    }

    public void setSessionListener(ISessionListener sessionListener) {
        this.sessionListener = sessionListener;
    }

    private void verifyContextSupportAccess() {
        List<ContextSupport> remove = new ArrayList<ContextSupport>();
        for (ContextSupport sup : getAllContextSupport()) {
            if (!app.isAccessGranted(sup.getContextType(), sup.getContextPluginInformation())) {
                Log.w(TAG, "Context Support Revoked: " + sup);
                remove.add(sup);
            }
        }
        for (ContextSupport sup : remove) {
            DynamixService.removeContextSupport(sup.getContextHandler(),
                    sup.getContextSupportInfo(), null);
        }
    }
}