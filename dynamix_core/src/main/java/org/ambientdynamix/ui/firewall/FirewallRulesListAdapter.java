/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.firewall;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.core.FirewallRule;
import org.ambientdynamix.core.FirewallRule.FirewallAccess;
import org.ambientdynamix.core.R;

import java.util.ArrayList;
import java.util.List;

public class FirewallRulesListAdapter extends BaseAdapter {
    List<FirewallRule> addContextSupportList;
    LayoutInflater layoutInflater;
    Activity activity;
    private String LOGTAG = getClass().getSimpleName();
    ArrayList<Integer> hiddenItems;

    public FirewallRulesListAdapter(Activity activity, List<FirewallRule> addContextSupportList) {
        this.addContextSupportList = addContextSupportList;
        this.activity = activity;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        hiddenItems = new ArrayList<Integer>();
    }

    @Override
    public int getCount() {
        return addContextSupportList.size() - hiddenItems.size();
    }

    @Override
    public Object getItem(int position) {
        return addContextSupportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        final ViewHolder holder;
        final int positionCopy = position;
        for (Integer hiddenIndex : hiddenItems) {
            if (hiddenIndex <= position) {
                position = position + 1;
            }
        }
        final FirewallRule rule = addContextSupportList.get(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.context_support_list_item_layout, null);
            holder = new ViewHolder();
            holder.riskLevelImage = (ImageView) convertView.findViewById(R.id.riskLevel);
            holder.riskLevelText = (TextView) convertView.findViewById(R.id.riskLevelText);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.supportedPluginsHeader = (TextView) convertView.findViewById(R.id.supportedPluginsHeader);
            holder.supportedPluginsList = (ListView) convertView.findViewById(R.id.supportedPlugins);
            holder.togglePluginList = (ImageView) convertView.findViewById(R.id.togglePlugins);
            holder.rightContainer = (RelativeLayout) convertView.findViewById(R.id.containerRight);
            holder.radioGroup = (RadioGroup) convertView.findViewById(R.id.radioGroup);
            holder.always = (RadioButton) convertView.findViewById(R.id.always);
            holder.once = (RadioButton) convertView.findViewById(R.id.once);
            holder.never = (RadioButton) convertView.findViewById(R.id.never);
            holder.deny = (Button) convertView.findViewById(R.id.denyButton);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (holder.radioGroup.getCheckedRadioButtonId()) {
                    case R.id.always:
                        rule.setAccess(FirewallAccess.ALLOWED_ALWAYS);
                        Log.d(LOGTAG, "Access level selected >> " + FirewallAccess.ALLOWED_ALWAYS);
                        break;
                    case R.id.once:
                        rule.setAccess(FirewallAccess.ALLOWED_SESSION);
                        Log.d(LOGTAG, "Access level selected >> " + FirewallAccess.ALLOWED_SESSION);
                        break;
                    case R.id.never:
                        rule.setAccess(FirewallAccess.BLOCKED);
                        Log.d(LOGTAG, "Access level selected >> " + FirewallAccess.BLOCKED);
                        break;
                    default:
                        break;
                }
            }
        });
        switch (rule.getFirewallAccess()) {
            case ALLOWED_ALWAYS:
                holder.always.setChecked(true);
                break;
            case ALLOWED_SESSION:
                holder.once.setChecked(true);
                break;
            case BLOCKED:
                holder.never.setChecked(true);
                break;
            case PENDING:
            case DENIED:
                holder.once.setChecked(true);
                break;
        }
        holder.deny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rule.setAccess(FirewallAccess.DENIED);
                hiddenItems.add(new Integer(positionCopy));
                notifyDataSetChanged();
                Log.d(LOGTAG, "Deny button clicked");
            }
        });
        holder.rightContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.supportedPluginsList.getVisibility() == View.VISIBLE) {
                    holder.supportedPluginsList.setVisibility(View.GONE);
                    holder.togglePluginList.setImageResource(R.drawable.ic_action_expand);
                    holder.supportedPluginsHeader.setVisibility(View.GONE);
                } else {
                    holder.supportedPluginsList.setVisibility(View.VISIBLE);
                    holder.togglePluginList.setImageResource(R.drawable.ic_action_collapse);
                    holder.supportedPluginsHeader.setVisibility(View.VISIBLE);
                }
            }
        });
        if (rule.getContextTypeName() != null)
            holder.title.setText(rule.getContextTypeName().trim());
        if (rule.getContextTypeDescription() != null)
            holder.description.setText(rule.getContextTypeDescription().trim());
        PluginListAdapter adapter = new PluginListAdapter(activity, new ArrayList<ContextPluginInformation>(
                rule.getPlugins()));
        holder.supportedPluginsList.setAdapter(adapter);
        return convertView;
    }

    private static class ViewHolder {
        ImageView riskLevelImage;
        TextView riskLevelText;
        TextView title;
        TextView description;
        TextView supportedPluginsHeader;
        ListView supportedPluginsList;
        ImageView togglePluginList;
        RelativeLayout rightContainer;
        RadioGroup radioGroup;
        FirewallRule rule;
        RadioButton always;
        RadioButton once;
        RadioButton never;
        Button deny;
    }
}
