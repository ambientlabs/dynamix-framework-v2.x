/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.firewall;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class SwipeDetector implements View.OnTouchListener {
	public static enum Action {
		LR, // Left to Right
		RL, // Right to Left
		TB, // Top to bottom
		BT, // Bottom to Top
		None // when no action was detected
	}

	private static final String logTag = "SwipeDetector";
	int HORIZONTAL_MIN_DISTANCE = 40;
	int VERTICAL_MIN_DISTANCE = 80;
	private float downX, downY, upX, upY;
	private Action mSwipeDetected = Action.None;

	public boolean swipeDetected() {
		return mSwipeDetected != Action.None;
	}

	public Action getAction() {
		return mSwipeDetected;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: {
			downX = event.getX();
			downY = event.getY();
			mSwipeDetected = Action.None;
			return false; // allow other events like Click to be processed
		}
		case MotionEvent.ACTION_MOVE: {
			upX = event.getX();
			upY = event.getY();
			float deltaX = downX - upX;
			float deltaY = downY - upY;
			// horizontal swipe detection
			if (Math.abs(deltaX) > HORIZONTAL_MIN_DISTANCE) {
				// left or right
				if (deltaX < 0) {
					Log.i(logTag, "Swipe Left to Right");
					mSwipeDetected = Action.LR;
					return true;
				}
				if (deltaX > 0) {
					Log.i(logTag, "Swipe Right to Left");
					mSwipeDetected = Action.RL;
					return true;
				}
			} else
			// vertical swipe detection
			if (Math.abs(deltaY) > VERTICAL_MIN_DISTANCE) {
				// top or down
				if (deltaY < 0) {
					Log.i(logTag, "Swipe Top to Bottom");
					mSwipeDetected = Action.TB;
					return false;
				}
				if (deltaY > 0) {
					Log.i(logTag, "Swipe Bottom to Top");
					mSwipeDetected = Action.BT;
					return false;
				}
			}
			return true;
		}
		}
		return false;
	}
}