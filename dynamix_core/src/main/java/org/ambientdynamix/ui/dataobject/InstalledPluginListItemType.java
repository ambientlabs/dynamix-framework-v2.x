package org.ambientdynamix.ui.dataobject;

/**
 * Created by shivam on 12/17/14.
 */
public class InstalledPluginListItemType {
	final public static int HEADER_VIEW = 0;
	final public static int CONTEXT_PLUGIN_VIEW = 1;
}
