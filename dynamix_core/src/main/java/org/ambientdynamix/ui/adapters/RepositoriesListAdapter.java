/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.adapters;

import java.util.ArrayList;

import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.util.Repository;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RepositoriesListAdapter extends BaseAdapter {
	ArrayList<Repository> repoURLList;
	Activity activity;

	public RepositoriesListAdapter(Activity activity, ArrayList<Repository> repoURLList) {
		this.activity = activity;
		this.repoURLList = repoURLList;
	}

	@Override
	public int getCount() {
		return repoURLList.size();
	}

	@Override
	public Object getItem(int position) {
		return repoURLList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final Repository info = repoURLList.get(position);
		if (convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.external_repo_list_item_layout, null);
			holder = new ViewHolder();
			holder.url = (TextView) convertView.findViewById(R.id.repoURL);
			holder.alias = (TextView) convertView.findViewById(R.id.alias);
			holder.enable = (Button) convertView.findViewById(R.id.enable);
			holder.disable = (Button) convertView.findViewById(R.id.disable);
			holder.repoOptions = (LinearLayout) convertView.findViewById(R.id.repoOptions);
			holder.expand = (ImageButton) convertView.findViewById(R.id.expand);
			holder.repoIcon = (ImageView) convertView.findViewById(R.id.repoIcon);
			holder.edit = (Button) convertView.findViewById(R.id.edit);
			holder.delete = (Button) convertView.findViewById(R.id.delete);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.url.setText(info.getUrl());
		holder.alias.setText(info.getAlias());
		holder.expand.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (holder.repoOptions.getVisibility() == View.VISIBLE) {
					holder.repoOptions.setVisibility(View.GONE);
					holder.expand.setBackgroundResource(R.drawable.ic_action_expand);
				} else {
					holder.repoOptions.setVisibility(View.VISIBLE);
					holder.expand.setBackgroundResource(R.drawable.ic_action_collapse);
				}
			}
		});
		holder.edit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showEditDialog(info, position);
			}
		});
		holder.delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DynamixService.removeContextPluginRepo(info);
				repoURLList.remove(position);
				notifyDataSetChanged();
				/*
				 * Not refreshing repos here, since multiple repo changes may be missed. Repos are refreshed in
				 * RepositoriesFragment.onPause();
				 */
				// DynamixService.refreshPluginsFromRepos();
			}
		});
		holder.disable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				info.setEnabled(false);
				DynamixService.updateContextPluginRepo(info);
				holder.disable.setVisibility(View.GONE);
				holder.enable.setVisibility(View.VISIBLE);
				holder.repoIcon.setImageResource(R.drawable.repo_disabled);
				/*
				 * Not refreshing repos here, since multiple repo changes may be missed. Repos are refreshed in
				 * RepositoriesFragment.onPause();
				 */
				// DynamixService.refreshPluginsFromRepos();
			}
		});
		holder.enable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				info.setEnabled(true);
				DynamixService.updateContextPluginRepo(info);
				holder.disable.setVisibility(View.VISIBLE);
				holder.enable.setVisibility(View.GONE);
				holder.repoIcon.setImageResource(R.drawable.repo_enabled);
				/*
				 * Not refreshing repos here, since multiple repo changes may be missed. Repos are refreshed in
				 * RepositoriesFragment.onPause();
				 */
				// DynamixService.refreshPluginsFromRepos();
			}
		});
		if (!info.isRemovable()) {
			holder.edit.setEnabled(false);
			holder.delete.setEnabled(false);
		} else {
			holder.edit.setEnabled(true);
			holder.delete.setEnabled(true);
		}
		if (info.isEnabled()) {
			holder.repoIcon.setImageResource(R.drawable.repo_enabled);
			holder.disable.setVisibility(View.VISIBLE);
			holder.enable.setVisibility(View.GONE);
		} else {
			holder.repoIcon.setImageResource(R.drawable.repo_disabled);
			holder.disable.setVisibility(View.GONE);
			holder.enable.setVisibility(View.VISIBLE);
		}
		return convertView;
	}

	private static class ViewHolder {
		TextView url;
		TextView alias;
		Button enable;
		Button disable;
		LinearLayout repoOptions;
		ImageButton expand;
		ImageView repoIcon;
		Button edit;
		Button delete;
	}

	private void showEditDialog(Repository info, final int position) {
		final Dialog repositoryEditDialog = new Dialog(activity);
		repositoryEditDialog.setContentView(R.layout.dialog);
		repositoryEditDialog.setTitle("Edit Repository");
		final EditText repoURL = (EditText) repositoryEditDialog.findViewById(R.id.repoURL);
		repoURL.setText(info.getUrl());
		final EditText userName = (EditText) repositoryEditDialog.findViewById(R.id.userName);
		userName.setText(info.getUsername());
		final EditText password = (EditText) repositoryEditDialog.findViewById(R.id.password);
		final EditText alias = (EditText) repositoryEditDialog.findViewById(R.id.alias);
		alias.setText(info.getAlias());
		Button save = (Button) repositoryEditDialog.findViewById(R.id.save);
		Button dismiss = (Button) repositoryEditDialog.findViewById(R.id.dismiss);
		dismiss.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				repositoryEditDialog.dismiss();
			}
		});
		save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = repoURL.getText().toString();
				url = url.trim();
				String uname = userName.getText().toString();
				uname = uname.trim();
				String pword = password.getText().toString();
				pword = pword.trim();
				if (url.trim().equals("")) {
					Toast.makeText(activity, "Please enter a valid URL", Toast.LENGTH_LONG).show();
				} else {
					Repository info = repoURLList.get(position);
					info.setAlias(alias.getText().toString());
					info.setUrl(url);
					info.setPassword(pword);
					info.setUsername(uname);
					DynamixService.updateContextPluginRepo(info);
					notifyDataSetChanged();
					DynamixService.refreshPluginsFromRepos();
					repositoryEditDialog.dismiss();
				}
			}
		});
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = repositoryEditDialog.getWindow();
		lp.copyFrom(window.getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		repositoryEditDialog.show();
	}
}
