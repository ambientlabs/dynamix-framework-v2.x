/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.adapters;

import org.ambientdynamix.ui.fragments.ApplicationsFragment;
import org.ambientdynamix.ui.fragments.FeaturesFragment;
import org.ambientdynamix.ui.fragments.InstalledPluginsFragment;
import org.ambientdynamix.ui.fragments.PendingPluginsFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class FragmentsListAdapter extends FragmentStatePagerAdapter {
	private ApplicationsFragment applicationsFragment;
	private InstalledPluginsFragment installedPluginsFragment;
	private PendingPluginsFragment pendingPluginsFragment;
	private FeaturesFragment featuresFragment;
	private String tabtitles[] = new String[] { "Applications", "Features", "Available Plugins", "Installed Plugins" };

	public ApplicationsFragment getApplicationsFragment() {
		return applicationsFragment;
	}

	public InstalledPluginsFragment getInstalledPluginsFragment() {
		return installedPluginsFragment;
	}

	public PendingPluginsFragment getPendingPluginsFragment() {
		return pendingPluginsFragment;
	}

	public FeaturesFragment getFeaturesFragment() {
		return featuresFragment;
	}

	public FragmentsListAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int pos) {
		switch (pos) {
		case 0:
			applicationsFragment = new ApplicationsFragment();
			return applicationsFragment;
		case 1:
			featuresFragment = new FeaturesFragment();
			return featuresFragment;
		case 3:
			installedPluginsFragment = new InstalledPluginsFragment();
			return installedPluginsFragment;
		case 2:
			pendingPluginsFragment = new PendingPluginsFragment();
			return pendingPluginsFragment;
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return 4;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabtitles[position];
	}
}