/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.adapters;

import java.util.ArrayList;

import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.FrameworkConstants;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.activities.FirewallRulesActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ApplicationsListAdapter extends BaseAdapter {
	Activity activity;
	ArrayList<DynamixApplication> applicationsList;
	PackageManager pm;
	Handler handler = new Handler();

	public ApplicationsListAdapter(Activity activity, ArrayList<DynamixApplication> applicationsList) {
		this.activity = activity;
		this.applicationsList = applicationsList;
		pm = activity.getPackageManager();
	}

	@Override
	public int getCount() {
		return applicationsList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		final DynamixApplication application = applicationsList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.app_list_item, null);
			holder.applicationIcon = (ImageView) convertView.findViewById(R.id.app_icon);
			holder.plugged = (ImageView) convertView.findViewById(R.id.plugged);
			holder.applicationName = (TextView) convertView.findViewById(R.id.app_name);
			holder.versionName = (TextView) convertView.findViewById(R.id.versionName);
			holder.applicationType = (TextView) convertView.findViewById(R.id.type);
			holder.deleteButton = (Button) convertView.findViewById(R.id.deleteButton);
			holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (DynamixService.hasOpenSessions(application)) {
			holder.plugged.setVisibility(View.VISIBLE);
		} else {
			/*
			 * holder.applicationIcon .setImageDrawable(convertToGrayscale(getAppIcon(packageName)));
			 */
			holder.plugged.setVisibility(View.INVISIBLE);
		}
		holder.applicationName.setText(application.getName());
		String version = application.getAppDetails().get(DynamixApplication.VERSION_CODE);
		holder.versionName.setText("Version Code : " + version);
		switch (application.getAppType()) {
		case AIDL:
			String packageName = application.getAppDetails().get(DynamixApplication.PACKAGE_NAME);
			holder.applicationType.setText("Android Application");
			holder.applicationIcon.setImageDrawable(getAppIcon(packageName));
			break;
		case WEB:
			holder.applicationType.setText("Web Application");
			break;
		case INTERNAL:
			holder.applicationType.setText("Internal");
			break;
		case PLUG_IN:
			holder.applicationType.setText("Plugin");
			break;
		}
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, FirewallRulesActivity.class);
				intent.putExtra(FrameworkConstants.APPLICATION_KEY, application);
				activity.startActivity(intent);
				activity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			}
		});
		holder.deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				holder.progressBar.setVisibility(View.VISIBLE);
				holder.deleteButton.setVisibility(View.GONE);
				DynamixService.removeDynamixApplication(application, new Callback() {
					@Override
					public void onSuccess() throws RemoteException {
						handler.post(new Runnable() {
							@Override
							public void run() {
								holder.progressBar.setVisibility(View.GONE);
								holder.deleteButton.setVisibility(View.VISIBLE);
							}
						});

						super.onSuccess();
					}

					@Override
					public void onFailure(String message, int errorCode) throws RemoteException {
						handler.post(new Runnable() {
							@Override
							public void run() {
								holder.progressBar.setVisibility(View.GONE);
								holder.deleteButton.setVisibility(View.VISIBLE);
							}
						});

						super.onFailure(message, errorCode);
					}
				});
			}
		});
		return convertView;
	}

	private class ViewHolder {
		ImageView applicationIcon;
		ImageView plugged;
		TextView applicationName;
		TextView versionName;
		TextView applicationType;
		Button deleteButton;
		ProgressBar progressBar;
	}

	private Drawable getAppIcon(String packageName) {
		try {
			ApplicationInfo app = pm.getApplicationInfo(packageName, 0);
			Drawable icon = pm.getApplicationIcon(app);
			return icon;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected Drawable convertToGrayscale(Drawable drawable) {
		ColorMatrix matrix = new ColorMatrix();
		matrix.setSaturation(0);
		ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
		drawable.setColorFilter(filter);
		return drawable;
	}
}
