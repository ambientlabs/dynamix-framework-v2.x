/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.activities;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.FirewallRule;
import org.ambientdynamix.core.FrameworkConstants;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.firewall.FirewallRulesListAdapter;
import org.ambientdynamix.util.Log;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FirewallRulesActivity extends Activity {
	static FirewallRulesListAdapter adapter;
	static ArrayList<FirewallRule> addContextSupportList;
	private String LOGTAG = getClass().getSimpleName();
	TextView noRulesText;
	Button okay, dismiss;
	ImageView icon;
	PackageManager pm;
	private DynamixApplication appExtra;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firewall_rules_activity);
		pm = getPackageManager();
		addContextSupportList = new ArrayList<FirewallRule>();
		adapter = new FirewallRulesListAdapter(this, addContextSupportList);
		ListView listView = (ListView) findViewById(R.id.contextSupportRequestList);
		listView.setAdapter(adapter);
		okay = (Button) findViewById(R.id.okay);
		dismiss = (Button) findViewById(R.id.dismiss);
		noRulesText = (TextView) findViewById(R.id.noRulesText);
		icon = (ImageView) findViewById(R.id.icon);
		appExtra = (DynamixApplication) getIntent().getParcelableExtra(FrameworkConstants.APPLICATION_KEY);
	}

	@Override
	protected void onResume() {
		super.onResume();
		addContextSupportList.clear();
		final DynamixApplication app = DynamixService.refreshDynamixApplication(appExtra);
		if (app != null) {
			String packageName = app.getAppDetails().get(DynamixApplication.PACKAGE_NAME);
			icon.setImageDrawable(getAppIcon(packageName));
			setupActivityHeader(app);
			initListUi(app.getAllFirewallRules());
			okay.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// app.clearFirewallRules();
					for (FirewallRule rule : addContextSupportList)
						app.addFirewallRule(rule);
					DynamixService.updateApplication(app);
					finish();
				}
			});
			dismiss.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
		} else {
			Log.w(LOGTAG, "refreshDynamixApplication failed for appExtra: " + appExtra);
			finish();
		}
	}

	private void initListUi(List<FirewallRule> list) {
		if (list.size() == 0) {
			noRulesText.setVisibility(View.VISIBLE);
			dismiss.setVisibility(View.INVISIBLE);
			okay.setVisibility(View.INVISIBLE);
		} else {
			addContextSupportList.addAll(list);
			adapter.notifyDataSetChanged();
		}
	}

	private void setupActivityHeader(DynamixApplication app) {
		TextView popupTitle = (TextView) findViewById(R.id.popupTitle);
		switch (app.getAppType()) {
		case WEB:
			popupTitle.setText("The " + app.getName()
					+ " web app has access to the following Dynamix resources. Use the options below to modify.");
			break;
		case AIDL:
			popupTitle.setText("The " + app.getName()
					+ " android app has access to the following Dynamix resources. Use the options below to modify.");
			break;
		default:
			popupTitle.setText("The " + app.getName()
					+ " app has access to the following Dynamix resources. Use the options below to modify.");
			break;
		}
	}

	private Drawable getAppIcon(String packageName) {
		try {
			ApplicationInfo app = pm.getApplicationInfo(packageName, 0);
			Drawable icon = pm.getApplicationIcon(app);
			return icon;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
