/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.adapters.InstalledPluginsListAdapter;
import org.ambientdynamix.ui.dataobject.PendingPluginListItemType;
import org.ambientdynamix.ui.dataobject.PluginsListViewItem;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class InstalledPluginsFragment extends Fragment {
	Activity activity;
	private String LOGTAG = getClass().getSimpleName();
	final ArrayList<ContextPluginInformation> installedPlugins = new ArrayList<ContextPluginInformation>();
	final ArrayList<PluginsListViewItem> installedPluginListItems = new ArrayList<PluginsListViewItem>(); // contains
																											// the
																											// context
																											// plugins
																											// info
																											// objects
																											// and the
																											// header
																											// objects
	final ArrayList<ContextPluginInformation> uninstalling = new ArrayList<ContextPluginInformation>();
	InstalledPluginsListAdapter installedPluginsAdapter;
	ListView installedPluginsListView;
	TextView noPluginsTextView;
	Button uninstall;
	HashMap<Boolean, ArrayList<ContextPluginInformation>> updateAvailableHashMap;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		activity = getActivity();
		installedPluginsAdapter = new InstalledPluginsListAdapter(activity, installedPluginListItems, uninstalling);
		View layoutView = inflater.inflate(R.layout.installed_plugins_fragment, container, false);
		noPluginsTextView = (TextView) layoutView.findViewById(R.id.noPluginsTextView);
		installedPluginsListView = (ListView) layoutView.findViewById(R.id.installedPluginsListView);
		uninstall = (Button) layoutView.findViewById(R.id.uninstallButton);
		installedPluginsListView.setAdapter(installedPluginsAdapter);
		updateAvailableHashMap = new HashMap<>();
		// Initialize with two values, one for plugins with updates available and the other one for up to date plugins.
		updateAvailableHashMap.put(true, new ArrayList<ContextPluginInformation>());
		updateAvailableHashMap.put(false, new ArrayList<ContextPluginInformation>());
		uninstall.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (installedPluginsAdapter.getPluginsToUninstall().size() > 0) {
					uninstall.setEnabled(false);
					DynamixService.uninstallPlugins(installedPluginsAdapter.getPluginsToUninstall());
					uninstalling.addAll(installedPluginsAdapter.getPluginsToUninstall());
					installedPluginsAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(activity, "No plugins selected", Toast.LENGTH_SHORT).show();
				}
			}
		});
		reInitList();
		return layoutView;
	}

	public void reInitList() {
		if (DynamixService.isFrameworkInitialized()) {
			synchronized (installedPluginListItems) {
				installedPlugins.clear();
				installedPlugins.addAll(DynamixService.getInstalledContextPluginInformation());
			}
			refreshList();
		}
	}

	private void refreshList() {
		Log.d(LOGTAG, "Refreshing Installed Plugins ListView");
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (DynamixService.isFrameworkInitialized()) {
					synchronized (installedPluginListItems) {
						if (uninstalling.size() == 0) {
							// All plugins have been uninstalled, therefore, enable the uninstall button
							uninstall.setEnabled(true);
						}
						installedPluginListItems.clear();
						updateAvailableHashMap.get(true).clear();
						updateAvailableHashMap.get(false).clear();
						for (ContextPluginInformation plugin : installedPlugins) {
							updateAvailableHashMap.get(false).add(plugin);
						}
						for (Map.Entry<Boolean, ArrayList<ContextPluginInformation>> entry : updateAvailableHashMap
								.entrySet()) {
							ArrayList<ContextPluginInformation> contextPluginInformationArrayList = updateAvailableHashMap
									.get(entry.getKey());
							Log.d(LOGTAG, "Plugins with update status " + entry.getKey() + " : "
									+ contextPluginInformationArrayList.size());
							if (contextPluginInformationArrayList.size() != 0) {
								installedPluginListItems.add(new PluginsListViewItem(
										PendingPluginListItemType.HEADER_VIEW, entry.getKey() ? "Update Available"
												: "Up To Date"));
								for (ContextPluginInformation plugin : contextPluginInformationArrayList) {
									installedPluginListItems.add(new PluginsListViewItem(
											PendingPluginListItemType.CONTEXT_PLUGIN_VIEW, plugin));
								}
							}
						}
						installedPluginsAdapter.notifyDataSetChanged();
					}
					updateViewVisibility();
				}
			}
		});
	}

	public void onPluginInstalled(ContextPluginInformation plugin) {
		Log.d(LOGTAG, "Plugin Installed : " + plugin.getPluginName());
		if (uninstalling.contains(plugin)) {
			uninstalling.remove(plugin);
		}
		if (!installedPlugins.contains(plugin)) {
			installedPlugins.add(plugin);
		}
		refreshList();
	}

	public void onPluginUninstalled(ContextPluginInformation plugin) {
		Log.d(LOGTAG, "Plugin UnInstalled : " + plugin.getPluginName());
		if (uninstalling.contains(plugin)) {
			uninstalling.remove(plugin);
		}
		if (installedPlugins.contains(plugin)) {
			installedPlugins.remove(plugin);
		}
		installedPluginsAdapter.onPluginUninstalled(plugin);
		refreshList();
	}

	private void updateViewVisibility() {
		if (installedPlugins.size() > 0) {
			installedPluginsListView.setVisibility(View.VISIBLE);
			noPluginsTextView.setVisibility(View.GONE);
			uninstall.setVisibility(View.VISIBLE);
		} else {
			installedPluginsListView.setVisibility(View.INVISIBLE);
			noPluginsTextView.setVisibility(View.VISIBLE);
			uninstall.setVisibility(View.INVISIBLE);
		}
	}
}
