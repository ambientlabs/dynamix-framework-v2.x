/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.fragments;

import java.util.ArrayList;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.adapters.FeaturesListAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FeaturesFragment extends Fragment {
	Activity activity;
	FeaturesListAdapter featuresListAdapter;
	ListView featuresListView;
	TextView noFeaturesTextView;
	ArrayList<DynamixFeature> featuresList;

	public void refreshList() {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (DynamixService.isFrameworkInitialized()) {
					featuresList.clear();
					for (ContextPluginInformation info : DynamixService.getInstalledContextPluginInformation()) {
						featuresList.addAll(new ArrayList<DynamixFeature>(info.getDynamixFeatures()));
					}
					featuresListAdapter.notifyDataSetChanged();
				}
				updateViewVisibility();
			}
		});
	}

	private void updateViewVisibility() {
		if (featuresList.size() > 0) {
			noFeaturesTextView.setVisibility(View.GONE);
		} else {
			noFeaturesTextView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		activity = getActivity();
		View rootView = inflater.inflate(R.layout.fragment_features, container, false);
		noFeaturesTextView = (TextView) rootView.findViewById(R.id.noFeaturesTextView);
		ListView featuresListView = (ListView) rootView.findViewById(R.id.featuresList);
		featuresList = new ArrayList<DynamixFeature>();
		featuresListAdapter = new FeaturesListAdapter(activity, featuresList);
		featuresListView.setAdapter(featuresListAdapter);
		featuresListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (DynamixService.isFrameworkStarted()) {
					DynamixFeature feature = featuresList.get(arg2);
					// Check if the plug-in is enabled
					ContextPluginInformation plug = DynamixService.getContextPluginInfo(feature.getPlugId(),
							feature.getPluginVersion());
					if (plug != null && plug.isEnabled()) {
						DynamixService.runDynamixFeature(featuresList.get(arg2));
					} else {
						Toast.makeText(activity, "Cannot Run Features While Plug-in is Disabled", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					Toast.makeText(activity, "Cannot Run Features While Dynamix is Disabled", Toast.LENGTH_LONG).show();
				}
			}
		});
		refreshList();
		return rootView;
	}
}
