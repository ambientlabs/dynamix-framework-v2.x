/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.fragments;

import java.util.ArrayList;

import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.adapters.ApplicationsListAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class ApplicationsFragment extends Fragment {
	Activity activity;
	private String LOGTAG = getClass().getSimpleName();
	ArrayList<DynamixApplication> applicationsList;
	ApplicationsListAdapter applicationsListAdapter;
	TextView noAppsTextView;

	public ApplicationsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layoutView;
		activity = getActivity();
		layoutView = inflater.inflate(R.layout.list_apps_fragment, container, false);
		noAppsTextView = (TextView) layoutView.findViewById(R.id.noAppsText);
		ListView listView = (ListView) layoutView.findViewById(R.id.listOfApps);
		applicationsList = new ArrayList<DynamixApplication>();
		applicationsListAdapter = new ApplicationsListAdapter(activity, applicationsList);
		listView.setAdapter(applicationsListAdapter);
		return layoutView;
	}

	@Override
	public void onResume() {
		refreshList();
		super.onResume();
	}

	private void updateViewVisibility() {
		if (applicationsList.size() > 0) {
			noAppsTextView.setVisibility(View.GONE);
		} else {
			noAppsTextView.setVisibility(View.VISIBLE);
		}
	}

	public void refreshList() {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (DynamixService.isFrameworkInitialized()) {
					applicationsList.clear();
					applicationsList.addAll(DynamixService.getAllDynamixApplications());
					applicationsListAdapter.notifyDataSetChanged();
					updateViewVisibility();
				}
			}
		});
	}
}
