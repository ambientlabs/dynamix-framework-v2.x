/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.remote;

import android.util.Base64;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.security.CryptoUtils;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.SecretKey;

/**
 * Represents a pairing with a remote entity.
 *
 * @author Darren Carlson
 */
public class RemotePairing {
    public String TAG = getClass().getSimpleName();
    private String pairingCode;
    private String masterKey;
    private boolean temporary;
    private int iterationRounds = -1;
    private String lookupKey; // Used to query web service for Dynamix instance info
    private long timestamp; // When the pairing was generated
    private boolean dashboardEnabled = false;
    private List<ContextPluginInformation> allowedDashboardPlugins = new ArrayList<ContextPluginInformation>();
    private String dashboardThemeId;
    private boolean pairingServerUpdated;
    // Volatile fields
    private volatile String sessionKey;
    private volatile String iv;

    public RemotePairing(){}

    /**
     * Creates a RemotePairing with a pairingCode.
     */
    public RemotePairing(String pairingCode, String lookupKey, boolean temporary) throws Exception {
        if (pairingCode == null || pairingCode.length() < 5)
            throw new Exception("Pairing code must be 6 characters or greater!");
        this.pairingCode = pairingCode;
        this.lookupKey = lookupKey;
        this.temporary = temporary;
        timestamp = System.currentTimeMillis();
    }

    /**
     * Sets the encryption key. Note that the masterKey must be Base64 encoded as per
     * Base64.NO_WRAP (omitting all line terminators, i.e., the output will be on one long line).
     */
    public void setMasterKey(String masterKey) {
        this.masterKey = masterKey;
    }

    /**
     * Returns true if this pairing is temporary (i.e., lasting 1 session); false if permanent.
     */
    public boolean isTemporary() {
        return temporary;
    }

    /**
     * Returns the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns true if this remote pairing exposes a dashboard to remote clients; false otherwise.
     */
    public boolean isDashboardEnabled() {
        return dashboardEnabled;
    }

    /**
     * Set true if this remote pairing exposes a dashboard to remote clients; false otherwise.
     */
    public void setDashboardEnabled(boolean dashboardEnabled) {
        this.dashboardEnabled = dashboardEnabled;
    }

    /**
     * Returns the list of plug-ins that are allowed to be exposed by the dashboard.
     */
    public List<ContextPluginInformation> getAllowedDashboardPlugins() {
        return allowedDashboardPlugins;
    }

    /**
     * Sets the list of plug-ins that are allowed to be exposed by the dashboard.
     */
    public void setAllowedDashboardPlugins(List<ContextPluginInformation> allowedDashboardPlugins) {
        this.allowedDashboardPlugins = allowedDashboardPlugins;
    }

    /**
     * Returns the dashboard theme id for this remote pairing, or possibly null if the dashboard isn't enabled.
     */
    public String getDashboardThemeId() {
        return dashboardThemeId;
    }

    /**
     * Sets the dashboard theme id for this remote pairing.
     */
    public void setDashboardThemeId(String dashboardThemeId) {
        this.dashboardThemeId = dashboardThemeId;
    }

    public String getPairingCode() {
        return pairingCode;
    }

    public boolean hasPairingCode() {
        return pairingCode != null;
    }

    public boolean hasMasterKey() {
        return this.masterKey != null;
    }

    public boolean hasSessionKey() {
        return this.sessionKey != null;
    }

    public boolean hasLookupKey() {
        return this.lookupKey != null;
    }

    public String getBase64SessionKey() {
        return this.sessionKey;
    }

    public byte[] getSessionKeyBytes() throws Exception {
        return Base64.decode(sessionKey.getBytes("UTF-8"), Base64.NO_WRAP);
    }

    public void generateSessionKey(String iv, int outputKeyLegnth) throws Exception {
        this.iv = iv;
        // Note: The master key is AES key bytes encoded with Base64.NO_WRAP
        SecretKey key = CryptoUtils.generatePBKDF2Key(masterKey.toCharArray(), Base64.decode(iv.getBytes("UTF-8"), Base64.NO_WRAP), iterationRounds, outputKeyLegnth);
        this.sessionKey = Base64.encodeToString(key.getEncoded(), Base64.NO_WRAP);
    }

    public boolean hasIterationRounds() {
        return iterationRounds != -1;
    }

    public int getIterationRounds() {
        return iterationRounds;
    }

    public void setIterationRounds(int iterationRounds) {
        this.iterationRounds = iterationRounds;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getIv() {
        return iv;
    }

    public byte[] getSessionIvBytes() throws Exception {
        return Base64.decode(iv.getBytes("UTF-8"), Base64.NO_WRAP);
    }

    public boolean isPairingServerUpdated() {
        return pairingServerUpdated;
    }

    public void setPairingServerUpdated(boolean pairingServerUpdated) {
        this.pairingServerUpdated = pairingServerUpdated;
    }
}
