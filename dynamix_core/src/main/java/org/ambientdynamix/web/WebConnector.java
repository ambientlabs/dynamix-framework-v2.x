/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.core.ContextManager;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.SessionManager;
import org.ambientdynamix.core.WebFacadeBinder;
import org.ambientdynamix.remote.RemotePairing;
import org.ambientdynamix.security.CryptoUtils;
import org.ambientdynamix.security.TrustedCert;
import org.apache.commons.validator.routines.UrlValidator;

import java.io.IOException;
import java.net.Socket;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

/**
 * Local web server implementation for handling Dynamix web client requests.
 *
 * @author Darren Carlson
 */
public class WebConnector extends DynamixNanoHTTPD {
    // Private data
    private static final int NONCE_TIMEOUT = 30000; // 30 seconds
    private static final int AES_KEY_LENGTH = 256;
    private static final int NONCE_BIT_LEGNTH = 128;
    private static final int HTTP_SERVER_TIMEOUT = 60000;
    private static WebConnector server;
    private static Map<String, WebAppManager<String>> webAppManagers = new HashMap<String, WebAppManager<String>>();
    static final String TAG = WebConnector.class.getSimpleName();
    private static ContextManager contextMgr;
    private String[] schemes = {"http", "https", "file", "content"}; // DEFAULT schemes = "http", "https", "ftp"
    private UrlValidator urlValidator = new UrlValidator(schemes);
    private static List<TrustedCert> authorizedCerts = new ArrayList<TrustedCert>();
    private static int port;
    private static String ip = "0.0.0.0";
    private final static Map<String, NonceData> issuedNonces = new HashMap<>();
    private static Map<Long, DynamixApplication> tmpPairedApps = new HashMap<>();
    private static Context context;

    private static class NonceData {
        public Long timestamp;
        public String iv;
        public DynamixApplication app;

        public NonceData(String iv) {
            this.iv = iv;
            timestamp = System.currentTimeMillis();
        }

        public NonceData(DynamixApplication app, String iv) {
            this(iv);
            this.app = app;
        }

        public NonceData(DynamixApplication app, Long timestamp) {
            this.app = app;
            this.timestamp = timestamp;
        }

        public boolean hasApp(DynamixApplication app) {
            return this.app != null && this.app.equals(app);
        }
    }

    /**
     * Singleton constructor.
     */
    private WebConnector(int port) throws IOException {
        super(port);
        this.port = port;
    }

    /**
     * Starts the WebConnector and related services.
     *
     * @param port             The port the server should use.
     * @param checkPeriodMills The period (in milliseconds) for determining web client timeouts.
     * @param timeoutMills     The time (in milliseconds) that a web client must interact with the WebConnector before the web client
     *                         times out.
     * @throws IOException
     */
    protected synchronized static void startServer(Context c, ContextManager contextMgr, int port, int checkPeriodMills,
                                                   int timeoutMills) throws IOException {
        startServer(c, contextMgr, port, checkPeriodMills, timeoutMills, null);
    }

    /**
     * Starts the WebConnector and related services.
     *
     * @param contextMgr       The context manager for the WebConnector.
     * @param port             The port the server should use.
     * @param checkPeriodMills The period (in milliseconds) for determining web client timeouts.
     * @param timeoutMills     The time (in milliseconds) that a web client must interact with the WebConnector before the web client
     *                         times out.
     * @param authCerts        A list of authorized X509Certificates for validating web calls.
     * @throws IOException
     */
    public synchronized static void startServer(Context c, final ContextManager contextMgr, int port, int checkPeriodMills,
                                                int timeoutMills, List<TrustedCert> authCerts) throws IOException {
        if (server == null) {
            context = c;
            WebConnector.contextMgr = contextMgr;
            server = new WebConnector(port);
            ListenerMonitor.start(checkPeriodMills, timeoutMills);
            setAuthorizedCerts(authCerts);
            server.start(HTTP_SERVER_TIMEOUT);
        }
        Log.d(TAG, "Dynamix Web Connector Running");
    }

    /**
     * Stops the WebConnector and related services. Clears all authorized certificates.
     */
    public synchronized static void stopServer() {
        if (server != null) {
            server.stop();
            server = null;
            ListenerMonitor.stop();
            // Close all sessions
            synchronized (webAppManagers) {
                for (WebAppManager<String> m : webAppManagers.values()) {
                    m.setDead(true);
                    m.getWebBinder().closeSession();
                }
                webAppManagers.clear();
            }
            clearAuthorizedCerts();
        }
        Log.d(TAG, "Dynamix Web Connector Stopped");
    }

    /**
     * Returns true if the WebConnector is started; false otherwise.
     */
    public synchronized static boolean isStarted() {
        return server != null;
    }

    /**
     * Sets the time period (in milliseconds) between checks for web client timeouts.
     */
    public static void setWebClientTimeoutCheckPeriod(int checkPeriodMills) {
        ListenerMonitor.setCheckPeriod(checkPeriodMills);
    }

    /**
     * Sets the web client timeout duration (in milliseconds).
     */
    public static void setWebClientTimeoutMills(int timeoutMills) {
        ListenerMonitor.setTimeoutMills(timeoutMills);
    }

    /**
     * Pauses timeout checking for web clients.
     */
    public static synchronized void pauseTimeoutChecking() {
        if (server != null) {
            ListenerMonitor.pause();
        } else
            Log.v(TAG, "Not started... ignoring pause request");
    }

    /**
     * Resumes timeout checking for web clients.
     */
    public static synchronized void resumeTimeoutChecking() {
        if (server != null) {
            ListenerMonitor.resume();
        } else
            Log.v(TAG, "Not started... ignoring resume request");
    }

    /**
     * Returns the list of authorized certificates, which are used to validate web calls.
     */
    protected static List<TrustedCert> getAuthorizedCerts() {
        return authorizedCerts;
    }

    /**
     * Adds an authorized certificate, which is used to validate web calls.
     */
    protected static void addAuthorizedCert(TrustedCert authorizedCert) {
        synchronized (authorizedCerts) {
            if (!authorizedCerts.contains(authorizedCert))
                authorizedCerts.add(authorizedCert);
        }
    }

    /**
     * Adds a list of authorized certificates, which are used to validate web calls.
     */
    protected static void setAuthorizedCerts(List<TrustedCert> authCerts) {
        if (authCerts != null)
            synchronized (authorizedCerts) {
                for (TrustedCert cert : authCerts) {
                    if (!authorizedCerts.contains(cert))
                        authorizedCerts.add(cert);
                }
            }
    }

    /**
     * Removes an authorized certificate, which is used to validate web calls.
     */
    protected static void removeAuthorizedCert(TrustedCert authorizedCert) {
        synchronized (authorizedCerts) {
            boolean success = authorizedCerts.remove(authorizedCert);
            Log.i(TAG, "Removing cert " + authorizedCert.getAlias() + " result " + success);
        }
    }

    /**
     * Clears the list of authorized certificates, which are used to validate web calls.
     */
    protected static void clearAuthorizedCerts() {
        synchronized (authorizedCerts) {
            authorizedCerts.clear();
        }
    }

    /**
     * Returns a simple http ok response.
     */
    public static Response createOkResponse() {
        return createOkResponse("OK");
    }

    /**
     * Returns an http ok response using the incoming message.
     */
    public static Response createOkResponse(String message) {
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "createOkResponse with: " + message);
        if (message == null) {
            Log.w(TAG, "createOkResponse with null message");
            message = "";
        }
        Response r = newFixedLengthResponse(message);
        addCorsHeaders(r);
        return r;
    }

    /**
     * Returns an error message.
     */
    public static Response createErrorResponse(String message, Status status, int errorCode) {
        if (message == null) {
            Log.w(TAG, "createErrorResponse with null message");
            message = "";
        }
        Response r = newFixedLengthResponse(status, NanoHTTPD.MIME_PLAINTEXT, message);
        addCorsHeaders(r);
        return r;
    }

    /**
     * Returns an error message.
     */
    public static Response errorResponse(String message, Status status, int errorCode, Exception e) {
        if (e != null)
            Log.e(TAG, message, e);
        else
            Log.w(TAG, message, e);
        // Create a map for return values
        Map<Object, Object> jsonMap = new HashMap<Object, Object>();
        jsonMap.put("message", message);
        jsonMap.put("errorCode", errorCode);
        Response r = newFixedLengthResponse(status, NanoHTTPD.MIME_PLAINTEXT, WebUtils.encodeMapAsJson(jsonMap));
        addCorsHeaders(r);
        return r;
    }

    /**
     * Handles web server processing for commands sent by Dynamix web clients. This method is called by NanoHTTPD for
     * each client call. Each call to this method runs on its own thread, so it's ok to block.
     */
    @Override
    public Response serve(IHTTPSession session) {
        /**
         * Access the origin. If we don't have an origin, the caller may be a remotely paired app. In this case,
         * we rely on the name provided by out-of-band credential exchange, which will be found in
         * the RemotePairing associated with the application.
         */
        String origin = session.getHeaders().get("origin");
        /**
         * Return HTTP_OK for OPTIONS requests to support CORS pre-flighting.
         */
        if (session.getMethod() == Method.OPTIONS) {
            return createOkResponse();
        }
        /**
         * Check if the call is coming from a verified local caller (e.g., Chrome, Firefox).
         */
        boolean localCall = false;
        Socket socket = null;
        try {
            socket = getSocket(session);
            if (socket == null) throw new Exception("Could not access socket");
            localCall = socket.getInetAddress().isLoopbackAddress();
        } catch (Exception e1) {
            // Instead of returning an error, we force clients to pair
            Log.w(TAG, "Problem checking for verified local caller; requiring pairing");
        }
        /**
         * Access the Dynamix app's http token (this may be null, e.g., for unbound local callers and unpaired remote
         * callers). We support both 'authorization' and 'httptoken' for backwards compatibility with older js.
         */
        String httpToken = null;
        if (session.getHeaders().containsKey("authorization"))
            httpToken = session.getHeaders().get("authorization");
        else {
            httpToken = session.getHeaders().get("httptoken");
        }
        // Try to access the WebAppManager based on the token (this may return null)
        WebAppManager<String> waMgr = getWebAppManagerForToken(httpToken);
        // Setup variable for the decrypted URI
        String decryptedUri = null;
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.i(TAG, "Call to URI " + session.getUri() + " with local " + localCall + " and session " + session.getHeaders().toString());
        // Handle local callers
        if (localCall) {
            /**
             * Handle HELLO for the local case. Here, we simply indicate that pairing is
             * not required (since the caller is local).
             */
            if (session.getUri().equalsIgnoreCase(RESTHandler.HELLO)) {
                try {
                    Map<Object, Object> jsonMap = new HashMap<Object, Object>();
                    jsonMap.put("pairingRequired", false);
                    String responseText = WebUtils.encodeMapAsJson(jsonMap);
                    return createOkResponse(responseText);
                } catch (Exception e) {
                    return errorResponse(e.toString(), Status.INTERNAL_ERROR, ErrorCodes.APPLICATION_EXCEPTION, e);
                }
            }
            /**
             * Handle DYNAMIX_BIND for the local case. We verify that the app is approved here only, relying
             * on the http token for future identification of authorized apps.
             */
            else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_BIND)) {
                try {
                    // Check for existing WebAppManager
                    if (waMgr != null && !waMgr.isDead()) {
                        Log.d(TAG, "Returning HTTP_OK for verified local DYNAMIX_BIND for existing origin: " + origin + " with token " + httpToken);
                        return createOkResponse(waMgr.getToken());
                    } else {
                        // Verify that the caller is verified as a valid caller (e.g., chrome, etc.)
                        if (isVerifiedLocalRequest(socket)) {
                            // Create a new httpToken for the caller
                            httpToken = UUID.randomUUID().toString();
                            // Add a new WebappManager
                            addWebappManager(origin, httpToken);
                            Log.d(TAG, "Returning HTTP_OK for verified local DYNAMIX_BIND for new origin: " + origin + " with token " + httpToken);
                            return createOkResponse(httpToken);
                        } else {
                            // App not verified
                            String message = "NOT_AUTHORIZED: Calling app is not allowed!";
                            Log.d(TAG, message);
                            return errorResponse(message, Status.FORBIDDEN, ErrorCodes.NOT_AUTHORIZED, null);
                        }
                    }
                } catch (Exception e) {
                    Log.w(TAG, "DYNAMIX_BIND (verified local) exception: " + e);
                    return errorResponse(e.toString(), Status.FORBIDDEN, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
                }
            }
            /**
             * For all other calls, extract the URI for further processing below.
             */
            else {
                // Set decryptedUri to session.getUri() since the request is not encrypted
                decryptedUri = session.getUri();
            }
        }
        /**
         * Handle remote callers
         */
        else {
            // Check for a remote pairing that matches the incoming httpToken
            DynamixApplication pairedApp = null;
            if (waMgr == null) {
                // Next, check for a DynamixApplication in the settings
                DynamixApplication app = DynamixService.getDynamixApplication(httpToken);
                if (app != null) {
                    if (app.hasRemotePairing()) {
                        waMgr = addWebappManager(app.getAppID(), app.getAppID());
                        pairedApp = app;
                    }
                }
            } else {
                // We have a mapped WebAppManager... check for a remote pairing
                if (waMgr.hasDynamixApplication() && waMgr.getDynamixApp().hasRemotePairing())
                    pairedApp = waMgr.getDynamixApp();
            }
            // Check if we found a pairedApp
            if (waMgr != null && pairedApp != null) {
                /**
                 * Handle the case where the remote caller DOES have an existing pairing.
                 */
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "Found remote pairing for " + pairedApp + " using httpToken " + httpToken);
                /**
                 * Handle HELLO for the paired app.
                 */
                if (session.getUri().equalsIgnoreCase(RESTHandler.HELLO)) {
                    /**
                     * Since we have a pairedApp, associate new nonce data with the app
                     */
                    Map<Object, Object> jsonMap = new HashMap<Object, Object>();
                    try {
                        // Create a random nonce
                        String nonce = CryptoUtils.generateRandomNonce(NONCE_BIT_LEGNTH);
                        // Create a random iv
                        String iv = CryptoUtils.generateRandomNonce(NONCE_BIT_LEGNTH);
                        // Add nonce, iv and the paired app to issuedNonces
                        synchronized (issuedNonces) {
                            issuedNonces.put(nonce, new NonceData(pairedApp, iv));
                        }
                        // Return values as json
                        jsonMap.put("pairingRequired", true);
                        jsonMap.put("nonce", nonce);
                        jsonMap.put("iv", iv);
                        String responseText = WebUtils.encodeMapAsJson(jsonMap);
                        return createOkResponse(responseText);
                    } catch (Exception e) {
                        return errorResponse("Security Error", Status.INTERNAL_ERROR, ErrorCodes.APPLICATION_EXCEPTION, e);
                    }
                }
                /**
                 * Handle DYNAMIX_UNPAIR for the paired app.
                 */
                else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_UNPAIR)) {
                    // TODO: Handle case where remote pairing cannot be removed
                    pairedApp.setRemotePairing(null, null);
                    // Save the application's state
                    DynamixService.updateApplication(pairedApp);
                    // Unbind
                    return unbind(waMgr);
                }
                /**
                 * Handle BIND for the paired app.
                 */
                else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_BIND)) {
                    try {
                        return setupPairedApp(session.getParms(), pairedApp);
                    } catch (Exception e) {
                        return errorResponse("Bind failed: " + e, Status.INTERNAL_ERROR,
                                ErrorCodes.APPLICATION_EXCEPTION, e);
                    }
                } else {
                    /**
                     * For all other calls, extract the URI for further processing below. In this
                     * case, the URI will be encrypted.
                     */
                    try {
                        // Make sure the caller has an valid WebAppManager
                        if (waMgr == null || waMgr.isDead()) {
                            String message = "NOT_AUTHORIZED: WebAppManager null or dead for: " + pairedApp + " (make sure to bind)";
                            Log.d(TAG, message);
                            return errorResponse(message, Status.FORBIDDEN, ErrorCodes.NOT_AUTHORIZED, null);
                        } else {
                            /*
                             * Since the uri was encrypted, session.getParms() will return an empty (INVALID) map since
                             * NanoHTTPD could NOT extract the values. Decrypt the uri and reconstruct the params map here.
                             */
                            session.getParms().clear();
                            String tmpUri = decryptUriAES(waMgr, session.getUri().substring(1));
                            if (tmpUri != null)
                                decryptedUri = parseUri("/" + tmpUri, session.getParms());
                            else {
                                Log.w(TAG, "Unable to properly decrypt URI: " + session.getUri().substring(1));
                            }
                            /*
                             * TODO: Decrypt body
                             */
                        }
                    } catch (Exception e) {
                        String message = "Decrypt exception: " + e;
                        Log.w(TAG, message);
                        return errorResponse(message, Status.INTERNAL_ERROR, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
                    }
                }
            } else {
                /**
                 * Handle the case where the remote caller DOES NOT have an existing pairing.
                 */
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "No paired applications for httpToken: " + httpToken);
                /**
                 * Handle HELLO requests for non-paired remote callers
                 */
                if (session.getUri().equalsIgnoreCase(RESTHandler.HELLO)) {
                    /**
                     * Since we don't have a pairedApp, issue a nonce and iv
                     */
                    Map<Object, Object> jsonMap = new HashMap<Object, Object>();
                    try {
                        // Create a random nonce and iv
                        String nonce = CryptoUtils.generateRandomNonce(NONCE_BIT_LEGNTH);
                        String iv = CryptoUtils.generateRandomNonce(NONCE_BIT_LEGNTH);
                        // Add to issues nonces
                        synchronized (issuedNonces) {
                            issuedNonces.put(nonce, new NonceData(iv));
                        }
                        // Return values as json
                        jsonMap.put("nonce", nonce);
                        jsonMap.put("iv", iv);
                        String responseText = WebUtils.encodeMapAsJson(jsonMap);
                        return createOkResponse(responseText);
                    } catch (Exception e) {
                        return errorResponse("Security Error", Status.INTERNAL_ERROR, ErrorCodes.APPLICATION_EXCEPTION, e);
                    }
                } else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_PAIR)) {
                    /**
                     * Handle DYNAMIX_PAIR requests for non-paired remote callers
                     */
                    try {
                        return pair(session.getParms());
                    } catch (Exception e) {
                        return errorResponse("pairing failed", Status.INTERNAL_ERROR, ErrorCodes.APPLICATION_EXCEPTION,
                                e);
                    }
                } else {
                    /**
                     * Other URIs cannot be called by unpaired apps
                     */
                    return errorResponse("Not Authorized", Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED, null);
                }
            }
        }
        /**
         * At this point, we have an authorized application (local or remote) with an unencrypted URI
         */
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "Processing decryptedUri: " + decryptedUri);
        if (decryptedUri != null) {
            // Handle DYNAMIX_UNBIND
            if (decryptedUri.equalsIgnoreCase(RESTHandler.DYNAMIX_UNBIND)) {
                return unbind(waMgr);
            }
            // Handle IS_DYNAMIX_TOKEN_VALID
            else if (decryptedUri.equalsIgnoreCase(RESTHandler.IS_DYNAMIX_TOKEN_VALID)) {
                return isTokenvalid(waMgr);
            }
            // Handle IS_DYNAMIX_SESSION_OPEN
            else if (decryptedUri.equalsIgnoreCase(RESTHandler.IS_DYNAMIX_SESSION_OPEN)) {
                return isSessionOpen(waMgr);
            }
            // Handle RESTHandler call
            else if (waMgr != null) {
                // Process Dynamix REST API call
                try {
                    // Use the RESTHandler to process the request
                    return waMgr.getRestHandler().processRequest(waMgr, session, decryptedUri);
                } catch (Exception e) {
                    Log.w(TAG, "RestHandler.processRequest exception: " + e);
                    // RESTHandler Error: Dynamix could not handle the request
                    return errorResponse("RESTHandler Error: " + e.toString(), Status.INTERNAL_ERROR,
                            ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
                }
            } else {
                Log.w(TAG, "UNAUTHORIZED caller with origin " + origin + " and token " + httpToken);
                return errorResponse("Not Authorized", Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED, null);
            }
        } else {
            Log.w(TAG, "Null decryptedUri for origin " + origin + " and token " + httpToken);
            return errorResponse("Not Authorized", Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED, null);
        }
    }

    /**
     * Returns a valid Response, encrypting it if the incoming WebAppManager is not null and
     * supports encryption.
     */
    public static Response encryptResponse(WebAppManager<String> waMgr, String message) {
        try {
            if (waMgr != null && waMgr.hasDynamixApplication() && waMgr.getDynamixApp().hasRemotePairing()) {
                String response = message;
                RemotePairing pairing = waMgr.getDynamixApp().getRemotePairing();
                if (pairing != null)
                    response = CryptoUtils
                            .encryptStringAES(pairing.getSessionKeyBytes(), pairing.getSessionIvBytes(), message);
                return createOkResponse(response);
            } else {
                return createOkResponse(message);
            }
        } catch (Exception e) {
            Log.w(TAG, "encryptResponse exception: " + e);
            return createErrorResponse("Security Error", Status.INTERNAL_ERROR,
                    ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
        }
    }

    /**
     * Returns a valid error Response, encrypting it if the incoming WebAppManager is not null and
     * supports encryption.
     */
    public static Response encryptErrorResponse(WebAppManager<String> waMgr, String message, Status status, int errorCode) {
        // Create a map for return values
        Map<Object, Object> jsonMap = new HashMap<Object, Object>();
        jsonMap.put("message", message);
        jsonMap.put("errorCode", errorCode);
        try {
            if (waMgr != null && waMgr.hasDynamixApplication() && waMgr.getDynamixApp().hasRemotePairing()) {
                /*
                 * Handle remotely paired case, if needed. Note that WebAppManagers will not have an associated
                 * Dynamix Application if the caller hasn't opened a session for the first time.
                 */
                RemotePairing pairing = waMgr.getDynamixApp().getRemotePairing();
                return createErrorResponse(CryptoUtils.encryptStringAES(pairing.getSessionKeyBytes(), pairing.getSessionIvBytes(),
                        WebUtils.encodeMapAsJson(jsonMap)), status, errorCode);
            } else {
                return createErrorResponse(message, status, errorCode);
            }
        } catch (Exception e) {
            Log.w(TAG, "encryptErrorResponse exception: " + e);
            return createErrorResponse("DYNAMIX_FRAMEWORK_ERROR", Status.INTERNAL_ERROR,
                    ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
        }
    }

    /**
     * Decryts the incoming string using the key and iv within the WebAppManager.
     */
    private String decryptUriAES(WebAppManager<String> manager, String uri) throws Exception {
        RemotePairing pairing = manager.getDynamixApp().getRemotePairing();
        if (pairing != null) {
            if (DynamixPreferences.isDetailedLoggingEnabled())
                Log.d(TAG, "decryptUriAES for " + manager.getWebAppId() + " for uri " + uri);
            return CryptoUtils.decryptAesBase64String(pairing.getSessionKeyBytes(), pairing.getSessionIvBytes(), uri);
        } else {
            throw new Exception("decryptUriAES called on application with no remote pairing!");
        }
    }

    /*
     * TODO: Decode parameters from the URI. This is a first hack, based on the NanoHTTP implementation for this.
     */
    private String parseUri(String uri, Map<String, String> parms) {
        int qmi = uri.indexOf('?');
        if (qmi >= 0) {
            decodeParms(uri.substring(qmi + 1), parms);
            uri = decodePercent(uri.substring(0, qmi));
        } else {
            uri = decodePercent(uri);
        }
        return uri;
    }

    private Response isSessionOpen(WebAppManager<String> waMgr) {
        try {
            // Create a map for return values
            Map<Object, Object> jsonMap = new HashMap<Object, Object>();
            if (waMgr != null) {
                jsonMap.put("sessionOpen", waMgr.getWebBinder().isSessionOpen());
            } else {
                jsonMap.put("sessionOpen", false);
            }
            return encryptResponse(waMgr, WebUtils.encodeMapAsJson(jsonMap));
        } catch (RemoteException e) {
            Log.w(TAG, e);
            return errorResponse(e.toString(), Status.INTERNAL_ERROR, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
        }
    }

    private Response isTokenvalid(WebAppManager<String> waMgr) {
        Map<Object, Object> jsonMap = new HashMap<Object, Object>();
        if (waMgr != null) {
            // A non-null waMgr means a valid token
            jsonMap.put("tokenValid", true);
        } else
            jsonMap.put("tokenValid", false);
        return encryptResponse(waMgr, WebUtils.encodeMapAsJson(jsonMap));
    }

    private Response unbind(WebAppManager<String> waMgr) {
        if (waMgr != null) {
            Log.d(TAG, "Processing unbind for: " + SessionManager.makeAppKey(waMgr.getDynamixApp(), waMgr.getToken()));
            // Close session with no callback
            waMgr.getWebBinder().closeSession();
            // Set the manager to dead so it can't be used
            waMgr.setDead(true);
        }
        // Return UNAUTHORIZED to signal unbound
        return encryptErrorResponse(waMgr, "UNAUTHORIZED: Dynamix is now unbound.", Status.UNAUTHORIZED, ErrorCodes.NOT_READY);
    }

    public synchronized static boolean addTempPairedApp(DynamixApplication app) {
        synchronized (tmpPairedApps) {
            if (!tmpPairedApps.values().contains(app)) {
                tmpPairedApps.put(SystemClock.currentThreadTimeMillis(), app);
                Log.v(TAG, "addTempPairedApp added " + app);
                return true;
            }
        }
        Log.w(TAG, "addTempPairedApp did not add " + app);
        return false;
    }

    private synchronized Response pair(Map<String, String> parms) throws Exception {
        // Access the signature parameter
        String signature = parms.get("signature");
        // Handle detailed logging
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "Pairing: Using encryption signature: " + signature);
        // Create local private data
        DynamixApplication callingApp = null;
        byte[] pairCodeKey = null;
        NonceData foundNonceData = null;
        /**
         * Loop through each Dynamix app looking for a remote pairing that can decrypt the incoming
         * signature into a previously issued nonce (from HELLO).
         */
        //for (DynamixApplication app : DynamixService.getAllDynamixApplications()) {
        for (DynamixApplication app : tmpPairedApps.values()) {
            if (app.hasPairingCode()) {
                // Grab the app's pairing code
                String tmpCode = app.getRemotePairing().getPairingCode();
                // Convert the pairing code to a SHA256 hash to use as the key
                pairCodeKey = CryptoUtils.getSHA256bytes(tmpCode);
                // Sync on issuedNonces
                synchronized (issuedNonces) {
                    String removeMe = null;
                    // Check each issued nonce
                    for (NonceData data : issuedNonces.values()) {
                        // Client:  includes a signature  parameter  AES256( nonce ) using the PAIRING_CODE  (from OOB)  as the password and the IV  (from HELLO) .
                        String check = CryptoUtils.decryptAesBase64String(pairCodeKey, Base64.decode(data.iv, Base64.NO_WRAP), signature);
                        // See if the decrypte
                        if (check != null && issuedNonces.containsKey(check)) {
                            // Found an app with a proper signature!
                            if (DynamixPreferences.isDetailedLoggingEnabled())
                                Log.d(TAG, "Pairing: Found calling app with proper signature: " + app);
                            // Retain calling app
                            callingApp = app;
                            // Remote the nonce
                            removeMe = check;
                            // Store the noncedata
                            foundNonceData = data;
                            // TODO: Clear the pairing code from the app?
                            break;
                        }
                    }
                    if (removeMe != null) {
                        issuedNonces.remove(removeMe);
                    }
                }
            }
        }
        // Check if we found the calling app
        if (callingApp != null) {
            if (DynamixPreferences.isDetailedLoggingEnabled())
                Log.d(TAG, "Pairing: Setting up pairing for app: " + callingApp);
            // Setup crypto for the app
            // Note: THe master AES key is the key bytes encoded as Base64.NO_WRAP
            String masterKey = CryptoUtils.generateAESKeyString(AES_KEY_LENGTH);
            // Generate a random number of key generation rounds
            int iterationRounds = CryptoUtils.generateRandomInt(5000, 10000);
            // Set master key, if needed
            if (!callingApp.getRemotePairing().hasMasterKey()) {
                // Set the master key
                callingApp.getRemotePairing().setMasterKey(masterKey);
                // Set the iteration rounds
                callingApp.getRemotePairing().setIterationRounds(iterationRounds);
                // Save values to app settings
                DynamixService.updateApplication(callingApp);
            }
            // Setup JSON map with the return values
            Map<Object, Object> jsonMap = new HashMap<Object, Object>();
            jsonMap.put("masterKey", masterKey);
            jsonMap.put("iterationRounds", iterationRounds);
            jsonMap.put("httpToken", callingApp.getAppID());
            String responseText = WebUtils.encodeMapAsJson(jsonMap);
            // Encrypt the return JSON responseText using the pairing code and iv
            String cryptoResponse = CryptoUtils.encryptStringAES(pairCodeKey, Base64.decode(foundNonceData.iv, Base64.NO_WRAP), responseText);
            // Add the web app manager
            addWebappManager(callingApp.getAppID(), callingApp.getAppID());
            // Remove calling app from tmpPaired
            synchronized (tmpPairedApps) {
                Long remove = null;
                for (Long l : tmpPairedApps.keySet())
                    if (tmpPairedApps.get(l).equals(callingApp)) {
                        remove = l;
                        break;
                    }
                if (remove != null) {
                    Log.d(TAG, "App " + callingApp + " is being removed from tmpPairedApps");
                    tmpPairedApps.remove(remove);
                }
            }
            // Add app to Dynamix settings
            DynamixService.addDynamixApplicationToSettings(callingApp);
            if (DynamixPreferences.isDetailedLoggingEnabled())
                Log.d(TAG, "App " + callingApp + " was paired successful with httptoken " + callingApp.getAppID());
            return createOkResponse(cryptoResponse);
        } else {
            String message = "FORBIDDEN: caller is not verified local and does not have a valid pairing code!";
            Log.w(TAG, message);
            return createErrorResponse(message, Status.FORBIDDEN, ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * Utility for setting up a paired app.
     */
    private Response setupPairedApp(Map<String, String> parms, DynamixApplication app) {
        // Access the signature parameter
        String signature = parms.get("signature");
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "setupPairedApp for app " + app + " with signature " + signature);
        // Make sure app has a remote pairing already
        if (app.hasRemotePairing()) {
            RemotePairing pairing = app.getRemotePairing();
            String appNonce = null;
            String appIv = null;
            // Try to access the nonce for the app
            synchronized (issuedNonces) {
                for (String nonce : issuedNonces.keySet()) {
                    NonceData data = issuedNonces.get(nonce);
                    if (data.hasApp(app)) {
                        // We've found the app's nonce
                        appNonce = nonce;
                        appIv = data.iv;
                        break;
                    }
                }
            }
            // See if we found the app's nonce
            if (appNonce != null) {
                // Remove the nonce
                synchronized (issuedNonces) {
                    issuedNonces.remove(appNonce);
                }
                // Setup paired app
                try {
                    // Generate a session key for the app
                    //Log.i(TAG, "generateAESKeyString with iv " + appIv);
                    app.getRemotePairing().generateSessionKey(appIv, AES_KEY_LENGTH);
                    byte[] sessionKeyBytes = pairing.getSessionKeyBytes();
                    // Access the iv bytes for the session
                    byte[] ivBytes = pairing.getSessionIvBytes();
                    // Verify the incoming signature is using the session key
                    if (appNonce.equals(CryptoUtils.decryptAesBase64String(sessionKeyBytes, ivBytes, signature))) {
                        // Signature verified - setup webapp manager
                        addWebappManager(app.getAppID(), app.getAppID());
                        Log.d(TAG, "setupPairedApp: Returning HTTP_OK for: " + app);
                        return createOkResponse();
                    } else {
                        String message = "setupPairedApp: Invalid signature!";
                        Log.w(TAG, message);
                        return errorResponse(message, Status.UNAUTHORIZED,
                                ErrorCodes.NOT_AUTHORIZED, null);
                    }
                } catch (Exception e) {
                    String message = "Could not create session key: " + e.toString();
                    Log.w(TAG, message);
                    return createErrorResponse(message, Status.FORBIDDEN, ErrorCodes.NOT_AUTHORIZED);
                }
            } else {
                String message = "Nonce not found for " + app + ". Make sure to say HELLO first!";
                Log.w(TAG, message);
                return errorResponse(message, Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED, null);
            }
        } else {
            String message = "Remote pairing not found for " + app + ". Make sure to pair first!";
            Log.w(TAG, message);
            return errorResponse(message, Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED, null);
        }
    }

    /**
     * Returns true if the socket is bound to the loopback address and the calling app is approved to interact with
     * Dynamix (i.e., Dynamix trusts its certificate).
     */
    private boolean isVerifiedLocalRequest(Socket socket) {
        // Verify that the socket is connected to the loopback address (i.e., local)
        if (socket.getInetAddress().isLoopbackAddress()) {
            // Get requesting app information using the socket
            //ApplicationInfo app = WebUtils.getAppProcessForSocket2(context, socket);
            ApplicationInfo app = WebUtils.getAppProcessForSocket(socket);
            if (app != null) {
                /**
                 * Verify that the requesting app has a valid certificate. According to the Android docs, multiple
                 * versions of an app (e.g., upgrades) should be signed by the same cert. There will typically be one
                 * packageName for an app. See http://developer.android.com/tools/publishing/app-signing.html
                 */
                X509Certificate cert = WebUtils.getCertForApp(app.packageName);
                if (cert != null) {
                    // Check cert validity
                    try {
                        // Ensure it's valid for this time period
                        cert.checkValidity(new Date());
                        // Check against the list of authorized certs
                        for (TrustedCert authorized : authorizedCerts) {
                            // Try to verify the app's cert
                            try {
                                authorized.getCert().verify(cert.getPublicKey());
                                return true;
                            } catch (Exception e) {
                                // Log.w(TAG, "doAuthorizedServe exception: " + e);
                                // e.printStackTrace();
                            }
                        }
                        // If we reach this point, no authorized cert could be found for the app
                        Log.w(TAG, "No certificate found for " + app.processName);
                        /**
                         * If 'collectCerts' is true, auto-authorize the calling app process and store its cert.
                         */
                        if (DynamixPreferences.collectCerts()) {
                            Log.i(TAG, "Web call from: " + app.processName + " with UID " + app.uid);
                            Log.w(TAG, "Auto-authorizing cert for " + app);
                            try {
                                DynamixService.storeAuthorizedCert(app.packageName, cert);
                                addAuthorizedCert(new TrustedCert(app.packageName, cert));
                                return true;
                            } catch (Exception e) {
                                Log.w(TAG, "Error storing cert: " + e);
                            }
                        } else {
                            return false;
                        }
                    } catch (CertificateExpiredException e1) {
                        Log.w(TAG, "Cert expired for " + app);
                    } catch (CertificateNotYetValidException e1) {
                        Log.w(TAG, "Cert not yet valid for " + app);
                    }
                }
            } else {
                Log.w(TAG, "App not found");
            }
        } else {
            //Log.d(TAG, "Non-localhost request");
        }
        return false;
    }

    /**
     * Adds the web app, which is identified by the origin and token.
     */
    protected WebAppManager<String> addWebappManager(String appId, String token) {
        if (token != null && token.length() > 0) {
            synchronized (webAppManagers) {
                if (!webAppManagers.keySet().contains(token)) {
                    // Create the Web app's binder
                    WebFacadeBinder binder = new WebFacadeBinder(DynamixService.getAndroidContext(), contextMgr, false,
                            appId, token);
                    // Create the Web app's Rest Handler
                    RESTHandler restHandler = new RESTHandler(binder, this);
                    WebAppManager<String> wlm = new WebAppManager<String>(appId, binder.getFacadeId(), binder, restHandler);
                    webAppManagers.put(binder.getFacadeId(), wlm);
                    Log.d(TAG, "WebAppManager added for appId " + appId + " using token " + binder.getFacadeId());
                    return wlm;
                } else {
                    Log.d(TAG, "WebAppManager found for appId " + appId + " using token " + token);
                    return webAppManagers.get(token);
                }
            }
        } else {
            Log.w(TAG, "addWebappManager missing token.");
            return null;
        }
    }

    /**
     * Returns the WebAppManager associated with the incoming token, or null if not found.
     */
    private WebAppManager<String> getWebAppManagerForToken(String token) {
        synchronized (webAppManagers) {
            WebAppManager<String> mgr = webAppManagers.get(token);
            if (mgr != null && !mgr.isDead()) {
                mgr.ping();
                return mgr;
            } else
                return null;
        }
    }

    /**
     * Monitor class that removes dead listeners as needed.
     *
     * @author Darren Carlson
     */
    private static class ListenerMonitor {
        private static int checkPeriod = 5000;
        private static int timeoutMills = 15000;
        private static boolean done = true;
        private static boolean paused = false;
        private static Thread t = null;

        /**
         * Stops the ListenerMonitor
         */
        public synchronized static void stop() {
            done = true;
            paused = false;
        }

        /**
         * Sets the check period (in milliseconds).
         */
        public synchronized static void setCheckPeriod(int checkPeriodMills) {
            if (checkPeriod > 0)
                ListenerMonitor.checkPeriod = checkPeriodMills;
            else
                ListenerMonitor.checkPeriod = 5000;
        }

        /**
         * Pauses timeout checking for web clients.
         */
        public synchronized static void pause() {
            if (!done) {
                if (!paused) {
                    Log.d(TAG, "Pausing timeout checking for web clients");
                    paused = true;
                } else
                    Log.w(TAG, "Already paused");
            } else
                Log.w(TAG, "Not started");
        }

        /**
         * Resumes timeout checking for web clients.
         */
        public synchronized static void resume() {
            if (paused) {
                // Ping all the listeners, since we've been paused
                synchronized (webAppManagers) {
                    for (WebAppManager<String> waMgr : webAppManagers.values())
                        // Ping the WebAppManager
                        waMgr.ping();
                }
                Log.d(TAG, "Resuming timeout checking for web clients");
                paused = false;
            } else
                Log.w(TAG, "Not paused");
        }

        /**
         * Sets the timeout period (in milliseconds).
         *
         * @param timeoutMills
         */
        public synchronized static void setTimeoutMills(int timeoutMills) {
            if (timeoutMills > 0)
                ListenerMonitor.timeoutMills = timeoutMills;
            else
                ListenerMonitor.timeoutMills = 15000;
        }

        /**
         * Starts monitoring web clients for timeouts.
         *
         * @param checkPeriod  The check period in milliseconds.
         * @param timeoutMills The timeout duration in milliseconds.
         */
        public synchronized static void start(int checkPeriod, int timeoutMills) {
            setCheckPeriod(checkPeriod);
            setTimeoutMills(timeoutMills);
            if (done) {
                done = false;
                t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "ListenerMonitor started");
                        while (!done) {
                            try {
                                // Sleep for the check period
                                Thread.sleep(ListenerMonitor.checkPeriod);
                            } catch (InterruptedException e) {
                            }
                            if (!paused) {
                                // Create an ArrayList of tokens to remove
                                List<String> remove = new ArrayList<String>();
                                // Remember the current time
                                Date now = new Date();
                                synchronized (webAppManagers) {
                                    // First, check for any existing dead listeners
                                    for (String managerToken : webAppManagers.keySet()) {
                                        WebAppManager<String> m = webAppManagers.get(managerToken);
                                        if (m.isDead())
                                            remove.add(managerToken);
                                    }
                                    // Next, remove the dead listeners
                                    for (String managerToken : remove) {
                                        Log.d(TAG, "Removing dead manager: " + managerToken);
                                        WebAppManager<String> m = webAppManagers.remove(managerToken);
                                    }
                                    // Finally, check for any listener timeouts
                                    for (String managerToken : webAppManagers.keySet()) {
                                        WebAppManager<String> m = webAppManagers.get(managerToken);
                                        Long lastAccessMills = now.getTime() - m.getLastAccess().getTime();
                                        if (lastAccessMills > ListenerMonitor.timeoutMills) {
                                            Log.d(TAG, "Timeout for " + m.getDynamixApp() + " last access was " + lastAccessMills + "ms ago - closing session");
                                            m.setDead(true);
                                            m.getWebBinder().closeSession();
                                        }
                                    }
                                }
                            }
                        }
                        paused = false;
                        Log.d(TAG, "ListenerMonitor stopped");
                    }
                });
                t.setDaemon(true);
                t.start();
            } else
                Log.w(TAG, "ListenerMonitor is already running!");
        }
    }

    /**
     * Check if a given nonce is valid. This method also cleans up all expired nonces and removes them
     * from the map
     *
     * @param nonce the nonce to validate
     * @return true if the pairing code is valid
     */
    public static boolean isNonceValid(String nonce) {
        synchronized (issuedNonces) {
            cleanNonces();
            if (issuedNonces.containsKey(nonce)) {
                return true;
            }
            return false;
        }
    }

    /**
     * Removes all expired nonces
     */
    public static void cleanNonces() {
        synchronized (issuedNonces) {
            long currentTimeMillis = System.currentTimeMillis();
            List<String> removeMe = new ArrayList<String>();
            for (String nonce : issuedNonces.keySet()) {
                if (currentTimeMillis - issuedNonces.get(nonce).timestamp > NONCE_TIMEOUT) {
                    removeMe.add(nonce);
                }
            }
            for (String obsolete : removeMe) {
                issuedNonces.remove(obsolete);
            }
        }
    }

    /**
     * Removes a nonce.
     *
     * @param nonce the nonce to remove
     */
    public void removeNonce(String nonce) {
        synchronized (issuedNonces) {
            issuedNonces.remove(nonce);
        }
    }

    public static int getPort() {
        return port;
    }

    /**
     * Decodes parameters in percent-encoded URI-format ( e.g. "name=Jack%20Daniels&pass=Single%20Malt" ) and adds them
     * to given Map. NOTE: this doesn't support multiple identical keys due to the simplicity of Map.
     */
    protected void decodeParms(String parms, Map<String, String> p) {
        if (parms == null) {
            // p.put(QUERY_STRING_PARAMETER, "");
            return;
        }
        // p.put(QUERY_STRING_PARAMETER, parms);
        StringTokenizer st = new StringTokenizer(parms, "&");
        while (st.hasMoreTokens()) {
            String e = st.nextToken();
            int sep = e.indexOf('=');
            if (sep >= 0) {
                p.put(decodePercent(e.substring(0, sep)).trim(), decodePercent(e.substring(sep + 1)));
            } else {
                p.put(decodePercent(e).trim(), "");
            }
        }
    }

    /*
     * Utility method that adds all required CORS headers to the response.
     */
    private static void addCorsHeaders(Response r) {
        // Configure CORS for the response
        r.addHeader("Access-Control-Allow-Origin", "*");
        r.addHeader("Access-Control-Allow-Credentials", "true");
        r.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
        r.addHeader("Access-Control-Allow-Headers",
                "Origin,httptoken,Accept,Content-Type,Content-Length,Cookie,Authorization");
        r.addHeader("Cache-Control", "no-cache");
        r.addHeader("Pragma", "no-cache");
        r.addHeader("Expires", "0");
        /*
         * 2015.07.29: While refactoring, NanoHTTPD complained about 'broken pipe' issues from time to time, and setting
		 * both 'setKeepAlive' and 'setChunkedTransfer' to false seemed to fix the problem. However, this issue may have
		 * been related to returning improper values during the refactoring. Once we fixed the return values these
		 * errors dissapeared. Leaving the comments for now.
		 */
        // r.setKeepAlive(false);
        // r.setChunkedTransfer(false);
    }
}
