/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.util.Log;

import org.ambientdynamix.api.application.ContextHandler;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.WebFacadeBinder;

import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Management class for web apps, which holds a linked queue of event commands to send.
 *
 * @author Darren Carlson
 */
public class WebAppManager<String> extends ConcurrentLinkedQueue<String> {
    // Private data
    private final java.lang.String TAG = this.getClass().getSimpleName();
    private Object lock = new Object();
    private Map<java.lang.String, ContextHandler> handlerMap = new ConcurrentHashMap<java.lang.String, ContextHandler>();
    private Date lastAccess = new Date();
    private boolean dead = false;
    private java.lang.String appId;
    private java.lang.String token;
    private WebFacadeBinder webBinder;
    private RESTHandler restHandler;

    /**
     * Creates a WebListenerManager.
     */
    public WebAppManager(java.lang.String appId, java.lang.String token, WebFacadeBinder webBinder,
                         RESTHandler restHandler) {
        this.appId = appId;
        this.token = token;
        this.webBinder = webBinder;
        this.restHandler = restHandler;
        ping();
    }

    /**
     * Returns the RESTHandler associated with this WebAppManager.
     */
    public RESTHandler getRestHandler() {
        return this.restHandler;
    }

    /**
     * Returns the WebFacadeBinder associated with this WebAppManager.
     */
    public WebFacadeBinder getWebBinder() {
        return this.webBinder;
    }

    /**
     * Returns the Dynamix application associated with this WebAppManager.
     */
    public DynamixApplication getDynamixApp() {
        return getWebBinder().getAuthorizedApplication(getWebAppId());
    }

    /**
     * Returns true if the WebappManager has an associated Dynamix application (i.e., the app is in the Dynamix
     * instance's database); false otherwise. Note that apps are not entered into the database until they
     * have their session opened for the first time or are remotely paired.
     */
    public boolean hasDynamixApplication() {
        return getDynamixApp() != null;
    }

    /**
     * Returns the appId of the Web app managed by this WebAppManager.
     */
    public java.lang.String getWebAppId() {
        return this.appId;
    }

    /**
     * Returns the token associated with this WebAppManager.
     */
    public java.lang.String getToken() {
        return this.token;
    }

    /**
     * Adds the context handler.
     */
    public void addContextHandler(ContextHandler handler) {
        handlerMap.put(handler.getId().toString(), handler);
    }

    /**
     * Returns the ContextHandler associated with the handlerId (or null if there is no mapping).
     */
    public ContextHandler getContextHandler(String handlerId) {
        synchronized (handlerMap) {
            if (handlerMap.containsKey(handlerId))
                return this.handlerMap.get(handlerId);
        }
        return null;
    }

    /**
     * Sets the last access to the current time.
     */
    public void ping() {
        lastAccess = new Date();
    }

    /**
     * Set true if we're dead (i.e., timed out); false otherwise.
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    /**
     * Returns true if we're dead (i.e., timed out); false otherwise.
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Returns the time of the last ping.
     */
    public Date getLastAccess() {
        return lastAccess;
    }

    /**
     * Block and wait for an event.
     *
     * @param millis The time to wait (block) in milliseconds.
     */
    public void waitForEvent(int millis) {
        synchronized (lock) {
            try {
                lock.wait(millis);
            } catch (InterruptedException e) {
                Log.i(TAG, " Interrupted ");
            }
        }
    }

    /**
     * Adds the String command to send to the web client to the queue, which notifies the wait lock.
     */
    public synchronized boolean add(String command) {
        //Log.i(TAG, "Adding " + command);
        if (super.add(command)) {
            // Get the current time
            Date now = new Date();
            // Calculate remaining time before we need to send events
            long remainingTime;
            if (lastNotify != null) {
                remainingTime = (lastNotify.getTime() + minNotifyMills) - now.getTime();
            } else {
                // We haven't notified yet, so wait minNotifyMills
                remainingTime = minNotifyMills;
            }
            //Log.i(TAG, "Notify remaining time: " + remainingTime);
            // Handle remaining time
            if (remainingTime > 0) {
                synchronized (timerLock) {
                    if (!timerRunning) {
                        timerRunning = true;
                        // Set a timer task to ensure event processing
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                // Double-check that we need to notify
                                Date now = new Date();
                                long remainingTime = 0;
                                if (lastNotify != null)
                                    remainingTime = (lastNotify.getTime() + minNotifyMills) - now.getTime();
                                if (remainingTime <= 0) {
                                    synchronized (lock) {
                                        //Log.i(TAG, "Notify after timeout");
                                        lock.notify();
                                        lastNotify = now;
                                    }
                                } else {
                                    // Log.i(TAG, "Notify remaining time in timer: " + remainingTime);
                                }
                                timerRunning = false;
                            }
                        }, remainingTime);
                    }
                }
            } else {
                // Remaining time is <= zero, so notify now
                try {
                    synchronized (lock) {
                        //Log.i(TAG, "Notify directly");
                        lock.notify();
                        lastNotify = now;
                    }
                } catch (Exception e) {
                    Log.w(TAG, e.toString());
                }
            }
            return true;
        } else
            return false;
    }

    private Object timerLock = new Object();
    private boolean timerRunning = false;
    private long minNotifyMills = 0;
    private Date lastNotify;
    private Timer timer = new Timer();
}