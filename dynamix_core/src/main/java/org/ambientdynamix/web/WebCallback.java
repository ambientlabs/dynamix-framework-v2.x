/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.ISessionCallback;

import android.os.RemoteException;

/**
 * Web-based implementation of the ICallback interface.
 * 
 * @see ISessionCallback
 * @author Darren Carlson
 */
public class WebCallback extends ICallback.Stub {
	private WebAppManager<String> wlMgr;
	private String callbackId;
	private Map<Object, Object> map = new HashMap<Object, Object>();

	/**
	 * Creates a WebCallback.
	 */
	public WebCallback(WebAppManager<String> wlMgr, String callbackId) {
		this.wlMgr = wlMgr;
		this.callbackId = callbackId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSuccess() throws RemoteException {
		WebEventHandler.sendEvent(wlMgr, callbackId, "onCallbackSuccess", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFailure(String message, int errorCode) throws RemoteException {	
		map.put("message", message);
		map.put("errorCode", errorCode);
		WebEventHandler.sendEvent(wlMgr, callbackId, "onCallbackFailure", map);
	}
}