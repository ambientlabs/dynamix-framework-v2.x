/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.ambientdynamix.api.application.AppConstants;
import org.ambientdynamix.api.application.BundleContextInfo;
import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.util.base64.Base64;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Collection of utilities for the Dynamix Web Connector and related classes.
 *
 * @author Darren Carlson
 */
public class WebUtils {
    private static String TAG = WebUtils.class.getSimpleName();
    // Shared JSON Object mapper
    private static ObjectMapper mapper = new ObjectMapper();
    private static ActivityManager actMgr;
    private static CertificateFactory certFactory;
    private static PackageManager packMgr;
    private static SimpleDateFormat timeFormatter;
    private static PackageManager pm = null;
    private static List<ApplicationInfo> installedPackages;
    private static Date lastPackagesCheck;
    private static boolean LOW_LEVEL_DEBUG = false;

    static {
        // Setup JSON encoding to use UTC-based ISO 8601 date/time formatting
        TimeZone tz = TimeZone.getTimeZone("UTC");
        timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ", Locale.US);
        timeFormatter.setTimeZone(tz);
        mapper.setDateFormat(timeFormatter);
    }

    // Singleton constructor
    private WebUtils() {
        mapper.enable(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS);
    }

    public static Map<Object, Object> encodeContextResult(ContextResult result) {
        try {
            // Create a root node and basic result properties
            Map<Object, Object> jNode = new HashMap<Object, Object>();
            jNode.put("sourcePluginId", result.getResultSource().getPluginId());
            jNode.put("responseId", result.getResponseId() == null ? "" : result.getResponseId());
            jNode.put("contextType", result.getContextType());
            jNode.put("implementingClassname", result.getIContextInfo().getImplementingClassname());
            jNode.put("timeStamp", timeFormatter.format(result.getTimeStamp()));
            jNode.put("expires", result.expires());
            jNode.put("expireTime", timeFormatter.format(result.getExpireTime()));
            /*
             * Now add the properties from the IContextInfo. First, handle automatic web encoding, if requested. If the
			 * IContextInfo uses JavaBean standards, it can be automatically encoded into JSON. If no automatic web
			 * encoding is requested, then we use the
			 */
            JsonNode encodedNode = mapper.createObjectNode();
            jNode.put("hasPojoData", false); // Default to false
            if (result.autoWebEncode()) {
                // Handle BundleContextInfo
                if (result.getIContextInfo() instanceof BundleContextInfo) {
                    BundleContextInfo sci = (BundleContextInfo) result.getIContextInfo();
                    // Ensure the data is not null
                    if (sci.getData() != null) {
                        // Create a JSON object to populate
                        ObjectNode tmp = mapper.createObjectNode();
                        /*
                         * Extract all fields in the BundleContextInfo's Bundle into the JSON object using toString.
						 */
                        for (String key : sci.getData().keySet()) {
                            tmp.put(key, sci.getData().get(key).toString());
                        }
                        encodedNode = tmp;
                        jNode.put("hasPojoData", true);
                    } else
                        Log.w(TAG, "BundleContextInfo did not contain a Bundle!");
                } else {
                    /*
                     * Handle custom IContextInfo implementations. In this case, we use the mapper to serialize the
					 * IContextInfo result into JSON using JavaBean conventions.
					 */
                    encodedNode = mapper.valueToTree(result.getIContextInfo());
                    jNode.put("hasPojoData", true);
                }
            } else {
                /*
				 * AutoEncode is false, so we need to handle pre-encoded data.
				 */
                if (!result.getWebEncodingFormat().equalsIgnoreCase(AppConstants.NO_AUTO_WEB_ENCODING)) {
                    ObjectNode tmp = mapper.createObjectNode();
                    tmp.put("encodedDataType", result.getWebEncodingFormat());
                    tmp.put("encodedData", result.getStringRepresentation(result.getWebEncodingFormat()));
                    encodedNode = tmp;
                } else {
                    // This object cannot be sent via web serialization
                    Log.w(TAG, "Result is configured with NO_WEB_ENCODING... ignoring");
                    return null;
                }
            }
            // Iterate through the IContextInfo fields, adding them to the event
            Iterator<String> itr = encodedNode.fieldNames();
            boolean stringify = false;
            while (itr.hasNext()) {
                String field = itr.next();
                jNode.put(field, encodedNode.get(field));
                if (field.equalsIgnoreCase("stringify"))
                    stringify = true;
            }
			/*
			 * If the event has a string-based representation ("text/plain"), add it for convenience as a 'stringify'
			 * field, but only if there wasn't a stringify field already defined by the event.
			 */
            if (!stringify) {
                String stringRep = result.getIContextInfo().getStringRepresentation("text/plain");
                if (stringRep != null && stringRep.length() > 0)
                    jNode.put("stringify", URLEncoder.encode(stringRep, "UTF-8"));
            }
            return jNode;
        } catch (Exception e) {
            Log.w(TAG, "Exception during onContextEvent: " + e);
            return null;
        }
    }

    /**
     * Returns the MAC address of the incoming InetAddress as a string (as in '08:00:27:DC:4A:9E'), or null if a MAC
     * cannot be determined (which will be the case if the address is remote and on another subnet. If multiple MACs are
     * associated with the address, they are concatenated using '-' as a delineator (e.g.,
     * 08:00:27:DC:4A:9E-03:01:25:CD:00:9F).
     *
     * @param address
     * @return
     */
    public static String getMacAddress(InetAddress address) {
		/*
		 * http://stackoverflow.com/questions/4436901/finding-mac-address-using-ip-address-in-java
		 */
        try {
            NetworkInterface ni = NetworkInterface.getByInetAddress(address);
            if (ni != null) {
                byte[] mac = ni.getHardwareAddress();
                if (mac != null) {
                    boolean first = true;
                    StringBuilder macs = new StringBuilder();
                    for (int i = 0; i < mac.length; i++) {
                        if (!first)
                            macs.append("-");
                        macs.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
                        first = false;
                    }
                    return macs.toString();
                } else {
                    Log.d(TAG, "Address doesn't exist or is not accessible.");
                }
            } else {
                Log.d(TAG, "Network Interface for the specified address is not found.");
            }
        } catch (Exception e) {
            Log.w(TAG, "getMacAddress failed: " + e);
        }
        return null;
    }

    /**
     * Preps the incoming arguments to be sent to a Web application.
     *
     * @param methodName The method to call
     * @param callbackId The callback
     * @param params     The parameters to send
     * @return An UTF-8 encoded JSON string containing the methodName, callbackId and params.
     * @throws UnsupportedEncodingException
     */
    public static String prepWebAgentEvent(String methodName, String callbackId, Map<Object, Object> params) {
        try {
            String event = mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(new WebAgentEvent(methodName, callbackId, params));
            return URLEncoder.encode(event, "UTF-8");
        } catch (Exception e) {
            Log.w(TAG, "prepWebAgentEvent exception: " + e);
            throw new RuntimeException("prepWebAgentEvent exception: " + e);
        }
    }

    public static String encodeMapAsJson(Map<Object, Object> params) {
        try {
            String event = mapper.writeValueAsString(params);
//            return URLEncoder.encode(event, "UTF-8");
            return event;
        } catch (Exception e) {
            Log.w(TAG, "encodeMapAsJson exception: " + e);
            throw new RuntimeException("encodeMapAsJson exception: " + e);
        }
    }

    /**
     * Returns a url with properly appended nocache=time element.
     */
    public static String getNoCacheUrl(String baseUrl) {
        String cachelessUrl = "";
        if (baseUrl.contains("?")) {
            cachelessUrl = baseUrl + "&nocache=" + new Date().getTime();
        } else {
            cachelessUrl = baseUrl + "?nocache=" + new Date().getTime();
        }
        return cachelessUrl;
    }

    /**
     * xports an certificate to a file.
     *
     * @param cert   The certificate to export.
     * @param file   The destination file.
     * @param binary True if the cert should be written as a binary file; false to encode using Base64.
     */
    public static void exportCertificate(java.security.cert.Certificate cert, File file, boolean binary) {
        Log.i(TAG, "Writing cert to: " + file.getAbsolutePath());
        try {
            // Get the encoded form which is suitable for exporting
            byte[] buf = cert.getEncoded();
            FileOutputStream os = new FileOutputStream(file);
            if (binary) {
                // Write in binary form
                os.write(buf);
            } else {
                // Write in text form
                Writer wr = new OutputStreamWriter(os, Charset.forName("UTF-8"));
                wr.write("-----BEGIN CERTIFICATE-----\n");
                Base64.encodeBase64(buf);
                wr.write("\n-----END CERTIFICATE-----\n");
                wr.flush();
            }
            os.close();
        } catch (Exception e) {
            Log.w(TAG, "Error writing cert for " + file);
        }
    }

    /**
     * Returns the X509Certificate for the incoming packageName. Returns null if the packageName cannot be found (or if
     * there was a certificate exception).
     */
    public static X509Certificate getCertForApp(String packageName) {
        try {
            // Create packMgr, if needed
            if (packMgr == null) {
                packMgr = DynamixService.getAndroidContext().getPackageManager();
            }
            // Create certFactory, if needed
            if (certFactory == null) {
                certFactory = CertificateFactory.getInstance("X509");
            }
            PackageInfo packageInfo = packMgr.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            Signature[] signatures = packageInfo.signatures;
            // signatures[0] is a DER encoded X.509 certificate
            byte[] cert = signatures[0].toByteArray();
            InputStream input = new ByteArrayInputStream(cert);
            X509Certificate c = (X509Certificate) certFactory.generateCertificate(input);
            return c;
        } catch (NameNotFoundException e) {
            Log.w(TAG, "Package not found for " + packageName + " - " + e);
        } catch (CertificateException e) {
            Log.w(TAG, "Certificate exception for " + packageName + " - " + e);
        }
        return null;
    }

    /**
     * Experimental app to socket mapper (not working reliably)
     */
    public static ApplicationInfo getAppProcessForSocket2(Context c, Socket socket) {
        return PortMapper.getApplicationInfo(c, socket);
    }

    /**
     * Returns the RunningAppProcessInfo bound to the incoming socket. Returns null if the socket is not bound to any
     * apps.
     * <p/>
     * Some alternatives that have been tested but are not yet reliable.
     * https://github.com/zyrorl/sandrop/tree/master/projects/SandroProxyLib/src/org/sandroproxy/utils/network
     * https://github.com/dextorer/AndroidTCPSourceApp/blob/master/src/com/megadevs/tcpsourceapp/TCPSourceApp.java (regex parsing not working properly)
     */
    public synchronized static ApplicationInfo getAppProcessForSocket(Socket socket) {
        // Cache the PackageManager
        if (pm == null) {
            pm = DynamixService.getAndroidContext().getPackageManager();
        }
        // Cache the original port, just in case it changes during the call.
        int callerPort = socket.getPort();
        /**
         *  Create a map of application UIDs -> a list of the ports used by that UID.
         *  2015.12.07: Map both tcp and tcp6 to catch browsers like Dolphin, which, for some reason
         *  show up only under tcp6.
         */
        Map<Integer, List<Integer>> merged = mergeMaps(getUidPortMappings("/proc/net/tcp"), getUidPortMappings("/proc/net/tcp6"));
        // Loop through UIDs in merged, grabing the app and checking for used ports
        for (Integer uid : merged.keySet()) {
            if (pm.getPackagesForUid(uid) != null)
                for (String packageName : pm.getPackagesForUid(uid)) {
                    try {
                        // Try to access the app info for the packageName
                        ApplicationInfo appInfo = pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
                        // Grab the ports bound to the appInfo
                        List<Integer> ports = merged.get(appInfo.uid);
                        if (ports != null) {
                            // Do detailed logging, if needed
                            if (DynamixPreferences.isDetailedLoggingEnabled())
                                Log.d(TAG, "App " + appInfo.processName + " with UID " + appInfo.uid
                                        + " is mapped to ports: " + ports);
                            // Check if the bound ports matched the incoming socket
                            if (ports.contains(callerPort)) {
                                if (DynamixPreferences.isDetailedLoggingEnabled()) {
                                    Log.d(TAG, "We have a winner: " + appInfo.processName + " was bound to port " + callerPort);
                                }
                                return appInfo;
                            }
                        }
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
        // Warn, since we can't locate the incoming port
        Log.w(TAG, "getAppProcessForSocket: could not find process bound to " + socket);
        return null;
    }

    /**
     * Utility that serializes the incoming Object into a format suitable for passing to web clients. Defaults to JSON
     * encoding format.
     */
    public static String serializeObject(Object value) throws Exception {
        return mapper.writeValueAsString(value);
    }

    /**
     * Utility that serializes the incoming Object into a format suitable for passing to web clients.
     */
    public static String serializeObject(Object value, PluginConstants format) throws Exception {
        return mapper.writeValueAsString(value);
    }

    /*
     * Helper method that obtains UidPortMappings from the incoming path. Returns process UIDs mapped to a list of bound
     * ports. The path must point to a valid TCP socket table, such as "/proc/net/tcp" or "/proc/net/tcp6". For a
     * description of the table format, see http://linuxdevcenter.com/pub/a/linux/2000/11/16/LinuxAdmin.html
     * http://stackoverflow.com/questions/14962808/android-get-tcp-connection-log
     * https://gitorious.org/0xdroid/cts/source
     * /6b126d7bb6e6f4b512b50685ca3f16097625028a:tests/tests/net/src/android/net/cts/ListeningPortsTest.java
     */
    private static Map<Integer, List<Integer>> getUidPortMappings(String path) {
        Map<Integer, List<Integer>> map = new ConcurrentHashMap<Integer, List<Integer>>();
        try {
            // Try to 'cat' the path
            // java.lang.Process proc = Runtime.getRuntime().exec(new String[] { "cat", path });
            // Create a reader for the proc's input stream
            // BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()), 8192);
            // Buffer for the proc file
            List<String> buffer = new ArrayList<>();
            String procLine = null;
            // Create a reader for the proc file
            BufferedReader reader = new BufferedReader(new FileReader(path));
            // Read all proc file files into the buffer
            while ((procLine = reader.readLine()) != null) {
                buffer.add(procLine);
            }
            // Close the reader
            reader.close();
            // Handle detailed logging, if needed
            if (DynamixPreferences.isDetailedLoggingEnabled()) {
                Log.d(TAG, "getUidPortMappings contained " + buffer.size() + " for path " + path);
            }
            // Parse each line of the buffer
            for (String line : buffer) {
                if (LOW_LEVEL_DEBUG) {
                    Log.d(TAG, "Parsing procLine " + line);
                }
                // Log.i(TAG, "LINE: " + line);
                // Make sure we're not parsing a header
                if (!line.contains("local_address")) {
                    // Replace multiple spaces with single spaces
                    String clean = line.trim().replaceAll(" +", " ");
                    // Split the clean string into tokens
                    String[] tokens = clean.split(" ");
                    // Token 1 is the 'local_address' (e.g., [A0BC1CAC, 81DC])
                    String[] localAdd = tokens[1].split(":");
                    // Token 7 is the 'uid'
                    String uid = tokens[7];
                    // Map the uid to a list of bound ports
                    Integer uidInt = Integer.parseInt(uid);
                    if (map.containsKey(uidInt)) {
                        map.get(uidInt).add(Integer.parseInt(localAdd[1], 16));
                    } else {
                        List<Integer> portList = new ArrayList<Integer>();
                        portList.add(Integer.parseInt(localAdd[1], 16));
                        map.put(uidInt, portList);
                    }
                }
            }
        } catch (Exception e) {
            Log.w(TAG, e);
        }
        return map;
    }

    /**
     * Merges the incoming maps, joining values if needed.
     */
    private static Map<Integer, List<Integer>> mergeMaps(Map<Integer, List<Integer>> map1, Map<Integer, List<Integer>> map2) {
        // Create a return Map
        Map<Integer, List<Integer>> returnMap = new HashMap<>(map1.keySet().size() > map2.keySet().size() ? map1.keySet().size() : map2.keySet().size());
        // Add map 1
        returnMap.putAll(map1);
        // Add map 2, joining values if needed
        for (Integer key : map2.keySet()) {
            if (returnMap.containsKey(key)) {
                returnMap.get(key).addAll(map2.get(key));
            } else {
                returnMap.put(key, map2.get(key));
            }
        }
        return returnMap;
    }

    /**
     * Utility class used to serialize JSON objects for Web agent requests.
     *
     * @author Darren Carlson
     */
    private static class WebAgentEvent {
        String method;
        String callbackId;
        Map<Object, Object> params;

        public WebAgentEvent(String method, String callbackId, Map<Object, Object> params) {
            this.method = method;
            this.callbackId = callbackId;
            this.params = params;
        }

        public String getMethod() {
            return method;
        }

        public String getCallbackId() {
            return callbackId;
        }

        // @JsonUnwrapped
        // @JsonAnyGetter
        public Map<Object, Object> getParams() {
            return params;
        }
    }
}
