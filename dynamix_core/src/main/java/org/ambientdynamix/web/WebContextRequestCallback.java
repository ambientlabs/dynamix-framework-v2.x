package org.ambientdynamix.web;

import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.IContextRequestCallback;
import org.ambientdynamix.core.DynamixPreferences;

import android.os.RemoteException;
import android.util.Log;

/**
 * Web-based implementation of the IContextRequestCallback interface.
 * 
 * @see IContextRequestCallback
 * @author Darren Carlson
 */
public class WebContextRequestCallback extends IContextRequestCallback.Stub {
	private final String TAG = this.getClass().getSimpleName();
	private WebAppManager<String> wlMgr;
	private String callbackId;
	private Map<Object, Object> map = new HashMap<Object, Object>();

	/**
	 * Creates a WebContextRequestCallback.
	 */
	public WebContextRequestCallback(WebAppManager<String> wlMgr, String callbackId) {
		this.wlMgr = wlMgr;
		this.callbackId = callbackId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSuccess(ContextResult result) throws RemoteException {
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.d(TAG, "Handling context request onSuccess: " + result);
		Map<Object, Object> encoded = WebUtils.encodeContextResult(result);
		if (encoded != null) {
			WebEventHandler.sendEvent(wlMgr, callbackId, "onContextRequestCallbackSuccess", encoded);
		} else {
			if (DynamixPreferences.isDetailedLoggingEnabled())
				Log.d(TAG, "Context result could not be encoded: " + result);
			onFailure("Result cannot be web encoded", ErrorCodes.NOT_SUPPORTED);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFailure(String message, int errorCode) throws RemoteException {
		map.put("message", message);
		map.put("errorCode", errorCode);
		WebEventHandler.sendEvent(wlMgr, callbackId, "onContextRequestCallbackFailure", map);
	}
}
