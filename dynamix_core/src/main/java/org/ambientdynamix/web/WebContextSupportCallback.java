/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.IContextSupportCallback;

import android.os.RemoteException;
import android.util.Log;

/**
 * Web-based implementation of the IContextSupportCallback interface.
 * 
 * @see IContextSupportCallback
 * @author Darren Carlson
 */
public class WebContextSupportCallback extends IContextSupportCallback.Stub {
	private final String TAG = this.getClass().getSimpleName();
	private WebAppManager<String> wlMgr;
	private String callbackId;
	private Map<Object, Object> map = new HashMap<Object, Object>();
	/**
	 * Creates a WebContextSupportCallback.
	 */
	public WebContextSupportCallback(WebAppManager<String> wlMgr, String callbackId) {
		this.wlMgr = wlMgr;
		this.callbackId = callbackId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onWarning(String message, int errorCode) throws RemoteException {		
		map.put("message", message);
		map.put("errorCode", errorCode);
		WebEventHandler.sendEvent(wlMgr, callbackId, "onContextSupportCallbackWarning", map);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
		try {			
			map.put("supportInfo", supportInfo);
			WebEventHandler.sendEvent(wlMgr, callbackId, "onContextSupportCallbackSuccess", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onProgress(int percentComplete) throws RemoteException {
		try {			
			map.put("percentComplete", percentComplete);
			WebEventHandler.sendEvent(wlMgr, callbackId, "onContextSupportCallbackProgress", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFailure(String message, int errorCode) throws RemoteException {		
		map.put("message", message);
		map.put("errorCode", errorCode);
		WebEventHandler.sendEvent(wlMgr, callbackId, "onContextSupportCallbackFailure", map);
	}
}