/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.data;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextType;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.api.application.SupportedInteraction;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.ContextPluginDependency;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.FirewallRule;
import org.ambientdynamix.core.Utils;
import org.ambientdynamix.remote.RemotePairing;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.util.Repository;

import android.util.Log;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;

/**
 * Object-based database implementation of the ISettingsManager based on the DB4o database engine. This SettingsManager
 * provides settings caching so that read intensive operations are more efficient.
 * 
 * @see DynamixSettings
 * @see ISettingsManager
 * @author Darren Carlson
 */
public class DB4oSettingsManager implements ISettingsManager {
	// Private data
	private static final String TAG = DB4oSettingsManager.class.getSimpleName();
	private ObjectContainer _db = null;
	private volatile DynamixSettings settingsCache;
	private final boolean CACHE_ENABLED = true;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean addDynamixApplication(DynamixApplication app) {
		Log.v(TAG, "addDynamixApplication for: " + app);
		DynamixSettings settings = getCachedSettings();
		if (!settings.getDynamixApplications().contains(app)) {
			settings.getDynamixApplications().add(app);
			// Store the updated settings
			return storeSettings(settings);
		} else
			return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean removeDynamixApplication(DynamixApplication app) {
		Log.v(TAG, "removeDynamixApplication for: " + app);
		DynamixSettings settings = getCachedSettings();
		if (settings.getDynamixApplications().contains(app)) {
			settings.getDynamixApplications().remove(app);
			// Store the updated settings
			return storeSettings(settings);
		} else
			return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean updateDynamixApplication(DynamixApplication app) {
		Log.v(TAG, "updateDynamixApplication for: " + app);
		DynamixSettings settings = getCachedSettings();
		if (settings.getDynamixApplications().contains(app)) {
			int index = settings.getDynamixApplications().indexOf(app);
			if (index != -1) {
				app.refreshFirewallRules();
				settings.getDynamixApplications().set(index, app);
				// Store the updated settings
				return storeSettings(settings);
			} else
				return false;
		} else
			return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addClassLoader(ClassLoader loader) {
		// Unused currently
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean addContextPlugin(ContextPlugin plugin) {
		if (Utils.validateContextPlugin(plugin)) {
			DynamixSettings settings = getCachedSettings();
			// Check if the plugin was already registered
			if (!settings.getInstalledContextPlugins().contains(plugin)) {
				// The plugin is new, so add it to our ContextPlugins List
				settings.getInstalledContextPlugins().add(plugin);
				// Store the updated settings
				storeSettings(settings);
				Log.v(TAG, "addContextPlugin added: " + plugin);
				return true;
			} else {
				Log.d(TAG, "During addContextPlugin, settings already contained: " + plugin);
				ContextPlugin originalPlugin = getContextPlugin(plugin);
				replaceContextPlugin(originalPlugin, plugin);
			}
		} else
			Log.w(TAG, "addContextPlugin found invalid plugin: " + plugin);
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void clearSettings() {
		Log.v(TAG, "clearSettings");
		if (isDatabaseOpen()) {
			// Clear the settings from the database
			DynamixSettings proto = new DynamixSettings();
			ObjectSet<DynamixSettings> result = _db.queryByExample(proto);
			while (result.hasNext()) {
				Object deleteMe = result.next();
				Log.v(TAG, "Deleting settings: " + deleteMe);
				_db.delete(deleteMe);
				_db.commit();
			}
			// Clear the settingsCache
			this.settingsCache = null;
		} else
			Log.w(TAG, "clearSettings encountered a closed database. Database was: " + _db);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void closeDatabase() {
		if (_db != null) {
			while (!_db.close())
				;
			_db = null;
			Log.i(TAG, "Settings database is closed.");
		} else {
			Log.e(TAG, "Settings was NULL!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized DynamixApplication getDynamixApplication(String appId) {
		DynamixSettings settings = getCachedSettings();
		for (DynamixApplication app : settings.getDynamixApplications()) {
			if (app.getAppID().equalsIgnoreCase(appId)) {
				return app;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DynamixApplication> getAllDynamixApplications() {
		return Utils.getSortedAppList(getCachedSettings().getDynamixApplications());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ContextPlugin> getInstalledContextPlugins() {
		return Utils.getSortedContextPluginList(getCachedSettings().getInstalledContextPlugins());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized ContextPluginSettings getContextPluginSettings(final ContextPlugin plug) {
		Log.v(TAG, "getInstalledContextPluginsettings for: " + plug);
		// Make sure we got a database (i.e. _db != null)
		if (isDatabaseOpen()) {
			DynamixSettings settings = getCachedSettings();
			// Find the ContextPlugin and return its settings
			int index = settings.getInstalledContextPlugins().indexOf(plug);
			if (index != -1) {
				return settings.getInstalledContextPlugins().get(index).getContextPluginSettings();
			} else
				Log.d(TAG, "No settings found for " + plug);
		} else
			throw new RuntimeException("Database was closed after open... this is bad: " + _db);
		Log.d(TAG, "No settings found in database for: " + plug);
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PendingContextPlugin> getPendingContextPlugins() {
		return Utils.getSortedDiscoveredPluginList(getCachedSettings().getPendingContextPlugins());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PowerScheme getPowerScheme() {
		return getCachedSettings().getPowerScheme();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized DynamixSettings getSettings() {
		// Make sure we have a database
		if (isDatabaseOpen()) {
			// Perform a DB4o queryByExample for the single settings object in the database.
			DynamixSettings proto = new DynamixSettings();
			// Log.i(TAG, "Constructing query using DynamixSettings: " + proto);
			// ObjectSet<DynamixSettings> result = _db.queryByExample(proto);
			ObjectSet<DynamixSettings> result = _db.queryByExample(DynamixSettings.class);
			/*
			 * Query query=_db.query(); query.constrain(DynamixSettings.class); ObjectSet<DynamixSettings>
			 * result=query.execute();
			 */
			// Warn if there are more than 1 settings entity
			if (result.size() > 1)
				Log.w(TAG, "More than 1 settings object Database");
			// If we got a result, return it.
			if (result.hasNext()) {
				DynamixSettings s = result.next();
				Log.v(TAG, "getSettings is returning: " + s);
				// _db.activate(s, Integer.MAX_VALUE);
				return s;
			} else {
				Log.e(TAG, "Settings NOT FOUND in database!");
			}
		} else
			Log.e(TAG, "Database was closed after open... this is bad: " + _db);
		Log.e(TAG, "Returning new settings");
		/*
		 * TODO: If we return new settings, we need to reset Dynamix to a default state, otherwise there may be plug-ins
		 * installed that won't be reflected by the settings object.
		 */
		return new DynamixSettings();
	}

	/**
	 * Returns True if Dynamix had a clean exit; false otherwise.
	 */
	@Override
	public synchronized Boolean hadCleanExit() {
		if (isDatabaseOpen()) {
			DynamixSettings settings = getCachedSettings();
			return settings.hadCleanExit();
		} else
			throw new RuntimeException("Database was closed after open... this is bad: " + _db);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void openDatabase(String path) throws Exception {
		Log.i(TAG, "Opening Dynamix database using: " + path + " on thread: " + Thread.currentThread());
		if (path != null) {
			/*
			 * Open the DB4o database and setup object handling (e.g. updateDepth, cascadeOnDelete, etc.)
			 */
			try {
				if (!isDatabaseOpen()) {
					Log.i(TAG, "Dynamix database was closed... opening");
					// Setup database
					EmbeddedConfiguration c1 = Db4oEmbedded.newConfiguration();
					c1.common().objectClass(DynamixSettings.class).cascadeOnActivate(true);
					c1.common().objectClass(DynamixSettings.class).cascadeOnUpdate(true);
					c1.common().objectClass(DynamixSettings.class).cascadeOnDelete(true);
					c1.common().objectClass(ContextPluginDependency.class).cascadeOnActivate(true);
					c1.common().objectClass(ContextPluginDependency.class).cascadeOnUpdate(true);
					c1.common().objectClass(ContextPluginDependency.class).cascadeOnDelete(true);
					c1.common().objectClass(VersionInfo.class).cascadeOnActivate(true);
					c1.common().objectClass(VersionInfo.class).cascadeOnUpdate(true);
					c1.common().objectClass(VersionInfo.class).cascadeOnDelete(true);
					c1.common().objectClass(PendingContextPlugin.class).cascadeOnActivate(true);
					c1.common().objectClass(PendingContextPlugin.class).cascadeOnUpdate(true);
					c1.common().objectClass(PendingContextPlugin.class).cascadeOnDelete(true);
					c1.common().objectClass(ContextPlugin.class).cascadeOnActivate(true);
					c1.common().objectClass(ContextPlugin.class).cascadeOnUpdate(true);
					c1.common().objectClass(ContextPlugin.class).cascadeOnDelete(true);
					c1.common().objectClass(FirewallRule.class).cascadeOnActivate(true);
					c1.common().objectClass(FirewallRule.class).cascadeOnUpdate(true);
					c1.common().objectClass(FirewallRule.class).cascadeOnDelete(true);
					c1.common().objectClass(DynamixApplication.class).cascadeOnActivate(true);
					c1.common().objectClass(DynamixApplication.class).cascadeOnUpdate(true);
					c1.common().objectClass(DynamixApplication.class).cascadeOnDelete(true);
					c1.common().objectClass(RemotePairing.class).cascadeOnActivate(true);
					c1.common().objectClass(RemotePairing.class).cascadeOnUpdate(true);
					c1.common().objectClass(RemotePairing.class).cascadeOnDelete(true);
					c1.common().objectClass(ContextType.class).cascadeOnActivate(true);
					c1.common().objectClass(ContextType.class).cascadeOnUpdate(true);
					c1.common().objectClass(ContextType.class).cascadeOnDelete(true);
					c1.common().objectClass(ContextPluginInformation.class).cascadeOnActivate(true);
					c1.common().objectClass(ContextPluginInformation.class).cascadeOnUpdate(true);
					c1.common().objectClass(ContextPluginInformation.class).cascadeOnDelete(true);
					c1.common().objectClass(DynamixFeature.class).cascadeOnActivate(true);
					c1.common().objectClass(DynamixFeature.class).cascadeOnUpdate(true);
					c1.common().objectClass(DynamixFeature.class).cascadeOnDelete(true);


					/*
					 * After changing to DB4o 8.x, we're getting NPEs with the Permission class. Values are null.
					 * Reverting to the 7.x branch for now.
					 */
					c1.common().objectClass(Permission.class).cascadeOnActivate(true);
					c1.common().objectClass(Permission.class).cascadeOnUpdate(true);
					c1.common().objectClass(Permission.class).cascadeOnDelete(true);
					c1.common().objectClass(Repository.class).cascadeOnActivate(true);
					c1.common().objectClass(Repository.class).cascadeOnUpdate(true);
					c1.common().objectClass(Repository.class).cascadeOnDelete(true);
					c1.common().objectClass(HashMap.class).cascadeOnActivate(true);
					c1.common().objectClass(HashMap.class).cascadeOnUpdate(true);
					c1.common().objectClass(HashMap.class).cascadeOnDelete(true);

					c1.common().objectClass(SupportedInteraction.class).cascadeOnActivate(true);
					c1.common().objectClass(SupportedInteraction.class).cascadeOnUpdate(true);
					c1.common().objectClass(SupportedInteraction.class).cascadeOnDelete(true);

					c1.common().objectClass(DynamixSettings.class).minimumActivationDepth(Integer.MAX_VALUE);
					c1.common().activationDepth(Integer.MAX_VALUE);
					// Create the database file, plus path (if needed)
					File dbFile = new File(path);
					if (!dbFile.exists()) {
						File parent = new File(dbFile.getParent());
						parent.mkdirs();
						dbFile.createNewFile();
					}
					Log.i(TAG, "Opening database using path: " + dbFile);
					_db = Db4oEmbedded.openFile(c1, dbFile.getCanonicalPath());
				}
				Log.i(TAG, "Database is open!");
			} catch (Exception e) {
				e.printStackTrace();
				closeDatabase();
				Log.e(TAG, e.getMessage());
				throw e;
			}
		} else
			throw new Exception("Database string was null");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeClassLoader(ClassLoader loader) {
		// Unused
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean removeInstalledContextPlugin(ContextPlugin plug) {
		// Remove any IContextPluginSettings settings first
		removeContextPluginSettings(plug);
		// Try to remove the plugin
		DynamixSettings settings = getCachedSettings();
		if (settings.getInstalledContextPlugins().remove(plug)) {
			// Remove firewall rules associated with the plug-in from each app
			for (DynamixApplication app : settings.getDynamixApplications()) {
				app.removeFirewallRulesForPlugin(plug.getContextPluginInformation());
			}
			Log.v(TAG, "Removed: " + plug);
			// Store the updated settings
			return storeSettings(settings);
		} else
			Log.d(TAG, "removeInstalledContextPlugin did not find " + plug);
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean removeContextPluginSettings(final ContextPlugin plug) {
		Log.v(TAG, "removeContextPluginSettings for: " + plug);
		// Make sure we got a database (i.e. _db != null)
		if (isDatabaseOpen()) {
			DynamixSettings settings = getCachedSettings();
			// Find the ContextPlugin and return its settings
			int index = settings.getInstalledContextPlugins().indexOf(plug);
			if (index != -1) {
				settings.getInstalledContextPlugins().get(index).setContextPluginSettings(null);
				// Store the updated settings
				storeSettings(settings);
				return true;
			} else
				Log.d(TAG, "No settings found for ContextPlugin: " + plug);
		} else
			throw new RuntimeException("Database was closed after open... this is bad: " + _db);
		Log.d(TAG, "No settings found in database for: " + plug);
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean replaceContextPlugin(ContextPlugin originalPlugin, ContextPlugin newPlugin) {
		if (Utils.validateContextPlugin(newPlugin)) {
			DynamixSettings settings = getCachedSettings();
			if (settings.getInstalledContextPlugins().contains(originalPlugin)) {
				Log.v(TAG, "replaceContextPlugin for: " + originalPlugin);
				int index = settings.getInstalledContextPlugins().indexOf(originalPlugin);
				if (index != -1) {
					settings.getInstalledContextPlugins().set(index, newPlugin);
					// Store the updated settings
					storeSettings(settings);
					return true;
				} else
					Log.w(TAG, "replaceContextPlugin could not find: " + originalPlugin);
			} else
				Log.w(TAG, "replaceContextPlugin could not find: " + originalPlugin);
		} else
			Log.w(TAG, "replaceContextPlugin failed for: " + originalPlugin);
		return false;
	}

	/**
	 * Sets if Dynamix had a clean exit.
	 * 
	 * @param cleanExit
	 */
	@Override
	public synchronized void setCleanExit(Boolean cleanExit) {
		DynamixSettings settings = getCachedSettings();
		settings.setCleanExit(cleanExit);
		storeSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void setPendingContextPlugins(List<PendingContextPlugin> pendingPlugins) {
		DynamixSettings settings = getCachedSettings();
		settings.setPendingContextPlugins(pendingPlugins);
		storeSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void setPowerScheme(PowerScheme newScheme) {
		if (newScheme != null) {
			DynamixSettings settings = getCachedSettings();
			settings.setPowerScheme(newScheme);
			storeSettings(settings);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean storeContextPluginSettings(ContextPlugin plug, ContextPluginSettings plugSettings) {
		Log.v(TAG, "storeContextPluginSettings for: " + plug);
		// Make sure we have a database
		if (isDatabaseOpen()) {
			DynamixSettings settings = getCachedSettings();
			// Find the ContextPlugin and return its settings
			int index = settings.getInstalledContextPlugins().indexOf(plug);
			if (index != -1) {
				settings.getInstalledContextPlugins().get(index).setContextPluginSettings(plugSettings);
				return true;
			} else
				Log.w(TAG, "storeContextPluginSettings could not find ContextPlugin: " + plug);
		} else
			throw new RuntimeException("Database was closed after open... this is bad: " + _db);
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean updateContextPlugin(ContextPlugin plug) {
		if (Utils.validateContextPlugin(plug)) {
			DynamixSettings settings = getCachedSettings();
			if (settings.getInstalledContextPlugins().contains(plug)) {
				Log.v(TAG, "updateContextPlugin for: " + plug);
				int index = settings.getInstalledContextPlugins().indexOf(plug);
				if (index != -1) {
					settings.getInstalledContextPlugins().set(index, plug);
					// Store the updated settings
					storeSettings(settings);
					return true;
				} else
					Log.w(TAG, "updateContextPlugin could not find: " + plug);
			} else
				Log.w(TAG, "updateContextPlugin could not find: " + plug);
		} else
			Log.w(TAG, "updateContextPlugin failed for: " + plug);
		return false;
	}

	@Override
	public synchronized Date getRepoLastModified(String repoId) {
		DynamixSettings settings = getCachedSettings();
		if (settings.getRepoLastModifiedMap().containsKey(repoId))
			return settings.getRepoLastModifiedMap().get(repoId.toLowerCase());
		else
			// Return TCP/IP Flag day, since we don't know the repo and we want an old date
			return new Date(83, 0, 1);
	}

	@Override
	public synchronized void setRepoLastModified(String repoId, Date repoLastModified) {
		DynamixSettings settings = getCachedSettings();
		settings.getRepoLastModifiedMap().put(repoId.toLowerCase(), repoLastModified);
		storeSettings(settings);
	}

	@Override
	public synchronized void clearRepoLastModified(String repoId) {
		DynamixSettings settings = getCachedSettings();
		settings.getRepoLastModifiedMap().remove(repoId.toLowerCase());
		storeSettings(settings);
	}

	@Override
	public Repository getPluginRepository(String url) {
		List<Repository> repos = getPluginRepositories();
		for (Repository repo : repos)
			if (repo.getUrl().equalsIgnoreCase(url))
				return repo;
		return null;
	}

	/**
	 * Returns the list of plug-in repositories.
	 */
	@Override
	public synchronized List<Repository> getPluginRepositories() {
		DynamixSettings settings = getCachedSettings();
		return settings.getPluginRepositories();
	}

	@Override
	public synchronized void setPluginRepositories(List<Repository> repos) {
		DynamixSettings settings = getCachedSettings();
		settings.getPluginRepositories().clear();
		settings.getPluginRepositories().addAll(repos);
		storeSettings(settings);
	}

	@Override
	public synchronized boolean addPluginRepository(Repository repo) {
		Log.d(TAG, "Adding repo to database: " + repo);
		DynamixSettings settings = getCachedSettings();
		// Check for duplicate repo URIs
		for (Repository existing : settings.getPluginRepositories())
			if (existing.getUrl().equalsIgnoreCase(repo.getUrl())) {
				Log.w(TAG, "Database already contained repo with URI: " + repo.getUrl());
				return false;
			}
		// Check for existing repo
		if (settings.getPluginRepositories().contains(repo)) {
			Log.w(TAG, "Database already contained repo with id " + repo.getId());
			return false;
		}
		settings.getPluginRepositories().add(repo);
		storeSettings(settings);
		return true;
	}

	@Override
	public synchronized boolean removePluginRepository(Repository repo) {
		Log.d(TAG, "Removing repo from database: " + repo);
		DynamixSettings settings = getCachedSettings();
		// Remove existing reference, if needed
		if (settings.getPluginRepositories().contains(repo)) {
			// Check if removable
			if (settings.getPluginRepositories().get(settings.getPluginRepositories().indexOf(repo)).isRemovable()) {
				settings.getPluginRepositories().remove(repo);
				storeSettings(settings);
				return true;
			} else {
				Log.w(TAG, "Repo is not removable: " + repo);
				return false;
			}
		} else {
			Log.w(TAG, "Could not find repo to remove: " + repo);
			return false;
		}
	}

	@Override
	public boolean updatePluginRepository(Repository repo) {
		Log.d(TAG, "Updating repo in database: " + repo);
		DynamixSettings settings = getCachedSettings();
		if (settings.getPluginRepositories().contains(repo)) {
			settings.getPluginRepositories().remove(repo);
			settings.getPluginRepositories().add(repo);
			storeSettings(settings);
			return true;
		} else {
			Log.w(TAG, "Could not find repo to update: " + repo);
			return false;
		}
	}

	/*
	 * Returns the current settings object from the database, caching it locally for future reads.
	 */
	private synchronized DynamixSettings getCachedSettings() {
		if (CACHE_ENABLED) {
			if (settingsCache == null) {
				settingsCache = getSettings();
			}
			return settingsCache;
		} else {
			return getSettings();
		}
	}

	/**
	 * Checks if the database is open.
	 */
	private boolean isDatabaseOpen() {
		return (_db != null && !_db.ext().isClosed());
	}

	/**
	 * Returns the ContextPlugin from the settings that match the searchPlug, or null if not found.
	 */
	private ContextPlugin getContextPlugin(ContextPlugin searchPlug) {
		DynamixSettings settings = getCachedSettings();
		if (settings.getInstalledContextPlugins().contains(searchPlug)) {
			return settings.getInstalledContextPlugins().get(settings.getInstalledContextPlugins().indexOf(searchPlug));
		} else
			return null;
	}

	/**
	 * Stores the incoming settings and updates the local settings cache.
	 */
	private synchronized boolean storeSettings(DynamixSettings settings) {
		Log.v(TAG, "Beginning storeSettings for thread: " + Thread.currentThread());
		if (isDatabaseOpen()) {
			if (settings != null) {
				try {
					// Then store the new settings in the database
					_db.store(settings);
					_db.commit();
					// Finally, make sure to cache the new settings
					this.settingsCache = settings;
					return true;
				} catch (Exception e) {
					e.printStackTrace();
					Log.w(TAG, "Exception during storeSettings: " + e.getMessage());
				}
			} else
				Log.w(TAG, "storeSettings received null settings");
		} else
			Log.w(TAG, "storeSettings encountered a closed database. Database was: " + _db);
		return false;
	}
}