/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.util.Repository;

/**
 * Internal storage object for user-based settings related to the Dynamix Framework. This class is intended to be
 * persisted by an ISettingsManager. To maintain proper state, all interactions with DynamixSettings should be handled
 * through the ISettingsManager.
 * 
 * @author Darren Carlson
 */
class DynamixSettings {
	/**
	 * Empty constructor to support DB4o searches.
	 */
	public DynamixSettings() {
	}

	/**
	 * Indicates whether or not Dynamix is enabled.
	 */
	private boolean enabled = true;
	/**
	 * Indicates whether or not Dynamix exited (closed) cleanly. Default to true for the first run.
	 */
	private Boolean cleanExit = true;
	/**
	 * Indicates whether or not Dynamix should auto-start when Android starts.
	 */
	private Boolean autoStart = true;
	/**
	 * The current power scheme.
	 */
	private PowerScheme scheme = PowerScheme.BALANCED;
	/**
	 * List of all installed ContextPlugins.
	 */
	private List<ContextPlugin> installedContextPlugins = new ArrayList<ContextPlugin>();
	/**
	 * List of all pending DiscoveredContextPlugins.
	 */
	private List<PendingContextPlugin> pendingContextPlugins = new ArrayList<PendingContextPlugin>();
	/**
	 * Mapping repository identifier strings to a date representing the last time the repo was modified.
	 */
	private Map<String, Date> repoLastModified = new HashMap<String, Date>();
	/**
	 * List of all repositories.
	 */
	private List<Repository> pluginRepositories = new ArrayList<Repository>();
	/**
	 * List of all Dynamix applications.
	 */
	private List<DynamixApplication> applications = new ArrayList<DynamixApplication>();

	/**
	 * Returns a list of all Dynamix applications.
	 */
	public List<DynamixApplication> getDynamixApplications() {
		return applications;
	}

	/**
	 * Returns a List of the installed ContextPlugins.
	 */
	public List<ContextPlugin> getInstalledContextPlugins() {
		return installedContextPlugins;
	}

	/**
	 * Returns the List of pending DiscoveredContextPlugins.
	 */
	public List<PendingContextPlugin> getPendingContextPlugins() {
		return pendingContextPlugins;
	}

	/**
	 * Returns the current PowerScheme.
	 */
	public PowerScheme getPowerScheme() {
		return scheme;
	}

	/**
	 * @return True if Dynamix had a clean exit; false otherwise.
	 */
	public Boolean hadCleanExit() {
		return cleanExit;
	}

	/**
	 * Returns true if Dynamix should auto-start when Android boots; false, otherwise.
	 */
	public boolean isAutoStart() {
		return autoStart;
	}

	/**
	 * Checks if the Dynamix Framework is enabled.
	 */
	public synchronized boolean isEnabled() {
		return this.enabled;
	}

	/**
	 * Sets if Dynamix should auto-start when Android boots.
	 * 
	 * @param autoStart
	 *            true if Dynamix should auto-start when Android boots; false otherwise.
	 */
	public void setAutoStart(boolean autoStart) {
		this.autoStart = autoStart;
	}

	/**
	 * Sets if Dynamix had a clean exit.
	 * 
	 * @param cleanExit
	 */
	public void setCleanExit(Boolean cleanExit) {
		this.cleanExit = cleanExit;
	}

	/**
	 * Sets the List of PendingContextPlugin.
	 * 
	 * @param pendingContextPlugins
	 *            The List of ContextPluginUpdates
	 */
	public void setPendingContextPlugins(List<PendingContextPlugin> pendingPlugins) {
		this.pendingContextPlugins = pendingPlugins;
	}

	/**
	 * Sets the enabled state of the Dynamix Framework.
	 */
	public synchronized void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets the current PowerScheme.
	 */
	public void setPowerScheme(PowerScheme scheme) {
		this.scheme = scheme;
	}

	/**
	 * Returns a map repository identifier strings to a date representing the last time the repo was modified.
	 */
	public Map<String, Date> getRepoLastModifiedMap() {
		return repoLastModified;
	}

	/**
	 * Returns the list of plug-in repositories.
	 */
	public List<Repository> getPluginRepositories() {
		return this.pluginRepositories;
	}
}