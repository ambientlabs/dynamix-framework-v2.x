/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.data;

import java.sql.Timestamp;

import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.event.SourcedContextInfoSet;

/**
 * A cached result containing a ContextPlugin source, an SourcedContextDataSet and a Timestamp indicating when the
 * SourcedContextDataSet was cached.
 * 
 * @author Darren Carlson
 */
public class ContextResultCacheEntry {
	// Private data
	private ContextPlugin source;
	private SourcedContextInfoSet dataSet;
	private Timestamp cachedTime;
	private IContextHandler targetHandler;
	private IContextListener targetListener;

	/**
	 * Creates a ContextResultCacheEntry.
	 * 
	 * @param source
	 *            The ContextPlugin event source.
	 * @param targetListener
	 *            The target listener for this event.
	 * @param eventSet
	 *            The SourcedContextDataSet to cache.
	 * @param cachedTime
	 *            The time the SourcedContextDataSet was cached, as a Timestamp.
	 */
	public ContextResultCacheEntry(ContextPlugin source, SourcedContextInfoSet eventSet, Timestamp cachedTime) {
		this.source = source;
		this.dataSet = eventSet;
		this.cachedTime = cachedTime;
	}

	/**
	 * Creates a ContextResultCacheEntry.
	 * 
	 * @param targetHandler
	 *            The target handler.
	 * @param source
	 *            The ContextPlugin event source.
	 * @param targetListener
	 *            The target listener for this event.
	 * @param eventSet
	 *            The SourcedContextDataSet to cache.
	 * @param cachedTime
	 *            The time the SourcedContextDataSet was cached, as a Timestamp.
	 */
	public ContextResultCacheEntry(IContextHandler targetHandler, ContextPlugin source, SourcedContextInfoSet eventSet,
			Timestamp cachedTime) {
		this(source, eventSet, cachedTime);
		this.targetHandler = targetHandler;
	}

	/**
	 * Creates a ContextResultCacheEntry.
	 * 
	 * @param targetHandler
	 *            The target handler.
	 * @param targetListener
	 *            The target listener.
	 * @param source
	 *            The ContextPlugin event source.
	 * @param targetListener
	 *            The target listener for this event.
	 * @param eventSet
	 *            The SourcedContextDataSet to cache.
	 * @param cachedTime
	 *            The time the SourcedContextDataSet was cached, as a Timestamp.
	 */
	public ContextResultCacheEntry(IContextHandler targetHandler, IContextListener targetListener,
			ContextPlugin source, SourcedContextInfoSet eventSet, Timestamp cachedTime) {
		this(targetHandler, source, eventSet, cachedTime);
		this.targetListener = targetListener;
	}

	/**
	 * Returns the cached time as a Timestamp.
	 */
	public Timestamp getCachedTime() {
		return cachedTime;
	}

	/**
	 * Returns the ContextPlugin source.
	 */
	public ContextPlugin getEventSource() {
		return this.source;
	}

	/**
	 * Returns the SourcedContextDataSet.
	 */
	public SourcedContextInfoSet getSourcedContextEventSet() {
		return this.dataSet;
	}

	/**
	 * Returns the target listener for this cached event, or null if there is no target.
	 */
	public IContextListener getTargetListener() {
		return targetListener;
	}

	/**
	 * Returns true if this cached event has a target listener.
	 */
	public boolean hasTargetListener() {
		return targetListener != null;
	}

	/**
	 * Returns the target handler for this cached event.
	 */
	public IContextHandler getTargetHandler() {
		return targetHandler;
	}

	/**
	 * Returns true if this cached event has a target listener.
	 */
	public boolean hasTargetHandler() {
		return targetHandler != null;
	}

	/**
	 * Sets the cached time as a Timestamp.
	 */
	public void setCachedTime(Timestamp cachedTime) {
		this.cachedTime = cachedTime;
	}

	/**
	 * Sets the ContextPlugin source.
	 */
	public void setEventSource(ContextPlugin source) {
		this.source = source;
	}

	/**
	 * Sets the SourcedContextDataSet.
	 */
	public void setSourcedContextEventSet(SourcedContextInfoSet dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public String toString() {
		return "ContextEventCacheEntry from: " + source;
	}
}