/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.util.Repository;

import android.util.Log;

/**
 * Updates for the Dynamix Framework.
 * 
 * @author Darren Carlson
 * 
 */
public class DynamixUpdates {
	private String TAG = getClass().getSimpleName();
	private boolean update;
	private boolean updateRequired;
	private String updateMessage = "";
	private String updateUrl = "";
	private List<TrustedCertBinder> trustedWebConnectorCerts = new ArrayList<TrustedCertBinder>();
	private List<TrustedRepoBinder> trustedRepos = new ArrayList<TrustedRepoBinder>();

	/**
	 * Creates a DynamixUpdates.
	 */
	public DynamixUpdates(DynamixUpdatesBinder updatesBinder) {
		setUpdate(updatesBinder.hasUpdate());
		setUpdateRequired(updatesBinder.isUpdateRequired());
		setUpgradeMessage(updatesBinder.getUpdateMessage());
		setUpdateUrl(updatesBinder.getUpdateUrl());
		setTrustedWebConnectorCerts(updatesBinder.getTrustedWebConnectorCerts());
		setTrustedRepos(updatesBinder.getTrustedRepos());
	}

	/**
	 * Returns true if Dynamix has an update; false otherwise.
	 */
	public boolean hasUpdate() {
		return update;
	}

	/**
	 * Set true if Dynamix has an update; false otherwise.
	 */
	public void setUpdate(boolean update) {
		this.update = update;
	}

	/**
	 * Returns true if the update is required; false otherwise.
	 */
	public boolean isUpdateRequired() {
		return updateRequired;
	}

	/**
	 * Set true if the upgrade is required; false otherwise.
	 */
	public void setUpdateRequired(boolean updateRequired) {
		this.updateRequired = updateRequired;
	}

	/**
	 * Returns the update message, if hasUpdate() is true.
	 */
	public String getUpdateMessage() {
		return updateMessage;
	}

	/**
	 * Sets the update message.
	 */
	public void setUpgradeMessage(String upgradeMessage) {
		this.updateMessage = upgradeMessage;
	}

	/**
	 * Returns the update URL, if hasUpdate() is true.
	 */
	public String getUpdateUrl() {
		return updateUrl;
	}

	/**
	 * Sets the update URL, if hasUpdate() is true.
	 */
	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

	/**
	 * Returns the list of TrustedCertBinders for the WebConnector.
	 */
	public List<TrustedCertBinder> getTrustedWebConnectorCerts() {
		return trustedWebConnectorCerts;
	}

	/**
	 * Sets the list of TrustedCertBinders for the WebConnector.
	 */
	public void setTrustedWebConnectorCerts(List<TrustedCertBinder> trustedWebConnectorCerts) {
		this.trustedWebConnectorCerts = trustedWebConnectorCerts;
	}

	/**
	 * Sets the list of trusted repos.
	 */
	public void setTrustedRepos(List<TrustedRepoBinder> repos) {
		this.trustedRepos = repos;
	}

	/**
	 * Gets the list of trusted repos.
	 */
	public List<Repository> getTrustedRepos() {
		List<Repository> returnList = new ArrayList<Repository>();
		for (TrustedRepoBinder binder : trustedRepos) {
			if (binder.remove()) {
				Log.d(TAG, "Force remove detected for repo: " + binder.url);
				Repository repo = new Repository(binder.url, binder.alias, binder.url, true, binder.enabled);
				repo.setForceRemove(true);
				returnList.add(repo);
			} else {
				if (binder.alias == null) {
					binder.alias = "Unnamed Repo";
				}
				returnList.add(new Repository(binder.url, binder.alias, binder.url, true, binder.enabled));
			}
		}
		return returnList;
	}
}
