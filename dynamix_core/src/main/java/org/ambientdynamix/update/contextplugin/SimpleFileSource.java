/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants.PLATFORM;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.util.Repository;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

/**
 * Supports extracting ContextPlugin updates from a file source.
 * 
 * @author Darren Carlson
 */
public class SimpleFileSource extends SimpleSourceBase implements IContextPluginConnector, Serializable {
	// Private data
	private static final long serialVersionUID = 5634095436990952985L;
	private final String TAG = this.getClass().getSimpleName();
	private Repository repo;
	private boolean cancel;

	/**
	 * Creates a SimpleFileSource using the source path for the local repository.
	 *
	 * @param repo
	 *            The repository.
	 */
	public SimpleFileSource(Repository repo) {
		if (repo != null) {
			this.repo = repo;
		} else
			Log.w(TAG, "Repo is null");
		/*
		 * Make sure local repo directory is created. Note that on Nexus 4 and 7, folders created here may not be
		 * immediately visible, due to the following bug: http://code.google.com/p/android/issues/detail?id=38282
		 */
		File folder = new File(repo.getUrl());
		if (!folder.exists()) {
			if (folder.mkdir()) {
				Log.i(TAG, "Created local Dynamix repo directory at " + folder.getAbsolutePath());
			} else
				Log.w(TAG, "Could not create local Dynamix repo directory at " + folder.getAbsolutePath());
		} else
			Log.i(TAG, "Local Dynamix repo directory already exists at " + folder.getAbsolutePath());
		/*
		 * Create a file and force a rescan to work around this bug:
		 * http://code.google.com/p/android/issues/detail?id=38282
		 */
		if (folder.exists()) {
			File tmpFile = new File(folder.getAbsolutePath() + "/dynamix_repo.tmp");
			if (!tmpFile.exists()) {
				try {
					tmpFile.createNewFile();
				} catch (IOException e) {
					Log.w(TAG, e);
					return;
				}
			}
			// Request a media scan so the folder shows up via MTP
			Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			intent.setData(Uri.fromFile(tmpFile));
			DynamixService.getAndroidContext().sendBroadcast(intent);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cancel() {
		cancel = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		else
			return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PendingContextPlugin> getContextPlugins(PLATFORM platform, VersionInfo platformVersion,
			VersionInfo frameworkVersion) throws Exception {
		/*
		 * TODO: Look into packaging plug-ins are self-contained zip files. Possibly keep config data in
		 * PLUG_INF/plug.xml http://stackoverflow.com/questions/4473256/reading-text-files-in-a-zip-archive
		 */
		cancel = false;
		List<PendingContextPlugin> updates = new ArrayList<PendingContextPlugin>();
		List<File> files = new ArrayList<File>();
		Log.i(TAG, "Checking for context plug-ins using: " + repo.getAlias());
		Log.i(TAG, "Repository URL is: " + repo.getUrl());
		File sourceFile = new File(repo.getUrl());
		if (sourceFile.isDirectory()) {
			for (File file : sourceFile.listFiles()) {
				if (cancel)
					break;
				if (file.isFile()
						&& (file.getAbsolutePath().endsWith(".xml") || file.getAbsolutePath().endsWith(".XML"))) {
					files.add(file);
				}
			}
		} else
			files.add(sourceFile);
		if (!cancel) {
			for (File f : files) {
				try {
					updates.addAll(createDiscoveredPlugins(repo, new FileInputStream(f), platform, platformVersion,
							frameworkVersion, false));
				} catch (Exception e) {
					Log.w(TAG, "Update exception: " + e + " for file " + f.getAbsolutePath());
					updates.add(new PendingContextPlugin(e.toString()));
				}
			}
			/*
			 * Because the path to user-manageable external storage may vary depending on the device, the path to each
			 * plugin Bundle JAR MUST be relative to the root of external storage. Dynamix automatically rewrites the
			 * install path using the proper URL format and external storage directory.
			 */
			for (PendingContextPlugin update : updates) {
				if (!update.hasError()) {
					String relativeInstallPath = update.getPendingContextPlugin().getInstallUrl();
					if (relativeInstallPath.contains("/") || relativeInstallPath.contains("\\")) {
						// Path is fully qualified (add external storage location)
						update.getPendingContextPlugin().setInstallUrl(
								"file:/" + Environment.getExternalStorageDirectory() + "/" + relativeInstallPath);
					} else {
						// Path is relative to the repo xml (which has the correct external storage location)
						File f = new File(update.getPendingContextPlugin().getRepoSource().getUrl());
						String xmlPath = f.getAbsolutePath();
						String fullPath = "file:/" + xmlPath + "/" + relativeInstallPath;
						update.getPendingContextPlugin().setInstallUrl(fullPath);
					}
				}
			}
		}
		return updates;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.getClass().hashCode();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getConnectorId() + ", enabled=" + repo.isEnabled();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getLastModified() {
		// For the moment, we use an immediate last modified to force a refresh of the file-system.
		return new Date();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getConnectorId() {
		return repo.getUrl();
	}
}